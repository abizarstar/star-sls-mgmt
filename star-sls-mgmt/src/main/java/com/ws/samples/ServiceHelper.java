/*
 *
 * PROPRIETARY PROGRAM MATERIAL 
 * 
 * This material is proprietary to Invera Inc. and is not
 * to be reproduced, used or disclosed except in accordance
 * with the program license or upon written authorization from
 * Invera Inc., 4333 St-Catherine Street West, Westmount
 * Quebec Canada H3Z 1P9
 *
 * Copyright (C) 2018, Invera Inc.
 */

package com.ws.samples;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import com.invera.stratix.services.CostReconDistService;
import com.invera.stratix.services.CostReconDistService_Service;
import com.invera.stratix.services.VoucherService;
import com.invera.stratix.services.VoucherService_Service;
import com.invera.stratix.ws.exec.authentication.AuthService;
import com.invera.stratix.ws.exec.authentication.AuthService_Service;
import com.invera.stratix.ws.services.security.LogoutService;
import com.invera.stratix.ws.services.security.LogoutService_Service;
import com.ws.samples.handler.MsgSOAPHandler;
import com.ws.samples.handler.SecSOAPHandler;
import com.ws.samples.util.WSUtils;

public abstract class ServiceHelper {

	private static final String STRATIX_NAMESPACE_ROOT = "http://stratix.invera.com/services";

	// EXEC Web Product Authentication Server
	private static final String WPA_ENDPOINT_BASE_URI = "/webservices";
	private static String wpaEndpointProtocol = "http";
	private static String wpaEndpointHostAndPort;
	private static String wpaEndpointUrlRoot;
	// private static final String WPA_ENDPOINT_URL_ROOT =
	// "http://hermes:35731/webservices";

	// STRATIX Server
	private static final String STX_ENDPOINT_BASE_URI = "/webservices";
	private static String stxEndpointProtocol = "http";
	private static String stxEndpointHostAndPort;
	private static String stxEndpointUrlRoot;
	// private static final String STX_ENDPOINT_URL_ROOT =
	// "http://saturn:60851/webservices";

	// Authentication service URI on the EXEC/WPA server
	private static final String AUTHENTICATION_SERVICE_ENDPOINT_URI = "/exec/AuthenticationService";

	// QDS service URI on the STRATIX Server
	private static final String QDS_SERVICE_ENDPOINT_URI = "/gateway/QDS/QdsService";
	
	// VOUCHER service URI on the STRATIX Server
	private static final String VOUCHER_SERVICE_ENDPOINT_URI = "/gateway/vouchers/VoucherService";
	
	private static final String VOUCHER_GL_SERVICE_ENDPOINT_URI = "/gateway/vouchers/GLDistributionService";
	
	private static final String VOUCHER_COST_RECON_SERVICE_ENDPOINT_URI = "/gateway/vouchers/CostReconciliationDistributionService";
	
	private static final String RCPT_SSN_SERVICE_ENDPOINT_URI = "/gateway/cashReceipts/CashReceiptSessionService";
	
	private static final String RCPT_SERVICE_ENDPOINT_URI = "/gateway/cashReceipts/CashReceiptService";
	
	private static final String AR_DIST_SERVICE_ENDPOINT_URI = "/gateway/cashReceipts/AccountsReceivableDistributionService";
	
	private static final String ADV_PMT_SERVICE_ENDPOINT_URI = "/gateway/cashReceipts/AdvancePaymentDistributionService";
	
	private static final String UN_DEBIT_SERVICE_ENDPOINT_URI = "/gateway/cashReceipts/UnappliedDebitDistributionService";
	
	private static final String GL_DIST_SERVICE_ENDPOINT_URI = "/gateway/cashReceipts/GLDistributionService";
	
	
	
	
	// Logout service URI on the STRATIX Server
	private static final String LOGOUT_SERVICE_ENDPOINT_URI = "/security/LogoutService";

	public static void setWpaEndpointProtocol(String httpOrHttps, String nginxURL) {
		if (nginxURL == null || nginxURL.length() == 0) {
			if (httpOrHttps != null) {
				wpaEndpointProtocol = httpOrHttps.trim();
				if ((httpOrHttps != null) && (wpaEndpointHostAndPort != null)) {
					wpaEndpointUrlRoot = wpaEndpointProtocol + "://" + wpaEndpointHostAndPort + WPA_ENDPOINT_BASE_URI;
					System.out.println(
							"Authentication Server Url: " + wpaEndpointProtocol + "://" + wpaEndpointHostAndPort);
				}
			}
		} else {
			// INVEX 2.0
			// "https://auxheidtmansteelproducts.invex.cloud/tsthsp-TST"
			wpaEndpointUrlRoot = nginxURL + WPA_ENDPOINT_BASE_URI;
			System.out.println("Authentication Server Url: " + wpaEndpointUrlRoot);

		}
	}

	public static void setWpaEndpointHostAndPort(String host, int port) {
		if (host != null) {
			wpaEndpointHostAndPort = host.trim() + ":" + port;
			if ((wpaEndpointProtocol != null) && (host != null)) {
				wpaEndpointUrlRoot = wpaEndpointProtocol + "://"
						+ wpaEndpointHostAndPort + WPA_ENDPOINT_BASE_URI;
				System.out.println("Authentication Server Url: "
						+ wpaEndpointProtocol + "://" + wpaEndpointHostAndPort);
			}
		}
	}

	private static String getWpaEndpointUrlRoot() {
		return wpaEndpointUrlRoot;
	}

	public static void setStxEndpointProtocol(String httpOrHttps) {
		if (httpOrHttps != null) {
			stxEndpointProtocol = httpOrHttps.trim();
			if ((httpOrHttps != null) && (stxEndpointHostAndPort != null)) {
				stxEndpointUrlRoot = stxEndpointProtocol + "://"
						+ stxEndpointHostAndPort + STX_ENDPOINT_BASE_URI;
				System.out.println("STRATIX Server Url: " + stxEndpointProtocol
						+ "://" + stxEndpointHostAndPort);
			}
		}
	}

	public static void setStxEndpointHostAndPort(String host, int port, String nginxURL) {

		if (nginxURL == null || nginxURL.length() == 0) {
			if (host != null) {
				stxEndpointHostAndPort = host.trim() + ":" + port;
				if ((stxEndpointProtocol != null) && (host != null)) {
					stxEndpointUrlRoot = stxEndpointProtocol + "://" + stxEndpointHostAndPort + STX_ENDPOINT_BASE_URI;
					System.out.println("STRATIX Server Url: " + stxEndpointProtocol + "://" + stxEndpointHostAndPort);
				}
			}
		} else {
			stxEndpointUrlRoot = nginxURL + STX_ENDPOINT_BASE_URI;
			System.out.println("STRATIX Server Url: " + stxEndpointUrlRoot);
		}
	}

	private static String getStxEndpointUrlRoot() {
		return stxEndpointUrlRoot;
	}

	public static AuthService createAuthenticationService(
			MsgSOAPHandler messageHandler) throws MalformedURLException {

		AuthService service = new AuthService_Service(
				new URL(getWpaEndpointUrlRoot()
						+ "/services/exec/AuthenticationService.wsdl"),
				new QName(STRATIX_NAMESPACE_ROOT, "AuthenticationService"))
				.getAuthenticationServiceSOAP();

		BindingProvider bp = (BindingProvider) service;
		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
				getWpaEndpointUrlRoot() + AUTHENTICATION_SERVICE_ENDPOINT_URI);

		WSUtils.addSOAPHandler(bp, messageHandler);

		return service;
	}
	
	
	
	

	public static LogoutService createLogoutService(
			MsgSOAPHandler messageHandler,
			SecSOAPHandler securityHandler) throws MalformedURLException {

		LogoutService service = new LogoutService_Service(new URL(
				getStxEndpointUrlRoot()
						+ "/services/security/LogoutService.wsdl"), new QName(
				STRATIX_NAMESPACE_ROOT, "LogoutService"))
				.getLogoutServiceSOAP();

		BindingProvider bp = (BindingProvider) service;
		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
				getStxEndpointUrlRoot() + LOGOUT_SERVICE_ENDPOINT_URI);

		WSUtils.addSOAPHandler(bp, messageHandler);
		WSUtils.addSOAPHandler(bp, securityHandler);

		return service;
	}
	
	
	public static VoucherService createVouchService(
			MsgSOAPHandler messageHandler,
			SecSOAPHandler securityHandler) throws MalformedURLException {

	VoucherService service = new VoucherService_Service(new URL(
				getStxEndpointUrlRoot() + "/services/gateway/vouchers/VoucherService.wsdl"), new QName(
				STRATIX_NAMESPACE_ROOT , "VoucherService")).getVoucherServiceSOAP(); 
				
		System.out.println("STRATIX Server Url(createVouchService): " + getStxEndpointUrlRoot() + "/services/gateway/vouchers/VoucherService.wsdl");
				
		
		BindingProvider bp = (BindingProvider) service;
		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
				getStxEndpointUrlRoot() + VOUCHER_SERVICE_ENDPOINT_URI);

		WSUtils.addSOAPHandler(bp, messageHandler);
		WSUtils.addSOAPHandler(bp, securityHandler);

		return service;
	}
	
	
	public static CostReconDistService createCostReconService(
			MsgSOAPHandler messageHandler,
			SecSOAPHandler securityHandler) throws MalformedURLException {

		CostReconDistService service = new CostReconDistService_Service(new URL(
				getStxEndpointUrlRoot() + "/services/gateway/vouchers/CostReconciliationDistributionService.wsdl"), new QName(
				STRATIX_NAMESPACE_ROOT , "CostReconciliationDistributionService")).getCostReconciliationDistributionServiceSOAP(); 
				
		
		BindingProvider bp = (BindingProvider) service;
		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
				getStxEndpointUrlRoot() + VOUCHER_COST_RECON_SERVICE_ENDPOINT_URI);

		WSUtils.addSOAPHandler(bp, messageHandler);
		WSUtils.addSOAPHandler(bp, securityHandler);

		return service;
	}
	
	public static com.invera.stratix.services.GLDistributionService createVouGLService(
			MsgSOAPHandler messageHandler,
			SecSOAPHandler securityHandler) throws MalformedURLException {

	com.invera.stratix.services.GLDistributionService service = new com.invera.stratix.services.GLDistributionService_Service(new URL(
				getStxEndpointUrlRoot() + "/services/gateway/vouchers/GLDistributionService.wsdl"), new QName(
				STRATIX_NAMESPACE_ROOT , "GLDistributionService")).getGLDistributionServiceSOAP(); 
				
		
		BindingProvider bp = (BindingProvider) service;
		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
				getStxEndpointUrlRoot() + VOUCHER_GL_SERVICE_ENDPOINT_URI);

		WSUtils.addSOAPHandler(bp, messageHandler);
		WSUtils.addSOAPHandler(bp, securityHandler);

		return service;
	}

	
}
