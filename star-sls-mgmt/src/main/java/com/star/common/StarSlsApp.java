package com.star.common;
 
import org.glassfish.jersey.server.ResourceConfig;
 
public class StarSlsApp extends ResourceConfig 
{
    public StarSlsApp() 
    {
        packages("com.star.services");
        register(AuthenticationFilter.class);
        register(CustomLoggingFilter.class);
    }
}