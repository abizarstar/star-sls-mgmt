package com.star.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.star.common.storage.S3Directory;
import com.star.common.storage.StorageType;

public class ManageDirectory {

	private static Logger logger = Logger.getLogger(ManageDirectory.class.getName());

	public Boolean createDir(String dirName, String dirPath) {
		boolean bool = false;
		S3Directory s3Directory = new S3Directory();

		if (StorageType.getStorgaeType() != null) {
			if (!StorageType.getStorgaeType().equals("S3")) {

				if (dirPath.length() > 0) {
					dirPath = dirPath.replace(".", "/");
				}

				// Creating a File object
				File file = new File(CommonConstants.ROOT_DIR + dirPath + "/" + dirName);
				// Creating the directory
				bool = file.mkdir();
				if (bool) {

					logger.log(Level.INFO, "Directory created successfully");

				} else {

					logger.log(Level.WARNING, "Sorry couldn�t create specified directory");

				}
			} else {
				if (dirPath.length() > 0) {
					dirPath = dirPath.replace(".", "/");
				}

				bool = s3Directory.createDir(StorageType.getBucketName(), dirPath, dirName, StorageType.getS3Client());
			}
		} else {
			if (dirPath.length() > 0) {
				dirPath = dirPath.replace(".", "/");
			}

			// Creating a File object
			File file = new File(CommonConstants.ROOT_DIR + dirPath + "/" + dirName);
			// Creating the directory
			bool = file.mkdir();
			if (bool) {

				logger.log(Level.INFO, "Directory created successfully");

			} else {

				logger.log(Level.WARNING, "Sorry couldn�t create specified directory");

			}
		}

		return bool;
	}

	public Boolean moveFile(String oldLocation, String newLocation) {
		Boolean fileMvSts = false;

		Path result = null;
		try {
			result = Files.move(Paths.get(oldLocation), Paths.get(newLocation));
		} catch (IOException e) {
			System.out.println("Exception while moving file: " + e.getMessage());
		}
		if (result != null) {
			fileMvSts = true;
			System.out.println("File moved successfully.");
		} else {
			fileMvSts = false;
			System.out.println("File movement failed.");
		}

		return fileMvSts;
	}

	public String getFullDocPath(String dirPath, String dirName, String typNm, String docNam, String docExtn) {
		if (dirPath.length() > 0) {
			dirPath = dirPath.replace(".", "/");

			dirPath = dirPath + "/";
		}

		String docPath = CommonConstants.ROOT_DIR + dirPath + dirName + "/" + typNm + "/" + docNam + "." + docExtn;

		return docPath;

	}
	
	public boolean createFile( String docNm, String docExtn,InputStream inputStream) {
		boolean flg = true;
		//if(!StorageType.getStorgaeType().equals("S3"))
		//{
			String UPLOAD_PATH = CommonConstants.ROOT_DIR;

			//UPLOAD_PATH = UPLOAD_PATH ;//+ docPath + docFol + "/" + docType + "/";

			int read = 0;
			byte[] bytes = new byte[1024];

			OutputStream out;
			try {
				out = new FileOutputStream(new File(UPLOAD_PATH + docNm + "." + docExtn));
			
			while ((read = inputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();
			
			} catch (FileNotFoundException e) {
				flg = false;
				e.printStackTrace();
			} catch (IOException e) {
				flg = false;
				e.printStackTrace();
			}
			
			return flg;
		//} 
		/*else
		{
			AmazonS3 s3client = StorageType.getS3Client();
			String bucketName = StorageType.getBucketName();
			String keyPath = docNm + "." + docExtn; //docPath + docFol + "/" + docType + "/" + docNm + "." + docExtn;
			
			PutObjectResult objectResult = s3client.putObject(bucketName, keyPath, inputStream, null);	
		} */
	}
	
	public String createFile(String docPath, String docNm, String docExtn,
			InputStream inputStream) {

		String rtnMsg = "";

		String UPLOAD_PATH = CommonConstants.ROOT_DIR;

		UPLOAD_PATH = UPLOAD_PATH + docPath + "/";

		int read = 0;
		byte[] bytes = new byte[1024];

		OutputStream out;
		try {
			out = new FileOutputStream(new File(UPLOAD_PATH + docNm + "." + docExtn));

			while ((read = inputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			rtnMsg = CommonConstants.PATH_ERR;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			rtnMsg = e.getMessage();

		}
		return rtnMsg;
	}
	
	public InputStream downloadFileRequest(String folPath, String docFol, String docTyp, String docNm, String docExtn)
	{
		InputStream fileInputStream = null;
		
		if(!StorageType.getStorgaeType().equals("S3"))
		{
		
			final String filePath = CommonConstants.ROOT_DIR + folPath + docFol + "/" + docTyp + "/" + docNm + "."
					+ docExtn;
			
			File file = new File(filePath);
			
			try {
				fileInputStream = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			AmazonS3 s3client = StorageType.getS3Client();
			String bucketName = StorageType.getBucketName();
			
			String filePath = folPath + docFol + "/" + docTyp + "/" + docNm + "." + docExtn;
			
			S3Object fileObject = s3client.getObject(new GetObjectRequest(bucketName, filePath));
			
			fileInputStream = fileObject.getObjectContent();
		}
		
		return fileInputStream;
	}

	public String validateDir(String dirPath)
	{
		String rtnMsg = "";
		
		File file = null;
		
		try
		{
			if (!Paths.get(dirPath).toFile().isDirectory())
		    {
				file = new File(dirPath);
		         file.createNewFile();
		         boolean flag = file.mkdir();
		         System.out.print("Directory created? " + flag);
		    }
			else
			{
				rtnMsg = "Directory already available";
			}
		}
		catch(IOException e)
		{
			rtnMsg = e.getMessage();
		}
		catch(Exception e)
		{
			
			rtnMsg = e.getMessage();
		}
		
		if(rtnMsg.length() == 0)
		{
			if(file != null)
			{
				file.delete();
			}
		}
		
		return rtnMsg;
	}
	
	public String createLclDir(String dirPath)
	{
		String rtnMsg = "";
		
		File file = null;
		
		try
		{
			if (!Paths.get(dirPath).toFile().isDirectory())
		    {
				file = new File(dirPath);
		         file.createNewFile();
		         boolean flag = file.mkdir();
		         System.out.print("Directory created? " + flag);
		    }
			else
			{
				rtnMsg = "Directory already available";
			}
		}
		catch(IOException e)
		{
			rtnMsg = e.getMessage();
		}
		catch(Exception e)
		{
			
			rtnMsg = e.getMessage();
		}
		

		return rtnMsg;
	}
	
	public InputStream downloadDoc(String folPath)
	{
		InputStream fileInputStream = null;
		
		/*if(!StorageType.getStorgaeType().equals("S3"))
		{*/
		
			final String filePath = CommonConstants.ROOT_DIR + folPath ;
			
			File file = new File(filePath);
			
			try {
				fileInputStream = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				fileInputStream = getClass().getClassLoader().getResourceAsStream(CommonConstants.FILE_NOT_FOUND_FILE);
			}
		/*}
		else
		{
			AmazonS3 s3client = StorageType.getS3Client();
			String bucketName = StorageType.getBucketName();
			
			String filePath = folPath ;
			
			S3Object fileObject = s3client.getObject(new GetObjectRequest(bucketName, filePath));
			
			fileInputStream = fileObject.getObjectContent();
		}*/
		
		return fileInputStream;
	}

	/* CREATE ACH FILE */
	public Boolean writeToFile(String docPath, String flNm, String flExtn, String input)
	{
		Boolean status = false;
		 try {
		      FileWriter myWriter = new FileWriter(CommonConstants.ROOT_DIR + docPath + flNm + flExtn);
		      myWriter.write(input);
		      myWriter.close();
		      System.out.println("Successfully wrote to the file.");
		      
		      status = true;
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      status = false;
		      e.printStackTrace();
		    }
		 
		 return status;
	}
	
	public InputStream downloadAchFile(String flNm)
	{
		InputStream fileInputStream = null;
		
		/*if(!StorageType.getStorgaeType().equals("S3"))
		{*/
		
			final String filePath = CommonConstants.ROOT_DIR + CommonConstants.ELEC_DIR + "/" + flNm ;
			
			File file = new File(filePath);
			
			try {
				fileInputStream = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		/*}
		else
		{
			AmazonS3 s3client = StorageType.getS3Client();
			String bucketName = StorageType.getBucketName();
			
			String filePath = folPath ;
			
			S3Object fileObject = s3client.getObject(new GetObjectRequest(bucketName, filePath));
			
			fileInputStream = fileObject.getObjectContent();
		}*/
		
		return fileInputStream;
	}
	
	
	public InputStream downloadFile(String flNm)
	{
		InputStream fileInputStream = null;
		
		/*if(!StorageType.getStorgaeType().equals("S3"))
		{*/
			
			File file = new File(flNm);
			
			try {
				fileInputStream = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		/*}
		else
		{
			AmazonS3 s3client = StorageType.getS3Client();
			String bucketName = StorageType.getBucketName();
			
			String filePath = folPath ;
			
			S3Object fileObject = s3client.getObject(new GetObjectRequest(bucketName, filePath));
			
			fileInputStream = fileObject.getObjectContent();
		}*/
		
		return fileInputStream;
	}
	
	
}
