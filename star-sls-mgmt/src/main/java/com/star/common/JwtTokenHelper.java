package com.star.common;

import java.security.Key;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.crypto.spec.SecretKeySpec;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;

public class JwtTokenHelper {

   private static JwtTokenHelper jwtTokenHelper = null;
   private static final long EXPIRATION_LIMIT = 180;
   private Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

   SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
   
   long nowMillis = System.currentTimeMillis();
   Date now = new Date(nowMillis);
   
   private JwtTokenHelper() {
   }

   public static JwtTokenHelper getInstance() {
      if(jwtTokenHelper == null)
           jwtTokenHelper = new JwtTokenHelper();
      return jwtTokenHelper;
   }

   //  1
   public String generatePrivateKey(String username, String password) {
       return Jwts
                .builder()
                .setSubject(username)
                .setSubject(password)
                .setExpiration(getExpirationDate())
                .signWith(key)
                .compact();
       
       
   }
   
   
   // 2
   public void claimKey(String privateKey) throws ExpiredJwtException, MalformedJwtException, SignatureException{
      Jwts
                .parser()
                .setSigningKey(key)
                .parseClaimsJws(privateKey);
                
   }

   // 3
   private Date getExpirationDate() {
       long currentTimeInMillis = System.currentTimeMillis();
       long expMilliSeconds = TimeUnit.MINUTES.toMillis(EXPIRATION_LIMIT);
       return new Date(currentTimeInMillis + expMilliSeconds);
   }

}