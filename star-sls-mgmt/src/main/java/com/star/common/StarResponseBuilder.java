package com.star.common;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

public class StarResponseBuilder {

	private static final String HEADER_SERVICE_STATUS	   = "star-service-status";
	private static final String HEADER_SERVICE_MESSAGES	   = "star-service-messages";
	
	private List<String> messages =  new ArrayList<String>(); 
	
	public Response getSuccessResponse(GenericEntity<?> genericEntity) {

		ResponseBuilder builder = Response.ok();
		builder.header(HEADER_SERVICE_STATUS,"SUCCESS");
		builder.header(HEADER_SERVICE_MESSAGES, messages);
		builder.entity(genericEntity);
		builder.type(MediaType.APPLICATION_JSON);
		return builder.build();
	}
	
	public Response getWarningResponse(GenericEntity<?> genericEntity) {

		ResponseBuilder builder = Response.ok();
		builder.header(HEADER_SERVICE_STATUS,"WARNING");
		builder.header(HEADER_SERVICE_MESSAGES, messages);
		builder.entity(genericEntity);
		builder.type(MediaType.APPLICATION_JSON);
		return builder.build();
	}
	
	public Response getErrorResponse(GenericEntity<?> genericEntity) {
		
		ResponseBuilder builder = Response.serverError();
		builder.header(HEADER_SERVICE_STATUS,"ERROR");
		builder.header(HEADER_SERVICE_MESSAGES, messages);
		builder.type(MediaType.APPLICATION_JSON);
		builder.entity(genericEntity);
		
		return builder.build();
	}
	
	
	
/*	
	public Response getSuccessResponse(StarMaintenanceResponse starMaintenanceResponse) {

		ResponseBuilder builder = Response.ok();
		builder.header(HEADER_SERVICE_STATUS,"SUCCESS");
		builder.header(HEADER_SERVICE_MESSAGES, messages);
		builder.entity(starMaintenanceResponse);
		
		return builder.build();
	}
	
	public Response getErrorResponse(StarMaintenanceResponse starMaintenanceResponse) {

		ResponseBuilder builder = Response.status(Response.Status.PARTIAL_CONTENT);
		builder.header(HEADER_SERVICE_STATUS,"ERROR");
		builder.header(HEADER_SERVICE_MESSAGES, messages);
		builder.entity(starMaintenanceResponse);
		
		return builder.build();
	}*/
	
	public void setMessage(String message) {
		
		if (message == null || message.trim().length() == 0) {
			return;
		}
		
		if(messages == null) {
			messages =  new ArrayList<String>();
		}
		
		messages.add(message);			
	}
	
	public boolean errorOccured() {
		
		if(messages.size() > 0) {
			return true;
		}
		else {
			return false;
		}
	}
}
