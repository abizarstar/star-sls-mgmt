package com.star.common;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

public class FormatPDFFile {

	public boolean addSignature(String filePath, String genFlPath, String signImg, double imgWdth, double imgHight,
			double imgPosX, double imgPosY, double docW, double docH) {

		boolean rtnSts = true;

		File file = new File(filePath);
		PDDocument doc;
		try {
			doc = PDDocument.load(file);

			// Retrieving the page
			PDPage page = doc.getPage(0);

			System.out.println(page.getMediaBox().getHeight());
			System.out.println(page.getMediaBox().getWidth());

			System.out.println((page.getMediaBox().getHeight() * 25.4 / 72) * 3.7795275591);
			System.out.println((page.getMediaBox().getWidth() * 25.4 / 72) * 3.7795275591);
			
			
			double tmpDocH = (docH - (docH*.11)) - imgPosY;
			
			double tmpDocW = (imgPosX - (imgPosX*.34));
			
			imgPosY = tmpDocH;
			imgPosX = tmpDocW;
			
			double xPos = imgPosX; /* pixel to page points .009 */
			double yPos = imgPosY;
			
			imgWdth = imgWdth - (imgWdth*.30);
			imgHight = imgHight - (imgHight*.30);
			
			// Creating PDImageXObject object
			PDImageXObject pdImage = PDImageXObject.createFromFile(signImg, doc);

			// creating the PDPageContentStream object
			PDPageContentStream contents = new PDPageContentStream(doc, page, AppendMode.APPEND, true);

			// Drawing the image in the PDF document
			contents.drawXObject(pdImage, (float) xPos, (float) yPos,(float) imgWdth, (float) imgHight);

			System.out.println("Image inserted");

			// Closing the PDPageContentStream object
			contents.close();

			// Saving the document
			doc.save(genFlPath);

			// Closing the document
			doc.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			rtnSts = false;
		}

		return rtnSts;
	}

}
