package com.star.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.pdfbox.multipdf.PDFMergerUtility;

public class MergePDFDocument {

	public void doMerge(String fileNm1, String fileNm2, String newFile)
	{
		File file1 = new File(fileNm1);       
	    File file2 = new File(fileNm2);
		
	    try {
	    	
	    if (file2.createNewFile()) {
            System.out.println("newFile.txt File is created! ");
        } else {
            System.out.println("newFile.txt File already exists! ");
        }
	    
		
		 PDFMergerUtility PDFmerger = new PDFMergerUtility();
			
	      //Setting the destination file
	      PDFmerger.setDestinationFileName(newFile);
			
	      //adding the source files
	     
	      PDFmerger.addSource(file1);
		
	      PDFmerger.addSource(file2);
			
	      //Merging the two documents
	      PDFmerger.mergeDocuments();
	      System.out.println("Documents merged");
	      } catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
