package com.star.common;

import com.invera.stratix.schema.common.datatypes.AuthenticationToken;
import com.ws.samples.ServiceHelper;
import com.ws.samples.handler.MsgSOAPHandler;
import com.ws.samples.handler.SecSOAPHandler;

public class HandlerService {

	public SecSOAPHandler createSecurityHandlers(String userId, String authToken,
			String appHost, String appPort,
			String cmpyId, String nginxURL) {
		ServiceHelper.setWpaEndpointProtocol("http", nginxURL);

		ServiceHelper.setStxEndpointHostAndPort(appHost, Integer.parseInt(appPort), nginxURL);

		AuthenticationToken authenticationToken = new AuthenticationToken();

		authenticationToken.setUsername(userId);
		authenticationToken.setValue(authToken);

		SecSOAPHandler securityHandler = new SecSOAPHandler(authenticationToken);
		
		return securityHandler;
	}
	
	public MsgSOAPHandler createMessageHandlers(String userId, String authToken,
			String appHost, String appPort,
			String cmpyId, String nginxURL) {
		ServiceHelper.setWpaEndpointProtocol("http", nginxURL);

		ServiceHelper.setStxEndpointHostAndPort(appHost, Integer.parseInt(appPort), nginxURL);

		MsgSOAPHandler messageHandler = new MsgSOAPHandler();
		
		return messageHandler;
	}
	
}
