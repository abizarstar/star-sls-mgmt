package com.star.common;


public class BrowseResponse<T_Output extends BrowseOutput> {

	public T_Output output = null;
	
	public void setOutput(T_Output output) {
		this.output = output; 
	}
	
}
