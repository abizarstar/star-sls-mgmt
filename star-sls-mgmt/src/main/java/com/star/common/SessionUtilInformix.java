package com.star.common;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.star.dao.SetgDAO;
import com.star.linkage.auth.SettingBrowseOutput;
import com.star.linkage.setg.ConCnfg;

public class SessionUtilInformix {

	private static SessionUtilInformix instance = new SessionUtilInformix();
	private SessionFactory sessionFactory;

	public static SessionUtilInformix getInstance() {
		return instance;
	}

	private SessionUtilInformix(){

		BrowseResponse<SettingBrowseOutput> starBrowseResponse = new BrowseResponse<SettingBrowseOutput>();
		starBrowseResponse.setOutput(new SettingBrowseOutput());
		
		ConCnfg conCnfg = new ConCnfg();
		SetgDAO settingDAO = new SetgDAO();

		conCnfg = settingDAO.getCon(starBrowseResponse, "M");

		Configuration configuration = new Configuration();

		if (conCnfg.getDriver().equals("Informix")) {
			configuration.configure("informix.cfg.xml");
			configuration.getProperties().setProperty("hibernate.connection.driver_class","com.informix.jdbc.IfxDriver");
		 } else {
			configuration.configure("hibernate.cfg.xml");
			configuration.getProperties().setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");
		 }

		configuration.getProperties().setProperty("hibernate.connection.url", conCnfg.getUrl());
		configuration.getProperties().setProperty("hibernate.connection.password", conCnfg.getPass());
		configuration.getProperties().setProperty("hibernate.connection.username", conCnfg.getUsr_nm());
		
		
		sessionFactory = configuration.buildSessionFactory();
	}
	
	public static Session getSession() {

		Session session = getInstance().sessionFactory.openSession();

		while (true) {
			try {
				session.createSQLQuery("select 1 from scrcsc_rec").list();
				
				break;
				
			} catch (Exception e) {
				
				if(session.isOpen())
				{
					session.close();
				}
				else
				{
					session = getInstance().sessionFactory.openSession();
				}
			}

		}

		return session;
	}

	public static void resetSessionDetails() {		
		if(getInstance().sessionFactory.isClosed())
		{
			SessionUtilInformix sessionUtilInformix = new SessionUtilInformix();
		}
	}

}

/*
 * String InformixServer = ""; String Database = ""; String DBport = ""; String
 * DBSrvrNm = "";
 * 
 * InformixServer = "va2fitdv1apspr12"; Database = "ap102pr03"; DBport =
 * "15002"; DBSrvrNm = "172.20.35.73";
 * 
 * Configuration configuration = new Configuration();
 * 
 * configuration.configure("informix.cfg.xml"); String
 * newUserName,newPassword;//set them as per your needs
 * configuration.getProperties().setProperty(
 * "hibernate.connection.driver_class", "com.informix.jdbc.IfxDriver");
 * 
 * configuration.getProperties().setProperty("hibernate.connection.url",
 * "jdbc:informix-sqli://" + DBSrvrNm + ":" + DBport.trim() + "/" +
 * Database.trim() + ":INFORMIXSERVER=" + InformixServer.trim());
 * configuration.getProperties().setProperty("hibernate.connection.password",
 * "ap10203");
 * configuration.getProperties().setProperty("hibernate.connection.username",
 * "ap10203");
 */
