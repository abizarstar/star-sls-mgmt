package com.star.common;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.Query;
import org.hibernate.Session;



public class ValidateValue {
	
	
 
 
 
  		
  		
  		
  
		public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
				    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

		public static boolean validateEMail(String emailStr) {
		        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
		        return matcher.find();
		}
		
		public static final Pattern VALID_PHONE_NO_REGEX = 
			    Pattern.compile("^((\\+|0)(\\d{1,3})[\\s-]?)?(\\d{7,10})$");
		
		public static boolean validatePhoneNo(String phnNo) {
	        Matcher matcher = VALID_PHONE_NO_REGEX .matcher(phnNo);
	        return matcher.find();
		}
		
		public static final Pattern VALID_NUMERIC_REGEX = 
			    Pattern.compile("[0-9]+([.][0-9]{1,4})?");
		
		public static boolean validateNumeric(String number) {
	        Matcher matcher = VALID_NUMERIC_REGEX .matcher(number);
	        return matcher.find();
		}
		
		public String getFileExtension(String fileName) {
			if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
				return fileName.substring(fileName.lastIndexOf(".") + 1);
			else
				return "";
		}

}
