package com.star.common;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ManageDirectory1 {

	private static Logger logger = Logger.getLogger(ManageDirectory1.class.getName());
	
	public Boolean createDir(String dirName, String dirPath)
	{
		if(dirPath.length() > 0)
		{
			dirPath = dirPath.replace(".", "/");
		}
		
	      //Creating a File object
	      File file = new File(CommonConstants.ROOT_DIR + dirPath + "/" + dirName);
	      //Creating the directory
	      boolean bool = file.mkdir();
	      if(bool){
	       
	    	  logger.log(Level.INFO, "Directory created successfully");
	    	  
	      }else{
	    	  
	    	  logger.log(Level.WARNING, "Sorry couldn�t create specified directory");
	         
	      }
	      
	      return bool;
	}
	
	
	public Boolean moveFile(String oldLocation, String newLocation)
	{
		Boolean fileMvSts = false;
		
		Path result = null;
	      try {
	         result =  Files.move(Paths.get(oldLocation), Paths.get(newLocation));
	      } catch (IOException e) {
	         System.out.println("Exception while moving file: " + e.getMessage());
	      }
	      if(result != null) {
	    	  fileMvSts = true;
	         System.out.println("File moved successfully.");
	      }else{
	    	  fileMvSts = false;
	         System.out.println("File movement failed.");
	      }  
	      
	      return fileMvSts;
	}
	
	public String getFullDocPath(String dirPath, String dirName, String typNm, String docNam, String docExtn)
	{
		if(dirPath.length() > 0)
		{
			dirPath = dirPath.replace(".", "/");
			
			dirPath = dirPath + "/";
		}
		
		String docPath = CommonConstants.ROOT_DIR + dirPath + dirName + "/" + typNm + "/" + docNam + "." + docExtn;
		
		return docPath;
	
	}
}
