package com.star.common;

public class MaintanenceResponse <T_Output extends MaintenanceOutput>{

	public T_Output output = null;
	
	public void setOutput(T_Output output) {
		this.output = output; 
	}
	
}