package com.star.common;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CommonFunctions {

	public String formatDate(Date dateVal) {
		String pattern = "yyyy-MM-dd hh:mm:ss";

		String formatPattern = "MM/dd/yyyy hh:mm:ss aaa";

		String formattedDate = "";

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(formatPattern);

		try {
			Date date = simpleDateFormat.parse(dateVal.toString());

			formattedDate = simpleDateFormat1.format(date);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return formattedDate;
	}

	public Date formatDateWithoutTime(String dateVal) {

		String formatPattern = "MM/dd/yyyy";

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatPattern);

		Date date = null;
		if (dateVal.length() < 2) {
			return null;
		}
		try {

			date = simpleDateFormat.parse(dateVal);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return date;
	}

	public String formatDateWithoutTime(Date dateVal) {
		String pattern = "yyyy-MM-dd";

		String formatPattern = "MM/dd/yyyy";

		String formattedDate = "";

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(formatPattern);

		try {

			Date date = simpleDateFormat.parse(dateVal.toString());

			formattedDate = simpleDateFormat1.format(date);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return formattedDate;
	}

	public String formatDateWithoutTimeVou(String dateVal) {
		String pattern = "yyyy-MM-dd";

		String formatPattern = "MM/dd/yyyy";

		String formattedDate = "";

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatPattern);

		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern);

		try {
			Date date = simpleDateFormat.parse(dateVal);

			formattedDate = simpleDateFormat1.format(date);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return formattedDate;
	}
	
	public String formatDateWithoutTimeVouc(String dateVal) {
		String pattern = "MMMM dd yyyy";

		String formatPattern = "MM/dd/yyyy";

		String formattedDate = "";

		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(formatPattern);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		try {
			Date date = simpleDateFormat.parse(dateVal);
			formattedDate = simpleDateFormat1.format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return formattedDate;
	}
	
	public String formatDateWithymd(String dateVal) {
		String pattern = "dd-mm-yyyy";

		String formatPattern = "YYYY-MM-DD";

		String formattedDate = "";

		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(formatPattern);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		try {
			Date date = simpleDateFormat.parse(dateVal);
			formattedDate = simpleDateFormat1.format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return formattedDate;
	}
	
	public Integer formatDateIntyyyymmdd(String dateVal) {
		String pattern = "MM/dd/yyyy";

		String formatPattern = "yyyyMMdd";

		String formattedDate = "";

		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(formatPattern);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		try {
			Date date = simpleDateFormat.parse(dateVal);
			formattedDate = simpleDateFormat1.format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return Integer.parseInt(formattedDate);
	}

	public String formatAmount(double amount) {
		NumberFormat nf = NumberFormat.getInstance(new Locale("en", "US"));

		String val = nf.format(amount);

		return val;
	}
	
	public static String checkNull(Object val)
	{
		String rtnVal = "";
		
		if(val != null)
		{
			rtnVal = val.toString().trim();
		}
		
		return rtnVal;
	}
	
	public boolean isInteger(String strNum) {
	    if (strNum == null) {
	        return false;
	    }
	    try {
	        Integer i = Integer.parseInt(strNum);
	    } catch (NumberFormatException nfe) {
	        return false;
	    }
	    return true;
	}
}
