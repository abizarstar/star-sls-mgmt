package com.star.common;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;

/**
 * This filter verify the access permissions for a user based on username and
 * passowrd provided in request
 */
@Provider
public class AuthenticationFilter implements javax.ws.rs.container.ContainerRequestFilter {

	@Context
	private ResourceInfo resourceInfo;

	private static final String AUTHORIZATION_PROPERTY = "app-token";
	private static final String PRIVATE_KEY = "privateKey";

	private JwtTokenHelper jwTokenHelper = JwtTokenHelper.getInstance();
	
	public void filter(ContainerRequestContext requestContext) {
		Method method = resourceInfo.getResourceMethod();
		// Access allowed for all

		if (!requestContext.getUriInfo().getPath().equals("auth/add")
				&& !requestContext.getUriInfo().getPath().equals("auth/validate")) {
			if (!method.isAnnotationPresent(PermitAll.class)) {
				// Access denied for all
				if (method.isAnnotationPresent(DenyAll.class)) {
					requestContext.abortWith(Response.status(Response.Status.FORBIDDEN)
							.entity("Access blocked for all users !!").build());
					return;
				}

				// Get request headers
				final MultivaluedMap<String, String> headers = requestContext.getHeaders();

				// Fetch authorization header
				
				List<String> authorization = null;
				
				if (!requestContext.getUriInfo().getPath().equals("cstmdoc/download")
						&& !requestContext.getUriInfo().getPath().equals("cstmdoc/downloadget")
						&& !requestContext.getUriInfo().getPath().equals("docinfo/download")
						&& !requestContext.getUriInfo().getPath().equals("payment/dwnld-ach") )
				{
					 authorization = headers.get(AUTHORIZATION_PROPERTY);
				}
				else
				{
					
					/*authorization = headers.get(AUTHORIZATION_PROPERTY);*/
					
					authorization = new ArrayList<String>();
					
					authorization.add(requestContext.getCookies().get(AUTHORIZATION_PROPERTY).getValue());
				}
				

				// If no authorization information present; block access
				if (authorization == null || authorization.isEmpty()) {
					requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
							.entity("You cannot access this resource").build());
					return;
				}
				
				final String privateKeyHeaderValue = authorization.get(0);
				
		        try {
		        	
		        	requestContext.getHeaders().add("star-auth-token", privateKeyHeaderValue);
		        	//jwTokenHelper.claimKey(privateKeyHeaderValue); 
		        	
		        } catch(Exception e) {
		            if (e instanceof ExpiredJwtException) {
		                throw new WebApplicationException(getUnAuthorizeResponse(PRIVATE_KEY + " is expired"));
		            } else if (e instanceof MalformedJwtException) {
		                throw new WebApplicationException(getUnAuthorizeResponse(PRIVATE_KEY + " is not correct"));
		            }
		           else if (e instanceof SignatureException) {
		                throw new WebApplicationException(getUnAuthorizeResponse("Signature is Invalid"));
		            }
		        }
				
				/*final String encodedUserPassword = authorization.get(0).replaceFirst(AUTHENTICATION_SCHEME + " ", "");

				final StringTokenizer tokenizer = new StringTokenizer(encodedUserPassword, ":");
				final String username = tokenizer.nextToken();
				final String password = tokenizer.nextToken();

				System.out.println(username);
				System.out.println(password);

				if (method.isAnnotationPresent(RolesAllowed.class)) {
					RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
					Set<String> rolesSet = new HashSet<String>(Arrays.asList(rolesAnnotation.value()));

					// Is user valid?
					if (!isUserAllowed(username, password, rolesSet)) {
						requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
								.entity("You cannot access this resource").build());
						return;
					}
				}*/
			}
		}
	}
	
	private Response getUnAuthorizeResponse(String message) {
	       return Response.ok(message
	       ).status(Response.Status.UNAUTHORIZED)
	        .type(MediaType.APPLICATION_JSON)
	        .build();
	    }

	private boolean isUserAllowed(final String username, final String password, final Set<String> rolesSet) {
		boolean isAllowed = false;

		// Step 1. Fetch password from database and match with password in argument
		// If both match then get the defined role for user from database and continue;
		// else return isAllowed [false]
		// Access the database and do this part yourself
		// String userRole = userMgr.getUserRole(username);

		if (username.equals("abizar") && password.equals("password")) {
			String userRole = "ADMIN";

			// Step 2. Verify user role
			if (rolesSet.contains(userRole)) {
				isAllowed = true;
			}
		}
		return isAllowed;
	}
}