package com.star.common.abbyy.venTemplate;

import java.io.File;
import java.util.Scanner;

public class Leeco_Invoice {
	private static String vendor = "Leeco Steel, LLC";

	public InvoiceDetailModel readData(String fileName) throws Exception {
		File file = new File(fileName);
		Scanner sc = new Scanner(file);

		String line = null;
		int lnth = 0;
		int lnth1 = 0;
		// code to extract data from file
		int count = 0;
		int l1, l2, l3, l4;
		String invoicenumber = "Invoice:";
		String invoicedate = "Date:";
		String duedate = "Payment Terms";
		String totalamt = "Total:";
		String InvoiceNumber = " - ";
		String InvoiceDate = " - ";
		String DueDate = " - ";
		String TotalAmt = " - ";
		String curr = "USD";
		String Currency = " - ";

		int c1 = 0, c2 = 0;

		while (sc.hasNext()) {
			count += 1;
			line = sc.nextLine();
			int lastindex = line.length();

			if (line.contains(invoicenumber) && c1 == 0) {
				l1 = line.indexOf(invoicenumber);
				lnth = invoicenumber.length() + l1;
				// System.out.println(l1);
				// System.out.println(sc.nextLine());
				InvoiceNumber = line.substring(lnth, lastindex).trim();
				c1++;
			}
			if (line.contains(invoicedate)) {
				l2 = line.indexOf(invoicedate);
				lnth = invoicedate.length() + l2;
				// System.out.println(lnth+"adsf"+lastindex);
				InvoiceDate = line.substring(lnth, lastindex).trim();
			}
			if (line.contains(duedate)) {
				l3 = line.indexOf(duedate);
				lnth = duedate.length() + l3;
				System.out.println(sc.nextLine());
				DueDate = sc.nextLine().substring(l3, lnth).replaceAll("[a-z A-Z,]", "").trim();
			}
			if (line.contains(curr)) {
				Currency = "USD";
			}
			if (line.contains(totalamt) && c2 == 0) {
				l4 = line.indexOf(totalamt);
				lnth = totalamt.length() + l4;
				TotalAmt = line.substring(lnth, lastindex).trim().replaceAll("[,a-zA-Z]", "");
				c2++;
			}
		}
		System.out.println("INVOICE NUMBER : " + InvoiceNumber);
		System.out.println("INVOICE DATE : " + InvoiceDate);
		System.out.println(duedate + " : " + DueDate);
	 System.out.println(duedate + " : " + DtDay.Dt(InvoiceDate,Integer.parseInt(DueDate)));
		System.out.println("CURRENCY : " + Currency);
		System.out.println("INVOICE TOTAL : " + TotalAmt);
		InvoiceDetailModel invs = new InvoiceDetailModel();
		invs.setInvoice(InvoiceNumber);
		  invs.setVendorName(vendor);
		invs.setInvoiceDate(InvoiceDate);
		invs.setDueDate(DtDay.Dt(InvoiceDate, Integer.parseInt(DueDate)));
		invs.setCurrency(curr);
		invs.setTotalamount(TotalAmt);
		return invs;
	}

	public static String getVendor() {
		return vendor;
	}

	public static void setVendor(String vendor) {
		Leeco_Invoice.vendor = vendor;

	}
}