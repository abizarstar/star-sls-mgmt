package com.star.common.abbyy.venTemplate;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import com.star.common.CommonFunctions;

public class ADSInvoice {
	int count = 0;
	int l1, l2, l3, l4, l5, l6;
	private static String invoicenum = "INVOICE NUMBER";
	private static String vendor = "Area Transportation division of ADS Logistics Co, LLC";
	private static String invoicedate = "INVOICE DATE";
	private static String duedate = "TERMS";
	private static String curr = "U.S. Dollars";
	private static String ttlamt = "TOTAL DUE";
	String invoice = "";
	String customer = "";
	String invoiceDate = "";
	String dueDate = "";
	String currency = "";
	String totalamount = "";

	public static String getVendor() {
		return vendor;
	}

	public InvoiceDetailModel readData(String fileName) throws Exception {
		File file = new File(fileName);
		Scanner sc = new Scanner(file);

		String line = null;
		int lnth = 0;
		int lnth1 = 0;
		// code to extract data from file

		while (sc.hasNext()) {
			count += 1;
			line = sc.nextLine();

			if (line.contains(invoicenum)) {
				l1 = line.indexOf(invoicenum);
				lnth = invoicenum.length() + l1 + 5;
				invoice = sc.nextLine().substring(l1 - 5, lnth).trim();
			}
			if (line.contains(vendor)) {
				l2 = line.indexOf(vendor);
				lnth = l2 + 35;
				customer = line.substring(l2, lnth).trim();
			}
			if (line.contains(invoicedate)) {
				l3 = line.indexOf(invoicedate);
				lnth = invoicedate.length() + l3 + 2;
				sc.nextLine();
				invoiceDate = sc.nextLine().substring(l3, lnth).trim();
				
				if(invoiceDate.length() > 0 && invoiceDate.contains("/"))
				{
					String invDtYear = invoiceDate.trim().substring(invoiceDate.lastIndexOf("/") + 1, invoiceDate.length());
					
					if(invDtYear.length() == 2)
					{
						invDtYear = "20" + invDtYear;
						
						invoiceDate = invoiceDate.substring(0,invoiceDate.lastIndexOf("/")) + "/" + invDtYear;
					}
				
				}
				
			}
			if (line.contains(duedate)) {
				l4 = line.indexOf(duedate);
				lnth = duedate.length() + l4 + 2;
				lnth1 = lnth + 10;
				dueDate = line.substring(lnth, lnth1).trim().replaceAll("[a-zA-Z, ]", "");
			}

			if (line.contains(ttlamt)) {
				l6 = line.indexOf(curr);
				lnth =  curr.length()+l6 + 2;
				totalamount = line.substring(lnth, line.length()).trim();
				if (line.contains(curr)) {
					currency = "USD";
				}
			}
		}
		CommonFunctions obj = new CommonFunctions();
		Date date = obj.formatDateWithoutTime(invoiceDate);
		Calendar c = Calendar.getInstance();
		c.setTime(date); // Now use today date.
		c.add(Calendar.DATE, Integer.parseInt(dueDate)); // Adding days
		dueDate = new SimpleDateFormat("MM/dd/yyyy").format(c.getTime());
		System.out.println(invoicenum + " : " + invoice);
		System.out.println("Vendor : " + customer);
		System.out.println(invoicedate + " : " + invoiceDate);
		System.out.println(duedate + " : " + dueDate);
		System.out.println(curr + " : " + currency);
		System.out.println(ttlamt + " : " + totalamount);

		count = 0;
		InvoiceDetailModel dtl = new InvoiceDetailModel(invoice, customer, invoiceDate, dueDate, currency, totalamount);
		return dtl;
	}
}
