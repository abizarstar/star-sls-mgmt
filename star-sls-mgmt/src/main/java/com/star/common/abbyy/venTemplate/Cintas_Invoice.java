package com.star.common.abbyy.venTemplate;

import java.io.File;
import java.util.Scanner;

public class Cintas_Invoice {
	private static String vendor = "CINTAS";

	public InvoiceDetailModel readData(String fileName) throws Exception {
		File file = new File(fileName);
		Scanner sc = new Scanner(file);

		String line = null;
		int lnth = 0;
		int lnth1 = 0;
		// code to extract data from file

		int count = 0;
		int l1, l2, l3, l4;

		String invoicenumber = "INVOICE # ";
		String InvoiceNumber = " - ";
		String invoicedate = "INVOICE DATE";
		String InvoiceDate = " - ";
		String duedate = "PAYMENT TERMS";
		String DueDate = " - ";
		String invoicetotal = "TOTAL USD";
		String InvoiceTotal = " - ";
		String curr = "USD";
		String Currency = " - ";

		String line1 = null;
		int lnth2 = 0;

		while (sc.hasNext()) {
			count += 1;
			line = sc.nextLine();
			if (line.contains(invoicenumber)) {

				l1 = line.indexOf(invoicenumber);// 206
				lnth = invoicenumber.length() + l1 + 5;// 14+206+5
				InvoiceNumber = line.substring(lnth, line.length()).trim();// substng(201, 225)

							}
			if (line.contains(invoicedate)) {
				l2 = line.indexOf(invoicedate); // 179
				lnth1 = invoicedate.length() + l2 + 2; // 193
				InvoiceDate = line.substring(lnth1, line.length()).trim();
			}
			
			  if (line.contains(curr)) {  Currency =
			  "USD"; }
			
			if (line.contains(duedate)) {
				l3 = line.indexOf(duedate); // 179

				lnth = duedate.length() + l3 + 2;
				lnth1 = lnth + 10;
				String dt = line.substring(lnth, line.length()).trim().replaceAll("[a-z A-Z]", "");
				System.out.println(dt);
				DueDate =DtDay.Dt(InvoiceDate, Integer.parseInt(dt)).toString();
			}
			if (line.contains(invoicetotal)) {
				l4 = line.indexOf(invoicetotal);
				lnth = invoicetotal.length() + l4;
				lnth2 = lnth + 5;
				InvoiceTotal = line.substring(lnth, line.length()).trim();
			}
		}
		System.out.println(invoicenumber + " : " + InvoiceNumber);
		System.out.println(invoicedate + " : " + InvoiceDate);
		System.out.println(duedate + " : " + DueDate);
		 System.out.println("Currency : "+ Currency);
		System.out.println(invoicetotal + " : " + InvoiceTotal);
		InvoiceDetailModel invs= new InvoiceDetailModel();
		invs.setInvoice(InvoiceNumber);
		invs.setInvoiceDate(InvoiceDate);
		invs.setDueDate(DueDate);
		invs.setCurrency(Currency);
		invs.setTotalamount(InvoiceTotal);
		return invs;
	}

	public static String getVendor() {
		return vendor;
	}

	public static void setVendor(String vendor) {
		Cintas_Invoice.vendor = vendor;

	}
}