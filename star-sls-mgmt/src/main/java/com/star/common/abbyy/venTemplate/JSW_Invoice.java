package com.star.common.abbyy.venTemplate;

import java.io.File;
import java.util.Scanner;

public class JSW_Invoice {
	private static String vendor = "JSW Steel USA Ohio, Inc.";

	public InvoiceDetailModel readData(String fileName) throws Exception {
		File file = new File(fileName);
		Scanner sc = new Scanner(file);

		String line = null;
		int lnth = 0;
		int lnth1 = 0;
		// code to extract data from file
		int count = 0;
		int l1, l2, l3, l4;
		String invoicenumber = "INVOICE NO.";
		String InvoiceNumber = " - ";
		String invoicedate = "INVOICE DATE";
		String InvoiceDate = " - ";
		String duedate = "DUE DATE";
		String DueDate = " - ";
		String invoicetotal = "TOTAL";
		String InvoiceTotal = " - ";
		String curr = "USD";
		String Currency = " - ";
		String line1 = null;
		int lnth2 = 0;
		while (sc.hasNext()) {
			count += 1;
			line = sc.nextLine();
			if (line.contains(invoicenumber) && InvoiceNumber == " - ") {
				l1 = line.indexOf(invoicenumber) - 5; // 206
				sc.nextLine();
				line1 = sc.nextLine();
				InvoiceNumber = line1.substring(l1, line1.length()).trim();// substng(201, 225)
			}
			if (line.contains(invoicedate)) {
				l2 = line.indexOf(invoicedate) - 5; // 179
				lnth1 = invoicedate.length() + l2 + 10; // 193
				line1 = sc.nextLine();
				InvoiceDate = line1.substring(l2, lnth1).trim();
			}
			if (line.contains(curr) && Currency == " - ") {
				Currency = "USD";
			}
			if (line.contains(duedate) && DueDate == " - ") {
				l3 = line.indexOf(duedate); // 179
				lnth = duedate.length() + l3 + 2;
				line1 = sc.nextLine();
				DueDate = line1.substring(lnth, line1.length()).trim().replaceAll("[a-z, ]", "");
			}
			if (line.contains(invoicetotal) && InvoiceTotal == " - ") {
				l4 = line.indexOf(invoicetotal);
				lnth = invoicetotal.length() + l4;
				InvoiceTotal = line.substring(lnth, line.length()).trim().replaceAll("[a-z A-Z,]", "");
			}
		}
		System.out.println(invoicenumber + " : " + InvoiceNumber);
		System.out.println(invoicedate + " : " + InvoiceDate);
		System.out.println(duedate + " : " + DueDate);
		System.out.println("Currency : " + Currency);
		System.out.println(invoicetotal + " : " + InvoiceTotal);
		InvoiceDetailModel invs = new InvoiceDetailModel();
		invs.setVendorName(vendor);
		invs.setInvoice(InvoiceNumber);
		invs.setInvoiceDate(InvoiceDate);
		invs.setDueDate(DueDate);
		invs.setCurrency(curr);
		invs.setTotalamount(InvoiceTotal);
		return invs;
	}

	public static String getVendor() {
		return vendor;
	}

	public static void setVendor(String vendor) {
		JSW_Invoice.vendor = vendor;

	}
}