package com.star.common.abbyy.venTemplate;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import com.star.common.CommonFunctions;

public class AKSteelInvoiceDetail implements invoiceRepo {

	private static String invoicenum = "Invoice Number";
	private static String vendor = "AK STEEL CORPORATION";
	private static String invoicedate = "INVOICE DATE";
	private static String duedate = "TERMS";
	private static String curr = "Currency";
	private static String ttlamt = "Invoice Total";
	String invoice = "";
	String customer = "";
	String invoiceDate = "";
	String dueDate = "";
	String currency = "";
	String totalamount = "0.00 ";
	int l1, l2, l3, l4, l5, l6;
	int count = 0;

	public static String getVendor() {
		return vendor;
	}

	public AKSteelInvoiceDetail() {
		// TODO Auto-generated constructor stub
	}

	public InvoiceDetailModel readData(String fileName) throws Exception {
		// TODO Auto-generated method stub
		File file = new File(fileName);
		Scanner sc = new Scanner(file);

		String line = null;
		int lnth = 0;
		// code to extract data from file

		while (sc.hasNext()) {
			count += 1;
			line = sc.nextLine();
			String[] str = line.split(" ");
			int lt = str.length;

			l1 = line.indexOf(invoicenum);
			if (line.contains(invoicenum)) {
				l1 = line.indexOf(invoicenum);
				lnth = invoicenum.length() + l1 + 5;
				sc.nextLine();
				invoice = sc.nextLine().substring(l1 - 5, lnth).trim();
			}
			if (line.contains(invoicedate)) {
				l2 = line.indexOf(invoicedate);
				lnth = invoicedate.length() + l2 + 5;
				String line2 = sc.nextLine();
				invoiceDate = line2.substring(l2, lnth).trim();
			}
			if (line.contains(vendor)) {
				l3 = line.indexOf(vendor);
				lnth = vendor.length() + l3 + 30;
				customer = line.substring(l3, lnth).trim();
			}
			if (line.contains(duedate)) {
				l4 = line.indexOf(duedate);
				lnth = duedate.length() + l4 + 5;
				dueDate = sc.nextLine().substring(l4, lnth).trim().replaceAll("[a-zA-Z, ]", "");
			}
			if (line.contains(ttlamt)) {
				l6 = line.indexOf(ttlamt);
				lnth = ttlamt.length() + l6 + 100;
				int lnth1 = lnth + 45;
				currency = line.substring(lnth, lnth1).trim();
				totalamount = line.substring(lnth1, line.length()).trim();
				if (currency.length() <= 0) {
					lnth += 5;
					lnth1 = lnth + 55;
					currency = line.substring(lnth, lnth1).trim();
					totalamount = line.substring(lnth1, line.length()).trim().replaceAll(",", "");
					System.out.println(totalamount);
				}
				if (totalamount.length() <= 0) {
					lnth1 -= 5;
					lnth1 = lnth + 55;
					currency = line.substring(lnth, lnth1).trim();
					totalamount = line.substring(lnth1, line.length()).trim().replaceAll("[$,]", "");
				}

			}
		}
		CommonFunctions obj = new CommonFunctions();
		Date date = obj.formatDateWithoutTime(invoiceDate);
		Calendar c = Calendar.getInstance();
		c.setTime(date); // Now use today date.
		c.add(Calendar.DATE, Integer.parseInt(dueDate)); // Adding days
		dueDate = new SimpleDateFormat("MM/dd/yyyy").format(c.getTime());
		System.out.println(invoicenum + " : " + invoice);
		System.out.println("Vendor : " + customer);
		System.out.println(invoicedate + " : " + invoiceDate);
		System.out.println(duedate + " : " + dueDate);
		System.out.println(curr + " : " + currency);
		System.out.println(ttlamt + " : " + totalamount);
		count = 0;
		InvoiceDetailModel dtl = new InvoiceDetailModel(invoice, customer, invoiceDate, dueDate, currency, totalamount);
		return dtl;

	}

}
