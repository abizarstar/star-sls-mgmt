package com.star.common.abbyy.venTemplate;

public class InvoiceDetailModel {
	private String invoice = "";
	private String vendorName = "";
	private String invoiceDate = "";
	private String dueDate = "";
	private String currency = "";
	private String totalamount = "";

	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getInvoice() {
		return invoice;
	}
	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(String totalamount) {
		this.totalamount = totalamount;
	}
	public InvoiceDetailModel() {
		// TODO Auto-generated constructor stub
	}
	public InvoiceDetailModel(String invoice, String vendorName, String invoiceDate, String dueDate, String currency,
			String totalamount) {
		super();
		this.invoice = invoice;
		this.vendorName = vendorName;
		this.invoiceDate = invoiceDate;
		this.dueDate = dueDate;
		this.currency = currency;
		this.totalamount = totalamount;
	}
	

}
