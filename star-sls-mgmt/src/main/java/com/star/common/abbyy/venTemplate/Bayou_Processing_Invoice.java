package com.star.common.abbyy.venTemplate;

import java.io.File;
import java.util.Scanner;

public class Bayou_Processing_Invoice {
	private static String vendor = " Bayou Processing and Storage ";

	public InvoiceDetailModel readData(String fileName) throws Exception {
		File file = new File(fileName);
		Scanner sc = new Scanner(file);

		String line = null;
		int lnth = 0;
		int lnth1 = 0;
		// code to extract data from file

		int count = 0;
		int l1, l2, l3, l4;


		String invoicenumber = " Invoice: ";
		String InvoiceNumber = " - ";
		String invoicedate = "Invoice Date";
		String InvoiceDate = " - ";
		String duedate = " Payment Due";
		String DueDate = " - ";
		String invoicetotal = "TOTAL DUE:";
		String InvoiceTotal = " - ";
		String curr = "$";
		String Currency = " - ";

		String line1 = null;
		int lnth2 = 0;


		while (sc.hasNext()) {
			count += 1;
			line = sc.nextLine();
			if (line.contains(invoicenumber)) {

				l1 = line.indexOf(invoicenumber);// 206
				lnth = invoicenumber.length() + l1;// 14+206+5
				InvoiceNumber = line.substring(lnth, line.length()).trim();// substng(201, 225)

			}
			if (line.contains(invoicedate)) {
				l2 = line.indexOf(invoicedate); // 179
				lnth1 = invoicedate.length() + l2 + 2; // 193
				InvoiceDate = line.substring(lnth1, line.length()).trim();
			}

			if (line.contains(curr)) {
				Currency = "USD";
			}

			if (line.contains(duedate)) {
				l3 = line.indexOf(duedate); // 179
				lnth = duedate.length() + l3 + 2;
				DueDate = line.substring(lnth, line.length()).trim().replaceAll("[a-z A-Z]", "");
			}
			if (line.contains(invoicetotal)) {
				l4 = line.indexOf(invoicetotal);
				lnth = invoicetotal.length() + l4;
				InvoiceTotal = line.substring(lnth, line.length()).trim().replaceAll("[$]", "");
			}
		}
		System.out.println(invoicenumber + " : " + InvoiceNumber);
		System.out.println(invoicedate + " : " + InvoiceDate);
		System.out.println(duedate + " : " + DueDate);
		System.out.println("Currency : "+ Currency);
		System.out.println(invoicetotal + " : " + InvoiceTotal);
		
		InvoiceDetailModel invs= new InvoiceDetailModel();
		
		 invs.setInvoice(InvoiceNumber);invs.setVendorName(vendor); invs.setInvoiceDate(InvoiceDate);
		 invs.setDueDate(DueDate); invs.setCurrency(Currency);
		 invs.setTotalamount(InvoiceTotal); return invs;
			}
	public static String getVendor() {
		return vendor;
	}

	public static void setVendor(String vendor) {
		Bayou_Processing_Invoice.vendor = vendor;

	}
}