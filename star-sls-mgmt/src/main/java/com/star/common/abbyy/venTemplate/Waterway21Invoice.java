package com.star.common.abbyy.venTemplate;

import java.io.File;
import java.util.Scanner;

public class Waterway21Invoice implements invoiceRepo {
	int count = 0;
	int l1, l2, l3, l4, l5, l6;
	private static String invoicenum = "Invoice Number";
	private static String vendor = "21 Waterway Holdings ";
	private static String invoicedate = " DATE:";
	private static String duedate = "Due Date";
	private static String curr = "Currency";
	private static String ttlamt = "BALANCE DUE";
	String invoice = "";
	String customer = "";
	String invoiceDate ="";
	String dueDate = "";
	String currency = "";
	String totalamount = "0.00 ";

	public static String getVendor() {
		return vendor;
	}

	public InvoiceDetailModel readData(String fileName) throws Exception {
		File file = new File(fileName);
		Scanner sc = new Scanner(file);

		String line = null;
		int lnth = 0;
		int lnth1 = 0;
		// code to extract data from file

		while (sc.hasNext()) {
			count += 1;
			line = sc.nextLine();
			String[] str = line.split(" ");
			int lt = str.length;
			if (line.contains(invoicenum)) {
				l1 = line.indexOf(invoicenum);
				lnth = invoicenum.length() + l1 + 5;
				sc.nextLine();
				invoice = sc.nextLine().substring(l1 - 5, lnth).trim();
			}
			if (line.contains(vendor)) {
				l2 = line.indexOf(vendor);
				lnth = vendor.length() + l2 + 20;
				customer = line.substring(l2, lnth).trim();
			}
			if (line.contains(invoicedate)) {
				l3 = line.indexOf(invoicedate);
				lnth = invoicedate.length() + l3 + 5;
				String line2 = sc.nextLine();
				lnth1 = lnth + 20;
				invoiceDate = line.substring(lnth, lnth1).trim();
				if (line.contains(duedate)) {
					l4 = line.indexOf(duedate);
					lnth = duedate.length() + l4 + 5;
					dueDate = line2.substring(l4, lnth).trim();
				}
			}
			if (line.contains(curr)) {
				l5 = line.indexOf(curr);
				lnth = curr.length() + l5 + 5;
				lnth1 = lnth + 30;
				currency = line.substring(lnth, lnth1).trim();
			}
			if (line.contains(ttlamt)) {
				l6 = line.indexOf(ttlamt);
				lnth = ttlamt.length() + l6 + 5;
				lnth1 = lnth + 30;
				totalamount = line.substring(lnth, lnth1).trim().replaceAll("[$,]", "");
			}
		}
		System.out.println(invoicenum + " : " + invoice);
		System.out.println("Vendor : " + customer);
		System.out.println(invoicedate + " : " + invoiceDate);
		System.out.println(duedate + " : " + dueDate);
		System.out.println(curr + " : " + currency);
		System.out.println(ttlamt + " : " + totalamount);
		count = 0;
		InvoiceDetailModel dtl= new InvoiceDetailModel(invoice, customer, invoiceDate, dueDate, currency,totalamount);
		return dtl;
			}
}
