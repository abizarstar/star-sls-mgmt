package com.star.common.abbyy.venTemplate;

import java.io.File;
import java.util.Scanner;

public class Donnelley_Invoice {
	private static String vendor = "RR Donnelley Logistics Services Worldwide Inc.";

	public InvoiceDetailModel readData(String fileName) throws Exception {
		File file = new File(fileName);
		Scanner sc = new Scanner(file);

		String line = null;
		int lnth = 0;
		int lnth1 = 0;
		// code to extract data from file
		int count = 0;
		int l1, l2, l3, l4;
		String invoicenumber = "Customer Invoice";
		String InvoiceNumber = " - ";
		String invoicedate = "DATE";
		String InvoiceDate = " - ";
		String duedate = "DAYS";
		String DueDate = " - ";
		String invoicetotal = "TOTAL CHARGE";
		String InvoiceTotal = " - ";
		String curr = "$";
		String Currency = " - ";

		String line1 = null;
		int lnth2 = 0;

		while (sc.hasNext()) {
			count += 1;
			line = sc.nextLine();

			if (line.contains(invoicedate) && InvoiceDate == " - ") {
				l2 = line.indexOf(invoicedate) - 5; // 179
				lnth = l2 - 40; // 193
				line1 = sc.nextLine();
				InvoiceNumber = line1.substring(lnth, l2).trim().replaceAll("[a-z,: A-Z]", "");
				InvoiceDate = line1.substring(l2, line.length()).trim().replaceAll("[a-z,: A-Z]", "");
			}
			if (line.contains(curr) && Currency == " - ") {
				// System.out.print("sjwiocjewncjndwj");
				Currency = "USD";
			}

			if (line.contains(duedate)) {
				l3 = line.indexOf(duedate); // 179
				lnth = duedate.length() + l3 - 10;
				String dt = line.substring(lnth, l3).trim().replaceAll("[a-z, A-Z]", "");
				System.out.println("Due Date is " + DueDate);
				DueDate = DtDay.Dt(InvoiceDate, Integer.parseInt(dt)).toString();

			}
			// System.out.println(line.contains(curr));
			if (line.contains(invoicetotal) && InvoiceTotal == " - ") {
				l4 = line.indexOf(invoicetotal);
				lnth = invoicetotal.length() + l4;
				lnth1 = lnth + 10;
				InvoiceTotal = line.substring(lnth, line.length()).trim().replaceAll("[a-z ,:A-Z$]", "");
			}
		}
		System.out.println(invoicenumber + " : " + InvoiceNumber);
		System.out.println(invoicedate + " : " + InvoiceDate);
		System.out.println(duedate + " : " + DueDate);
		System.out.println("Currency : " + Currency);
		System.out.println(invoicetotal + " : " + InvoiceTotal);

		InvoiceDetailModel invs = new InvoiceDetailModel();
		invs.setVendorName(vendor);
		invs.setInvoice(InvoiceNumber);
		invs.setInvoiceDate(InvoiceDate);
		invs.setDueDate(DueDate);
		invs.setCurrency(Currency);
		invs.setTotalamount(InvoiceTotal);
		return invs;

	}

	public static String getVendor() {
		return vendor;
	}

	public static void setVendor(String vendor) {
		Donnelley_Invoice.vendor = vendor;

	}
}