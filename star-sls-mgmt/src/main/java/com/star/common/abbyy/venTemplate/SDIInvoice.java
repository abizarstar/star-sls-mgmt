package com.star.common.abbyy.venTemplate;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import com.star.common.CommonFunctions;

public class SDIInvoice {
	int count = 0;
	int l1, l2, l3, l4, l5, l6;
	private static String invoicenum = "Invoice No";
	private static String vendor = "Steel Dynamics, Inc";
	private static String invoicedate = "Invoice Date";
	private static String duedate = "TERMS";
	private static String curr = "AMOUNT";
	private static String ttlamt = "INVOICE TOTAL";
	String invoice = "";
	String customer = "";
	String invoiceDate = "";
	String dueDate = "";
	String currency = "";
	String totalamount = "0.00 ";

	public static String getVendor() {
		return vendor;
	}

	public InvoiceDetailModel readData(String fileName) throws Exception {
		File file = new File(fileName);
		Scanner sc = new Scanner(file);

		String line = null;
		int lnth = 0;
		int lnth1 = 0;
		// code to extract data from file

		while (sc.hasNext()) {
			count += 1;
			line = sc.nextLine();

			if (line.contains(invoicedate)) {
				l3 = line.indexOf(invoicedate) + invoicedate.length() + 2;
				lnth = l3 + 70;
				sc.nextLine();
				invoiceDate = line.substring(l3, lnth).trim();
				if (line.contains(invoicenum)) {
					l1 = line.indexOf(invoicenum) + invoicenum.length();
					lnth = invoicenum.length() + l1 + 5;
					invoice = line.substring(lnth, line.length()).trim();
				}
			}
			if (line.contains(vendor)) {
				l2 = line.indexOf(vendor);
				lnth = vendor.length() + l2 + 30;
				customer = line.substring(l2, lnth).trim();
			}

			if (line.contains(duedate)) {
				l4 = line.indexOf(duedate);
				lnth = l4 + 2;
				lnth1 = lnth + 10;
				dueDate = sc.nextLine().substring(lnth, lnth1).trim().replaceAll("[a-zA-Z, ]", "");
			}
			if (line.contains(curr)) {
				l5 = line.indexOf(curr);
				lnth = curr.length() + l5;
				lnth1 = lnth + 5;
				currency = line.substring(lnth, lnth1).trim().replaceAll("[()]", "");
			}
			if (line.contains(ttlamt)) {
				l6 = line.indexOf(ttlamt);
				lnth = ttlamt.length() + l6 + 3;
				lnth1 = lnth + l6;
				totalamount = line.substring(lnth, line.length()).trim().replaceAll("[$,]", "");
			}
		}
		CommonFunctions obj = new CommonFunctions();
		Date date = obj.formatDateWithoutTime(invoiceDate);
		Calendar c = Calendar.getInstance();
		c.setTime(date); // Now use today date.
		c.add(Calendar.DATE, Integer.parseInt(dueDate)); // Adding days
		dueDate = new SimpleDateFormat("MM/dd/yyyy").format(c.getTime());
		System.out.println(invoicenum + " : " + invoice);
		System.out.println("Vendor : " + customer);
		System.out.println(invoicedate + " : " + invoiceDate);
		System.out.println(duedate + " : " + dueDate);
		System.out.println(curr + " : " + currency);
		System.out.println(ttlamt + " : " + totalamount);

		count = 0;
		InvoiceDetailModel dtl = new InvoiceDetailModel(invoice, customer, invoiceDate, dueDate, currency, totalamount);
		return dtl;
	}
}
