package com.star.common.abbyy.venTemplate;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class BigRiver_Invoice {
	private static String vendor = "BIG RIVER STEEL";

	public InvoiceDetailModel readData(String fileName) throws Exception {
		File file = new File(fileName);
		Scanner sc = new Scanner(file);

		String line = null;
		int lnth = 0;
		int lnth1 = 0;
		// code to extract data from file

		int count = 0;
		int l1, l2, l3, l4;

		String invoicenumber = "INVOICE NO.";
		String InvoiceNumber = " - ";
		String invoicedate = "INVOICE/SHIP DATE";
		String InvoiceDate = " - ";
		String curr = "$";
		String Currency = " - ";
		String dueDate = "Payment Term";
		String DueDate = " - ";
		String invoicetotal = "INVOICE TOTAL";
		String InvoiceTotal = " - ";

		int lnth2 = 0;

		while (sc.hasNext()) {
			count += 1;
			line = sc.nextLine();

			if (line.contains(invoicenumber)) {

				l1 = line.indexOf(invoicenumber);// 206
				lnth = invoicenumber.length() + l1 + 5;// 14+206+5
				String line1 = sc.nextLine();
				String line11 = sc.nextLine();
				InvoiceNumber = line11.substring(l1, lnth).trim();// substng(201, 225)

				if (line.contains(invoicedate)) {

					l2 = line.indexOf(invoicedate); // 179
					lnth1 = invoicedate.length() + l2 + 4; // 193
					InvoiceDate = line11.substring(l2, lnth1).trim();

						}
			}

			if (line.contains(dueDate)) {
				l4 = line.indexOf(dueDate); // 203
				lnth = dueDate.length() + l4+2; // 13+203 = 216
				lnth2 = lnth + l4;
				String dt = line.substring(lnth, lnth2).trim().replaceAll("[a-z A-Z]", "");
				DueDate =DtDay.Dt(InvoiceDate, Integer.parseInt(dt)).toString();
				
			}
			if (line.contains(curr)) {
				Currency = "USD";
			}
			if (line.contains(invoicetotal)) {
				l4 = line.indexOf(invoicetotal); // 203
				lnth = invoicetotal.length() + l4; // 13+203 = 216
				lnth2 = lnth + 51;
				InvoiceTotal = line.substring(lnth, lnth2).trim().replaceAll("[$ ,]", "");
			}
			

		}
		System.out.println(invoicenumber + " : " + InvoiceNumber);
		System.out.println(invoicedate + " : " + InvoiceDate);
		System.out.println(dueDate + " : " + DueDate);
		System.out.println("Currency : " + Currency);
		System.out.println(invoicetotal + " : " + InvoiceTotal);
		InvoiceDetailModel invs= new InvoiceDetailModel();
		invs.setInvoice(InvoiceNumber);
		invs.setVendorName(vendor);
		invs.setInvoiceDate(InvoiceDate);
		invs.setDueDate(DueDate);
		invs.setCurrency(Currency);
		invs.setTotalamount(InvoiceTotal);
		invs.setVendorName(vendor);
		return invs;

	}

	public static String getVendor() {
		return vendor;
	}

	public static void setVendor(String vendor) {
		BigRiver_Invoice.vendor = vendor;

	}
}