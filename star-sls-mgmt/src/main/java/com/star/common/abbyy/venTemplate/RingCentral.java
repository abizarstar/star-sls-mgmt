package com.star.common.abbyy.venTemplate;

import java.io.File;
import java.util.Scanner;

public class RingCentral implements invoiceRepo {
	int count = 0;
	int l1, l2, l3, l4, l5, l6;
	private static String invoicenum = "Invoice No.";
	private static String vendor = "RingCentral";
	private static String invoicedate = "Invoice Date";
	private static String duedate = "Due Date";
	private static String curr = "Currency";
	private static String ttlamt = "Invoice Amount to Pay";
	String invoice = "";
	String customer = "";
	String invoiceDate ="";
	String dueDate = "";
	String currency = "";
	String totalamount = "0.00 ";

	public static String getVendor() {
		return vendor;
	}

	public InvoiceDetailModel readData(String fileName) throws Exception {
		File file = new File(fileName);
		Scanner sc = new Scanner(file);

		String line = null;
		int lnth = 0;
		int lnth1 = 0;
		// code to extract data from file

		while (sc.hasNext()) {
			count += 1;
			line = sc.nextLine();
			
			if (line.contains(invoicenum)) {
				l1 = line.indexOf(invoicenum);
				lnth = invoicenum.length() + l1 + 2;
				lnth1 = lnth + 15;
				invoice = line.substring(lnth, lnth1).trim();
			}
			if (line.contains(vendor)) {
				l2 = line.indexOf(vendor);
				lnth = vendor.length() + l2 + 30;
				customer = line.substring(l2, lnth).trim();
			}
			if (line.contains(invoicedate)) {
				l3 = line.indexOf(invoicedate);
				lnth = invoicedate.length() + l3 + 2;
				lnth1 = lnth + l3;
				invoiceDate = line.substring(lnth, lnth1).trim();
			}
			if (line.contains(duedate)) {
				l4 = line.indexOf(duedate);
				lnth = duedate.length() + l4 + 2;
				lnth1 = lnth + l4;
				dueDate = line.substring(lnth, lnth1).trim();
			}
			if (line.contains(curr)) {
				l5 = line.indexOf(curr);
				lnth = curr.length() + l5 + 2;
				lnth1 = lnth + 15;
				currency = line.substring(lnth, lnth1).trim();
			}
			if (line.contains(ttlamt)) {
				l6 = line.indexOf(ttlamt);
				lnth = ttlamt.length() + l6 + 2;
				lnth1 = lnth + l6;
				totalamount = line.substring(lnth, lnth1).trim().replaceAll("[$,]", "");
			}
		}
		System.out.println(invoicenum + " : " + invoice);
		System.out.println("Vendor : " + customer);
		System.out.println(invoicedate + " : " + invoiceDate);
		System.out.println(duedate + " : " + dueDate);
		System.out.println(curr + " : " + currency);
		System.out.println(ttlamt + " : " + totalamount);

		count = 0;
		InvoiceDetailModel dtl= new InvoiceDetailModel(invoice, customer, invoiceDate, dueDate, currency,totalamount);
		return dtl;
			}

}
