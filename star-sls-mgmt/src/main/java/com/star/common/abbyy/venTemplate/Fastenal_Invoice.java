package com.star.common.abbyy.venTemplate;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Fastenal_Invoice {
	private static String vendor = "Fastenal Company";

	public InvoiceDetailModel readData(String fileName) throws Exception {
		File file = new File(fileName);
		Scanner sc = new Scanner(file);

		String line = null;
		int lnth = 0;
		int lnth1 = 0;
		// code to extract data from file

		int count = 0;
		int l1, l2, l3, l4;

		String invoicenumber = "Invoice No.";
		String InvoiceNumber = " - ";
		String invoicedate = "Invoice Date";
		String InvoiceDate = " - ";
		String duedate = "Due Date";
		String DueDate = " - ";
		String invoicetotal = "Invoice Total";
		String InvoiceTotal = " - ";
		String curr = "USD";
		String Currency = " - ";

		String line1 = null;
		int lnth2 = 0;

		while (sc.hasNext()) {
			count += 1;
			line = sc.nextLine();
			if (line.contains(invoicenumber)) {

				l1 = line.indexOf(invoicenumber);// 206
				lnth = invoicenumber.length() + l1 + 5;// 14+206+5
				line1 = sc.nextLine();
				InvoiceNumber = line1.substring(l1 - 5, lnth).trim();// substng(201, 225)

				if (line.contains(invoicedate)) {

					l2 = line.indexOf(invoicedate); // 179
					lnth1 = invoicedate.length() + l2 + 2; // 193
					InvoiceDate = line1.substring(l2, lnth1).trim();

				}
			}
			/*
			 * if (line.contains(curr)) { System.out.print("sjwiocjewncjndwj"); Currency =
			 * "USD"; }
			 */

			if (line.contains(duedate)) {
				l3 = line.indexOf(duedate); // 179

				lnth = duedate.length() + l3 + 2;
				lnth1 = lnth + 10;
				String line2 = sc.nextLine();
				String line3 = sc.nextLine();
				DueDate = line3.substring(l3, lnth1).trim();
			}
			// System.out.println(line.contains(curr));

			if (line.contains(invoicetotal)) {
				l4 = line.indexOf(invoicetotal);
				lnth = invoicetotal.length() + l4;
				lnth2 = lnth + 5;
				String line4 = sc.nextLine();
				InvoiceTotal = sc.nextLine().substring(l4, lnth2).trim().replaceAll("[a-z A-Z]", "");
			}

		}
		System.out.println(invoicenumber + " : " + InvoiceNumber);
		System.out.println(invoicedate + " : " + InvoiceDate);
		System.out.println(duedate + " : " + DueDate);
		// System.out.println("Currency : "+ Currency);
		System.out.println(invoicetotal + " : " + InvoiceTotal);
		InvoiceDetailModel invs = new InvoiceDetailModel();
		invs.setInvoice(InvoiceNumber);
		invs.setVendorName(vendor);
		invs.setInvoiceDate(InvoiceDate);
		invs.setDueDate(DueDate);
		invs.setCurrency(curr);
		invs.setTotalamount(InvoiceTotal);
		invs.setVendorName(vendor);
		return invs;
	}

	public static String getVendor() {
		return vendor;
	}

	public static void setVendor(String vendor) {
		Fastenal_Invoice.vendor = vendor;

	}
}