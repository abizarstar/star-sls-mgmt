package com.star.common.abbyy.venTemplate;

import java.nio.file.Files;
import java.nio.file.Paths;

public class Vendor {
	String data;

	public InvoiceDetailModel getVendor(String str) throws Exception {
		// TODO Auto-generated constructor stub
		this.data = str;
		String input = new String(Files.readAllBytes(Paths.get(data)));
		if (input.contains(PascoInvoiceDetail.getVendor())) {
			PascoInvoiceDetail vendor = new PascoInvoiceDetail();
			return vendor.readData(data);
		}
		if (input.contains(AKSteelInvoiceDetail.getVendor())) {
			AKSteelInvoiceDetail vendor = new AKSteelInvoiceDetail();
			return vendor.readData(data);
		}
		if (input.contains(Waterway21Invoice.getVendor())) {
			Waterway21Invoice vendor = new Waterway21Invoice();
			return vendor.readData(data);
		}
		if (input.contains(RingCentral.getVendor())) {
			RingCentral vendor = new RingCentral();
			return vendor.readData(data);
		}
		if (input.contains(ADSInvoice.getVendor())) {
			ADSInvoice vendor = new ADSInvoice();
			return vendor.readData(data);
		}
		if (input.contains(Amazon.getVendor())) {
			Amazon vendor = new Amazon();
			return vendor.readData(data);
		}
		if (input.contains(SDIInvoice.getVendor())) {
			SDIInvoice vendor = new SDIInvoice();
			return vendor.readData(data);
		}
		if (input.contains(WasteManagementInvoice.getVendor())) {
			WasteManagementInvoice vendor = new WasteManagementInvoice();
			return vendor.readData(data);
		}
		if (input.contains(BigRiver_Invoice.getVendor())) {
			BigRiver_Invoice vendor = new BigRiver_Invoice();
			return vendor.readData(data);
		}
		if (input.contains(Fastenal_Invoice.getVendor())) {
			Fastenal_Invoice vendor = new Fastenal_Invoice();
			return vendor.readData(data);
		}
		if (input.contains(Cintas_Invoice.getVendor())) {
			Cintas_Invoice vendor = new Cintas_Invoice();
			return vendor.readData(data);
		}
		if (input.contains(Bayou_Processing_Invoice.getVendor())) {
			Bayou_Processing_Invoice vendor = new Bayou_Processing_Invoice();
			return vendor.readData(data);
		}
		if (input.contains(Grainger_Invoice.getVendor())) {
			Grainger_Invoice vendor = new Grainger_Invoice();
			return vendor.readData(data);
		} if (input.contains(JSW_Invoice.getVendor())) {
			JSW_Invoice vendor = new JSW_Invoice();
			return vendor.readData(data);
		}if (input.contains(Leeco_Invoice.getVendor())) {
			Leeco_Invoice vendor = new Leeco_Invoice();
			return vendor.readData(data);
		} if (input.contains(Vulcan_Invoice.getVendor())) {
			Vulcan_Invoice vendor = new Vulcan_Invoice();
			return vendor.readData(data);
		}if (input.contains(Donnelley_Invoice.getVendor())) {
			Donnelley_Invoice vendor = new Donnelley_Invoice();
			return vendor.readData(data);
		}else {
			System.out.println("Vendor Not registered");
			return null;
		}
	}
}
