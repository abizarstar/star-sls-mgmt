package com.star.common.abbyy.venTemplate;

import java.io.File;
import java.util.Scanner;

public class WasteManagementInvoice implements invoiceRepo {
	int count = 0;
	int l1, l2, l3, l4, l5, l6;
	private static String invoicenum = "Invoice Number";
	private static String vendor = "WASTE MANAGEMENT";
	private static String invoicedate = "Invoice Date";
	private static String duedate = "Payment Terms";
	private static String curr = "$";
	private static String ttlamt = "Total Due";
	String invoice = "";
	String customer = "";
	String invoiceDate = "";
	String dueDate = "";
	String currency = "USD";
	String totalamount = "0.00 ";

	public static String getVendor() {
		return vendor;
	}

	public WasteManagementInvoice() {
		// TODO Auto-generated constructor stub
	}

	public InvoiceDetailModel readData(String fileName) throws Exception {
		File file = new File(fileName);
		Scanner sc = new Scanner(file);

		String line = null;
		int lnth = 0;
		int lnth1 = 0;
		// code to extract data from file

		while (sc.hasNext()) {
			count += 1;
			line = sc.nextLine();

			if (line.contains(invoicenum)) {
				l1 = line.indexOf(invoicenum);
				lnth = invoicenum.length() + l1 + 5;
				sc.nextLine();
				String line2 = sc.nextLine();
				invoice = line2.substring(l1 - 5, lnth).trim();
				if (line.contains(invoicedate)) {
					l3 = line.indexOf(invoicedate);
					lnth = invoicedate.length() + l3 + 5;
					invoiceDate = line2.substring(l3, lnth).trim();
				}
			}
			if (line.contains(vendor)) {
				l2 = line.indexOf(vendor);
				lnth = vendor.length() + l2 + 30;
				customer = line.substring(l2, lnth).trim();
			}

			if (line.contains(duedate)) {
				l4 = line.indexOf(duedate);
				lnth = duedate.length() + l4 + 5;
				String line2 = sc.nextLine();
				dueDate = line2.substring(l4 - 7, lnth).trim();
				if (line.contains(ttlamt)) {
					l6 = line.indexOf(ttlamt);
					lnth = ttlamt.length() + l6;
					lnth1 = lnth + 50;
					totalamount = line2.substring(l6, lnth).trim().replaceAll("[$,]", "");
				}
				if (!line.contains(curr)) {
					currency = "USD";
				}
			}

		}
		System.out.println(invoicenum + " : " + invoice);
		System.out.println("Vendor : " + customer);
		System.out.println(invoicedate + " : " + invoiceDate);
		System.out.println(duedate + " : " + dueDate);
		System.out.println(curr + " : " + currency);
		System.out.println(ttlamt + " : " + totalamount);
		count = 0;
		InvoiceDetailModel dtl = new InvoiceDetailModel(invoice, customer, invoiceDate, dueDate, currency, totalamount);
		return dtl;

	}
}
