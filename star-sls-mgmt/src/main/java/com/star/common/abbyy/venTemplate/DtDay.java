package com.star.common.abbyy.venTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DtDay {

	public static String Dt(String dt, int day) {
		String output = "";
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Calendar c = Calendar.getInstance();
		try {
			Date date1 = sdf.parse(dt);
			c.setTime(date1);

			c.add(Calendar.DATE, day);
			// Adding 5 days

			output = sdf.format(c.getTime());

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return output;
	}

}
