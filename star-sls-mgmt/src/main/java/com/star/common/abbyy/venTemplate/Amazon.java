package com.star.common.abbyy.venTemplate;

import java.io.File;
import java.util.Scanner;

import com.star.common.CommonFunctions;

public class Amazon {
	int count = 0;
	int l1, l2, l3, l4, l5, l6;
	private static String invoicenum = "Invoice # ";
	private static String vendor = "Amazon Capital Services, Inc";
	private static String invoicedate = "Invoice";
	private static String duedate = "Payment due by";
	private static String curr = "$";
	private static String ttlamt = "Amount due ";
	String invoice = "";
	String customer = "";
	String invoiceDate ="";
	String dueDate = "";
	String currency = "";
	String totalamount = "0.00 ";

	public static String getVendor() {
		return vendor;
	}

	public InvoiceDetailModel readData(String fileName) throws Exception {
		File file = new File(fileName);
		Scanner sc = new Scanner(file);

		String line = null;
		int lnth = 0;
		int lnth1 = 0;
		// code to extract data from file

		while (sc.hasNext()) {
			count += 1;
			line = sc.nextLine();

			if (line.contains(invoicenum) && invoiceDate.length()<2) {
				l1 = line.indexOf(invoicenum)+invoicenum.length();
				lnth = invoicenum.length() + l1 +5;
				invoice = line.substring(l1, lnth).trim();
				invoiceDate = line.substring(lnth+5, line.length()).trim().replaceAll(",", "");
				
						}
			if (line.contains(vendor)) {
				l2 = line.indexOf(vendor);
				lnth = vendor.length() + l2 + 15;
				customer = line.substring(l2, lnth).trim();
			}
			if (line.contains(duedate)) {
				l4 = line.indexOf(duedate);
				lnth = duedate.length() + l4 ;
				lnth1 = lnth + 20;
				dueDate = line.substring(lnth, lnth1).trim().replaceAll(",", "");
			}

			if (line.contains(ttlamt) && line.contains(curr)) {
				l6 = line.indexOf(ttlamt);
				lnth = ttlamt.length() + l6 ;
				totalamount = line.substring(lnth, line.length()).trim().replaceAll("[$,a-z]", "");;
				lnth = lnth1 + 5;
				currency = "USD";
		
			}
		}
		
		CommonFunctions obj = new CommonFunctions();
 invoiceDate = obj.formatDateWithoutTimeVouc(invoiceDate).toString();
 dueDate= obj.formatDateWithoutTimeVouc(dueDate).toString();
		System.out.println(invoicenum + " : " + invoice);
		System.out.println("Vendor : " + customer);
		System.out.println(invoicedate + " : " + invoiceDate);
		System.out.println(duedate + " : " + dueDate);
		System.out.println(curr + " : " + currency);
		System.out.println(ttlamt + " : " + totalamount);

		count = 0;
		InvoiceDetailModel dtl= new InvoiceDetailModel(invoice, customer, invoiceDate, dueDate, currency,totalamount);
		return dtl;
			}
}
