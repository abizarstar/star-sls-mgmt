package com.star.common.abbyy.venTemplate;

import java.io.File;
import java.util.Scanner;

public class Vulcan_Invoice {
	private static String vendor = "Vulcan Threaded Products, Inc.";

	public InvoiceDetailModel readData(String fileName) throws Exception {
		File file = new File(fileName);
		Scanner sc = new Scanner(file);

		String line = null;
		int lnth = 0;
		int lnth1 = 0;
		// code to extract data from file
		int count = 0;
		int l1, l2, l3, l4;
		String invoicenumber = "Customer Invoice";
		String InvoiceNumber = " - ";
		String invoicedate = "Invoice Date";
		String InvoiceDate = " - ";
		String duedate = "Due Date";
		String DueDate = " - ";
		String invoicetotal = "Total";
		String InvoiceTotal = " - ";
		String curr = "$";
		String Currency = " - ";

		String line1 = null;
		int lnth2 = 0;

		while (sc.hasNext()) {
			count += 1;
			line = sc.nextLine();
			if (line.contains(invoicenumber) && InvoiceNumber == " - ") {
				l1 = line.indexOf(invoicenumber);// 206
				lnth = invoicenumber.length() + l1 + 15;
				sc.nextLine();
				line1 = sc.nextLine();// 14+206+5
				InvoiceNumber = line1.substring(l1, line1.length()).trim();// substng(201, 225)
				if (InvoiceNumber.contains("0") == false) {
					String line2 = sc.nextLine();// 14+206+5
					InvoiceNumber = line2.substring(l1, line2.length()).trim();// substng(201, 225)
				}
			}
			if (line.contains(invoicedate) && InvoiceDate == " - ") {
				l2 = line.indexOf(invoicedate) - 5; // 179
				lnth = invoicedate.length() + l2 + 2; // 193
				lnth1 = lnth + 25;
				InvoiceDate = line.substring(lnth, lnth1).trim().replaceAll("[a-z,: A-Z]", "");
			}

			if (line.contains(curr) && Currency == " - ") {
				// System.out.print("sjwiocjewncjndwj");
				Currency = "USD";
			}

			if (line.contains(duedate)) {
				l3 = line.indexOf(duedate); // 179
				lnth = duedate.length() + l3 + 2;
				lnth1 = lnth + 80;
				DueDate = line.substring(lnth, lnth1).trim().replaceAll("[a-z, ]", "");
				System.out.println("Due Date is " + DueDate);
			}
			// System.out.println(line.contains(curr));
			if (line.contains(invoicetotal) && InvoiceTotal == " - ") {
				l4 = line.indexOf(invoicetotal);
				lnth = invoicetotal.length() + l4;
				lnth1 = lnth + 10;
				InvoiceTotal = line.substring(lnth, line.length()).trim().replaceAll("[a-z ,:A-Z$]", "");
			}
		}
		System.out.println(invoicenumber + " : " + InvoiceNumber);
		System.out.println(invoicedate + " : " + InvoiceDate);
		System.out.println(duedate + " : " + DueDate);
		System.out.println("Currency : " + Currency);
		System.out.println(invoicetotal + " : " + InvoiceTotal);

		InvoiceDetailModel invs = new InvoiceDetailModel();
		invs.setVendorName(vendor);
		invs.setInvoice(InvoiceNumber);
		invs.setInvoiceDate(InvoiceDate);
		invs.setDueDate(DueDate);
		invs.setCurrency(Currency);
		invs.setTotalamount(InvoiceTotal);
		return invs;

	}

	public static String getVendor() {
		return vendor;
	}

	public static void setVendor(String vendor) {
		Vulcan_Invoice.vendor = vendor;

	}
}