package com.star.common;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.star.linkage.other.MailInfoBrowseOutput;
/*import com.star.dao.DocInfoDAO;
import com.star.dao.DocumentFolderDAO;
import com.star.linkage.docinfo.DocInfoManOutput;
import com.star.linkage.docinfo.DocInfoMergeInput;*/
import com.star.linkage.other.MailInfoInput;

public class SendEmail {

	private static Logger logger = Logger.getLogger(SendEmail.class.getName());
	
	 static final String FROM = "info@starsoftware.co";
	    static final String FROMNAME = "STAR-ERP";
		
	    // Replace recipient@example.com with a "To" address. If your account 
	    // is still in the sandbox, this address must be verified.
	  //  static final String TO = "abizarh@starsoftware.co";
	    
	    // Replace smtp_username with your Amazon SES SMTP user name.
	    static final String SMTP_USERNAME = "AKIAJWGWDYRBUB2H73VQ";
	    
	    // Replace smtp_password with your Amazon SES SMTP password.
	    static final String SMTP_PASSWORD = "ApPN2T9bn/ArbZERXj7gE45SsNU5QSEZtAf3VRfVOvx+";
	    
	    // The name of the Configuration Set to use for this message.
	    // If you comment out or remove this variable, you will also need to
	    // comment out or remove the header on line 65.
	    /*static final String CONFIGSET = "ConfigSet";*/
	    
	    // Amazon SES SMTP host name. This example uses the US West (Oregon) Region.
	    static final String HOST = "email-smtp.us-east-1.amazonaws.com";
	    
	    // The port you will connect to on the Amazon SES SMTP endpoint. 
	    static final int PORT = 465;
	    
	    String BODY="";
	   
	//public void sendEmail1(String modPfx, String modNo,String emailId) 
	public Response sendEmail(MailInfoInput mailDetails) throws MessagingException
	{
		Transport transport = null;
		
		try
        {
			BODY = mailDetails.getEmlBody();
			
			// Create a Properties object to contain connection configuration information.
	    	Properties props = System.getProperties();
	    	props.put("mail.transport.protocol", "smtp");
	    	props.put("mail.smtp.port", PORT); 
	    	props.put("mail.smtp.ssl.enable", "true");
	    	props.put("mail.smtp.auth", "true");
	    	props.put("mail.smtp.starttls.enable", "true");
	    	
	    	Session session = Session.getDefaultInstance(props);
	       
	    	transport = session.getTransport();
	    	
    		Multipart multipart = new MimeMultipart(); 	    	
	    	
	        BodyPart htmlBodyPart = new MimeBodyPart(); //4
	        htmlBodyPart.setContent(BODY, "text/html"); //5
	        multipart.addBodyPart(htmlBodyPart);
    		
	        // Create a message with the specified information. 
	        MimeMessage msg = new MimeMessage(session);
	        msg.setFrom(new InternetAddress(FROM, FROMNAME));
	        String[] recipientList = mailDetails.getEmlTo().split(",");
	        InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
	        int counter = 0;
	        for (String recipient : recipientList) {
	            recipientAddress[counter] = new InternetAddress(recipient.trim());
	            counter++;
	        }
	        
	        msg.setFrom(new InternetAddress(mailDetails.getEmlFrm(), mailDetails.getEmlFrmUsrNm()));
	        msg.setRecipients(Message.RecipientType.TO, recipientAddress);
	        
	        if( mailDetails.getEmlCc()!= null && !mailDetails.getEmlCc().equals("") )
	        {
	        	msg.addRecipients(Message.RecipientType.CC, mailDetails.getEmlCc());
	        }
	        msg.setSubject(mailDetails.getEmlSub());
	        msg.setContent(multipart);
	        
	        // Add a configuration set header. Comment or delete the 
	        // next line if you are not using a configuration set
	        /* msg.setHeader("X-SES-CONFIGURATION-SET", CONFIGSET);*/
                
	        // Send the message.

            // Connect to Amazon SES using the SMTP username and password you specified above.
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
        	
            // Send the email.
            transport.sendMessage(msg, msg.getAllRecipients());
            //System.out.println("Email sent!");
        }
        catch (Exception ex) {
        	
        	logger.log(Level.WARNING, ex.getMessage(), ex);
        }
        finally
        {
            // Close and terminate the connection.
            transport.close();
        }
		
        BrowseResponse<MailInfoBrowseOutput> starBrowseResponse = new BrowseResponse<MailInfoBrowseOutput>();
        
        starBrowseResponse.setOutput(new MailInfoBrowseOutput());
        
        StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
        
        return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
	}
	
	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<MailInfoBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<MailInfoBrowseOutput>>(starBrowseResponse) {
		};
	}
	
}