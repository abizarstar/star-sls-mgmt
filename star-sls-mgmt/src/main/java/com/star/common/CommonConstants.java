package com.star.common;

public class CommonConstants {

	public static String ROOT_DIR = "";
	public static String DB_TYP = "";
	public static final String BAI_DIR = "bai";
	public static final String ELEC_DIR = "elec-pay";
	public static final int DB_OUT_REC = 50;
	public static final String DRV_SUFFIX = "/";
	public static final String CLIENT_ID = "135558245086-14tjdi1gatcpo352v9kjveb4sl1f7np3.apps.googleusercontent.com";
	public static final String CLIENT_SECRET = "UxBcOKMBozP8JSU83GMiRHpf";
	public static final String APPLICATION_NAME = "STAR FINANCE";
	/*
	 * public static final String ROOT_DIR =
	 * "/var/lib/tomcat8/webapps/ROOT/doc-mgmt/doc-fol/";
	 */
	
	public static final String SOMETHING_WENT_WRONG = "Something went wrong, please try again later";
	public static final String SERVER_ERR = "Server error, please check the logs for more details";
	public static final String DIR_ERR = "Unable to create the Directory, check server log for more details";
	public static final String FILE_ERR = "Unable to rename the File, check the logs for more details";
	public static final String FILE_NOT_FOUND_FILE = "Not_Found.pdf";
	public static final String ACCT_ERR = "Account already available for this Vendor";
	public static final String USR_NA = "User not available in the System";
	public static final String USR_DIS = "User has been disabled in the System, Please contact Admin.";
	public static final String USR_ERR = "User already exists with same ID";
	public static final String EXEC_USR_ERR = "EXEC User not configured";
	public static final String PASS_ERR = "Invalid User Name/Password";
	public static final String REQ_ERR = "Request already exists with same ID";
	public static final String ERP_CON_FAILED = "ERP Connection failed, Please validate your configuration";
	public static final String PATH_ERR = "Path not found for the Document Type, please check with Admin for more details ";
	public static final String PAY_MTHD_ERR = "Payment Method not available for Vendor - ";
	public static final String NO_SFTP_ERR = "SFTP setup not completed, please complete it to next step.";
	public static final String FTP_USR_ERR = "Username/Password Incorrect, FTP login failed";
	public static final String FTP_UPLD_DIR = "/u/ap10203/";
	public static final String CHK_LOT_ERR = "Unable to process the Request, Check LOT information not available, Please create and additional LOT and try again";
	public static final String TYP_CODE_ERR = "GL Combination already exists for the BAI code";
	public static final String GL_COMB_ERR = "GL Combination already exists";
	public static final String VCHR_ERR = "Error while creating Voucher, Contact Administrator for more details";
	public static final String OLD_PASS_ERR = "The old password you have entered is incorrect";
	public static final String AUTH_TOKN_VALD = "Authentication Token is not Correct <br> Please try again";
	
	public static final int TOTAL_FLD_COUNT = 20;
	public static String AUTH_TOKEN = "";

	/* TYPE CODE HEADER VALUES */
	public static final String OPEN_BAL = "010";
	public static final String CLOSE_BAL = "015";
	public static final String AVLBL_CLOSE_BAL = "045";
	public static final String TOT_CRED_TRN = "100";
	public static final String TOT_DEBIT_TRN = "400";

	/* DEPOSIT CODES */
	public static final String ZBA_CREDIT = "275";
	public static final String LOCKBOX_DEPOSIT = "115";
	public static final String PRE_AUTH_ACH_CREDIT = "165";
	public static final String INCOMING_MONEY_TRFR = "195";

	/* DEBIT CODES */
	public static final String CHECK_PAID = "475";
	public static final String PRE_AUTH_ACH_DEBIT = "455";
	public static final String OUTGOING_MONEY_TRFR = "495";
	public static final String ZBA_DEBIT = "575";
	public static final String DISBURSING_DEBIT = "581";
	public static final String ACCOUNT_FEE = "661";

	/* PAIN-----> 001 CODES */
	/* GROUP HEADER */

	public static final String PAY_HDR = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
			+ "<Document xmlns=\"urn:iso:std:iso:20022:tech:xsd:pain.001.001.03\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
			+ "   <CstmrCdtTrfInitn>";
	public static final String PAY_END = "</CstmrCdtTrfInitn>\n" + "</Document>";

	public static final String GRP_HDR_STRT = "<GrpHdr>";
	public static final String GRP_HDR_END = "</GrpHdr>";
	public static final String MSG_ID_STRT = "<MsgId>";
	public static final String MSG_ID_END = "</MsgId>";
	public static final String CRE_DTTM_STRT = "<CreDtTm>";
	public static final String CRE_DTTM_END = "</CreDtTm>";
	public static final String AUTH_STN_STRT = "<Authstn>";
	public static final String AUTH_STN_END = "</Authstn>";
	public static final String PROP_STRT = "<Prtry>";
	public static final String PROP_END = "</Prtry>";
	public static final String NO_OF_TXN_STRT = "<NbOfTxs>";
	public static final String NO_OF_TXN_END = "</NbOfTxs>";
	public static final String CONTROL_SUM_STRT = "<CtrlSum>";
	public static final String CONTROL_SUM_END = "</CtrlSum>";
	public static final String INI_PARTY_STRT = "<InitgPty>";
	public static final String INI_PARTY_END = "</InitgPty>";
	public static final String NM_STRT = "<Nm>";
	public static final String NM_END = "</Nm>";
	public static final String ID_STRT = "<Id>";
	public static final String ID_END = "</Id>";
	public static final String ORG_ID_STRT = "<OrgId>";
	public static final String ORG_ID_END = "</OrgId>";
	public static final String OTHER_STRT = "<Othr>";
	public static final String OTHER_END = "</Othr>";

	/* PAYMENT INFORMATION */

	public static final String PMNT_INF_STRT = "<PmtInf>";
	public static final String PMNT_INF_END = "</PmtInf>";
	public static final String PMNT_INF_ID_STRT = "<PmtInfId>";
	public static final String PMNT_INF_ID_END = "</PmtInfId>";
	public static final String PMNT_MTHD_STRT = "<PmtMtd>";
	public static final String PMNT_MTHD_END = "</PmtMtd>";
	public static final String SVC_LVL_STRT = "<SvcLvl>";
	public static final String SVC_LVL_END = "</SvcLvl>";
	public static final String PMNT_TYP_STRT = "<PmtTpInf>";
	public static final String PMNT_TYP_END = "</PmtTpInf>";
	public static final String CTG_PRPS_STRT = "<CtgyPurp>";
	public static final String CTG_PRPS_END = "</CtgyPurp>";
	public static final String LCL_INSTRM_STRT = "<LclInstrm>";
	public static final String LCL_INSTRM_END = "</LclInstrm>";
	public static final String READ_EXEC_DT_STRT = "<ReqdExctnDt>";
	public static final String READ_EXEC_DT_END = "</ReqdExctnDt>";
	public static final String DEBTR_STRT = "<Dbtr>";
	public static final String DEBTR_END = "</Dbtr>";
	public static final String PSTL_ADDR_STRT = "<PstlAdr>";
	public static final String PSTL_ADDR_END = "</PstlAdr>";
	public static final String STRT_NM_STRT = "<StrtNm>";
	public static final String STRT_NM_END = "</StrtNm>";
	public static final String PSTL_CD_STRT = "<PstCd>";
	public static final String PSTL_CD_END = "</PstCd>";
	public static final String TWN_NM_STRT = "<TwnNm>";
	public static final String TWN_NM_END = "</TwnNm>";
	public static final String CNTRY_SUB_DVSN_STRT = "<CtrySubDvsn>";
	public static final String CNTRY_SUB_DVSN_END = "</CtrySubDvsn>";
	public static final String CNTRY_STRT = "<Ctry>";
	public static final String CNTRY_END = "</Ctry>";
	public static final String ADDR_LINE_STRT = "<AdrLine>";
	public static final String ADDR_LINE_END = "</AdrLine>";
	public static final String DBTR_ACCT_STRT = "<DbtrAcct>";
	public static final String DBTR_ACCT_END = "</DbtrAcct>";
	public static final String CCY_STRT = "<Ccy>";
	public static final String CCY_END = "</Ccy>";
	public static final String DBTR_AGNT_STRT = "<DbtrAgt>";
	public static final String DBTR_AGNT_END = "</DbtrAgt>";
	public static final String FIN_INSTN_ID_STRT = "<FinInstnId>";
	public static final String FIN_INSTN_ID_END = "</FinInstnId>";
	public static final String CLR_SYS_MMB_ID_STRT = "<ClrSysMmbId>";
	public static final String CLR_SYS_MMB_ID_END = "</ClrSysMmbId>";
	public static final String MMB_ID_STRT = "<MmbId>";
	public static final String MMB_ID_END = "</MmbId>";
	public static final String DBTR_AGNT_ACT_STRT = "<DbtrAgtAcct>";
	public static final String DBTR_AGNT_ACT_END = "</DbtrAgtAcct>";
	public static final String CREDIT_TRF_TRN_STRT = "<CdtTrfTxInf>";
	public static final String CREDIT_TRF_TRN_END = "</CdtTrfTxInf>";
	public static final String PMT_ID_STRT = "<PmtId>";
	public static final String PMT_ID_END = "</PmtId>";
	public static final String INSTR_ID_STRT = "<InstrId>";
	public static final String INSTR_ID_END = "</InstrId>";
	public static final String END_TO_END_ID_STRT = "<EndToEndId>";
	public static final String END_TO_END_ID_END = "</EndToEndId>";
	public static final String AMT_STRT = "<Amt>";
	public static final String AMT_END = "</Amt>";
	public static final String INSTRUC_AMT_STRT = "<InstdAmt>";
	public static final String INSTRUC_AMT_END = "</InstdAmt>";
	public static final String CRED_AGNT_STRT = "<CdtrAgt>";
	public static final String CRED_AGNT_END = "</CdtrAgt>";
	public static final String CLEARING_SYS_ID_STRT = "<ClrSysId>";
	public static final String CLEARING_SYS_ID_END = "</ClrSysId>";
	public static final String CODE_STRT = "<Cd>";
	public static final String CODE_END = "</Cd>";
	public static final String CREDITOR_STRT = "<Cdtr>";
	public static final String CREDITOR_END = "</Cdtr>";
	public static final String CREDITOR_ACCT_STRT = "<CdtrAcct>";
	public static final String CREDITOR_ACCT_END = "</CdtrAcct>";
	public static final String RMT_INF_STRT = "<RmtInf>";
	public static final String RMT_INF_END = "</RmtInf>";
	public static final String UNSTRUCTURED_STRT = "<Ustrd>";
	public static final String UNSTRUCTURED_END = "</Ustrd>";
	public static final String CHK_INSTR_STRT = "<ChqInstr>";
	public static final String CHK_INSTR_END = "</ChqInstr>";
	public static final String CHK_NB_STRT = "<ChqNb>";
	public static final String CHK_NB_END = "</ChqNb>";
	public static final String DLVRY_MTHD_STRT = "<DlvryMtd>";
	public static final String DLVRY_MTHD_END = "</DlvryMtd>";
	public static final String STRD_STRT = "<Strd>";
	public static final String STRD_END = "</Strd>";
	public static final String REF_DOC_INFO_STRT = "<RfrdDocInf>";
	public static final String REF_DOC_INFO_END = "</RfrdDocInf>";
	public static final String TP_STRT = "<Tp>";
	public static final String TP_END = "</Tp>";
	public static final String CD_PRTRY_STRT = "<CdOrPrtry>";
	public static final String CD_PRTRY_END = "</CdOrPrtry>";
	public static final String NB_STRT = "<Nb>";
	public static final String NB_END = "</Nb>";
	public static final String RLTD_DT_STRT = "<RltdDt>";
	public static final String RLTD_DT_END = "</RltdDt>";
	public static final String REF_DOC_AMT_STRT = "<RfrdDocAmt>";
	public static final String REF_DOC_AMT_END = "</RfrdDocAmt>";
	public static final String DUE_PAY_AMT_STRT = "<DuePyblAmt>";
	public static final String DUE_PAY_AMT_END = "</DuePyblAmt>";
	public static final String DSCNT_APLD_AMT_STRT = "<DscntApldAmt>";
	public static final String DSCNT_APLD_AMT_END = "</DscntApldAmt>";
	public static final String RMTD_AMT_STRT = "<RmtdAmt>";
	public static final String RMTD_AMT_END = "</RmtdAmt>";

}
