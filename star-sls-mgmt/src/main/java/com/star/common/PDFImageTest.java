package com.star.common; 

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

public class PDFImageTest {

	public static void main(String[] args) {
		
		File file = new File("D:\\temp\\SAL-001-001\\SAL02\\SAL-001-001-SAL02-0000000061.pdf");
	      PDDocument doc;
		try {
			doc = PDDocument.load(file);
		
	        
	      //Retrieving the page
	      PDPage page = doc.getPage(0);
	      
	      
	      System.out.println(page.getMediaBox().getHeight());
	      System.out.println(page.getMediaBox().getWidth());
	      
	      System.out.println((page.getMediaBox().getHeight()*25.4 / 72)*3.7795275591);
	      System.out.println((page.getMediaBox().getWidth()*25.4 / 72)*3.7795275591);
	     
	      
	      double xPos = 639 - (639*.34); /* pixel to page points .009 */
	      double yPos = 33;
	    		  
	    		  
	    		  /*1188-1024-100-18;*/
	    		  
	    		  /*1188-1024;*/
	      
	      
	      System.out.println((250*25.4 / 72)*3.7795275591);
	      System.out.println((100*25.4 / 72)*3.7795275591);
	      
	      //Creating PDImageXObject object
	      PDImageXObject pdImage = PDImageXObject.createFromFile("D:\\temp\\Sign_1591178914845.png",doc);
	       
	      //creating the PDPageContentStream object
	      PDPageContentStream contents = new PDPageContentStream(doc, page, AppendMode.APPEND, true);

	      //Drawing the image in the PDF document
	      contents.drawXObject(pdImage, (float)xPos, (float)yPos, 250-75, 100-30);
	      
	      System.out.println("Image inserted");
	      
	      //Closing the PDPageContentStream object
	      contents.close();		
			
	      //Saving the document
	      doc.save("D:\\\\temp\\\\Sign_1591178914845_19.pdf");
	            
	      //Closing the document
	      doc.close();
	      
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
