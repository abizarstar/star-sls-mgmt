package com.star.common;

import java.util.ArrayList;
import java.util.List;

public class BrowseOutput {
	
	public int rtnSts = 0;
	public int eof = 1;
	public int nbrRecRtn = 0;
	public String authToken;
	
	public List<String> messages = new ArrayList<String>();
	public List<String> references = new ArrayList<String>();
	
	public void setErrorMessage(String errorMessage) {
		if (messages ==  null) {
			messages = new ArrayList<String>();
		}
		
		messages.add(errorMessage);
	}
	
}
