package com.star.common;

import java.util.ArrayList;
import java.util.List;

public class MaintenanceOutput {
	public int rtnSts = 0;	
	public List<String> messages = new ArrayList<String>();
	public List<String> references = new ArrayList<String>();
	public String authToken;
	
	public void setErrorMessage(String errorMessage) {
		if (messages ==  null) {
			messages = new ArrayList<String>();
		}
		
		messages.add(errorMessage);
	}
}
