package com.star.common.storage;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;

public class UploadObject {

	public static void main(String[] args) throws IOException {
		
		System.out.println("welcome to Amazon S3 !!");
		String bucketName = "doc-fol";
		
		AWSCredentials credentials = new BasicAWSCredentials("AKIA3425DQSCGSHESB7Z",
				"uGqbQ0R6JR2ZkewOWTvgiwko55q5xLttGQN6v7f7");


			AmazonS3 s3client = AmazonS3ClientBuilder
					  .standard()
					  .withCredentials(new AWSStaticCredentialsProvider(credentials))
					  .withRegion(Regions.AP_SOUTH_1)
					  .build();
			
			for (Bucket bucket : s3client.listBuckets()) {
				System.out.println(" - " + bucket.getName());
			}
			
			
			createFolder(bucketName, "test2", s3client);
			
			/*s3client.createBucket(bucketName);*/
			
			String fileName = "test2" + "/" + "abc1.txt";
			PutObjectResult objectResult = s3client.putObject(new PutObjectRequest(bucketName, fileName, 
					new File("C:\\Users\\ABIZAR\\Documents\\Desktop\\edgen.txt")));
			
			
			S3Object fileObject = s3client.getObject(new GetObjectRequest(bucketName, fileName));
			
			
			Files.copy(fileObject.getObjectContent(), Paths.get("D://abc_abizar.txt"));
			
	}
	
	
	public static void createFolder(String bucketName, String folderName, AmazonS3 client) {
		
		final String SUFFIX = "/";
		
		// create meta-data for your folder and set content-length to 0
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(0);
		// create empty content
		InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
		// create a PutObjectRequest passing the folder name suffixed by /
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
					folderName + SUFFIX, emptyContent, metadata);
		// send request to S3 to create folder
		
		boolean b = client.doesObjectExist(bucketName, folderName+ SUFFIX) ;
		
		if(b == false)
		{
			client.putObject(putObjectRequest);
		}
	}

			
	}
