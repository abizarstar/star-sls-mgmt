package com.star.common.storage;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.log4j.Logger;

import com.amazonaws.AmazonClientException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.S3ClientOptions;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.IllegalBucketNameException;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

public class S3Directory {
	
	 private final Logger log = Logger.getLogger(S3Directory.class);
	
	public boolean createDir(String bucketName, String folderPath, String folderName, AmazonS3 client) {
		
		final String SUFFIX = "/";
		String folder = "";
		
		if(folderPath.length() > 0 )
		{
			folder = folderPath + SUFFIX + folderName;
		}
		else
		{
			folder = folderPath + folderName;
		}
		
		// create meta-data for your folder and set content-length to 0
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(0);
		// create empty content
		InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
		// create a PutObjectRequest passing the folder name suffixed by /
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
				folder + SUFFIX, emptyContent, metadata);
		// send request to S3 to create folder
		
		boolean b = client.doesObjectExist(bucketName, folder+ SUFFIX) ;
		
		if(b == false)
		{
			client.putObject(putObjectRequest);
			
			b = true;
		}
		else
		{
			b = false;
		}
		
		return b;
	}
	
	
	public String createBucket(String accessKey, String secretKey, String region, String bucketNm)
	{
		Boolean bucketSts = false;
		String errMsg = "";
		AmazonS3 s3client = null;
		Regions regions = null;
		
		for (Regions c : Regions.values()) {
	        if (c.name().equals(region)) {
	        	regions = c;
	        }
	    }
		
		if(regions != null)
		{
			AWSCredentials credentials = new BasicAWSCredentials(accessKey,
					secretKey);
				
			s3client = AmazonS3ClientBuilder
						  .standard()
						  .withCredentials(new AWSStaticCredentialsProvider(credentials))
						  .withRegion(regions)
						  .build();
			try
			{
				s3client.createBucket(bucketNm);
			}
			catch (AmazonS3Exception e) {
				// TODO: handle exception
				errMsg = e.getErrorMessage();
			}
			catch (IllegalBucketNameException e) {
				// TODO: handle exception
				errMsg = e.getMessage();
			}
		}
		
		if(errMsg.length() == 0)
		{
			s3client.deleteBucket(bucketNm);
		}
		
		
		return errMsg;
		
	}
	
	public String validateStorage(String accessKey, String secretKey, String region)
	{
		String errMsg = "";
		Regions regions = null;
		
		for (Regions c : Regions.values()) {
	        if (c.name().equals(region)) {
	        	regions = c;
	        }
	    }
		
		if(regions != null)
		{
			AWSCredentials credentials = new BasicAWSCredentials(accessKey,
					secretKey);
				
			AmazonS3 s3client = AmazonS3ClientBuilder
						  .standard()
						  .withCredentials(new AWSStaticCredentialsProvider(credentials))
						  .withRegion(regions)
						  .build();
			try
			{
				for (Bucket bucket : s3client.listBuckets()) {
				System.out.println(" - " + bucket.getName());
				}
			}
			catch (AmazonS3Exception e) {
				// TODO: handle exception
				
				errMsg = e.getErrorMessage();
			}
		}
		else
		{
			errMsg = "Region not available";
		}
		
		return errMsg;
	}
	
}
