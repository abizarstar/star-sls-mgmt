package com.star.common.storage;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.star.common.CommonConstants;
import com.star.dao.DirStorageDAO;
import com.star.modal.DocDirConfig;

public class StorageType {
	
    private static StorageType instance=new StorageType();
    private String storageType;
    private AmazonS3 s3client;
    private String bucketName;
    
    public static StorageType getInstance(){
            return instance;
    }
    
    private StorageType(){
       
    	DirStorageDAO dao = new DirStorageDAO();
    	
    	DocDirConfig dirConfig = dao.getStorgae();
    	
    	storageType = dirConfig.getDirType();
    	
    	if(storageType.equals("S3"))
    	{
    		Regions regions = null;
    		
    		for (Regions c : Regions.values()) {
    	        if (c.name().equals(dirConfig.getS3Region())) {
    	        	regions = c;
    	        }
    	    }
    		
    		AWSCredentials credentials = new BasicAWSCredentials(dirConfig.getS3AccessKey(),
    				dirConfig.getS3SecretKey());
    			
    			s3client = AmazonS3ClientBuilder
    					  .standard()
    					  .withCredentials(new AWSStaticCredentialsProvider(credentials))
    					  .withRegion(regions)
    					  .build();
    			
    			bucketName = dirConfig.getS3BktNm();		
    	}
    	else
    	{
    		CommonConstants.ROOT_DIR = dirConfig.getRootPath() + CommonConstants.DRV_SUFFIX;
    	}
    	
    }
    
    public static String getStorgaeType(){
        String storageType =  getInstance().storageType;
               
        return storageType;
    }
    
    
    public static AmazonS3 getS3Client(){
    	AmazonS3 s3ClientAmazon =  getInstance().s3client;
               
        return s3ClientAmazon;
    }
    
    
    public static String getBucketName(){
    	String bucketNm =  getInstance().bucketName;
               
        return bucketNm;
    }


	
}
