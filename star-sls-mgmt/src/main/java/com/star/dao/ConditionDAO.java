package com.star.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.linkage.condition.ConditionAmount;
import com.star.linkage.condition.ConditionBrowseOutput;
import com.star.linkage.condition.ConditionInfo;
import com.star.linkage.condition.ConditionManOutput;
import com.star.linkage.condition.ConditionOutput;
import com.star.modal.UsrAprCon;
import com.star.modal.UsrAprConAmt;
import com.star.modal.UsrAprConVen;

public class ConditionDAO {

	private static Logger logger = LoggerFactory.getLogger(ConditionDAO.class);
	
	public void addCondition(MaintanenceResponse<ConditionManOutput> starManResponse, ConditionInfo conditionInfo)
	{
		Session session = null;
		Transaction tx = null;

		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			
			
			for(int i = 0; i < conditionInfo.getUsrList().size(); i++)
			{
				delVendorCondition(session, conditionInfo.getUsrList().get(i));
				
				for(int j =0; j < conditionInfo.getVenList().size(); j++)
				{
					UsrAprConVen aprConVen= new UsrAprConVen();
					
					aprConVen.setUsrId(conditionInfo.getUsrList().get(i));
					aprConVen.setVenId(conditionInfo.getVenList().get(j));
					
					session.save(aprConVen);
				}
			}
			
			for(int i = 0; i < conditionInfo.getUsrList().size(); i++)
			{
				delCondition(session, conditionInfo.getUsrList().get(i));
				
				UsrAprCon usrAprCon = new UsrAprCon();
				
				usrAprCon.setUsrId(conditionInfo.getUsrList().get(i));
				usrAprCon.setCon(conditionInfo.getCond());
				
				session.save(usrAprCon);
				
			}
			
			
			for(int i = 0; i < conditionInfo.getUsrList().size(); i++)
			{
				delAmountCondition(session, conditionInfo.getUsrList().get(i));
				
				for(int j =0; j < conditionInfo.getAmtList().size(); j++)
				{
					UsrAprConAmt aprConAmt = new UsrAprConAmt();
					
					aprConAmt.setUsrId(conditionInfo.getUsrList().get(i));
					aprConAmt.setFrmAmt(conditionInfo.getAmtList().get(j).getFrmAmoumt());
					aprConAmt.setToAmt(conditionInfo.getAmtList().get(j).getToAmount());
					
					session.save(aprConAmt);
				}
			}
			
			tx.commit();
		} catch (Exception e) {

			logger.debug("Condition Info : {}", e.getMessage(), e);
			starManResponse.output.messages.add(e.getMessage());
			starManResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	public void getUserCondition(BrowseResponse<ConditionBrowseOutput> starBrowseResponse, String usrId)
	{

		Session session= null;
		
    	try{
    		    		
    		String hql = "";
    		
    		session = SessionUtil.getSession();
    		
			hql = "select * from usr_dtls_sls where 1=1 and usr_id in (select usr_id from usr_apr_con_amt "
					+ "union select usr_id from usr_apr_con_ven)";
	    	
			if(usrId.length() > 0)
			{
				hql = hql + " and usr_id=:usr";
			}
			
		    Query queryValidate= session.createSQLQuery(hql);
		    
		    if(usrId.length() > 0)
			{
		    	queryValidate.setParameter("usr", usrId);
			}
		    
			 List<Object[]> listUser =  queryValidate.list();
			 
			 for (Object[] user : listUser)
		     {
				 ConditionOutput conditionOutput = new ConditionOutput();
				 
				 conditionOutput.setUsrId(user[0].toString());
				 
				 getVendorCondition(session, conditionOutput, user[0].toString());
				 
				 getAmountCondition(session, conditionOutput, user[0].toString());

				 getCondition(session, conditionOutput, user[0].toString());
				 
				 starBrowseResponse.output.fldTblCondition.add(conditionOutput);
		     }
		   
    	}
    	catch (Exception e) {
    		logger.debug("Condition Info : {}", e.getMessage(), e);
    		starBrowseResponse.output.rtnSts = 1;
    		starBrowseResponse.output.messages.add(e.getMessage());
    		
		}
    	finally {
			session.close();
		}
	
	}
	
	public void getAmountCondition(Session session, ConditionOutput conditionOutput, String usrId) throws Exception
	{
		String hql = "";
		
		hql = "select * from usr_apr_con_amt where 1=1";
    	
		if(usrId.length() > 0)
		{
			hql = hql + " and usr_id=:usr";
		}
		
	    Query queryValidate= session.createSQLQuery(hql);
	    
	    if(usrId.length() > 0)
		{
	    	queryValidate.setParameter("usr", usrId);
		}
	    
		 List<Object[]> listAmt =  queryValidate.list();
		 
		 List<ConditionAmount> amtList =  new ArrayList<ConditionAmount>();
		 
		 for (Object[] amt : listAmt)
	     {
			 ConditionAmount amount = new ConditionAmount();
			 
			 amount.setFrmAmoumt(Double.parseDouble(amt[1].toString()));
			 amount.setFrmAmoumt(Double.parseDouble(amt[2].toString()));
			 
			 amtList.add(amount);
	     }
		 
		 conditionOutput.setAmtList(amtList);
	}
	
	public void getVendorCondition(Session session, ConditionOutput conditionOutput, String usrId) throws Exception
	{
		String hql = "";
		
		hql = "select * from usr_apr_con_ven where 1=1";
    	
		if(usrId.length() > 0)
		{
			hql = hql + " and usr_id=:usr";
		}
		
	    Query queryValidate= session.createSQLQuery(hql);
	    
	    if(usrId.length() > 0)
		{
	    	queryValidate.setParameter("usr", usrId);
		}
	    
		 List<Object[]> listVen =  queryValidate.list();
		 
		 List<String> venList = new ArrayList<String>();
		 
		 for (Object[] ven : listVen)
		 {
			 
			 venList.add(ven[1].toString());
	     }
		 
		 conditionOutput.setVenList(venList);
	}
	
	public void getCondition(Session session, ConditionOutput conditionOutput, String usrId) throws Exception
	{
		String hql = "";
		
		hql = "select * from usr_apr_con where 1=1";
    	
		if(usrId.length() > 0)
		{
			hql = hql + " and usr_id=:usr";
		}
		
	    Query queryValidate= session.createSQLQuery(hql);
	    
	    if(usrId.length() > 0)
		{
	    	queryValidate.setParameter("usr", usrId);
		}
	    
		 List<Object[]> listCon =  queryValidate.list();
		 
		 for (Object[] con : listCon)
	     {
			 conditionOutput.setCond(con[1].toString());
	     }		 
	}
	
	public void delVendorCondition(Session session, String usrId) throws Exception
	{
		String hql = "";
		
		hql = "delete from usr_apr_con_ven where usr_id=:usr";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("usr", usrId);
		queryValidate.executeUpdate();
	}
	
	
	public void delAmountCondition(Session session, String usrId) throws Exception
	{
		String hql = "";
		
		hql = "delete from usr_apr_con_amt where usr_id=:usr";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("usr", usrId);
		queryValidate.executeUpdate();
	}
	
	public void delCondition(Session session, String usrId) throws Exception
	{
		String hql = "";
		
		hql = "delete from usr_apr_con where usr_id=:usr";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("usr", usrId);
		queryValidate.executeUpdate();
	}
	
}
