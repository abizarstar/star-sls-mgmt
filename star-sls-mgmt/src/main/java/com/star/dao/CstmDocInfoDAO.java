package com.star.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.common.SessionUtilInformix;
import com.star.linkage.cstmdocinfo.CstmDocInfoBrowseOutput;
import com.star.linkage.cstmdocinfo.CstmDocInfoManOutput;
import com.star.linkage.cstmdocinfo.GlInfo;
import com.star.linkage.cstmdocinfo.ReconInfo;
import com.star.linkage.cstmdocinfo.VchrInfo;
import com.star.linkage.cstmorddocs.CstmOrdDocsInput;
import com.star.linkage.cstmorddocs.CstmOrdDocsManOutput;
import com.star.modal.CstmOrdDocs;
import com.star.modal.CstmParamInv;
import com.star.modal.CstmVchrGlInfo;
import com.star.modal.CstmVchrReconInfo;

public class CstmDocInfoDAO {

	private static Logger logger = LoggerFactory.getLogger(ConditionDAO.class);

	public void addOrderDocument(MaintanenceResponse<CstmOrdDocsManOutput> maintanenceResponse,
			CstmOrdDocsInput cstmOrdDocsInp) {
		Session session = null;
		Transaction tx = null;
		try {
			Date date = new Date();

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			CstmOrdDocs cstmOrdDocs = new CstmOrdDocs();

			cstmOrdDocs.setDocId(cstmOrdDocsInp.getDocId());
			cstmOrdDocs.setDocName(cstmOrdDocsInp.getDocName());
			cstmOrdDocs.setDocOrigName(cstmOrdDocsInp.getDocOrigName());
			cstmOrdDocs.setCrtDtts(date);
			cstmOrdDocs.setUpldBy(cstmOrdDocsInp.getUpldBy());

			session.save(cstmOrdDocs);

			tx.commit();
		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			maintanenceResponse.output.messages.add(e.getMessage());
			maintanenceResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	/* GET ALL THE DEFINE FIELDS LIST */
	public void readFields(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseRes, String trnSts, String userId,
			String usrGrp, String year) throws Exception {
		Session session = null;
		String hql = "";

		WkfMapDAO wkfMapDAO = new WkfMapDAO();

		try {
			session = SessionUtil.getSession();

			hql = " select ctl_no,gat_ctl_no,fl_pdf,fl_pdf_name,fl_txt,crt_dtts,prs_dtts, "
					+ " vchr_ctl_no,vchr_inv_no,vchr_ven_id,vchr_ven_nm,vchr_brh,vchr_amt,vchr_ext_ref,"
					+ " vchr_inv_dt,vchr_due_dt,vchr_pay_term, vchr_cry, upld_by,usr_nm,trn_rmk,src_flg "
					+ " from cstm_doc_info doc_info , cstm_vchr_info vchr_info, usr_dtls_sls where"
					+ " usr_dtls_sls.usr_id = doc_info.upld_by and"
					+ " doc_info.ctl_no = vchr_info.vchr_ctl_no and (vchr_stx_ref is null or vchr_stx_ref='')";
			if (trnSts.length() == 0) {
				hql = hql + " and trn_sts is NULL";
			} else if (trnSts.length() > 0) {
				hql = hql + " and trn_sts=:trn_sts ";
			}

			if (!usrGrp.equals("1")) {
				hql = hql
						+ " and ((upld_by=:usr) OR doc_info.ctl_no in (select ctl_no from cstm_wkf_map where wkf_asgn_to=:usr and wkf_sts='S'))  ";
			}

			hql = hql + " order by crt_dtts desc";

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			if (trnSts.length() > 0) {
				queryValidate.setParameter("trn_sts", trnSts);
			}

			if (!usrGrp.equals("1")) {
				queryValidate.setParameter("usr", userId);
			}

			List<Object[]> listCstmDocVal = queryValidate.list();
			CommonFunctions objCom = new CommonFunctions();
			for (Object[] cstmDocVal : listCstmDocVal) {
				VchrInfo cstmdocInfo = new VchrInfo();

				cstmdocInfo.setCtlNo((Integer) (cstmDocVal[0]));
				cstmdocInfo.setGatCtlNo((Integer) (cstmDocVal[1]));
				cstmdocInfo.setFlPdf(String.valueOf(cstmDocVal[2]));
				cstmdocInfo.setFlPdfName(String.valueOf(cstmDocVal[3]));
				cstmdocInfo.setFlTxt(String.valueOf(cstmDocVal[4]));
				if (cstmDocVal[5] != null) {
					cstmdocInfo.setCrtDttsStr((objCom.formatDateWithoutTime((Date) cstmDocVal[5])));
					cstmdocInfo.setCrtDtts((Date) (cstmDocVal[5]));
				}
				if (cstmDocVal[6] != null) {
					cstmdocInfo.setPrsDttsStr((objCom.formatDateWithoutTime((Date) cstmDocVal[6])));
					cstmdocInfo.setPrsDtts((Date) (cstmDocVal[6]));
				}
				cstmdocInfo.setVchrCtlNo((Integer) (cstmDocVal[7]));
				cstmdocInfo.setVchrInvNo(String.valueOf(cstmDocVal[8]));
				cstmdocInfo.setVchrVenId(String.valueOf(cstmDocVal[9]));
				cstmdocInfo.setVchrVenNm(String.valueOf(cstmDocVal[10]));
				cstmdocInfo.setVchrBrh(String.valueOf(cstmDocVal[11]));

				cstmdocInfo.setVchrAmt(Double.parseDouble(String.valueOf(cstmDocVal[12])));

				if (Double.parseDouble(String.valueOf(cstmDocVal[12])) > 0) {
					cstmdocInfo.setVchrAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(cstmDocVal[12]))));
				} else {
					cstmdocInfo.setVchrAmtStr("");
				}
				cstmdocInfo.setVchrExtRef(String.valueOf(cstmDocVal[13]));
				if (cstmDocVal[14] != null) {
					cstmdocInfo.setVchrInvDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[14])));
					cstmdocInfo.setVchrInvDt((Date) (cstmDocVal[14]));
				} else {
					cstmdocInfo.setVchrInvDtStr("");
				}
				if (cstmDocVal[15] != null) {
					cstmdocInfo.setVchrDueDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[15])));
					cstmdocInfo.setVchrDueDt((Date) (cstmDocVal[15]));
				} else {
					cstmdocInfo.setVchrDueDtStr("");
				}
				cstmdocInfo.setVchrPayTerm(String.valueOf(cstmDocVal[16]));
				cstmdocInfo.setVchrCry(String.valueOf(cstmDocVal[17]));
				cstmdocInfo.setUpldBy(String.valueOf(cstmDocVal[18]));
				cstmdocInfo.setUsrNm(String.valueOf(cstmDocVal[19]));
				if (cstmDocVal[20] != null) {
					cstmdocInfo.setTrnRmk(String.valueOf(cstmDocVal[20]));
				} else {
					cstmdocInfo.setTrnRmk("");
				}
				cstmdocInfo.setSrcFlg(String.valueOf(cstmDocVal[21]));

				wkfMapDAO.getWorkflowSpecific(String.valueOf(cstmdocInfo.getCtlNo()), cstmdocInfo);

				starBrowseRes.output.fldTblDoc.add(cstmdocInfo);
			}

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);

			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* GET ALL THE DEFINE FIELDS LIST */
	public void readStxFields(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseRes, String trnSts, int pageNo, String cmpyId, String year)
			throws Exception {
		Session session = null;
		String hql = "";
		String voucherList = "";

		List<CstmParamInv> invsListPending = new ArrayList<CstmParamInv>();

		invsListPending = getPendingVoucherList(trnSts);

		voucherList = getVoucherList(invsListPending);

		try {
			session = SessionUtilInformix.getSession();

			/*
			 * hql =
			 * "SELECT cast(vch_ven_inv_no as varchar(22)) vch_ven_inv_no, cast(vch_ven_id as varchar(8)) vch_ven_id,"
			 * +
			 * " cast(aprven_rec.ven_ven_long_nm as varchar(35)) ven_ven_long_nm, cast(vch_vchr_brh as varchar(3)) vch_vchr_brh,"
			 * +
			 * " vch_vchr_amt, cast(vch_cry as varchar(3)) vch_cry, vch_ven_inv_dt, vch_due_dt"
			 * + " FROM aptvch_rec, aprven_rec" +
			 * " WHERE aptvch_rec.vch_cmpy_id = aprven_rec.ven_cmpy_id AND aptvch_rec.vch_ven_id = aprven_rec.ven_ven_id"
			 * ;
			 */
			if (CommonConstants.DB_TYP.equals("INF")) {
				hql = "SELECT cast(trim(vch_ven_id) as varchar(8)) as ven_id, cast(trim(ven_ven_long_nm) as varchar(35)) as ven_long_nm, cast(trim(vch_ven_inv_no) as varchar(22)) as ven_inv_no, "
						+ "cast((select trim(usr_nm) from mxrusr_rec where usr_lgn_id=vch_lgn_id) as varchar(35)) as lgn_id, "
						+ "vch_ven_inv_dt, vch_due_dt, cast(trim(vch_po_pfx) as varchar(2)) as po_pfx, vch_po_no, vch_po_itm, 					 "
						+ "cast(trim(vch_cry) as varchar(3)) as cry, cast(trim(vch_vchr_pfx) as varchar(2)) as vchr_pfx, vch_vchr_no, "
						+ "cast(trim(vch_vchr_brh) as varchar(3)) as vchr_brh, 					 vch_ent_dt, vch_vchr_amt, "
						+ "cast(trim(vct_vchr_cat) as varchar(4)) as vchr_cat, (select cast(tsa_sts_actn as varchar(1)) from tcttsa_rec "
						+ "					 where tsa_cmpy_id=vch_cmpy_id  and tsa_ref_pfx=vch_vchr_pfx and tsa_ref_no=vch_vchr_no and tsa_ref_itm=0 "
						+ "and 					 tsa_ref_sbitm=0 and tsa_sts_typ='T') as trn_actn, (select cast(tsa_sts_actn as varchar(1)) from tcttsa_rec "
						+ "where tsa_cmpy_id=vch_cmpy_id and 					 tsa_ref_pfx=vch_vchr_pfx and tsa_ref_no=vch_vchr_no and tsa_ref_itm=0 "
						+ " and tsa_ref_sbitm=0 and tsa_sts_typ='N') as pay_actn 					  FROM aptvch_rec,      aprven_rec,      aprvct_rec "
						+ "WHERE vch_cmpy_id=:cmpy and   ven_cmpy_id = vch_cmpy_id AND   ven_ven_id = vch_ven_id AND   vct_vchr_cat = vch_vchr_cat and year(vch_ent_dt)=:year";
			} else if (CommonConstants.DB_TYP.equals("POS")) {
				hql = "SELECT trim(vch_ven_id) as ven_id, trim(ven_ven_long_nm) as ven_long_nm, trim(vch_ven_inv_no) as ven_inv_no,"
						+ " (select trim(usr_nm) from mxrusr_rec where usr_lgn_id=vch_lgn_id) as lgn_id, vch_ven_inv_dt, vch_due_dt, trim(vch_po_pfx) as po_pfx, vch_po_no, vch_po_itm,"
						+ " trim(vch_cry) as cry, trim(vch_vchr_pfx) as vchr_pfx, vch_vchr_no, trim(vch_vchr_brh) as vchr_brh,"
						+ " vch_ent_dt, vch_vchr_amt, trim(vct_vchr_cat) as vchr_cat, (select tsa_sts_actn from tcttsa_rec"
						+ " where tsa_cmpy_id=vch_cmpy_id  and tsa_ref_pfx=vch_vchr_pfx and tsa_ref_no=vch_vchr_no and tsa_ref_itm=0 and"
						+ " tsa_ref_sbitm=0 and tsa_sts_typ='T'), (select tsa_sts_actn from tcttsa_rec where tsa_cmpy_id=vch_cmpy_id and"
						+ " tsa_ref_pfx=vch_vchr_pfx and tsa_ref_no=vch_vchr_no and tsa_ref_itm=0  and tsa_ref_sbitm=0 and tsa_sts_typ='N')"
						+ " FROM aptvch_rec, aprven_rec, aprvct_rec  WHERE 1=1 and ven_cmpy_id = vch_cmpy_id and ven_ven_id = vch_ven_id and"
						+ " vct_vchr_cat = vch_vchr_cat and date_part('year', vch_ent_dt)=:year and vch_cmpy_id=:cmpy";
			}

			/* SENT FOR APPROVAL AND APPROVED RECORD */
			if (trnSts.equals("S") || trnSts.equals("A")) {
				hql = hql + " and vch_vchr_pfx || '-' || vch_vchr_no in (:vchrList)";
			} else if (trnSts.equals("C")) { /* COMPLETED RECORD */

				hql = hql
						+ " and vch_vchr_pfx||vch_vchr_no in (select pyh_opa_pfx||pyh_opa_no from aptpyh_rec where pyh_balamt=0)";
			} else {
				hql = hql + " and vch_vchr_pfx || '-' || vch_vchr_no not in (:vchrList)";
			}

			int skipVal = pageNo * 50;

			if (CommonConstants.DB_TYP.equals("POS")) {
				hql = hql + " order by vch_ent_dt desc, vch_vchr_no desc offset " + skipVal + " limit "
						+ CommonConstants.DB_OUT_REC;
			} else if (CommonConstants.DB_TYP.equals("INF")) {
				hql = hql + " order by vch_vchr_no desc skip " + skipVal + " limit " + CommonConstants.DB_OUT_REC;
			}

			/* hql = hql + " order by vch_ent_dt desc, vch_vchr_no desc"; */

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			if (!trnSts.equals("C")) {
				List<String> items = Arrays.asList(voucherList.split("\\s*,\\s*"));

				queryValidate.setParameterList("vchrList", items);
			}
			
			queryValidate.setParameter("cmpy", cmpyId);
			queryValidate.setParameter("year", Double.parseDouble(year));

			/*
			 * queryValidate.setParameter("rec_val", pageNo * 100);
			 * queryValidate.setParameter("rcrd_out", CommonConstants.DB_OUT_REC);
			 */

			List<Object[]> listCstmDocVal = queryValidate.list();
			CommonFunctions objCom = new CommonFunctions();
			for (Object[] cstmDocVal : listCstmDocVal) {
				VchrInfo cstmdocInfo = new VchrInfo();

				cstmdocInfo.setVchrVenId(String.valueOf(cstmDocVal[0]));
				cstmdocInfo.setVchrVenNm(String.valueOf(cstmDocVal[1]));
				cstmdocInfo.setVchrInvNo(String.valueOf(cstmDocVal[2]));
				cstmdocInfo.setUpldBy(String.valueOf(cstmDocVal[3]));
				if (cstmDocVal[4] != null) {
					cstmdocInfo.setVchrInvDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[4])));
					cstmdocInfo.setVchrInvDt((Date) (cstmDocVal[4]));
				}
				if (cstmDocVal[5] != null) {
					cstmdocInfo.setVchrDueDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[5])));
					cstmdocInfo.setVchrDueDt((Date) (cstmDocVal[5]));
				}
				cstmdocInfo.setPoPfx(String.valueOf(cstmDocVal[6]));
				cstmdocInfo.setPoNo(String.valueOf(cstmDocVal[7]));
				cstmdocInfo.setPoItm(String.valueOf(cstmDocVal[8]));
				cstmdocInfo.setVchrCry(String.valueOf(cstmDocVal[9]));
				cstmdocInfo.setVchrPfx(String.valueOf(cstmDocVal[10]));
				cstmdocInfo.setVchrNo(String.valueOf(cstmDocVal[11]));
				cstmdocInfo.setVchrBrh(String.valueOf(cstmDocVal[12]));
				if (cstmDocVal[13] != null) {
					cstmdocInfo.setCrtDttsStr((objCom.formatDateWithoutTime((Date) cstmDocVal[13])));
					cstmdocInfo.setCrtDtts((Date) (cstmDocVal[13]));
				}
				cstmdocInfo.setVchrAmt(Double.parseDouble(String.valueOf(cstmDocVal[14])));
				cstmdocInfo.setVchrAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(cstmDocVal[14]))));
				cstmdocInfo.setVchrCat(String.valueOf(cstmDocVal[15]));
				cstmdocInfo.setTransSts(String.valueOf(cstmDocVal[16]));
				cstmdocInfo.setPymntSts(String.valueOf(cstmDocVal[17]));

				if (String.valueOf(cstmDocVal[10]).length() > 0 && String.valueOf(cstmDocVal[11]).length() > 0) {
					getFileNameByVchrNo(cstmdocInfo, String.valueOf(cstmDocVal[10]), String.valueOf(cstmDocVal[11]));
				}

				for (int i = 0; i < invsListPending.size(); i++) {
					if ((cstmdocInfo.getVchrPfx() + "-" + cstmdocInfo.getVchrNo())
							.equals(invsListPending.get(i).getInvNo())) {
						cstmdocInfo.setChkNo(invsListPending.get(i).getChkNo());
						cstmdocInfo.setReqId(invsListPending.get(i).getReqId());
						break;
					}
				}

				cstmdocInfo.setWrkFlw("");
				cstmdocInfo.setWrkFlwId(0);
				cstmdocInfo.setWrkFlwSts("");
				cstmdocInfo.setWrkFlwAprvr("");

				starBrowseRes.output.fldTblDoc.add(cstmdocInfo);
			}

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);

		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* GET RECORD COUNT ON READ SERVICE */
	public void readCounts(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseRes, String userId, String cmpyId, String year)
			throws Exception {
		Session session = null;
		String hql = "";
		String voucherListPending = "";
		String voucherListSent = "";
		String voucherListApproved = "";
		List<CstmParamInv> invsListPending = new ArrayList<CstmParamInv>();
		List<CstmParamInv> invsListSent = new ArrayList<CstmParamInv>();
		List<CstmParamInv> invsListApproved = new ArrayList<CstmParamInv>();

		invsListPending = getPendingVoucherList("");
		invsListSent = getPendingVoucherList("S");
		invsListApproved = getPendingVoucherList("A");

		voucherListPending = getVoucherList(invsListPending);
		voucherListSent = getVoucherList(invsListSent);
		voucherListApproved = getVoucherList(invsListApproved);

		try {
			session = SessionUtilInformix.getSession();

			if (CommonConstants.DB_TYP.equals("POS")) {
				hql = "select (SELECT count(*) to_review FROM aptvch_rec, aprven_rec, aprvct_rec  "
						+ "WHERE 1=1 and ven_cmpy_id = vch_cmpy_id and ven_ven_id = vch_ven_id and "
						+ "vct_vchr_cat = vch_vchr_cat and vch_vchr_pfx || '-' || vch_vchr_no not "
						+ "in (:vchrListReview) and vch_cmpy_id=:cmpy and date_part('year', vch_ent_dt)=:year) to_review, (SELECT count(*) confirmed FROM aptvch_rec, aprven_rec, aprvct_rec "
						+ " WHERE 1=1 and ven_cmpy_id = vch_cmpy_id and ven_ven_id = vch_ven_id and vct_vchr_cat = "
						+ "vch_vchr_cat and vch_vchr_pfx || '-' || vch_vchr_no in (:vchrListSent) and vch_cmpy_id=:cmpy and date_part('year', vch_ent_dt)=:year) confirmed,"
						+ "(SELECT count(*) pay_approved FROM aptvch_rec, aprven_rec, aprvct_rec  WHERE 1=1 and "
						+ "ven_cmpy_id = vch_cmpy_id and ven_ven_id = vch_ven_id and vct_vchr_cat = vch_vchr_cat "
						+ "and vch_vchr_pfx || '-' || vch_vchr_no in (:vchrListApproved) and vch_cmpy_id=:cmpy and date_part('year', vch_ent_dt)=:year ) pay_approved, "
						+ "(SELECT count(*) to_review FROM aptvch_rec,aptpyh_rec where vch_cmpy_id = pyh_cmpy_id "
						+ "and vch_vchr_pfx = pyh_opa_pfx and vch_vchr_no = pyh_opa_no and pyh_balamt=0 and vch_cmpy_id=:cmpy and date_part('year', vch_ent_dt)=:year ) completed";
			} else if (CommonConstants.DB_TYP.equals("INF")) {

				hql = "select (SELECT count(*) to_review FROM aptvch_rec  WHERE vch_vchr_pfx || '-' || vch_vchr_no not in (:vchrListReview) and vch_cmpy_id=:cmpy and year(vch_ent_dt)=:year) to_review, "
						+ "0 confirmed," + " 0 pay_approved, "
						+ "(SELECT COUNT(*) to_review FROM aptpyh_rec WHERE pyh_balamt = 0 AND   pyh_cmpy_id =:cmpy and year(pyh_ent_dt)=:year) completed "
						+ "from scrcsc_rec limit 1";
			}

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			List<String> itemsReview = Arrays.asList(voucherListPending.split("\\s*,\\s*"));

			queryValidate.setParameterList("vchrListReview", itemsReview);

			List<String> itemsSent = Arrays.asList(voucherListSent.split("\\s*,\\s*"));

			List<String> itemsApproved = Arrays.asList(voucherListApproved.split("\\s*,\\s*"));
			
			if (CommonConstants.DB_TYP.equals("POS")) {
				queryValidate.setParameterList("vchrListSent", itemsSent);
			
				queryValidate.setParameterList("vchrListApproved", itemsApproved);
			}


			queryValidate.setParameter("cmpy", cmpyId);
			
			queryValidate.setParameter("year", Double.parseDouble(year));

			List<Object[]> listCstmDocVal = queryValidate.list();

			for (Object[] cstmDocVal : listCstmDocVal) {

				starBrowseRes.output.toReviewCount = Integer.parseInt(cstmDocVal[0].toString());
				starBrowseRes.output.confirmedCount = itemsSent.size();
				starBrowseRes.output.payApprovedCount = itemsApproved.size();
				starBrowseRes.output.completeCount = Integer.parseInt(cstmDocVal[3].toString());

			}

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void readCountDocument(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseRes, String userId, String usrGrp, String year)
			throws Exception {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			/*
			 * hql =
			 * " select count(*), 1  from cstm_doc_info doc_info , cstm_vchr_info vchr_info, usr_dtls_sls where"
			 * + " usr_dtls_sls.usr_id = doc_info.upld_by and" +
			 * " doc_info.ctl_no = vchr_info.vchr_ctl_no and (vchr_stx_ref is null or vchr_stx_ref='')"
			 * ;
			 * 
			 * hql = hql + " and trn_sts is NULL";
			 */

			hql = "select (select count(*) from cstm_doc_info doc_info , cstm_vchr_info vchr_info, "
					+ "usr_dtls_sls where 	usr_dtls_sls.usr_id = doc_info.upld_by and 	"
					+ "doc_info.ctl_no = vchr_info.vchr_ctl_no and (vchr_stx_ref is null or vchr_stx_ref='') "
					+ "and trn_sts is NULL";

			if (!usrGrp.equals("1"))
				hql = hql
						+ " and (doc_info.upld_by =:usr OR doc_info.ctl_no in (select ctl_no from cstm_wkf_map where wkf_asgn_to=:usr and wkf_sts='S'))";

			hql = hql + ") rvw_inv," + "(select count(*) from cstm_doc_info doc_info , cstm_vchr_info vchr_info, "
					+ "usr_dtls_sls where 	usr_dtls_sls.usr_id = doc_info.upld_by and 	"
					+ "doc_info.ctl_no = vchr_info.vchr_ctl_no and (vchr_stx_ref is null or vchr_stx_ref='') "
					+ "and trn_sts='H'";

			if (!usrGrp.equals("1"))
				hql = hql
						+ " and (doc_info.upld_by =:usr OR doc_info.ctl_no in (select ctl_no from cstm_wkf_map where wkf_asgn_to=:usr and wkf_sts='S'))";

			hql = hql + ") hld_inv where 1=1 ";

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			if (!usrGrp.equals("1"))
				queryValidate.setParameter("usr", userId);

			List<Object[]> listCstmDocVal = queryValidate.list();

			for (Object[] cstmDocVal : listCstmDocVal) {

				starBrowseRes.output.toReviewDocCount = Integer.parseInt(cstmDocVal[0].toString());
				starBrowseRes.output.toHoldDocCount = Integer.parseInt(cstmDocVal[1].toString());
			}

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void getFileNameByVchrNo(VchrInfo cstmdocInfo, String vchrPfx, String vchrNo) {
		Session session = null;
		String hql = "";
		String vchrNoStr = vchrPfx + "-" + vchrNo;

		try {
			session = SessionUtil.getSession();

			hql = "select ctl_no, fl_pdf_name, fl_pdf from cstm_doc_info, cstm_vchr_info where ctl_no=vchr_ctl_no and vchr_stx_ref=:stx_ref";

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("stx_ref", vchrNoStr);

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				cstmdocInfo.setCtlNo((Integer) (row[0]));
				cstmdocInfo.setFlPdfName(String.valueOf(row[1]));
				cstmdocInfo.setFlPdf(String.valueOf(row[2]));
			}
		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	/* GET ALL THE DEFINE FIELDS LIST */
	public List<CstmParamInv> getPendingVoucherList(String sts) {
		Session session = null;
		String hql = "";
		String voucherList = "";

		List<CstmParamInv> cstmParamInvsList = new ArrayList<CstmParamInv>();

		try {
			session = SessionUtil.getSession();

			hql = "select inv_no, chk_no, inv.req_id from cstm_pay_params param,cstm_param_inv inv where inv.req_id=param.req_id and inv.inv_sts=true";

			if (sts.length() > 0) {
				hql += " and pay_sts=:sts";
			}

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			if (sts.length() > 0) {
				queryValidate.setParameter("sts", sts);
			}

			List<Object[]> listVouchers = queryValidate.list();

			for (Object[] vouchers : listVouchers) {

				CstmParamInv cstmParamInv = new CstmParamInv();

				if (vouchers[0] != null) {
					cstmParamInv.setInvNo(vouchers[0].toString());
				} else {
					cstmParamInv.setInvNo("");
				}

				if (vouchers[1] != null) {
					cstmParamInv.setChkNo(vouchers[1].toString());
				} else {
					cstmParamInv.setChkNo("");
				}

				if (vouchers[2] != null) {
					cstmParamInv.setReqId(vouchers[2].toString());
				} else {
					cstmParamInv.setReqId("");
				}

				cstmParamInvsList.add(cstmParamInv);

				voucherList = voucherList + vouchers[0].toString() + ",";
			}

			if (voucherList.length() > 0) {
				voucherList = voucherList.substring(0, voucherList.length() - 1);
			}
		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return cstmParamInvsList;
	}

	public int getDocId() {
		Session session = null;
		String hql = "";
		int docId = 0;

		try {
			session = SessionUtil.getSession();

			hql = " SELECT COALESCE(max(doc_id),0) doc_id FROM cstm_ord_docs";

			Query queryValidate = session.createSQLQuery(hql);

			docId = Integer.parseInt(queryValidate.list().get(0).toString()) + 1;

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return docId;
	}

	public String getFileSeqNo() {

		Session session = null;
		String docCtl_no = "";

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select to_char(nextval('ctl_no'),'0000000000FM')";

			Query queryValidate = session.createSQLQuery(hql);

			docCtl_no = queryValidate.list().get(0).toString();

		} catch (Exception e) {
			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return docCtl_no;

	}

	public String getPdf(int ctlNo) {
		Session session = null;
		String hql = "";
		String pdfLocation = "";

		try {
			session = SessionUtil.getSession();

			hql = " select  cast(fl_pdf as varchar(100)) fl_pdf,cast(fl_pdf as varchar(100)) fl_pdf_name  from cstm_doc_info where ctl_no =:ctlNo";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ctlNo", ctlNo);

			Object obj = queryValidate.list();

			if (obj != null) {
				pdfLocation = queryValidate.list().get(0).toString();
			}

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return pdfLocation;
	}

	/* GET doc name and it's Original Name */
	public List<Object[]> getDocName(int docId) throws Exception {
		Session session = null;
		String hql = "";
		List<Object[]> listCstmDocVal = null;
		try {
			session = SessionUtil.getSession();
			
			hql = "SELECT doc_name, doc_orig_name FROM cstm_ord_docs WHERE doc_id =:doc_id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("doc_id", docId);

			listCstmDocVal = queryValidate.list();

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return listCstmDocVal;
	}

	public void updDocDtls(MaintanenceResponse<CstmDocInfoManOutput> maintanenceResponse, VchrInfo fieldsInput,
			JsonArray reconList, JsonArray glList) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "update cstm_vchr_info set vchr_inv_no = :vchr_inv_no , vchr_ven_id=:vchr_ven_id, vchr_ven_nm=:vchr_ven_nm, "
					+ " vchr_brh = :vchr_brh , vchr_amt=:vchr_amt, vchr_ext_ref=:vchr_ext_ref, "
					+ " vchr_inv_dt = :vchr_inv_dt , vchr_due_dt=:vchr_due_dt, vchr_pay_term=:vchr_pay_term, vchr_cry=:vchr_cry "
					+ "where " + "vchr_ctl_no=:vchr_ctl_no ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("vchr_ctl_no", fieldsInput.getVchrCtlNo());
			queryValidate.setParameter("vchr_inv_no", String.valueOf(fieldsInput.getVchrInvNo()));
			queryValidate.setParameter("vchr_ven_id", String.valueOf(fieldsInput.getVchrVenId()));
			queryValidate.setParameter("vchr_ven_nm", String.valueOf(fieldsInput.getVchrVenNm()));
			queryValidate.setParameter("vchr_brh", String.valueOf(fieldsInput.getVchrBrh()));
			queryValidate.setParameter("vchr_amt", Double.parseDouble(String.valueOf(fieldsInput.getVchrAmt())));
			queryValidate.setParameter("vchr_ext_ref", String.valueOf(fieldsInput.getVchrExtRef()));
			queryValidate.setParameter("vchr_inv_dt", ((Date) fieldsInput.getVchrInvDt()));
			queryValidate.setParameter("vchr_due_dt", ((Date) fieldsInput.getVchrDueDt()));
			queryValidate.setParameter("vchr_pay_term", String.valueOf(fieldsInput.getVchrPayTerm()));
			queryValidate.setParameter("vchr_cry", String.valueOf(fieldsInput.getVchrCry()));

			/* Update Recon Information */
			deleteReconInformation(session, reconList, fieldsInput.getVchrCtlNo());

			addReconInformation(session, reconList, fieldsInput.getVchrCtlNo());

			/* Update GL Information */
			deleteGLInformation(session, glList, fieldsInput.getVchrCtlNo());

			addGLInformation(session, glList, fieldsInput.getVchrCtlNo());

			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			maintanenceResponse.output.rtnSts = 1;
			maintanenceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* UPDATE RECON INFORMATION */
	public void deleteReconInformation(Session session, JsonArray reconList, int ctlNo) throws Exception {
		String hql = "";

		hql = "delete from cstm_vchr_recon_info where vchr_ctl_no=:vchr_ctl_no";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("vchr_ctl_no", ctlNo);
		queryValidate.executeUpdate();
	}

	public void addReconInformation(Session session, JsonArray reconList, int ctlNo) throws Exception {
		for (int i = 0; i < reconList.size(); i++) {
			JsonObject jsonObject = reconList.get(i).getAsJsonObject();

			CstmVchrReconInfo reconInfo = new CstmVchrReconInfo();

			reconInfo.setVchrCtlNo(ctlNo);
			reconInfo.setVchrCrcnNo(Integer.parseInt(jsonObject.get("crcnNo").getAsString()));
			reconInfo.setVchrTrsNo(jsonObject.get("trsNo").getAsString());
			reconInfo.setVchrPoNo(jsonObject.get("poNo").getAsString());
			reconInfo.setVchrCstNo(Integer.parseInt(jsonObject.get("costNo").getAsString()));
			reconInfo.setVchrBalAmt(Double.parseDouble(jsonObject.get("balAmt").getAsString()));
			session.save(reconInfo);
		}
	}

	/* UPDATE GL INFORMATION */
	public void deleteGLInformation(Session session, JsonArray reconList, int ctlNo) throws Exception {
		String hql = "";

		hql = "delete from cstm_vchr_gl_info where vchr_ctl_no=:vchr_ctl_no";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("vchr_ctl_no", ctlNo);
		queryValidate.executeUpdate();
	}

	public void addGLInformation(Session session, JsonArray glList, int ctlNo) throws Exception {
		for (int i = 0; i < glList.size(); i++) {
			JsonObject jsonObject = glList.get(i).getAsJsonObject();

			CstmVchrGlInfo glInfo = new CstmVchrGlInfo();

			glInfo.setVchrCtlNo(ctlNo);
			glInfo.setVchrAcct(jsonObject.get("glAcc").getAsString());
			glInfo.setVchrSubAcct(jsonObject.get("glSubAcc").getAsString());
			glInfo.setVchrDrAmt(Double.parseDouble(jsonObject.get("drAmt").getAsString()));
			glInfo.setVchrCrAmt(Double.parseDouble(jsonObject.get("crAmt").getAsString()));
			glInfo.setVchrRmk(jsonObject.get("remark").getAsString());
			session.save(glInfo);
		}
	}

	public void updStxReference(MaintanenceResponse<CstmDocInfoManOutput> maintanenceResponse, VchrInfo fieldsInput) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "update cstm_vchr_info set vchr_stx_ref = :vchr_no where vchr_ctl_no=:vchr_ctl_no ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("vchr_ctl_no", fieldsInput.getVchrCtlNo());
			queryValidate.setParameter("vchr_no", String.valueOf(fieldsInput.getVchrNo()));

			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			maintanenceResponse.output.rtnSts = 1;
			maintanenceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* GET specific THE DEFINE FIELDS LIST */
	public void viewRecord(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseRes, int ctlNo, String cmpyId) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			hql = " select ctl_no,gat_ctl_no,fl_pdf,fl_pdf_name,fl_txt,crt_dtts,prs_dtts, "
					+ " vchr_ctl_no,vchr_inv_no,vchr_ven_id,vchr_ven_nm,vchr_brh,vchr_amt,"
					+ " vchr_ext_ref,vchr_inv_dt,vchr_due_dt,vchr_pay_term, vchr_cry , upld_by, usr_nm,trn_rmk"
					+ " from cstm_doc_info doc_info , cstm_vchr_info vchr_info, usr_dtls_sls where" + " ctl_no=:ctl_no and "
					+ " usr_dtls_sls.usr_id = doc_info.upld_by and" + " doc_info.ctl_no = vchr_info.vchr_ctl_no";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ctl_no", ctlNo);
			CommonFunctions objCom = new CommonFunctions();
			List<Object[]> listCstmDocVal = queryValidate.list();
			GetCommonDAO objComDAO = new GetCommonDAO();

			for (Object[] cstmDocVal : listCstmDocVal) {
				VchrInfo cstmdocInfo = new VchrInfo();

				cstmdocInfo.setCtlNo((Integer) (cstmDocVal[0]));
				cstmdocInfo.setGatCtlNo((Integer) (cstmDocVal[1]));
				cstmdocInfo.setFlPdf(String.valueOf(cstmDocVal[2]));
				cstmdocInfo.setFlPdfName(String.valueOf(cstmDocVal[3]));
				cstmdocInfo.setFlTxt(String.valueOf(cstmDocVal[4]));

				if (cstmDocVal[5] != null) {
					cstmdocInfo.setCrtDttsStr((objCom.formatDateWithoutTime((Date) cstmDocVal[5])));
					cstmdocInfo.setCrtDtts((Date) (cstmDocVal[5]));
				}

				if (cstmDocVal[6] != null) {
					cstmdocInfo.setPrsDttsStr((objCom.formatDateWithoutTime((Date) cstmDocVal[6])));
					cstmdocInfo.setPrsDtts((Date) (cstmDocVal[6]));
				}
				cstmdocInfo.setVchrCtlNo((Integer) (cstmDocVal[7]));
				cstmdocInfo.setVchrInvNo(String.valueOf(cstmDocVal[8]));
				cstmdocInfo.setVchrVenId(String.valueOf(cstmDocVal[9]));
				cstmdocInfo.setVchrVenNm(String.valueOf(cstmDocVal[10]));
				cstmdocInfo.setVchrBrh(String.valueOf(cstmDocVal[11]));
				cstmdocInfo.setVchrAmt(Double.parseDouble(String.valueOf(cstmDocVal[12])));
				cstmdocInfo.setVchrExtRef(String.valueOf(cstmDocVal[13]));

				if (cstmDocVal[14] != null) {
					cstmdocInfo.setVchrInvDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[14])));
					cstmdocInfo.setVchrInvDt((Date) (cstmDocVal[14]));
				}

				if (cstmDocVal[15] != null) {
					cstmdocInfo.setVchrDueDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[15])));
					cstmdocInfo.setVchrDueDt((Date) (cstmDocVal[15]));
				}

				cstmdocInfo.setVchrPayTerm(String.valueOf(cstmDocVal[16]));
				cstmdocInfo.setVchrCry(String.valueOf(cstmDocVal[17]));
				cstmdocInfo.setUpldBy(String.valueOf(cstmDocVal[18]));
				cstmdocInfo.setUsrNm(String.valueOf(cstmDocVal[19]));
				cstmdocInfo.setTrnRmk(String.valueOf(cstmDocVal[20]));
				// GET VENDOR LIST
				//cstmdocInfo.setVendorList(objComDAO.getVendorList(String.valueOf(cstmDocVal[10]), cmpyId));

				String vchrNo = validateInvoice(cstmdocInfo.getVchrVenId(), cstmdocInfo.getVchrInvNo());

				getReconDetails(cmpyId, cstmdocInfo.getVchrVenId(), cstmdocInfo.getPoNo(), cstmdocInfo.getVchrCry(),
						cstmdocInfo.getVchrExtRef(), cstmdocInfo);

				List<ReconInfo> reconInfos = getReconDetails(session, ctlNo);

				List<GlInfo> glInfos = getGLDetails(session, ctlNo);

				cstmdocInfo.setCostRecon(reconInfos);
				cstmdocInfo.setGlData(glInfos);

				if (vchrNo.length() > 0) {
					cstmdocInfo.setReferenceVoucherNo(vchrNo);
				} else {
					cstmdocInfo.setReferenceVoucherNo("");
				}

				starBrowseRes.output.fldTblDoc.add(cstmdocInfo);
			}

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);

			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public List<ReconInfo> getReconDetails(Session session, int ctlNo) {
		String hql = "";
		List<ReconInfo> reconInfoList = new ArrayList<ReconInfo>();

		hql = " select * from cstm_vchr_recon_info where vchr_ctl_no=:ctl_no";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("ctl_no", ctlNo);
		CommonFunctions objCom = new CommonFunctions();
		List<Object[]> listRecon = queryValidate.list();

		for (Object[] reconInfo : listRecon) {

			ReconInfo recon = new ReconInfo();

			recon.setCrcnNo(Integer.parseInt(reconInfo[1].toString()));
			recon.setTrsNo(reconInfo[2].toString());
			recon.setPoNo(reconInfo[3].toString());
			recon.setCostNo(Integer.parseInt(reconInfo[4].toString()));
			recon.setBalAmt(Double.parseDouble(reconInfo[5].toString()));

			reconInfoList.add(recon);
		}

		return reconInfoList;

	}

	public List<GlInfo> getGLDetails(Session session, int ctlNo) {
		String hql = "";
		List<GlInfo> glInfos = new ArrayList<GlInfo>();

		hql = " select * from cstm_vchr_gl_info where vchr_ctl_no=:ctl_no";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("ctl_no", ctlNo);
		CommonFunctions objCom = new CommonFunctions();
		List<Object[]> listGL = queryValidate.list();

		for (Object[] gl : listGL) {

			GlInfo glInfo = new GlInfo();

			glInfo.setGlAcc(gl[1].toString());
			glInfo.setGlSubAcc(gl[2].toString());
			glInfo.setCrAmt(Double.parseDouble(gl[3].toString()));
			glInfo.setDrAmt(Double.parseDouble(gl[4].toString()));
			glInfo.setRemark(gl[5].toString());

			glInfos.add(glInfo);
		}

		return glInfos;
	}

	/* GET FIELDS FOR IN-PROCESS/PAID */
	public void isPrsOrPaidFields(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseRes, Boolean isPrs)
			throws Exception {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			hql = " select vchr_ven_nm,vchr_amt,vchr_inv_dt, is_prs,vchr_ven_id"
					+ " from cstm_doc_info doc_info , cstm_vchr_info vchr_info where" + " doc_info.is_prs=:is_prs and "
					+ " doc_info.ctl_no = vchr_info.vchr_ctl_no";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("is_prs", isPrs);
			CommonFunctions objCom = new CommonFunctions();
			List<Object[]> listCstmDocVal = queryValidate.list();

			for (Object[] cstmDocVal : listCstmDocVal) {
				VchrInfo cstmdocInfo = new VchrInfo();

				cstmdocInfo.setVchrVenNm(String.valueOf(cstmDocVal[0]));
				cstmdocInfo.setVchrAmt(Double.parseDouble(String.valueOf(cstmDocVal[1])));
				if (cstmDocVal[2] != null) {
					cstmdocInfo.setVchrInvDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[2])));
					cstmdocInfo.setVchrInvDt((Date) (cstmDocVal[2]));
				}
				cstmdocInfo.setIsPrs(Boolean.parseBoolean(String.valueOf(cstmDocVal[3])));
				cstmdocInfo.setVchrVenId(String.valueOf(cstmDocVal[4]));

				starBrowseRes.output.fldTblDoc.add(cstmdocInfo);
			}

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);

			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void delVchrInfo(int ctlNo, Session session) throws Exception {
		String hql = "";

		hql = "delete from cstm_vchr_info where vchr_ctl_no=:vchr_ctl_no";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("vchr_ctl_no", ctlNo);
		queryValidate.executeUpdate();
	}

	public void deleteDoc(MaintanenceResponse<CstmDocInfoManOutput> starManResponse, int ctlNo) {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		WkfMapDAO wkfMapDAO = new WkfMapDAO();

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			delVchrInfo(ctlNo, session);

			hql = "delete from cstm_doc_info where ctl_no=:ctl_no ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ctl_no", ctlNo);

			queryValidate.executeUpdate();

			wkfMapDAO.delWkfMap(session, ctlNo);
			wkfMapDAO.delWkfMapHis(session, ctlNo);

			tx.commit();

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void rejectDoc(MaintanenceResponse<CstmDocInfoManOutput> starManResponse, int ctlNo, String trnRmk) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "update cstm_doc_info set trn_sts =:trn_sts, trn_rmk =:trn_rmk where ctl_no=:ctl_no ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ctl_no", ctlNo);
			queryValidate.setParameter("trn_sts", "R");
			queryValidate.setParameter("trn_rmk", trnRmk);

			queryValidate.executeUpdate();

			tx.commit();

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public String validateInvoice(String venId, String invNo) {
		String vchrNo = "";

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(vch_vchr_pfx as varchar(2)) vchr_pfx, vch_vchr_no from aptvch_rec where 1=1"
					+ " AND vch_ven_id=:ven_id and vch_ven_inv_no=:inv_no";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ven_id", venId);
			queryValidate.setParameter("inv_no", invNo);

			List<Object[]> listVoucher = queryValidate.list();

			for (Object[] voucher : listVoucher) {

				vchrNo = voucher[0].toString() + "-" + voucher[1].toString();

			}
		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return vchrNo;
	}

	public void getReconDetails(String cmpyId, String venId, String poNo, String cry, String extRef,
			VchrInfo vchrInfo) {
		Session session = null;
		String hql = "";
		CommonFunctions objCom = new CommonFunctions();
		session = SessionUtilInformix.getSession();

		hql = " select count(*) rcrd_cnt, coalesce(sum(crh_balamt),0) bal_amt from irtcrh_rec where crh_ven_id=:ven_id "
				+ "and crh_cry=:cry and crh_cmpy_id=:cmpy  and crh_balamt<>0 and crh_balamt <>crh_ip_amt";

		if (extRef.length() > 0) {
			hql += " and crh_extl_ref=:extRef";
		}

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("ven_id", venId);
		queryValidate.setParameter("cry", cry);
		queryValidate.setParameter("cmpy", cmpyId);

		if (extRef.length() > 0) {
			queryValidate.setParameter("extRef", extRef);
		}

		List<Object[]> listReconInfo = queryValidate.list();

		for (Object[] voucher : listReconInfo) {

			vchrInfo.setReconCount(Integer.parseInt(voucher[0].toString()));
			vchrInfo.setReconAmt(Double.parseDouble(voucher[1].toString()));
			vchrInfo.setReconAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(voucher[1]))));

		}

	}

	public String validatePO(String poNo, String poItm) {
		String vendorNo = "";

		Session session = null;
		String hql = "";

		session = SessionUtilInformix.getSession();

		if (poNo.length() > 0 && poItm.length() == 0) {
			hql = " select cast(poh_ven_id as varchar(8)),1 from potpoh_rec where poh_po_pfx='PO' and poh_po_no=:po_no";
		}

		if (poNo.length() > 0 && poItm.length() > 0) {
			hql = " select cast(poh_ven_id as varchar(8)),1 from potpoh_rec, potpoi_Rec where poh_po_pfx = poi_po_pfx "
					+ "and poh_po_no = poi_po_no and poh_po_pfx='PO' and poh_po_no=:po_no and poi_po_itm=:po_itm";
		}

		Query queryValidate = session.createSQLQuery(hql);

		if (poNo.length() > 0 && poItm.length() == 0) {
			queryValidate.setParameter("po_no", Integer.parseInt(poNo));
		}

		if (poNo.length() > 0 && poItm.length() > 0) {
			queryValidate.setParameter("po_no", Integer.parseInt(poNo));
			queryValidate.setParameter("po_itm", Integer.parseInt(poItm));
		}

		List<Object[]> listVoucher = queryValidate.list();

		for (Object[] voucher : listVoucher) {

			vendorNo = voucher[0].toString();

		}

		return vendorNo;
	}

	public String getVoucherList(List<CstmParamInv> cstmParamInvs) {

		String voucherList = "";

		for (int i = 0; i < cstmParamInvs.size(); i++) {

			voucherList = voucherList + cstmParamInvs.get(i).getInvNo() + ",";
		}

		if (voucherList.length() > 0) {
			voucherList = voucherList.substring(0, voucherList.length() - 1);
		}

		return voucherList;

	}

	public void updateDocStatus(MaintanenceResponse<CstmDocInfoManOutput> starManResponse, int ctlNo, String trnSts,
			String rmk) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "update cstm_doc_info set ";

			if (trnSts.length() == 0) {
				hql += " trn_sts = NULL, ";
			} else {
				hql += " trn_sts =:trn_sts, ";
			}

			hql += " trn_rmk =:trn_rmk where ctl_no=:ctl_no ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ctl_no", ctlNo);
			if (trnSts.length() > 0) {
				queryValidate.setParameter("trn_sts", trnSts);
			}
			queryValidate.setParameter("trn_rmk", rmk);

			queryValidate.executeUpdate();

			tx.commit();

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
