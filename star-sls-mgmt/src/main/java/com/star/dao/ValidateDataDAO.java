package com.star.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.CommonFunctions;
import com.star.common.SessionUtilInformix;

public class ValidateDataDAO {

	private static Logger logger = LoggerFactory.getLogger(ValidateDataDAO.class);
	
	
	public List<String> validateMetalStandard(String sdo, String stdId, String addnlId) {
		Session session = null;
		String hql = "";
		int mssCtlNo = 0;
		String sdoOut ="";
		String stdIdOut = "";
		String addtnIdOut ="";
		List<String> mtlStdLst = new ArrayList<String>();
		
		sdo = sdo.trim();
		stdId = stdId.trim();
		addnlId = addnlId.trim();
		
		try {
			session = SessionUtilInformix.getSession();
			
			hql = "SELECT mss_mss_ctl_no, cast(mss_sdo as varchar(6)),  cast(mss_std_id as varchar(20)),  cast(mss_addnl_id as varchar(20)) , 1 FROM "
					+ "mcrmss_rec WHERE mss_sdo=:sdo and :stdId like '%'|| mss_std_id || '%' and :stdId like '%'|| mss_addnl_id || '%'  order by length(mss_std_id) desc limit 1";
			
			//hql = "SELECT mss_mss_ctl_no, cast(mss_sdo as varchar(6)),  cast(mss_std_id as varchar(20)),  cast(mss_addnl_id as varchar(20)) , 1 FROM " + "mcrmss_rec WHERE mss_sdo || ' '|| mss_std_id || ' ' || mss_addnl_id like '%" + sdo +" "+ stdId + " " + addnlId + "%' order by length(mss_std_id) desc limit 1";
			
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("sdo", sdo);
			queryValidate.setParameter("stdId", stdId);
			//queryValidate.setParameter("addnlId", addnlId);
			
			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				mssCtlNo = Integer.parseInt(row[0].toString());
				
				mtlStdLst.add(CommonFunctions.checkNull(row[1].toString()));
				mtlStdLst.add(CommonFunctions.checkNull(row[2].toString()));
				mtlStdLst.add(CommonFunctions.checkNull(row[3].toString()));
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		
		return mtlStdLst;
	}
	
}
