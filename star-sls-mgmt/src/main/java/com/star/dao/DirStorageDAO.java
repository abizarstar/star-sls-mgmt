package com.star.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.linkage.docstorage.StorageInput;
import com.star.linkage.docstorage.StorageManOutput;
import com.star.modal.DocDirConfig;

public class DirStorageDAO {

	private static Logger logger = LoggerFactory.getLogger(DirStorageDAO.class);
	
	public DocDirConfig getStorgae() 
	{
		Session session= null;
		
		DocDirConfig config = null;
		
		try
		{
			session = SessionUtil.getSession();
			
			String hql = "";
			
			hql = " select * from doc_dir_config ";
			
			Query queryValidate= session.createSQLQuery(hql);

			List<Object[]> storageLst =  queryValidate.list();

			for (Object[] storage : storageLst) {
			
				config = new DocDirConfig();
				
				if(storage[0] != null)
				{
					config.setDirId(Integer.parseInt(storage[0].toString()));
				}
				
				if(storage[1] != null)
				{
					config.setS3AccessKey(storage[1].toString());
				}
				
				if(storage[2] != null)
				{
					config.setS3SecretKey(storage[2].toString());
				}
				
				if(storage[3] != null)
				{
					config.setS3Region(storage[3].toString());
				}
				
				if(storage[4] != null)
				{
					config.setS3BktNm(storage[4].toString());
				}
				
				if(storage[5] != null)
				{
					config.setRootPath(storage[5].toString());
				}
				
				if(storage[6] != null)
				{
					config.setDirType(storage[6].toString());
				}
			}
			
		}
		catch (Exception e) {
			
			logger.debug("Directory Storage Info : {}", e.getMessage(), e);
		}
		finally{
        	if(session!=null){
        		session.close();
        	}
        }
		
		return config;
	}
	
	public void updStorage(MaintanenceResponse<StorageManOutput> starManResponse, StorageInput input) 
	{
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "update doc_dir_config set s3_accs_ky = :accssKy , s3_scrt_ky=:scrtKy, s3_rgn=:rgn, "
					+ "s3_bkt_nm=:bktNm, root_pth=:rootPth, dir_typ=:typ where dir_id=:id" ;

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("id", input.getDirId());
			queryValidate.setParameter("accssKy", input.getAccessKey());
			queryValidate.setParameter("scrtKy", input.getSecretKey());
			queryValidate.setParameter("rgn", input.getRegion());
			queryValidate.setParameter("bktNm", input.getBucketName());
			queryValidate.setParameter("rootPth", input.getRootPath());
			queryValidate.setParameter("typ", input.getStorageType());

			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {

			logger.debug("Directory Storage Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		
	}
	
	
	public void insStorage(MaintanenceResponse<StorageManOutput> starManResponse, StorageInput input) 
	{
		Session session = null;
		Transaction tx = null;
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			DocDirConfig config = new DocDirConfig();
			
			config.setS3AccessKey(input.getAccessKey());
			config.setS3SecretKey(input.getSecretKey());
			config.setS3Region(input.getRegion());
			config.setS3BktNm(input.getBucketName());
			config.setRootPath(input.getRootPath());
			config.setDirType(input.getStorageType());
			
			session.save(config);
			
			tx.commit();
		} catch (Exception e) {

			logger.debug("Directory Storage Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		
	}
	
}
