package com.star.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.linkage.wkf.WkfAprvr;
import com.star.linkage.wkf.WorkflowBrowseOutput;
import com.star.linkage.wkf.WorkflowInfo;
import com.star.linkage.wkf.WorkflowManOutput;

public class WorkflowDAO {

	private static Logger logger = LoggerFactory.getLogger(WorkflowDAO.class);

	CommonFunctions cmn = new CommonFunctions();

	public void readWorkflow(BrowseResponse<WorkflowBrowseOutput> starBrowseResponse, String wkfId) {

		logger.info("AP Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());

		Session session = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			hql = "SELECT W.wkf_id, W.wkf_name, W.crtd_dtts, W.crtd_by, S.step_id, S.step_name, S.amt_flg, S.brh_flg, S.ven_flg, S.day_flg, "
					+ " S.amt_frm, S.amt_to, S.brh, S.ven_id, S.days, S.day_bef, S.apvr, S.crcn_flg,"
					+ "(select count(*) from cstm_wkf_map where wkf_id=S.wkf_id and wkf_sts not in ('A', 'R')) pendg_doc FROM cstm_ocr_wkf W, cstm_ocr_wkf_step S "
					+ " WHERE deleted_dtts is null AND W.wkf_id=S.wkf_id ";

			if (wkfId.trim().length() > 0) {
				hql += " AND W.wkf_id=:wkf_id";
			}

			hql += " ORDER BY W.crtd_dtts DESC";

			Query queryValidate = session.createSQLQuery(hql);

			if (wkfId.trim().length() > 0) {
				queryValidate.setParameter("wkf_id", Integer.parseInt(wkfId));
			}

			List<Object[]> listRows = queryValidate.list();

			for (Object[] row : listRows) {

				WorkflowInfo output = new WorkflowInfo();
				if (row[0] != null) {
					output.setWkfId(row[0].toString());
				}
				if (row[1] != null) {
					output.setWkfName(row[1].toString());
				}
				if (row[2] != null) {
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
					String formatDate = formatter.format((Date) row[2]);

					output.setCrtdDtts(formatDate);
				} else {
					output.setCrtdDtts("");
				}
				if (row[3] != null) {
					output.setCrtdBy(row[3].toString());
				}
				if (row[4] != null) {
					output.setStpId(row[4].toString());
				}
				if (row[5] != null) {
					output.setStpNm(row[5].toString());
				}
				if (row[6] != null) {
					output.setAmtFlg(row[6].toString());
				}
				if (row[7] != null) {
					output.setBrhFlg(row[7].toString());
				}
				if (row[8] != null) {
					output.setVenFlg(row[8].toString());
				}
				if (row[9] != null) {
					output.setDayFlg(row[9].toString());
				}
				if (row[10] != null) {
					output.setAmtFrm(cmn.formatAmount(Double.parseDouble(row[10].toString())));
				}
				if (row[11] != null) {
					output.setAmtTo(cmn.formatAmount(Double.parseDouble(row[11].toString())));
				}
				if (row[12] != null) {
					output.setBrh(row[12].toString());
				}
				if (row[13] != null) {
					output.setVenId(row[13].toString());
				}
				if (row[14] != null) {
					output.setDays(row[14].toString());
				}
				if (row[15] != null) {
					output.setDayBef(row[15].toString());
				}
				if (row[16] != null) {
					output.setApvr(row[16].toString());
				}
				if (row[17] != null) {
					output.setCostRecon(row[17].toString());
				}

				output.setPndgDoc(row[18].toString());
				
				output.setWkfAprvrList(readWorkflowAprvr(session, output.getWkfId()));

				starBrowseResponse.output.fldTblWorkflow.add(output);
				
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("Workflow Info : {}", e.getMessage(), e);

			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public List<WkfAprvr> readWorkflowAprvr(Session session, String wkfId) throws Exception{

		String hql = "";
		
		List<WkfAprvr> aprvrList= new ArrayList<WkfAprvr>();
		
		session = SessionUtil.getSession();

		hql = "select wkf_aprv_ordr, wkf_aprvr, (select usr_nm from usr_dtls_sls where usr_id=wkf_aprvr) usr_nm from cstm_wkf_aprvr where 1=1 ";

		if (wkfId.trim().length() > 0) {
			hql += " AND wkf_id=:wkf";
		}

		hql += " ORDER BY wkf_aprv_ordr ASC";

		Query queryValidate = session.createSQLQuery(hql);

		if (wkfId.trim().length() > 0) {
			queryValidate.setParameter("wkf", Integer.parseInt(wkfId));
		}
		
		List<Object[]> listRows = queryValidate.list();

		for (Object[] row : listRows) {

			WkfAprvr output = new WkfAprvr();
			if (row[0] != null) {
				output.setOrder(Integer.parseInt(row[0].toString()));
			}
			
			if (row[1] != null) {
				output.setUsrId(row[1].toString());
			}
			else
			{
				output.setUsrId("");
			}
			
			if (row[2] != null) {
				output.setUsrNm(row[2].toString());
			}
			else
			{
				output.setUsrNm("");
			}
			
			aprvrList.add(output);
		}
		
		return aprvrList;
	}

	public void deleteWorkflow(MaintanenceResponse<WorkflowManOutput> starManResponse, String wkfId, String userId) {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "UPDATE cstm_ocr_wkf SET deleted_dtts=NOW(), deleted_by=:user_id WHERE wkf_id=:wkf_id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("user_id", userId);
			queryValidate.setParameter("wkf_id", Integer.parseInt(wkfId));

			queryValidate.executeUpdate();

			tx.commit();

		} catch (Exception e) {
			logger.debug("Workflow Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());

			tx.rollback();
		} finally {

			session.close();
		}
	}

}
