package com.star.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.linkage.cstmdocinfo.VchrInfo;
import com.star.linkage.wkf.WkfHisBrowseOutput;
import com.star.linkage.wkf.WkfMapManOutput;
import com.star.linkage.wkf.WorkflowMappingInfo;
import com.star.modal.CstmWkfMap;
import com.star.modal.CstmWkfMapHis;
import com.star.modal.WorkflowStep;

public class WkfMapDAO {

	private static Logger logger = LoggerFactory.getLogger(WkfMapDAO.class);

	public List<WorkflowStep> getWorkflowDetails() {
		Session session = null;

		List<WorkflowStep> workflowSteps = new ArrayList<WorkflowStep>();

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select step_id, wkf_id, step_name, amt_flg, brh_flg, "
					+ "ven_flg, day_flg, amt_frm, amt_to, brh, ven_id, days, day_bef, "
					+ "apvr, crcn_flg, (amt_flg + brh_flg + ven_flg + day_flg) AS tot_cond from cstm_ocr_wkf_step "
					+ "ORDER BY tot_cond ASC, day_bef DESC, day_flg DESC, ven_flg DESC, brh_flg DESC, amt_flg DESC, amt_frm desc";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listWkf = queryValidate.list();

			for (Object[] wkf : listWkf) {
				WorkflowStep workflow = new WorkflowStep();

				workflow.setStepId(Integer.parseInt(wkf[0].toString()));
				workflow.setWkfId(Integer.parseInt(wkf[1].toString()));
				workflow.setStepName(wkf[2].toString());
				workflow.setAmtFlg(Integer.parseInt(wkf[3].toString()));
				workflow.setBrhFlg(Integer.parseInt(wkf[4].toString()));
				workflow.setVenFlg(Integer.parseInt(wkf[5].toString()));
				workflow.setDayFlg(Integer.parseInt(wkf[6].toString()));
				workflow.setAmtFrm(Float.parseFloat(wkf[7].toString()));
				workflow.setAmtTo(Float.parseFloat(wkf[8].toString()));
				workflow.setBrh(wkf[9].toString());
				workflow.setVenId(wkf[10].toString());
				workflow.setDays(Integer.parseInt(wkf[11].toString()));

				if (wkf[12] != null) {
					workflow.setDayBef(Integer.parseInt(wkf[12].toString()));
				}

				workflow.setApvr(wkf[13].toString());
				workflow.setCrcnFlg(Integer.parseInt(wkf[14].toString()));

				workflowSteps.add(workflow);

			}

		} catch (Exception e) {

			logger.debug("Workflow Map Information : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return workflowSteps;
	}

	public void addWkfMap(int ctlNo, int wkfId, String usrId) {
		Session session = null;
		Transaction tx = null;
		Date date = new Date();
		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			CstmWkfMap cstmWkfMap = new CstmWkfMap();
			cstmWkfMap.setCtlNo(ctlNo);
			cstmWkfMap.setWkfId(wkfId);
			cstmWkfMap.setWkfSts("S");
			cstmWkfMap.setWkfAsgnOn(date);
			cstmWkfMap.setWkfAsgnTo(usrId);
			cstmWkfMap.setWkfRmk("Workflow Added");

			session.save(cstmWkfMap);

			addWkfMapHis(session, cstmWkfMap);

			tx.commit();
		} catch (Exception e) {

			logger.debug("Workflow Mapping Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	/* UPDATE WORKFLOW MAPPING */
	public void updateWkfMap(MaintanenceResponse<WkfMapManOutput> starManResponse, CstmWkfMap cstmWkfMap) {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		Date date = new Date();
		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			if (cstmWkfMap.getWkfAsgnTo().length() > 0) {
				hql = "update cstm_wkf_map set wkf_sts = :sts, wkf_asgn_to = :asgn_to , wkf_asgn_on=:asgn_on, wkf_rmk=:rmk"
						+ " where ctl_no=:ctl and wkf_id=:wkf";
			} else {
				hql = "update cstm_wkf_map set wkf_sts = :sts, wkf_asgn_on=:asgn_on, wkf_rmk=:rmk"
						+ " where ctl_no=:ctl and wkf_id=:wkf";
			}

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("sts", cstmWkfMap.getWkfSts());
			if (cstmWkfMap.getWkfAsgnTo().length() > 0) {
				queryValidate.setParameter("asgn_to", cstmWkfMap.getWkfAsgnTo());
			}
			queryValidate.setParameter("asgn_on", date);
			queryValidate.setParameter("ctl", cstmWkfMap.getCtlNo());
			queryValidate.setParameter("wkf", cstmWkfMap.getWkfId());
			queryValidate.setParameter("rmk", cstmWkfMap.getWkfRmk());

			queryValidate.executeUpdate();

			cstmWkfMap.setWkfAsgnOn(date);

			addWkfMapHis(session, cstmWkfMap);

			tx.commit();
		} catch (Exception e) {

			logger.debug("Workflow Mapping Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void addWkfMapHis(Session session, CstmWkfMap cstmWkfMap) {

		CstmWkfMapHis cstmWkfMapHis = new CstmWkfMapHis();
		cstmWkfMapHis.setCtlNo(cstmWkfMap.getCtlNo());
		cstmWkfMapHis.setWkfId(cstmWkfMap.getWkfId());
		cstmWkfMapHis.setWkfSts(cstmWkfMap.getWkfSts());
		cstmWkfMapHis.setWkfAsgnOn(cstmWkfMap.getWkfAsgnOn());
		cstmWkfMapHis.setWkfAsgnTo(cstmWkfMap.getWkfAsgnTo());
		cstmWkfMapHis.setWkfRmk(cstmWkfMap.getWkfRmk());

		session.save(cstmWkfMapHis);
	}

	public void getWorkflowHistory(BrowseResponse<WkfHisBrowseOutput> starBrowseResponse, String wkfId, String ctlNo) {
		String hql = "";
		Session session = null;
		CommonFunctions objCom = new CommonFunctions();
		try {

			session = SessionUtil.getSession();

			hql = "select wkf_asgn_on, (select usr_nm from usr_dtls_sls where usr_id=wkf_asgn_to) wkf_asgn_nm, wkf_sts, wkf_rmk from cstm_wkf_map_his where 1=1";

			if (wkfId.length() > 0) {
				hql += " and wkf_id=:wkf";
			}

			if (ctlNo.length() > 0) {
				hql += " and ctl_no=:ctl_no";
			}

			hql += " order by wkf_asgn_on asc";

			Query queryValidate = session.createSQLQuery(hql);

			if (wkfId.length() > 0) {
				queryValidate.setParameter("wkf", Integer.parseInt(wkfId));
			}

			if (ctlNo.length() > 0) {
				queryValidate.setParameter("ctl_no", Integer.parseInt(ctlNo));
			}

			List<Object[]> listWkfHis = queryValidate.list();

			for (Object[] wkf : listWkfHis) {
				WorkflowMappingInfo mappingInfo = new WorkflowMappingInfo();

				if (wkf[0] != null) {
					mappingInfo.setAsgnOnDt((Date) wkf[0]);
					mappingInfo.setAsgnOnDtStr((objCom.formatDate((Date) wkf[0])));
				}

				mappingInfo.setAsgnToNm(wkf[1].toString());
				mappingInfo.setSts(wkf[2].toString());

				if (mappingInfo.getSts().equals("S")) {
					mappingInfo.setSts("In Review");
				} else if (mappingInfo.getSts().equals("R")) {
					mappingInfo.setSts("Rejected");
				} else if (mappingInfo.getSts().equals("A")) {
					mappingInfo.setSts("Approved");
				} else if (mappingInfo.getSts().equals("B")) {
					mappingInfo.setSts("Sent Back");
				}else {
					mappingInfo.setSts("");
				}

				mappingInfo.setRmk(wkf[3].toString());
				
				WorkflowDAO workflowDAO = new WorkflowDAO();
				
				mappingInfo.setWkfAprvrList(workflowDAO.readWorkflowAprvr(session, wkfId));

				starBrowseResponse.output.fldTblWkHistory.add(mappingInfo);

			}

		} catch (Exception e) {

			logger.debug("Workflow Map Information : {}", e.getMessage(), e);
		} finally {
			session.close();
		}
	}

	/* GET WORK FLOW DETAILS */
	public void getWorkflowSpecific(String ctlNo, VchrInfo vchrInfo) {
		String hql = "";
		Session session = null;

		try {

			session = SessionUtil.getSession();

			hql = "select  map.wkf_id, wkf_name, wkf_sts,wkf_asgn_to, (select usr_nm from usr_dtls_sls where usr_id=wkf_asgn_to) wkf_asgn_nm"
					+ " from cstm_wkf_map map, cstm_ocr_wkf ocr where map.wkf_id=ocr.wkf_id";

			if (ctlNo.length() > 0) {
				hql += " and ctl_no=:ctl_no";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (ctlNo.length() > 0) {
				queryValidate.setParameter("ctl_no", Integer.parseInt(ctlNo));
			}

			List<Object[]> listWkfHis = queryValidate.list();

			for (Object[] wkf : listWkfHis) {

				vchrInfo.setWrkFlwId(Integer.parseInt(wkf[0].toString()));
				vchrInfo.setWrkFlw(wkf[1].toString());
				vchrInfo.setWrkFlwSts(wkf[2].toString());
				vchrInfo.setWrkFlwAprvr(wkf[3].toString());
				vchrInfo.setWrkFlwAprvrNm(wkf[4].toString());
			}

			if (listWkfHis.size() == 0) {
				vchrInfo.setWrkFlwId(0);
				vchrInfo.setWrkFlw("");
				vchrInfo.setWrkFlwSts("");
				vchrInfo.setWrkFlwAprvr("");
			}
			
			

		} catch (Exception e) {

			logger.debug("Workflow Map Information : {}", e.getMessage(), e);
		} finally {
			session.close();
		}
	}

	public void delWkfMap(Session session, int ctlNo) throws Exception {
		String hql = "";

		hql = "delete from cstm_wkf_map where ctl_no=:ctl_no";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("ctl_no", ctlNo);
		queryValidate.executeUpdate();
	}

	public void delWkfMapHis(Session session, int ctlNo) throws Exception {
		String hql = "";

		hql = "delete from cstm_wkf_map_his where ctl_no=:ctl_no";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("ctl_no", ctlNo);
		queryValidate.executeUpdate();
	}

}
