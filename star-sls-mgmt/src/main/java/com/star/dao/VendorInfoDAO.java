package com.star.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.SessionUtil;
import com.star.common.SessionUtilInformix;
import com.star.linkage.vendor.CompanyBrowseOutput;
import com.star.linkage.vendor.CompanyInfo;
import com.star.linkage.vendor.VendorBrowseOutput;
import com.star.linkage.vendor.VendorDefaultGlSettingInput;
import com.star.linkage.vendor.VendorDefaultSettingInput;
import com.star.linkage.vendor.VendorInfo;
import com.star.linkage.vendor.VendorInput;

public class VendorInfoDAO {

	private static Logger logger = LoggerFactory.getLogger(UserDAO.class);

	public void readVendorList(BrowseResponse<VendorBrowseOutput> starBrowseResponse, String cmpyId) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(ven_cmpy_id as varchar(3)) cmpy_id, cast(ven_ven_id as varchar(8)) ven_id, "
					+ "cast(ven_ven_nm as varchar(15)) ven_nm, cast(ven_ven_long_nm as varchar(35)) ven_long_nm, "
					+ "cast(ven_cry as varchar(3))cry, cast(ven_pmt_typ as varchar(1)) pmt_typ, "
					+ "cast(ven_admin_brh as varchar(3)) admin_brh from aprven_rec where ven_cmpy_id=:cmpy";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy", cmpyId);

			List<Object[]> listVendor = queryValidate.list();

			for (Object[] vendor : listVendor) {

				VendorInfo output = new VendorInfo();

				output.setCmpyId(vendor[0].toString());
				output.setVenId(vendor[1].toString());
				output.setVenNm(vendor[2].toString());
				output.setVenLongNm(vendor[3].toString());
				output.setVenCry(vendor[4].toString());
				output.setPmtTyp(vendor[5].toString());
				output.setVenAdminBrh(vendor[6].toString());

				starBrowseResponse.output.fldTblVendor.add(output);

			}

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			session.close();
		}

	}

	public void readCompanyList(BrowseResponse<CompanyBrowseOutput> starBrowseResponse) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(csc_cmpy_id as varchar(3)) cmpy_id, cast(csc_co_nm as varchar(35)) co_nm from scrcsc_rec";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listCompany = queryValidate.list();

			for (Object[] company : listCompany) {

				CompanyInfo output = new CompanyInfo();

				output.setCmpyId(company[0].toString());
				output.setCmpyNm(company[1].toString());
				starBrowseResponse.output.fldTblCompany.add(output);

			}

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			session.close();
		}

	}

	public int validateVendor(VendorInput vendorInput) {

		Session session = null;
		String hql = "";
		int ircrdCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = "select count(*) from cstm_ven_add_info where cmpy_id=:cmpy and ven_id=:ven";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy", vendorInput.getCmpyId());
			queryValidate.setParameter("ven", vendorInput.getVenId());

			ircrdCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return ircrdCount;
	}

	public int validateVendorAccount(String cmpyId, String venId, String accNo) {

		Session session = null;
		String hql = "";
		int ircrdCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = "select count(*) from cstm_ven_bnk_info where cmpy_id=:cmpy and ven_id=:ven and bnk_acc_no=:acc_no";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy", cmpyId);
			queryValidate.setParameter("ven", venId);
			queryValidate.setParameter("acc_no", accNo);

			ircrdCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return ircrdCount;
	}

	public void readStxVendorDftSetting(VendorDefaultSettingInput output, String cmpyId, String venId, String userId) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(pyt_desc30 as varchar(30)) pyt_trm, "
					+ "cast((select ven_cry from aprven_rec where ven_cmpy_id=:cmpy and ven_ven_id=:ven) as varchar(3)) ven_cry,  "
					+ " cast((select usr_usr_brh from mxrusr_rec where usr_lgn_id=:usr) as varchar(3)) usr_brh,"
					+ " cast((select ven_admin_brh from aprven_rec where ven_cmpy_id=:cmpy and ven_ven_id=:ven) as varchar(3)) usr_brh_adm"
					+ " from aprshf_rec, scrpyt_rec where shf_pttrm = pyt_pttrm and shf_shp_fm=0 ";

			if (cmpyId.length() > 0) {
				hql = hql + " AND shf_cmpy_id = :cmpy ";
			}

			if (venId.length() > 0) {
				hql = hql + " and shf_ven_id=:ven";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (cmpyId.length() > 0) {
				queryValidate.setParameter("cmpy", cmpyId);
			}

			if (venId.length() > 0) {
				queryValidate.setParameter("ven", venId);
			}
			
			queryValidate.setParameter("usr", userId);

			List<Object[]> listVndrDfltStng = queryValidate.list();

			for (Object[] vndrDfltStng : listVndrDfltStng) {

				if(vndrDfltStng[0] != null)
				{
					output.setPayTerm(vndrDfltStng[0].toString());
				}
				else
				{
					output.setPayTerm("");
				}
				
				if(vndrDfltStng[1] != null)
				{
					output.setCry(vndrDfltStng[1].toString());
				}
				else
				{
					output.setCry("");
				}
				
				if(vndrDfltStng[2] != null)
				{
					output.setVchrBrh(vndrDfltStng[2].toString());
				}
				else
				{
					output.setVchrBrh("");
				}
				
				if(vndrDfltStng[3] != null)
				{
					if(output.getVchrBrh().length() == 0)
					{
						output.setVchrBrh(vndrDfltStng[3].toString());
					}
				}

			}

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public void readVendorGlData(VendorDefaultSettingInput vendorOuput, String cmpyId, String venId, Session session) {
		String hql = "";

		try {

			hql = " SELECT * FROM cstm_ocr_vndr_gl_setg WHERE 1=1";

			if (cmpyId.length() > 0 && venId.length() > 0) {
				hql = hql + " AND cmpy_id = :cmpy AND ven_id=:ven";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (cmpyId.length() > 0 && venId.length() > 0) {
				queryValidate.setParameter("cmpy", cmpyId);
				queryValidate.setParameter("ven", venId);
			}

			List<Object[]> listGlData = queryValidate.list();

			List<VendorDefaultGlSettingInput> vndrGlData = new ArrayList<VendorDefaultGlSettingInput>();

			for (Object[] glData : listGlData) {

				VendorDefaultGlSettingInput output = new VendorDefaultGlSettingInput();

				output.setCmpyId(glData[0].toString());
				output.setVenId(glData[1].toString());
				output.setBscGlAcct(glData[2].toString());
				output.setSacct(glData[3].toString());
				output.setSacctTxt(glData[4].toString());
				output.setDistRmk(glData[5].toString());

				vndrGlData.add(output);

			}

			vendorOuput.setGlAcctList(vndrGlData);

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);

		}

	}

}
