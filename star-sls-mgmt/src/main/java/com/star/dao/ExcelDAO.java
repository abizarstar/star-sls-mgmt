package com.star.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.SessionUtil;
import com.star.common.SessionUtilInformix;
import com.star.linkage.excel.Excel;
import com.star.linkage.excel.ProductInfo;
import com.star.linkage.excel.ProductSpecs;

public class ExcelDAO {

	private static Logger logger = LoggerFactory.getLogger(ExcelDAO.class);
	
	public String getCusIdByName(String cusNm) {
		String rtnStr = "";
		Session session = null;
		String hql = "SELECT cast(cus_cus_id as varchar(8)) cus_id , 1 FROM arrcus_rec WHERE cus_actv=1 AND "
				+ "(LOWER(cus_cus_nm) like LOWER('%"+ cusNm +"%') OR LOWER(cus_cus_id)=LOWER('"+ cusNm +"')) and cus_cus_acct_typ <> 'L'  LIMIT 1";

		session = SessionUtilInformix.getSession();

		Query queryValidate = session.createSQLQuery(hql);

		List<Object[]> rows = queryValidate.list();

		for (Object[] row : rows) {

			rtnStr = row[0].toString();

		}

		return rtnStr;
	}
	
	public String getShpToByName(Integer cusId, String shpToNm) {
		String rtnStr = "";
		Session session = null;
		String hql = "SELECT shp_shp_to, cast(trim(shp_shp_to_nm) as varchar(15)) FROM arrshp_rec WHERE shp_actv=1 "
				+ "AND shp_cus_id='"+ cusId +"' AND LOWER(shp_shp_to_nm) like LOWER('%"+ shpToNm +"%') LIMIT 1";

		try {
			session = SessionUtilInformix.getSession();
	
			Query queryValidate = session.createSQLQuery(hql);
	
			List<Object[]> rows = queryValidate.list();
	
			for (Object[] row : rows) {
	
				rtnStr = row[0].toString() + "-" + row[1].toString();
	
			}
		} catch (Exception e) {
			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return rtnStr;
	}
	
	public String getShpToByCusId(Integer cusId, String shpTo) {
		String rtnStr = "";
		Session session = null;
		String hql = "SELECT shp_shp_to, cast(trim(shp_shp_to_nm) as varchar(15)) FROM arrshp_rec WHERE shp_actv=1 AND "
				+ "shp_cus_id='"+ cusId +"' AND shp_shp_to="+ Integer.parseInt(shpTo) +" LIMIT 1";

		try {
			session = SessionUtilInformix.getSession();
	
			Query queryValidate = session.createSQLQuery(hql);
	
			List<Object[]> rows = queryValidate.list();
	
			for (Object[] row : rows) {
	
				rtnStr = row[0].toString() + "-" + row[1].toString();
	
			}
		} catch (Exception e) {
			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return rtnStr;
	}
	
	
	public String getSalesCategoryByName(String shpToNm) {
		String rtnStr = "";
		Session session = null;		
		String hql = "SELECT cast(trim(sca_sls_cat) as varchar(2)) sca_sls_cat, 1 FROM orrsca_rec WHERE sca_actv=1 AND "
				+ "(LOWER(sca_desc30) like LOWER('%"+ shpToNm +"%') OR LOWER(sca_sls_cat)=LOWER('"+ shpToNm +"')) LIMIT 1";

		try {
			session = SessionUtilInformix.getSession();
	
			Query queryValidate = session.createSQLQuery(hql);
	
			List<Object[]> rows = queryValidate.list();
	
			for (Object[] row : rows) {
	
				rtnStr = row[0].toString();
	
			}
		} catch (Exception e) {
			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return rtnStr;
	}
	
	public String getSalesPersonByName(String slsPrsnNm) {
		String rtnStr = "";
		Session session = null;
		String hql = "SELECT cast(trim(slp_slp) as varchar(4)) slp_slp, 1 FROM scrslp_rec, mxrusr_rec "
				+ " WHERE slp_actv=1 AND usr_actv=1 AND usr_lgn_id=slp_lgn_id "
				+ "AND (LOWER(usr_nm) like LOWER('%"+ slsPrsnNm +"%') OR LOWER(slp_slp)=LOWER('"+ slsPrsnNm +"')) LIMIT 1";

		try {
			session = SessionUtilInformix.getSession();
	
			Query queryValidate = session.createSQLQuery(hql);
	
			List<Object[]> rows = queryValidate.list();
	
			for (Object[] row : rows) {
	
				rtnStr = row[0].toString();
	
			}
		} catch (Exception e) {
			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return rtnStr;
	}
	
	public String getSalesBranchByName(String brhNm) {
		String rtnStr = "";
		Session session = null;

		String hql = "SELECT cast(trim(brh_brh) as varchar(3)) brh_brh, 1 FROM scrbrh_rec WHERE "
				+ "(LOWER(brh_brh_nm) like LOWER('%"+ brhNm +"%') OR LOWER(brh_brh)=LOWER('"+ brhNm +"')) LIMIT 1";

		try {
			session = SessionUtilInformix.getSession();
	
			Query queryValidate = session.createSQLQuery(hql);
	
			List<Object[]> rows = queryValidate.list();
	
			for (Object[] row : rows) {
	
				rtnStr = row[0].toString();
	
			}
		} catch (Exception e) {
			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return rtnStr;
	}
	
	public String getCPSInfo(String cps, ProductInfo prdInfo, String cmpyId) {
		Session session = null;
		String hql = "SELECT cast(trim(ppd_frm) as varchar(6)) ppd_frm, cast(trim(ppd_grd) as varchar(8)) ppd_grd, " + 
				" cast(trim(ppd_size) as varchar(15)) ppd_size, cast(trim(ppd_fnsh) as varchar(8)) ppd_fnsh, " + 
				" ppd_wdth, ppd_lgth, ppd_ga_size, cast(trim(ppd_ga_typ) as varchar(1)) ppd_ga_typ, "+
				" cast(trim(cpr_sls_cat) as varchar(2)) cpr_sls_cat, cast(trim(cpr_src) as varchar(2)) cpr_src, " + 
				" cast(trim(cpr_admin_brh) as varchar(3)) cpr_admin_brh FROM cprclg_rec, cprppd_rec, cprcpr_rec WHERE " + 
				" clg_cmpy_id=cpr_cmpy_id AND clg_cmpy_id=ppd_cmpy_id AND " + 
				" clg_part_ctl_no=cpr_part_ctl_no AND clg_part_ctl_no=ppd_part_ctl_no AND " + 
				" clg_actv=1 AND ppd_actv=1 AND cpr_actv=1 AND clg_cus_ven_typ='C' AND clg_cmpy_id=:cmpy_id " + 
				" AND trim(clg_part)=:cps limit 1";

		try {
			session = SessionUtilInformix.getSession();
	
			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("cmpy_id", cmpyId);
			queryValidate.setParameter("cps", cps);
	
			List<Object[]> rows = queryValidate.list();
			
			if(rows.size() > 0) {
				for (Object[] row : rows) {					
					prdInfo.setFlg(1);
					prdInfo.setCps(cps);
					if(row[0] != null) {
						prdInfo.setFrm(row[0].toString());
					}
					if(row[1] != null) {
						prdInfo.setGrd(row[1].toString());
					}
					if(row[2] != null) {
						prdInfo.setSize(row[2].toString());
					}
					if(row[3] != null) {
						prdInfo.setFnsh(row[3].toString());
					}
					if(row[4] != null) {
						prdInfo.setWdth(row[4].toString());
					}
					if(row[5] != null) {
						prdInfo.setLgth(row[5].toString());
					}
					if(row[6] != null) {
						prdInfo.setGaSize(row[6].toString());
					}
					if(row[7] != null) {
						prdInfo.setGaType(row[7].toString());
					}
					if(row[8] != null) {
						prdInfo.setSlsCat(row[8].toString());
					}
					if(row[9] != null) {
						prdInfo.setSrc(row[9].toString());
					}
					if(row[10] != null) {
						prdInfo.setAdminBrh(row[10].toString());
					}
				}
			}
		} catch (Exception e) {
			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return cps;
	}
	
	
	public ProductSpecs getRoundBarOd(String frm, String grd, String size) {
		Session session = null;
		ProductSpecs productSpecs = new ProductSpecs();
		
		
		String hql = "select rdr_e_odia_dflt,rdr_m_odia_dflt from inrrdr_rec where rdr_frm = :frm and rdr_grd=:grd and rdr_size= :size";

		try {
			session = SessionUtilInformix.getSession();
	
			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("frm", frm);
			queryValidate.setParameter("grd", grd);
			queryValidate.setParameter("size", size);
	
			List<Object[]> rows = queryValidate.list();
			
			if(rows.size() > 0) {
				for (Object[] row : rows) {					

					productSpecs.seteOdDflt(row[0].toString());
					productSpecs.setmOdDflt(row[1].toString());
				}
			}
		} catch (Exception e) {
			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return productSpecs;
	}
	
	public String getPrsRoute(String slsCat) {
		Session session = null;
		String dfltPrs = "";
		
		
		String hql = "select cast(sca_dflt_prs_rte as varchar(3)), 1 from orrsca_rec where sca_sls_cat=:sls_cat";

		try {
			session = SessionUtilInformix.getSession();
	
			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("sls_cat", slsCat);
	
			List<Object[]> rows = queryValidate.list();
			
			if(rows.size() > 0) {
				for (Object[] row : rows) {					
					dfltPrs = row[0].toString();
				}
			}
		} catch (Exception e) {
			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return dfltPrs;
	}
	
	public String getStatusFromOrderInfo(Excel output, int docId, int rowNo) {
		Session session = null;
		String poNo = "";
		
		String hql = "SELECT ref_pfx, ref_no, ref_itm, msg, cus_po FROM order_info WHERE doc_id=:doc_id AND doc_row_no=:row_no";

		try {
			session = SessionUtil.getSession();
	
			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("doc_id", docId);
			queryValidate.setParameter("row_no", rowNo);
	
			List<Object[]> rows = queryValidate.list();
			
			if(rows.size() > 0) {
				for (Object[] row : rows) {	
					String ordNo = "";
					
					if( row[1] != null && !row[1].toString().equalsIgnoreCase("0") ) {
						ordNo = row[0].toString() + "-" + row[1].toString();
						
						if( row[2] != null && !row[2].toString().equalsIgnoreCase("0") ) {
							ordNo += "-" + row[2].toString();
						}
						
						output.setOrdNoInfo(ordNo);
					}
					
					if( row[3] != null ) {
						output.setErrMsg( row[3].toString() );
					}
					
					if( row[4] != null ) {
						poNo = row[4].toString();
					}
				}
			}
		} catch (Exception e) {
			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		
		return poNo;
	}
	
	public String getVenIdByName(String venNm) {
		String rtnStr = "";
		Session session = null;
		String hql = "SELECT cast(ven_ven_id as varchar(8)) ven_id, 1 FROM aprven_rec WHERE ven_actv=1 AND "
				+ "(LOWER(ven_ven_nm) like LOWER('%"+ venNm +"%') OR LOWER(ven_ven_id)=LOWER('"+ venNm +"')) LIMIT 1";

		session = SessionUtilInformix.getSession();

		Query queryValidate = session.createSQLQuery(hql);

		List<Object[]> rows = queryValidate.list();

		for (Object[] row : rows) {

			rtnStr = row[0].toString();

		}

		return rtnStr;
	}

}
