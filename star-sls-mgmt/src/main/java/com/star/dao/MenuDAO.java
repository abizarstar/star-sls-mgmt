package com.star.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.linkage.menu.Menu;
import com.star.linkage.menu.MenuBrowseOutput;
import com.star.linkage.menu.MenuInfo;
import com.star.linkage.menu.MenuInput;
import com.star.linkage.menu.MenuManOutput;
import com.star.modal.UsrGrpMenu;
import com.star.modal.UsrGrpSubMenu;

public class MenuDAO {

	private static Logger logger = LoggerFactory.getLogger(MenuDAO.class);

	public void getMenubyGroup(BrowseResponse<MenuBrowseOutput> browseResponse, String grpId) {
		String hql = "";
		Session session = null;

		try {

			session = SessionUtil.getSession();

			hql = " select menu.menu_id, menu_nm, menu_html_pg, menu_icon, grp.grp_id, "
					+ "(select grp_nm from usr_group where grp_id=grp.grp_id) grp_nm from usr_grp_menu grp , "
					+ "usr_menu menu where grp.menu_id = menu.menu_id ";

			if (grpId.length() > 0) {
				hql = hql + "and grp_id=:grpid";
			}

			hql = hql + " order by grp.grp_id, menu.menu_id";

			Query queryValidate = session.createSQLQuery(hql);

			if (grpId.length() > 0) {
				queryValidate.setParameter("grpid", Integer.parseInt(grpId));
			}

			List<Object[]> listMenu = queryValidate.list();

			for (Object[] menu : listMenu) {

				MenuInfo info = new MenuInfo();

				if (menu[0] != null) {
					info.setMenuId(menu[0].toString());
				} else {
					info.setMenuId("");
				}

				if (menu[1] != null) {
					info.setMenuNm(menu[1].toString());
				} else {
					info.setMenuNm("");
				}

				if (menu[2] != null) {
					info.setMenuHtml(menu[2].toString());
				} else {
					info.setMenuHtml("");
				}

				if (menu[3] != null) {
					info.setMenuIcon(menu[3].toString());
				} else {
					info.setMenuIcon("");
				}

				info.setGrpId(menu[4].toString());
				info.setGrpNm(menu[5].toString());

				List<Menu> subMenuList = getSubMenu(session, menu[4].toString(), info.getMenuId());

				info.setSubMenuList(subMenuList);

				browseResponse.output.fldTblMenu.add(info);

			}

		} catch (Exception e) {

			logger.debug("Menu Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public List<Menu> getSubMenu(Session session, String grpId, String menuId) throws Exception {
		String hql = "";

		List<Menu> subMenuList = new ArrayList<Menu>();

		hql = " select menu.sub_menu_id, menu.sub_menu_nm, sub_menu_html_pg, sub_menu_icon from "
				+ "usr_grp_sub_menu sub_user, usr_sub_menu menu where sub_user.sub_menu_id = menu.sub_menu_id "
				+ "and sub_user.menu_id = menu.menu_id " + " and grp_id=:grpid and menu.menu_id=:menuid";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("grpid", Integer.parseInt(grpId));
		queryValidate.setParameter("menuid", Integer.parseInt(menuId));

		List<Object[]> listMenu = queryValidate.list();

		for (Object[] menu : listMenu) {

			Menu info = new Menu();

			if (menu[0] != null) {
				info.setMenuId(menu[0].toString());
			} else {
				info.setMenuId("");
			}

			if (menu[1] != null) {
				info.setMenuNm(menu[1].toString());
			} else {
				info.setMenuNm("");
			}

			if (menu[2] != null) {
				info.setMenuHtml(menu[2].toString());
			} else {
				info.setMenuHtml("");
			}

			if (menu[3] != null) {
				info.setMenuIcon(menu[3].toString());
			} else {
				info.setMenuIcon("");
			}

			subMenuList.add(info);
		}

		return subMenuList;
	}

	public void getMenuAll(BrowseResponse<MenuBrowseOutput> browseResponse) {
		String hql = "";
		Session session = null;

		try {

			session = SessionUtil.getSession();

			hql = " select menu_id, menu_nm from usr_menu order by menu_seq";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listMenu = queryValidate.list();

			for (Object[] menu : listMenu) {

				MenuInfo info = new MenuInfo();

				if (menu[0] != null) {
					info.setMenuId(menu[0].toString());
				} else {
					info.setMenuId("");
				}

				if (menu[1] != null) {
					info.setMenuNm(menu[1].toString());
				} else {
					info.setMenuNm("");
				}

				List<Menu> subMenuList = getSubMenuAll(session, info.getMenuId());

				info.setSubMenuList(subMenuList);

				browseResponse.output.fldTblMenu.add(info);

			}

		} catch (Exception e) {

			logger.debug("Menu Info : {}", e.getMessage(), e);
			
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public List<Menu> getSubMenuAll(Session session, String menuId) throws Exception {
		String hql = "";

		List<Menu> subMenuList = new ArrayList<Menu>();

		hql = " select sub_menu_id, sub_menu_nm from usr_sub_menu where menu_id=:menuid";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("menuid", Integer.parseInt(menuId));

		List<Object[]> listMenu = queryValidate.list();

		for (Object[] menu : listMenu) {

			Menu info = new Menu();

			if (menu[0] != null) {
				info.setMenuId(menu[0].toString());
			} else {
				info.setMenuId("");
			}

			if (menu[1] != null) {
				info.setMenuNm(menu[1].toString());
			} else {
				info.setMenuNm("");
			}

			subMenuList.add(info);
		}

		return subMenuList;
	}

	public void addMenuInformation(MaintanenceResponse<MenuManOutput> starManResponse, MenuInput menuInput) {
		Session session = null;
		Transaction tx = null;
		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			/* DELETE INFORMATION IF AVAILABLE */
			delMenuInformation(session, menuInput.getGrpId());

			for (int i = 0; i < menuInput.getMenuId().size(); i++) {
				UsrGrpMenu grpMenu = new UsrGrpMenu();
				grpMenu.setGrpId(Integer.parseInt(menuInput.getGrpId()));
				grpMenu.setMenuId(Integer.parseInt(menuInput.getMenuId().get(i)));

				session.save(grpMenu);
			}

			for (int i = 0; i < menuInput.getSubMenList().size(); i++) {
				UsrGrpSubMenu grpSubMenu = new UsrGrpSubMenu();
				grpSubMenu.setGrpId(Integer.parseInt(menuInput.getGrpId()));
				grpSubMenu.setMenuId(Integer.parseInt(menuInput.getSubMenList().get(i).getMenuId()));
				grpSubMenu.setSubMenuId(Integer.parseInt(menuInput.getSubMenList().get(i).getSubMenuId()));
				session.save(grpSubMenu);
			}

			tx.commit();
		} catch (Exception e) {

			logger.debug("Menu Info : {}", e.getMessage(), e);

			starManResponse.output.messages.add(e.getMessage());
			starManResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void delMenuInformation(Session session, String grpId) throws Exception {
		String hql = "";

		hql = "delete from usr_grp_menu where grp_id=:grpid";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("grpid", Integer.parseInt(grpId));

		queryValidate.executeUpdate();

		hql = "delete from usr_grp_sub_menu where grp_id=:grpid";

		queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("grpid", Integer.parseInt(grpId));

		queryValidate.executeUpdate();
	}

}
