package com.star.dao;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.common.SessionUtilInformix;
import com.star.linkage.SalesOrder.I21RecManOutput;
import com.star.linkage.SalesOrder.I25RecManOutput;
import com.star.linkage.SalesOrder.I26RecManOutput;
import com.star.linkage.SalesOrder.I47RecManOutput;
import com.star.linkage.SalesOrder.I48RecManOutput;
import com.star.linkage.SalesOrder.S01RecManOutput;
import com.star.linkage.SalesOrder.S03RecManOutput;
import com.star.linkage.SalesOrder.S04RecManOutput;
import com.star.linkage.SalesOrder.S05RecManOutput;
import com.star.linkage.SalesOrder.S06RecManOutput;
import com.star.linkage.SalesOrder.S09RecManOutput;
import com.star.linkage.SalesOrder.S11RecManOutput;
import com.star.linkage.SalesOrder.S12RecManOutput;
import com.star.linkage.SalesOrder.S16RecManOutput;
import com.star.linkage.SalesOrder.S17RecInput;
import com.star.linkage.SalesOrder.S17RecManOutput;
import com.star.linkage.SalesOrder.S20RecInput;
import com.star.linkage.SalesOrder.S20RecManOutput;
import com.star.linkage.SalesOrder.S52RecManOutput;
import com.star.linkage.SalesOrder.S54RecManOutput;
import com.star.linkage.SalesOrder.TI00RecManOutput;
import com.star.linkage.slsord.SlsOrdInput;
import com.star.linkage.slsord.SlsOrdManOutput;
import com.star.modal.OrderInfo;

public class SlsOrdDAO {

	private static Logger logger = LoggerFactory.getLogger(SlsOrdDAO.class);
	CommonFunctions cmn = new CommonFunctions();

	public void updtS01Rec(MaintanenceResponse<S01RecManOutput> maintanenceResponse, SlsOrdInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvs01_rec (s01_cmpy_id, s01_src_co_id, s01_trc_ctl_no, s01_ord_brh, s01_ord_pfx, s01_sld_cus_id,"
					+ " s01_shp_to, s01_is_slp, s01_os_slp, s01_tkn_slp, s01_cr_dt_ovrd, s01_shp_itm_tgth, s01_blnd_shpt_mthd,"
					+ " s01_shpg_whs, s01_ord_typ, s01_cry, s01_exrt, s01_ex_rt_typ, s01_pttrm, s01_disc_trm, s01_mthd_pmt,"
					+ " s01_sls_cat, s01_cntr_sls, s01_multp_rls, s01_slp_flwp_dt, s01_expy_dt, s01_flwp_dy, s01_expy_dy)"
					+ " VALUES(:cmpy_id, 'SOG', :trc_ctl_no, :ord_brh, '" + input.getOrdPfx()
					+ "', :cus_id, :shp_to, :is_slp, :os_slp, :tkn_slp, 0, 0, 'N', "
					+ ":shpg_whs, :ord_typ, :cry, 1, 'V', :pttrm, :disc_trm, :mthd_pmt, :sls_cat, 0, 0, :slp_flw_up_dt, :expy_dt, :flw_days, :exp_days)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());

			if (input.getRefBrh() != null && input.getRefBrh().trim().length() > 0) {
				queryValidate.setParameter("ord_brh", input.getRefBrh());
			} else {
				queryValidate.setParameter("ord_brh", "");
			}

			if (input.getCusId() != null && input.getCusId().trim().length() > 0) {
				queryValidate.setParameter("cus_id", input.getCusId());
			} else {
				queryValidate.setParameter("cus_id", "");
			}

			if (input.getShpTo() != null && input.getShpTo().trim().length() > 0) {
				queryValidate.setParameter("shp_to", Integer.parseInt(input.getShpTo()));
			} else {
				queryValidate.setParameter("shp_to", 0);
			}

			if (input.getIsSlp() != null && input.getIsSlp().trim().length() > 0) {
				queryValidate.setParameter("is_slp", input.getIsSlp());
			} else {
				queryValidate.setParameter("is_slp", "");
			}

			if (input.getOsSlp() != null && input.getOsSlp().trim().length() > 0) {
				queryValidate.setParameter("os_slp", input.getOsSlp());
			} else {
				queryValidate.setParameter("os_slp", "");
			}

			if (input.getTknSlp() != null && input.getTknSlp().trim().length() > 0) {
				queryValidate.setParameter("tkn_slp", input.getTknSlp());
			} else {
				queryValidate.setParameter("tkn_slp", "");
			}

			if (input.getShpgWhs() != null && input.getShpgWhs().trim().length() > 0) {
				queryValidate.setParameter("shpg_whs", input.getShpgWhs());
			} else {
				queryValidate.setParameter("shpg_whs", "");
			}

			if (input.getOrdTyp() != null && input.getOrdTyp().trim().length() > 0) {
				queryValidate.setParameter("ord_typ", input.getOrdTyp());
			} else {
				queryValidate.setParameter("ord_typ", "");
			}

			if (input.getCry() != null && input.getCry().trim().length() > 0) {
				queryValidate.setParameter("cry", input.getCry());
			} else {
				queryValidate.setParameter("cry", "USD");
			}

			if (input.getSlsCat() != null && input.getSlsCat().trim().length() > 0) {
				queryValidate.setParameter("sls_cat", input.getSlsCat());
			} else {
				queryValidate.setParameter("sls_cat", "");
			}

			if (input.getPttrm() != null) {
				queryValidate.setParameter("pttrm", input.getPttrm());
			} else {
				queryValidate.setParameter("pttrm", 2);
			}

			if (input.getDiscTrm() != null) {
				queryValidate.setParameter("disc_trm", input.getDiscTrm());
			} else {
				queryValidate.setParameter("disc_trm", 1);
			}

			if (input.getMthdPmt() != null && input.getMthdPmt().trim().length() > 0) {
				queryValidate.setParameter("mthd_pmt", input.getMthdPmt());
			} else {
				queryValidate.setParameter("mthd_pmt", "CH");
			}

			java.util.Date utilDate = new java.util.Date(); // This is the current date and time

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(utilDate);
			calendar.add(Calendar.DAY_OF_MONTH, Integer.parseInt(input.getExpiryDays())); // Add 5 days
			java.util.Date expiryDate = calendar.getTime();

			calendar = Calendar.getInstance();
			calendar.setTime(utilDate);
			calendar.add(Calendar.DAY_OF_MONTH, Integer.parseInt(input.getFlwUpDays()));
			java.util.Date flwUpDate = calendar.getTime();

			queryValidate.setParameter("slp_flw_up_dt", flwUpDate);
			queryValidate.setParameter("expy_dt", expiryDate);
			queryValidate.setParameter("flw_days", 10);
			queryValidate.setParameter("exp_days", 999);

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			maintanenceResponse.output.rtnSts = 1;
			maintanenceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtI25Rec(MaintanenceResponse<I25RecManOutput> starMaintenanceResponse, SlsOrdInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvi25_rec (i25_cmpy_id, i25_intchg_pfx, i25_intchg_no, i25_intchg_itm, i25_src_co_id, i25_trc_ctl_no, "
					+ "i25_trc_itm, i25_ord_pfx, i25_ord_no, i25_ord_itm, i25_ord_sitm, i25_gat_ent_md, i25_gat_lvl, i25_ord_asgn_no) "
					+ "VALUES (:cmpy_id, 'XI', :intch_no, 0, 'SOG', :trc_ctl_no, 0, '" + input.getOrdPfx()
					+ "', :so_no, 0, 0, 'A', :lvl, 0)";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("intch_no", input.getTrcCtlNo());
			queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());

			if (input.getSoNo() > 0) {
				queryValidate.setParameter("so_no", input.getSoNo());
				queryValidate.setParameter("lvl", "I");
			} else {
				queryValidate.setParameter("so_no", 0);
				queryValidate.setParameter("lvl", "H");
			}

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtI21Rec(MaintanenceResponse<I21RecManOutput> starMaintenanceResponse, SlsOrdInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		List<String> pwcDetails = new ArrayList<String>();
		List<String> prsCostDetails = new ArrayList<String>();

		getPwcInformation(input, pwcDetails);

		getProcessCharge(input, prsCostDetails, pwcDetails.get(2));

		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvs21_rec (s21_cmpy_id, s21_src_co_id, s21_trc_ctl_no, s21_trc_itm, s21_trc_sitm, s21_trc_ln_no, "
					+ "s21_actvy_whs, s21_pwg, s21_pwc, s21_prs) "
					+ "VALUES (:cmpy_id, 'SOG', :trc_ctl_no, 1,1,1,:whs, :pwg, :pwc, :prs)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());
			queryValidate.setParameter("whs", input.getSrcWhs());
			queryValidate.setParameter("pwg", pwcDetails.get(0));
			queryValidate.setParameter("pwc", pwcDetails.get(1));
			queryValidate.setParameter("prs", pwcDetails.get(2));

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();

			tx.commit();

			MaintanenceResponse<S54RecManOutput> s54MaintenanceResponse = new MaintanenceResponse<S54RecManOutput>();
			s54MaintenanceResponse.setOutput(new S54RecManOutput());

			updtS54AdditionalRec(s54MaintenanceResponse, input, prsCostDetails.get(0), prsCostDetails.get(1));

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updts12Rec(MaintanenceResponse<S12RecManOutput> starMaintenanceResponse, SlsOrdInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		for (int i = 0; i < input.getMtlStdList().size(); i++) {

			try {
				session = SessionUtilInformix.getSession();
				tx = session.beginTransaction();
				hql = "INSERT INTO xcvs12_rec (s12_cmpy_id, s12_src_co_id, s12_trc_ctl_no, s12_trc_itm, s12_trc_ln_no, "
						+ "s12_std_spec_no, s12_sdo, s12_std_id, s12_addnl_id) "
						+ "VALUES (:cmpy_id, 'SOG', :trc_ctl_no, 1,:seq_id,1, :sdo, :std_id, :addtnl_id)";

				Query queryValidate = session.createSQLQuery(hql);

				queryValidate.setParameter("cmpy_id", input.getCmpyId());
				queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());
				queryValidate.setParameter("seq_id", i + 1);
				queryValidate.setParameter("sdo", input.getMtlStdList().get(i).getSdo());
				queryValidate.setParameter("std_id", input.getMtlStdList().get(i).getStdId());
				queryValidate.setParameter("addtnl_id", input.getMtlStdList().get(i).getAddId());

				System.out.print("\n" + hql + "\n");

				queryValidate.executeUpdate();

				tx.commit();

			} catch (Exception e) {
				logger.debug("User Details Info : {}", e.getMessage(), e);
				starMaintenanceResponse.output.rtnSts = 1;
				starMaintenanceResponse.output.messages.add(e.getMessage());
				tx.rollback();
			} finally {
				if (session != null) {
					session.close();
				}
			}

		}
	}

	public void updtI23Rec(MaintanenceResponse<I21RecManOutput> starMaintenanceResponse, SlsOrdInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		List<String> pwcDetails = new ArrayList<String>();

		getPwcInformation(input, pwcDetails);

		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvs23_rec (s23_cmpy_id, s23_src_co_id, s23_trc_ctl_no, s23_trc_itm, s23_trc_sitm, s23_trc_ln_no, "
					+ "s23_rmk_ln_no, s23_rmk_typ, s23_lng) "
					+ "VALUES (:cmpy_id, 'SOG', :trc_ctl_no, 1,1,1,1, '06', 'en')";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtI26Rec(MaintanenceResponse<I26RecManOutput> starMaintenanceResponse, SlsOrdInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvi26_rec (i26_cmpy_id, i26_src_co_id, i26_trc_ctl_no, i26_hdr_gat, i26_nm_addr_gat, i26_cntc_gat, "
					+ "i26_frt_gat, i26_tx_gat, i26_instr_gat, i26_sts_gat, i26_toll_blg_gat) "
					+ "VALUES (:cmpy_id, 'SOG', :trc_ctl_no, 'S', 'N', 'N', :frt_gat, 'N', :instr_gat, 'N', 'N')";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());

			if (input.getFrtChrg() != null && input.getVenId() > 0) {
				queryValidate.setParameter("frt_gat", "S");
			} else {
				queryValidate.setParameter("frt_gat", "N");
			}

			if (input.getHdrRmk() != null && input.getHdrRmk().trim().length() > 0) {
				queryValidate.setParameter("instr_gat", "S");
			} else {
				queryValidate.setParameter("instr_gat", "N");
			}

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtI47Rec(MaintanenceResponse<I47RecManOutput> starMaintenanceResponse, SlsOrdInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		String prdFlg = "";

		if (input.getCustItem().length() > 0) {
			prdFlg = "C";
		} else {
			prdFlg = "P";
		}

		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvi47_rec (i47_cmpy_id, i47_src_co_id, i47_trc_ctl_no, i47_trc_itm, i47_ord_gat, i47_instr_gat, "
					+ "i47_sts_gat, i47_prd_gat, i47_prd_desc_gat, i47_chrg_gat, i47_tx_gat, i47_cond_gat, i47_tol_gat, i47_coil_pkg_gat, "
					+ "i47_pkg_gat, i47_std_spec_gat, i47_tst_gat, i47_chm_spec_gat, i47_tst_cert_gat, i47_orig_zn_gat, i47_rdflt_cps, "
					+ "i47_frt_gat, i47_nm_addr_gat, i47_byot_prd_gat, i47_byot_cst_gat, i47_byot_instr_gat, i47_trgt_oir_gat) "
					+ "VALUES (:cmpy_id, 'SOG', :trc_ctl_no, 1, 'N', 'N', 'N', :prd_flg, 'N', 'S', 'N', 'N', :tol_flg, 'N', :pkg_flg, :specs_flg, 'N', "
					+ "'N', 'N', 'N', '', 'N', 'N', 'N', 'N', 'N', 'N')";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());
			// queryValidate.setParameter("so_itm", input.getItmNo());
			queryValidate.setParameter("prd_flg", prdFlg);
			queryValidate.setParameter("tol_flg", input.getTolFlg());
			queryValidate.setParameter("pkg_flg", input.getPkgFlg());

			if (input.getMtlStdList().size() > 0) {
				queryValidate.setParameter("specs_flg", "S");
			} else {
				queryValidate.setParameter("specs_flg", "N");
			}

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtI48Rec(MaintanenceResponse<I48RecManOutput> starMaintenanceResponse, SlsOrdInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvi48_rec (i48_cmpy_id, i48_src_co_id, i48_trc_ctl_no, i48_trc_itm, i48_trc_sitm, i48_rls_gat, "
					+ "i48_sts_gat, i48_rtng_gat) VALUES (:cmpy_id, 'SOG', :trc_ctl_no, 1, 1, 'S', 'N', 'S')";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());
			// queryValidate.setParameter("so_itm", input.getItmNo());

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtS05Rec(MaintanenceResponse<S05RecManOutput> starMaintenanceResponse, SlsOrdInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvs05_rec (s05_cmpy_id, s05_src_co_id, s05_trc_ctl_no, s05_trc_itm, s05_alw_prtl_shp, "
					+ "s05_ovrshp_pct, s05_undshp_pct, s05_cus_po, s05_cus_po_dt, s05_cus_ctrct, s05_end_usr_po, s05_ord_pcs, "
					+ "s05_ord_msr, s05_ord_wgt, s05_ord_qty, s05_ord_wgt_um, s05_bkt_pcs, s05_bkt_msr, s05_bkt_wgt, s05_bkt_qty, "
					+ "s05_bkt_wgt_um, s05_sls_cat, s05_chrg_qty_typ, s05_wfl_sls, s05_use_wgt_mult, s05_aly_schg_cls, s05_src, "
					+ "s05_bld_mtl_chrg, s05_end_use, s05_end_use_desc, s05_krf_wdth, s05_krf_lgth, s05_krf_ls_wdth, s05_krf_ls_lgth, "
					+ "s05_std_prs_rte, s05_invt_qlty, s05_src_whs, s05_jbspy_chrg_um, s05_multp_dim_tmpl, s05_apl_reb, "
					+ "s05_toll_dist_mthd, s05_blnd_shpt, s05_blg_dim, s05_multp_dim_id, s05_desc30, s05_ovrd_chrg, s05_itm_cat,s05_eps_row_id_no) "
					+ "VALUES (:cmpy_id, 'SOG', :trc_ctl_no, 1, 1, 0, 0, :cus_po, :cus_po_dt, '', '', :ord_pcs, :ord_msr, :ord_wgt, "
					+ ":ord_qty, :ord_wgt_um, 0, 0, 0, 0, '', :sls_cat, 'A', 0, 0, 'N', :src, :bld_up, '', '', 0, '001', 0, 0, :stndrd_rte, '-', "
					+ ":src_whs, '', '0', 0, '', '',0, '','',0, '',0)";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());
			// queryValidate.setParameter("itm_no", input.getItmNo());

			if (input.getPoNo() != null) {
				queryValidate.setParameter("cus_po", input.getPoNo());
			} else {
				queryValidate.setParameter("cus_po", "");
			}

			if (input.getPoDt() != null) {
				SimpleDateFormat sdfOut = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat sdfIn = new SimpleDateFormat("MM/dd/yyyy");
				// System.out.print("\ncus_po_dt input: "+ input.getPoDt());
				Date inptDate = sdfIn.parse(input.getPoDt());
				// System.out.print("\ncus_po_dt parsed : "+ inptDate);
				String poDtStr = sdfOut.format(inptDate);
				// System.out.print("\ncus_po_dt parsed converted string : "+ poDtStr);
				Date poDt = sdfOut.parse(poDtStr);
				// System.out.print("\ncus_po_dt parsed converted DATE : "+ poDt);

				queryValidate.setParameter("cus_po_dt", poDt);
			} else {
				queryValidate.setParameter("cus_po_dt", "");
			}

			if (input.getOrdPcs() != null && input.getOrdPcs().length() > 0) {
				queryValidate.setParameter("ord_pcs", Integer.parseInt(input.getOrdPcs()));
			} else {
				queryValidate.setParameter("ord_pcs", 0);
			}

			if (input.getOrdMsr() != null) {
				queryValidate.setParameter("ord_msr", Double.parseDouble(input.getOrdMsr()));
			} else {
				queryValidate.setParameter("ord_msr", 0);
			}

			if (input.getOrdWgt() != null && input.getOrdWgt().length() > 0) {
				queryValidate.setParameter("ord_wgt", Double.parseDouble(input.getOrdWgt()));
				queryValidate.setParameter("ord_qty", Double.parseDouble(input.getOrdWgt()));
			} else {
				queryValidate.setParameter("ord_wgt", 0);
				queryValidate.setParameter("ord_qty", 0);
			}

			if (input.getUm() != null) {
				queryValidate.setParameter("ord_wgt_um", input.getUm());
			} else {
				queryValidate.setParameter("ord_wgt_um", "");
			}

			if (input.getSlsCat() != null) {
				queryValidate.setParameter("sls_cat", input.getSlsCat());
			} else {
				queryValidate.setParameter("sls_cat", "");
			}

			if (input.getSource() != null) {
				queryValidate.setParameter("src", input.getSource());
			} else {
				queryValidate.setParameter("src", "");
			}

			if (input.getSrcWhs() != null) {
				queryValidate.setParameter("src_whs", input.getSrcWhs());
			} else {
				queryValidate.setParameter("src_whs", "");
			}

			if (input.getStndrdRoute() != null) {
				queryValidate.setParameter("stndrd_rte", input.getStndrdRoute());
			} else {
				queryValidate.setParameter("stndrd_rte", "");
			}

			queryValidate.setParameter("bld_up", Integer.parseInt(input.getBuildUp()));

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtS06Rec(MaintanenceResponse<S06RecManOutput> starMaintenanceResponse, SlsOrdInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvs06_rec (s06_cmpy_id, s06_src_co_id, s06_trc_ctl_no, s06_trc_itm, s06_frm, s06_grd, "
					+ " s06_num_size1, s06_num_size2, s06_num_size3, s06_num_size4, s06_num_size5, s06_size, s06_fnsh, "
					+ " s06_ef_evar, s06_wdth, s06_lgth, s06_dim_dsgn, s06_idia, s06_odia, s06_wthck, s06_ga_size, s06_ga_typ, "
					+ " s06_alt_size, s06_rdm_dim_1, s06_rdm_dim_2, s06_rdm_dim_3, s06_rdm_dim_4, s06_rdm_dim_5, s06_rdm_dim_6, "
					+ " s06_rdm_dim_7, s06_rdm_dim_8, s06_rdm_area, s06_ent_msr, s06_lgth_disp_fmt, s06_frc_fmt, s06_dia_frc_fmt, "
					+ " s06_ord_lgth_typ, s06_fmt_desc_frc, s06_fmt_size_desc, s06_part_cus_id, s06_part, s06_part_revno, s06_grs_wdth, "
					+ " s06_grs_lgth, s06_grs_pcs, s06_grs_wdth_intgr, s06_grs_wdth_nmr, s06_grs_wdth_dnm, s06_grs_lgth_intgr, "
					+ " s06_grs_lgth_nmr, s06_grs_lgth_dnm, s06_wdth_intgr, s06_wdth_nmr, s06_wdth_dnm, s06_lgth_intgr, s06_lgth_nmr, "
					+ " s06_lgth_dnm, s06_idia_intgr, s06_idia_nmr, s06_idia_dnm, s06_odia_intgr, s06_odia_nmr, s06_odia_dnm, s06_cstm_sha, s06_grs_size, s06_grs_ga_size) "
					+ " VALUES (:cmpy_id, 'SOG', :trc_ctl_no, 1, :frm, :grd, 0, 0, 0, 0, 0, :size, :fnsh, '', :wdth, :lgth, 'F', :idia, :odia, 0, :ga_size, :ga_type, '', "
					+ " 0, 0, 0, 0, 0, 0, 0, 0, 0, 'E', 0, 0, 0, :lgth_typ, 0, 0, :part_cus_id, :part, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());
			// queryValidate.setParameter("itm_no", input.getItmNo());

			if (input.getFrm() != null && input.getFrm() != "") {
				queryValidate.setParameter("frm", input.getFrm());
			} else {
				queryValidate.setParameter("frm", "");
			}

			if (input.getGrd() != null && input.getGrd().trim().length() > 0) {
				queryValidate.setParameter("grd", input.getGrd());
			} else {
				queryValidate.setParameter("grd", "");
			}

			if (input.getSize() != null && input.getSize().trim().length() > 0) {
				queryValidate.setParameter("size", input.getSize());
			} else {
				queryValidate.setParameter("size", "");
			}

			if (input.getFnsh() != null && input.getFnsh().trim().length() > 0) {
				queryValidate.setParameter("fnsh", input.getFnsh());
			} else {
				queryValidate.setParameter("fnsh", "");
			}

			if (input.getWdth() != null && input.getWdth().trim().length() > 0) {
				queryValidate.setParameter("wdth", Float.parseFloat(input.getWdth()));
			} else {
				queryValidate.setParameter("wdth", 0);
			}

			if (input.getLgth() != null && input.getLgth().trim().length() > 0) {
				queryValidate.setParameter("lgth", Float.parseFloat(input.getLgth()));
			} else {
				queryValidate.setParameter("lgth", 0);
			}

			if (input.getGaSize() != null && input.getGaSize().trim().length() > 0) {
				queryValidate.setParameter("ga_size", Float.parseFloat(input.getGaSize()));
			} else {
				queryValidate.setParameter("ga_size", 0);
			}

			if (input.getGaType() != null && input.getGaType().trim().length() > 0) {
				queryValidate.setParameter("ga_type", input.getGaType());
			} else {
				queryValidate.setParameter("ga_type", "");
			}

			if (input.getIdia() != null && input.getIdia().trim().length() > 0) {
				queryValidate.setParameter("idia", Float.parseFloat(input.getIdia()));
			} else {
				queryValidate.setParameter("idia", 0);
			}

			if (input.getOdia() != null && input.getOdia().trim().length() > 0) {
				queryValidate.setParameter("odia", Float.parseFloat(input.getOdia()));
			} else {
				queryValidate.setParameter("odia", 0);
			}

			if (input.getLgthType() != null && input.getLgthType().trim().length() > 0) {
				queryValidate.setParameter("lgth_typ", input.getLgthType());
			} else {
				queryValidate.setParameter("lgth_typ", "");
			}

			if (input.getCusId() != null && input.getCusId().trim().length() > 0) {
				queryValidate.setParameter("part_cus_id", input.getCusId());
			} else {
				queryValidate.setParameter("part_cus_id", "");
			}

			if (input.getCustItem() != null && input.getCustItem().trim().length() > 0) {
				queryValidate.setParameter("part", input.getCustItem());
			} else {
				queryValidate.setParameter("part", "");
			}

			if (input.getMemoPart() != null && input.getMemoPart().trim().length() > 0) {
				queryValidate.setParameter("part", input.getMemoPart());
			} else {
				queryValidate.setParameter("part", "");
			}

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtS09Rec(MaintanenceResponse<S09RecManOutput> starMaintenanceResponse, SlsOrdInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvs09_rec (s09_cmpy_id, s09_src_co_id, s09_trc_ctl_no, s09_trc_itm, s09_wdth_tol_posv, s09_wdth_tol_neg, s09_lgth_tol_posv, s09_lgth_tol_neg, s09_idia_tol_posv, "
					+ " s09_idia_tol_neg, s09_odia_tol_posv, s09_odia_tol_neg, s09_wthck_tol_posv, s09_wthck_tol_neg, s09_ga_tol_posv, s09_ga_tol_neg, s09_diag_tol, s09_fltns, s09_cmbr) "
					+ " VALUES (:cmpy_id, 'SOG', :trc_ctl_no, 1, :wdth_tol_posv, :wdth_tol_neg, :lgth_tol_posv, :lgth_tol_neg, :idia_tol_posv, :idia_tol_neg, :odia_tol_posv, :odia_tol_neg, "
					+ ":wthck_tol_posv, :wthck_tol_neg, :ga_tol_posv, :ga_tol_neg, :diag_tol, :fltns, :cmbr)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());
			// queryValidate.setParameter("itm_no", input.getItmNo());
			queryValidate.setParameter("wdth_tol_posv", input.getWdthTolPosv());
			queryValidate.setParameter("wdth_tol_neg", input.getWdthTolNeg());
			queryValidate.setParameter("lgth_tol_posv", input.getLgthTolPosv());
			queryValidate.setParameter("lgth_tol_neg", input.getLgthTolNeg());
			queryValidate.setParameter("idia_tol_posv", input.getIdiaTolPosv());
			queryValidate.setParameter("idia_tol_neg", input.getIdiaTolNeg());
			queryValidate.setParameter("odia_tol_posv", input.getOdiaTolPosv());
			queryValidate.setParameter("odia_tol_neg", input.getOdiaTolNeg());
			queryValidate.setParameter("wthck_tol_posv", input.getWthckTolPosv());
			queryValidate.setParameter("wthck_tol_neg", input.getWthckTolNeg());
			queryValidate.setParameter("ga_tol_posv", input.getGaTolPosv());
			queryValidate.setParameter("ga_tol_neg", input.getGaTolNeg());
			queryValidate.setParameter("diag_tol", input.getDiagTol());
			queryValidate.setParameter("fltns", input.getFltns());
			queryValidate.setParameter("cmbr", input.getCmbr());

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtS11Rec(MaintanenceResponse<S11RecManOutput> starMaintenanceResponse, SlsOrdInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();

			hql = "INSERT INTO xcvs11_rec (s11_cmpy_id, s11_src_co_id, s11_trc_ctl_no, s11_trc_itm, s11_pkg, s11_skd_typ, s11_pcs_per_tag, s11_min_pcs_ptag, s11_min_pkg_wgt, s11_max_pkg_wgt, "
					+ " s11_max_hgt) VALUES (:cmpy_id, 'SOG', :trc_ctl_no, 1, :pkg, :skd_typ, :pcs_per_tag, :min_pcs_ptag, :min_pkg_wgt, :max_pkg_wgt, :max_hgt)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());
			// queryValidate.setParameter("itm_no", input.getItmNo());
			queryValidate.setParameter("pkg", input.getPkg());
			queryValidate.setParameter("skd_typ", input.getSkdTyp());
			queryValidate.setParameter("pcs_per_tag", input.getPcsPerTag());
			queryValidate.setParameter("min_pcs_ptag", input.getMinPcsPtag());
			queryValidate.setParameter("min_pkg_wgt", input.getMinPkgWgt());
			queryValidate.setParameter("max_pkg_wgt", input.getMaxPkgWgt());
			queryValidate.setParameter("max_hgt", input.getMaxHgt());

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtS16Rec(MaintanenceResponse<S16RecManOutput> starMaintenanceResponse, SlsOrdInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvs16_rec (s16_cmpy_id,s16_src_co_id,s16_trc_ctl_no,s16_trc_itm,s16_trc_sitm,s16_cus_rls,s16_rls_pcs,"
					+ "s16_rls_pcs_typ,s16_rls_msr,s16_rls_msr_typ,s16_rls_wgt,s16_rls_wgt_typ,s16_rls_qty,s16_rls_qty_typ,"
					+ "s16_due_dt_qlf,s16_due_dt_fmt,s16_due_dt_ent,s16_due_fm_dt,s16_due_to_dt,s16_due_dt_wk_no,s16_due_dt_wk_cy,"
					+ "s16_due_fm_hr,s16_due_fm_mt,s16_due_to_hr,s16_due_to_mt,s16_rqst_dt_qlf,s16_rqst_dt_fmt,s16_rqst_dt_ent,"
					+ "s16_rqst_fm_dt,s16_rqst_to_dt,s16_rqst_dt_wk_no,s16_rqst_dt_wk_cy,s16_rqst_fm_hr,s16_rqst_fm_mt,"
					+ "s16_rqst_to_hr,s16_rqst_to_mt,s16_rdy_by_dt,s16_rdy_by_hr,s16_rdy_by_mt,s16_shpg_whs,s16_plng_whs,"
					+ "s16_dflt_trrte,s16_bko,s16_ord_plng_mthd,s16_plng_mthd_ovrd) "
					+ "VALUES (:cmpy_id,'SOG',:trc_ctl_no,1,1,'',:rls_pcs,:rls_pcs_typ,0,'T',:rls_wgt,:rls_wgt_typ,0,'T','','D',:due_dt_ent,"
					+ "NULL,NULL,0,0,0,0,0,0,'','D',:rqst_dt_ent,NULL,NULL,0,0,0,0,0,0,NULL,0,0,:shpg_whs,:plng_whs,'',0,0,0)";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());
			// queryValidate.setParameter("itm_no", input.getItmNo());

			if (input.getOrdWgt() != null && input.getOrdWgt().length() > 0) {
				queryValidate.setParameter("rls_wgt", Double.parseDouble(input.getOrdWgt()));
				queryValidate.setParameter("rls_wgt_typ", "A");

			} else {
				queryValidate.setParameter("rls_wgt", 0);
				queryValidate.setParameter("rls_wgt_typ", "T");
			}

			if (input.getOrdPcs() != null && input.getOrdPcs().length() > 0) {
				queryValidate.setParameter("rls_pcs", Double.parseDouble(input.getOrdPcs()));
				queryValidate.setParameter("rls_pcs_typ", "A");

			} else {
				queryValidate.setParameter("rls_pcs", 0);
				queryValidate.setParameter("rls_pcs_typ", "T");
			}

			/*
			 * if (input.getQty() != null) { queryValidate.setParameter("rls_wgt",
			 * Double.parseDouble(input.getQty())); queryValidate.setParameter("rls_qty",
			 * Double.parseDouble(input.getQty())); } else {
			 * queryValidate.setParameter("rls_wgt", 0);
			 * queryValidate.setParameter("rls_qty", 0); }
			 */

			if (input.getDelDt() != null) {
				// SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				int yyyymmdd = cmn.formatDateIntyyyymmdd(input.getDelDt());
				queryValidate.setParameter("due_dt_ent", yyyymmdd);
			} else {
				queryValidate.setParameter("due_dt_ent", "");
			}

			if (input.getShpgWhs() != null) {
				queryValidate.setParameter("shpg_whs", input.getShpgWhs());
			} else {
				queryValidate.setParameter("shpg_whs", "");
			}

			if (input.getPlngWhs() != null) {
				queryValidate.setParameter("plng_whs", input.getPlngWhs());
			} else {
				queryValidate.setParameter("plng_whs", "");
			}

			String frmt = new SimpleDateFormat("yyyyMMdd").format(new Date());
			queryValidate.setParameter("rqst_dt_ent", Integer.parseInt(frmt));

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtS17Rec(MaintanenceResponse<S17RecManOutput> starMaintenanceResponse, int so, S17RecInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "";
			Query queryValidate = session.createSQLQuery(hql);

			if (input.getS17CmpyId() != null) {
				queryValidate.setParameter("s17_cmpy_id", input.getS17CmpyId());
			} else {
				queryValidate.setParameter("s17_cmpy_id", "");
			}
			if (input.getS17TrcCtlNo() > 0) {
				queryValidate.setParameter("s17_trc_ctl_no", so);
			} else {
				queryValidate.setParameter("s17_trc_ctl_no", 0);
			}

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();
			tx.commit();
			session.close();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtS20Rec(MaintanenceResponse<S20RecManOutput> starMaintenanceResponse, int so, S20RecInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			Query queryValidate = session.createSQLQuery(hql);

			if (input.getS20CmyId() != null) {
				queryValidate.setParameter("s20_cmpy_id", input.getS20CmyId());
			} else {
				queryValidate.setParameter("s20_cmpy_id", "");
			}
			if (input.getS20TrcCtlNo() > 0) {
				queryValidate.setParameter("s20_trc_ctl_no", so);
			} else {
				queryValidate.setParameter("s20_trc_ctl_no", 0);
			}

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtTI00Rec(MaintanenceResponse<TI00RecManOutput> starMaintenanceResponse, SlsOrdInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcti00_rec (i00_cmpy_id, i00_intchg_pfx, i00_intchg_no, i00_evnt, i00_usr_id, i00_sts_cd, "
					+ "i00_ssn_log_ctl_no, i00_intrf_cl) VALUES(:cmpy_id, 'XI', :intchg_no, 'SOG', :usr_id, 'N', 0, 'E')";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("intchg_no", input.getTrcCtlNo());
			queryValidate.setParameter("usr_id", input.getUsrId());

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);

			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtS52Rec(MaintanenceResponse<S52RecManOutput> starMaintenanceResponse, SlsOrdInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvs52_rec (s52_cmpy_id,s52_src_co_id,s52_trc_ctl_no,s52_trc_itm,s52_ptord_pfx,s52_ptord_no,"
					+ "s52_ptord_itm,s52_ptord_rls_no,s52_ord_pfx,s52_ord_no)"
					+ " VALUES (:cmpy_id,'SOG',:trc_ctl_no,1,'" + input.getOrdPfx() + "',0,0,1,'" + input.getOrdPfx()
					+ "',0)";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());
			// queryValidate.setParameter("itm_no", input.getItmNo());

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtS54Rec(MaintanenceResponse<S54RecManOutput> starMaintenanceResponse, SlsOrdInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();

			hql = "INSERT INTO xcvs54_rec (s54_cmpy_id, s54_src_co_id, s54_trc_ctl_no, s54_trc_itm, s54_trc_ln_no, s54_chrg_no, "
					+ "s54_ref_ln_no, s54_chrg_desc20, s54_chrg_cl, s54_chrg, s54_chrg_um, s54_chrg_qty, s54_chrg_qty_um, s54_osprs, "
					+ "s54_prs_dy, s54_prs_cst_bss, s54_sawg_nbr_cut,s54_lnl_msr,s54_auto_cmp) VALUES "
					+ "(:cmpy_id, 'SOG', :trc_ctl_no, 1, 1, 1, 0, 'Metal', 'E', :chrg, :chrg_um, :qty, :wgt_um, 0, 0, '', 0,0,0)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());
			// queryValidate.setParameter("so_itm", input.getItmNo());
			queryValidate.setParameter("chrg", input.getUnitPrice());
			queryValidate.setParameter("chrg_um", input.getPriceUm());

			if (input.getOrdWgt() != null && input.getOrdWgt().length() > 0) {
				queryValidate.setParameter("qty", Double.parseDouble(input.getOrdWgt()));
			} else {
				queryValidate.setParameter("qty", 0);
			}

			queryValidate.setParameter("wgt_um", input.getUm());

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtS54AdditionalRec(MaintanenceResponse<S54RecManOutput> starMaintenanceResponse, SlsOrdInput input,
			String ccNo, String ccDesc) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			
			if(Integer.parseInt(ccNo) == 1)
			{
				hql = "INSERT INTO xcvs54_rec (s54_cmpy_id, s54_src_co_id, s54_trc_ctl_no, s54_trc_itm, s54_trc_ln_no, s54_chrg_no, "
					+ "s54_ref_ln_no, s54_chrg_desc20, s54_chrg_cl, s54_chrg, s54_chrg_um, s54_chrg_qty, s54_chrg_qty_um, s54_osprs, "
					+ "s54_prs_dy, s54_prs_cst_bss, s54_sawg_nbr_cut,s54_lnl_msr,s54_auto_cmp) VALUES "
					+ "(:cmpy_id, 'SOG', :trc_ctl_no, 1, 2, :chrg_no, 0, :chrg_desc, 'E', :chrg, :chrg_um, :qty, :wgt_um, 0, 0, '', 0,0,0)";
				
			}
			else
			{
				hql = "INSERT INTO xcvs54_rec (s54_cmpy_id, s54_src_co_id, s54_trc_ctl_no, s54_trc_itm, s54_trc_ln_no, s54_chrg_no, "
						+ "s54_ref_ln_no, s54_chrg_desc20, s54_chrg_cl, s54_chrg, s54_chrg_um, s54_chrg_qty, s54_chrg_qty_um, s54_osprs, "
						+ "s54_prs_dy, s54_prs_cst_bss, s54_sawg_nbr_cut,s54_lnl_msr,s54_auto_cmp) VALUES "
						+ "(:cmpy_id, 'SOG', :trc_ctl_no, 1, 2, :chrg_no, 0, :chrg_desc, 'I', :chrg, :chrg_um, 0, :wgt_um, 0, 0, '', 0,0,0)";
			}

			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());
			// queryValidate.setParameter("so_itm", input.getItmNo());
			queryValidate.setParameter("chrg_no", Integer.parseInt(ccNo));
			queryValidate.setParameter("chrg_desc", ccDesc);
			
			if(Integer.parseInt(ccNo) == 1)
			{
				queryValidate.setParameter("chrg", input.getUnitPrice());
				queryValidate.setParameter("chrg_um", input.getPriceUm());
				
				if (input.getOrdWgt() != null && input.getOrdWgt().length() > 0) {
					queryValidate.setParameter("qty", Double.parseDouble(input.getOrdWgt()));
				} else {
					queryValidate.setParameter("qty", 0);
				}

				queryValidate.setParameter("wgt_um", input.getUm());
			}
			else
			{
				
				queryValidate.setParameter("chrg", new BigDecimal("0.00"));
				queryValidate.setParameter("chrg_um", "SIN");
				
				queryValidate.setParameter("wgt_um", "SFT");
			}

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void insertPrcChrgs(MaintanenceResponse<S54RecManOutput> starMaintenanceResponse, SlsOrdInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();

			hql = "INSERT INTO xcvs54_rec (s54_cmpy_id, s54_src_co_id, s54_trc_ctl_no, s54_trc_itm, s54_trc_ln_no, s54_chrg_no, "
					+ "s54_ref_ln_no, s54_chrg_desc20, s54_chrg_cl, s54_chrg, s54_chrg_um, s54_chrg_qty, s54_chrg_qty_um, s54_osprs, "
					+ "s54_prs_dy, s54_prs_cst_bss, s54_sawg_nbr_cut) VALUES "
					+ "(:cmpy_id, 'SOG', :trc_ctl_no, 1, 2, 100, 0, '', 'I', 0, :chrg_um, :qty, :wgt_um, 0, 0, '', 0)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());
			// queryValidate.setParameter("so_itm", input.getItmNo());
			// queryValidate.setParameter("chrg", input.getUnitPrice());
			queryValidate.setParameter("chrg_um", input.getPriceUm());
			queryValidate.setParameter("qty", Double.parseDouble(input.getQty()));
			queryValidate.setParameter("wgt_um", input.getUm());

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtS03Rec(MaintanenceResponse<S03RecManOutput> starMaintenanceResponse, SlsOrdInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();

			hql = "INSERT INTO xcvs03_rec (s03_cmpy_id, s03_src_co_id, s03_trc_ctl_no, s03_trc_itm, s03_trm_trd, s03_dlvy_mthd, "
					+ "s03_frt_ven_id, s03_cry, s03_exrt, s03_ex_rt_typ, s03_cst, s03_cst_um, s03_chrg, s03_chrg_um) VALUES "
					+ "(:cmpy_id, 'SOG', :trc_ctl_no, 0, :trm_trd, :dlvy_mthd, :ven_id, :cry, :exrt, :exrt_typ, :cst_chrg, :cst_um, "
					+ ":frt_chrg, :frt_chrg_um)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());
			queryValidate.setParameter("trm_trd", input.getTrmTrd());
			queryValidate.setParameter("dlvy_mthd", input.getDlvyMthd());
			if (input.getVenId() > 0) {
				queryValidate.setParameter("ven_id", input.getVenId());
			} else {
				queryValidate.setParameter("ven_id", "10000");
			}
			queryValidate.setParameter("cry", input.getCry());
			queryValidate.setParameter("exrt", input.getExrt());
			queryValidate.setParameter("exrt_typ", input.getExrtTyp());
			if (input.getCstChrg() != null) {
				queryValidate.setParameter("cst_chrg", input.getCstChrg());
			} else {
				queryValidate.setParameter("cst_chrg", 0);
			}
			queryValidate.setParameter("cst_um", input.getFrtUm());
			queryValidate.setParameter("frt_chrg", input.getFrtChrg());
			queryValidate.setParameter("frt_chrg_um", input.getFrtUm());

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtS04Rec(MaintanenceResponse<S04RecManOutput> starMaintenanceResponse, SlsOrdInput input,
			Integer lnNo, String rmkTyp, String txt) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();

			hql = "INSERT INTO xcvs04_rec (s04_cmpy_id, s04_src_co_id, s04_trc_ctl_no, s04_trc_itm, s04_trc_ln_no, s04_rmk_typ, s04_lng, s04_txt) "
					+ "VALUES (:cmpy_id, 'SOG', :trc_ctl_no, 0, :ln_no, :rmk_typ, :bas_lng, :hdr_rmk) ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", input.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", input.getTrcCtlNo());

			if (lnNo != null && lnNo >= 0) {
				queryValidate.setParameter("ln_no", lnNo);
			} else {
				queryValidate.setParameter("ln_no", 0);
			}

			if (rmkTyp != null && rmkTyp.trim().length() > 0) {
				queryValidate.setParameter("rmk_typ", rmkTyp);
			} else {
				queryValidate.setParameter("rmk_typ", "05");
			}

			if (input.getBasLng() != null && input.getBasLng().trim().length() > 0) {
				queryValidate.setParameter("bas_lng", input.getBasLng());
			} else {
				queryValidate.setParameter("bas_lng", "en");
			}

			if (txt != null && txt.trim().length() > 0) {
				queryValidate.setParameter("hdr_rmk", txt);
			} else {
				queryValidate.setParameter("hdr_rmk", "");
			}

			System.out.print("\n" + hql + "\n");

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	private int validateUser(String usrId) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getmaxRec(String cmpyId) {
		Session session = null;
		int trcCtlNo = 0;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			hql = "SELECT max(i00_intchg_no), 1 FROM xcti00_rec WHERE i00_cmpy_id=:cmpy_id AND i00_intchg_pfx='XI'";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", cmpyId);

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				trcCtlNo = Integer.parseInt(row[0].toString());
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return trcCtlNo;
	}

	public void getCustRefData(SlsOrdInput slsOrdInpt) {
		Session session = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT CAST(cus_cry AS VARCHAR(3)) cus_cry, crd_pttrm, crd_disc_trm, CAST(crd_mthd_pmt AS VARCHAR(2)) FROM arrcus_rec, "
					+ "arrcrd_rec WHERE cus_cmpy_id=crd_cmpy_id AND cus_cus_id=crd_cus_id AND cus_actv=1 AND "
					+ "crd_cmpy_id=:cmpy_id AND crd_cus_id=:cus_id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("cus_id", slsOrdInpt.getCusId());

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				if (row[0] != null) {
					slsOrdInpt.setCry(row[0].toString());
				}

				if (row[1] != null) {
					slsOrdInpt.setPttrm(Integer.parseInt(row[1].toString()));
				}

				if (row[2] != null) {
					slsOrdInpt.setDiscTrm(Integer.parseInt(row[2].toString()));
				}

				if (row[3] != null) {
					slsOrdInpt.setMthdPmt(row[3].toString());
				}
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void createSalesOrder(MaintanenceResponse<SlsOrdManOutput> starMaintenanceResponse, SlsOrdInput slsOrdInpt) {

		MaintanenceResponse<I25RecManOutput> i25MaintenanceResponse = new MaintanenceResponse<I25RecManOutput>();
		i25MaintenanceResponse.setOutput(new I25RecManOutput());
		updtI25Rec(i25MaintenanceResponse, slsOrdInpt);

		MaintanenceResponse<I21RecManOutput> i21MaintenanceResponse = new MaintanenceResponse<I21RecManOutput>();
		i21MaintenanceResponse.setOutput(new I21RecManOutput());
		updtI21Rec(i21MaintenanceResponse, slsOrdInpt);

		if (slsOrdInpt.getMtlStdList().size() > 0) {
			MaintanenceResponse<S12RecManOutput> s12MaintenanceResponse = new MaintanenceResponse<S12RecManOutput>();
			s12MaintenanceResponse.setOutput(new S12RecManOutput());
			updts12Rec(s12MaintenanceResponse, slsOrdInpt);
		}

		/*
		 * MaintanenceResponse<I21RecManOutput> i23MaintenanceResponse = new
		 * MaintanenceResponse<I21RecManOutput>(); i23MaintenanceResponse.setOutput(new
		 * I21RecManOutput()); updtI23Rec(i21MaintenanceResponse, slsOrdInpt);
		 */
		MaintanenceResponse<I26RecManOutput> i26MaintenanceResponse = new MaintanenceResponse<I26RecManOutput>();
		i26MaintenanceResponse.setOutput(new I26RecManOutput());
		updtI26Rec(i26MaintenanceResponse, slsOrdInpt);

		MaintanenceResponse<I47RecManOutput> i47MaintenanceResponse = new MaintanenceResponse<I47RecManOutput>();
		i47MaintenanceResponse.setOutput(new I47RecManOutput());
		updtI47Rec(i47MaintenanceResponse, slsOrdInpt);

		MaintanenceResponse<I48RecManOutput> i48MaintenanceResponse = new MaintanenceResponse<I48RecManOutput>();
		i48MaintenanceResponse.setOutput(new I48RecManOutput());
		updtI48Rec(i48MaintenanceResponse, slsOrdInpt);

		MaintanenceResponse<S01RecManOutput> s01MaintenanceResponse = new MaintanenceResponse<S01RecManOutput>();
		s01MaintenanceResponse.setOutput(new S01RecManOutput());
		updtS01Rec(s01MaintenanceResponse, slsOrdInpt);

		MaintanenceResponse<S03RecManOutput> s03MaintenanceResponse = new MaintanenceResponse<S03RecManOutput>();
		s03MaintenanceResponse.setOutput(new S03RecManOutput());
		if (slsOrdInpt.getFrtChrg() != null && slsOrdInpt.getVenId() > 0) {
			updtS03Rec(s03MaintenanceResponse, slsOrdInpt);
		}

		MaintanenceResponse<S04RecManOutput> s04MaintenanceResponse = new MaintanenceResponse<S04RecManOutput>();
		s04MaintenanceResponse.setOutput(new S04RecManOutput());
		if (slsOrdInpt.getHdrRmk() != null && slsOrdInpt.getHdrRmk().trim().length() > 0) {
			updtS04Rec(s04MaintenanceResponse, slsOrdInpt, 0, "05", slsOrdInpt.getHdrRmk().trim());

			insertCusShpRmk(s04MaintenanceResponse, slsOrdInpt);
		}

		MaintanenceResponse<S05RecManOutput> s05MaintenanceResponse = new MaintanenceResponse<S05RecManOutput>();
		s05MaintenanceResponse.setOutput(new S05RecManOutput());
		updtS05Rec(s05MaintenanceResponse, slsOrdInpt);

		MaintanenceResponse<S06RecManOutput> s06MaintenanceResponse = new MaintanenceResponse<S06RecManOutput>();
		s06MaintenanceResponse.setOutput(new S06RecManOutput());
		updtS06Rec(s06MaintenanceResponse, slsOrdInpt);

		MaintanenceResponse<S09RecManOutput> s09MaintenanceResponse = new MaintanenceResponse<S09RecManOutput>();
		s09MaintenanceResponse.setOutput(new S09RecManOutput());
		if (slsOrdInpt.getTolFlg().equalsIgnoreCase("S")) {
			updtS09Rec(s09MaintenanceResponse, slsOrdInpt);
		}

		MaintanenceResponse<S11RecManOutput> s11MaintenanceResponse = new MaintanenceResponse<S11RecManOutput>();
		s11MaintenanceResponse.setOutput(new S11RecManOutput());
		if (slsOrdInpt.getPkgFlg().equalsIgnoreCase("S")) {
			updtS11Rec(s11MaintenanceResponse, slsOrdInpt);
		}

		MaintanenceResponse<S16RecManOutput> s16MaintenanceResponse = new MaintanenceResponse<S16RecManOutput>();
		s16MaintenanceResponse.setOutput(new S16RecManOutput());
		updtS16Rec(s16MaintenanceResponse, slsOrdInpt);

		MaintanenceResponse<S52RecManOutput> s52MaintenanceResponse = new MaintanenceResponse<S52RecManOutput>();
		s52MaintenanceResponse.setOutput(new S52RecManOutput());
		updtS52Rec(s52MaintenanceResponse, slsOrdInpt);

		MaintanenceResponse<S54RecManOutput> s54MaintenanceResponse = new MaintanenceResponse<S54RecManOutput>();
		s54MaintenanceResponse.setOutput(new S54RecManOutput());
		if (slsOrdInpt.getUnitPrice() != null) {
			updtS54Rec(s54MaintenanceResponse, slsOrdInpt);

			if (slsOrdInpt.getSlsCat().equalsIgnoreCase("PR")) {
				insertPrcChrgs(s54MaintenanceResponse, slsOrdInpt);
			}
		}

		if (i25MaintenanceResponse.output.rtnSts == 0 && i26MaintenanceResponse.output.rtnSts == 0
				&& i47MaintenanceResponse.output.rtnSts == 0 && i48MaintenanceResponse.output.rtnSts == 0
				&& s01MaintenanceResponse.output.rtnSts == 0 && s03MaintenanceResponse.output.rtnSts == 0
				&& s04MaintenanceResponse.output.rtnSts == 0 && s05MaintenanceResponse.output.rtnSts == 0
				&& s06MaintenanceResponse.output.rtnSts == 0 && s09MaintenanceResponse.output.rtnSts == 0
				&& s11MaintenanceResponse.output.rtnSts == 0 && s16MaintenanceResponse.output.rtnSts == 0
				&& s52MaintenanceResponse.output.rtnSts == 0 && s54MaintenanceResponse.output.rtnSts == 0) {

			MaintanenceResponse<TI00RecManOutput> i00MaintenanceResponse = new MaintanenceResponse<TI00RecManOutput>();
			i00MaintenanceResponse.setOutput(new TI00RecManOutput());
			updtTI00Rec(i00MaintenanceResponse, slsOrdInpt);

			if (i00MaintenanceResponse.output.rtnSts == 0) {
				String stsCd = getSlsOrdStsCd(slsOrdInpt);

				addOrderInfo(starMaintenanceResponse, slsOrdInpt);

				Date d1 = new Date();

				// long finish = System.currentTimeMillis() + 10000;

				// while ( System.currentTimeMillis() < finish ) {
				while (true) {
					Date d2 = new Date();

					long seconds = (d2.getTime() - d1.getTime()) / 1000;

					if (seconds >= 60) {
						break;
					}

					if (!stsCd.equalsIgnoreCase("E") && !stsCd.equalsIgnoreCase("Y")) {
						stsCd = getSlsOrdStsCd(slsOrdInpt);
					} else {
						break;
					}
				}

				starMaintenanceResponse.output.stsCd = stsCd;

				if (stsCd.equalsIgnoreCase("Y")) {
					slsOrdInpt.setSoNo(getSlsOrdNo(slsOrdInpt));

					starMaintenanceResponse.output.soNo = slsOrdInpt.getSoNo();
					starMaintenanceResponse.output.ordItmNo = getSoOrdItm(slsOrdInpt);

					updtOdrInfo(starMaintenanceResponse, slsOrdInpt, getSlsOrdNo(slsOrdInpt));
				} else if (stsCd.equalsIgnoreCase("E")) {
					getSlsOrdErr(starMaintenanceResponse, slsOrdInpt);
				} else if (stsCd.equalsIgnoreCase("N")) {
					delInsrtdSOGTblData(slsOrdInpt);

					starMaintenanceResponse.output.rtnSts = 1;
					starMaintenanceResponse.output.messages.add("E - " + CommonConstants.SERVER_ERR);

					System.out.print("\n\n Sales Order Gateway not processing data.... \n\n");
				}
			} else {
				delInsrtdSOGTblData(slsOrdInpt);

				starMaintenanceResponse.output.rtnSts = 1;
				starMaintenanceResponse.output.messages = i00MaintenanceResponse.output.messages;

				System.out.print("\n\n INSERT ERROR in updtTI00Rec \n\n");
			}

		} else {
			delInsrtdSOGTblData(slsOrdInpt);

			starMaintenanceResponse.output.rtnSts = 1;

			if (i25MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = i25MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtI25Rec \n\n");
			} else if (i26MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = i26MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtI26Rec \n\n");
			} else if (i47MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = i47MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtI47Rec \n\n");
			} else if (i48MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = i48MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtI48Rec \n\n");
			} else if (s01MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = s01MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtS01Rec \n\n");
			} else if (s03MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = s03MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtS03Rec \n\n");
			} else if (s04MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = s04MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtS04Rec \n\n");
			} else if (s05MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = s05MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtS05Rec \n\n");
			} else if (s06MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = s06MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtS06Rec \n\n");
			} else if (s09MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = s09MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtS09Rec \n\n");
			} else if (s11MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = s11MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtS11Rec \n\n");
			} else if (s16MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = s16MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtS16Rec \n\n");
			} else if (s52MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = s52MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtS52Rec \n\n");
			} else if (s54MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = s54MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtS54Rec \n\n");
			}
		}
	}

	public void delInsrtdSOGTblData(SlsOrdInput slsOrdInpt) {
		deleteI25Record(slsOrdInpt);
		deleteI26Record(slsOrdInpt);
		deleteI47Record(slsOrdInpt);
		deleteI48Record(slsOrdInpt);
		deleteS01Record(slsOrdInpt);
		deleteS03Record(slsOrdInpt);
		deleteS04Record(slsOrdInpt);
		deleteS05Record(slsOrdInpt);
		deleteS06Record(slsOrdInpt);
		deleteS09Record(slsOrdInpt);
		deleteS11Record(slsOrdInpt);
		deleteS16Record(slsOrdInpt);
		deleteS52Record(slsOrdInpt);
		deleteS54Record(slsOrdInpt);
	}

	public void deleteI25Record(SlsOrdInput slsOrdInpt) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "DELETE FROM xcvi25_rec WHERE i25_cmpy_id=:cmpy_id AND i25_intchg_pfx='XI' AND i25_intchg_no=:trc_ctl_no AND "
					+ "i25_src_co_id='SOG' AND i25_trc_ctl_no=:trc_ctl_no AND i25_ord_pfx='" + slsOrdInpt.getOrdPfx()
					+ "'";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", slsOrdInpt.getTrcCtlNo());

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void deleteI26Record(SlsOrdInput slsOrdInpt) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "DELETE FROM xcvi26_rec WHERE i26_cmpy_id=:cmpy_id AND i26_src_co_id='SOG' AND i26_trc_ctl_no=:trc_ctl_no";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", slsOrdInpt.getTrcCtlNo());

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void deleteI47Record(SlsOrdInput slsOrdInpt) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "DELETE FROM xcvi47_rec WHERE i47_cmpy_id=:cmpy_id AND i47_src_co_id='SOG' AND i47_trc_ctl_no=:trc_ctl_no";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", slsOrdInpt.getTrcCtlNo());

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void deleteI48Record(SlsOrdInput slsOrdInpt) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "DELETE FROM xcvi48_rec WHERE i48_cmpy_id=:cmpy_id AND i48_src_co_id='SOG' AND i48_trc_ctl_no=:trc_ctl_no";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", slsOrdInpt.getTrcCtlNo());

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void deleteS01Record(SlsOrdInput slsOrdInpt) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "DELETE FROM xcvs01_rec WHERE s01_cmpy_id=:cmpy_id AND s01_src_co_id='SOG' AND s01_trc_ctl_no=:trc_ctl_no";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", slsOrdInpt.getTrcCtlNo());

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void deleteS03Record(SlsOrdInput slsOrdInpt) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "DELETE FROM xcvs03_rec WHERE s03_cmpy_id=:cmpy_id AND s03_src_co_id='SOG' AND s03_trc_ctl_no=:trc_ctl_no";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", slsOrdInpt.getTrcCtlNo());

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void deleteS04Record(SlsOrdInput slsOrdInpt) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "DELETE FROM xcvs04_rec WHERE s04_cmpy_id=:cmpy_id AND s04_src_co_id='SOG' AND s04_trc_ctl_no=:trc_ctl_no";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", slsOrdInpt.getTrcCtlNo());

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void deleteS05Record(SlsOrdInput slsOrdInpt) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "DELETE FROM xcvs05_rec WHERE s05_cmpy_id=:cmpy_id AND s05_src_co_id='SOG' AND s05_trc_ctl_no=:trc_ctl_no";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", slsOrdInpt.getTrcCtlNo());

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void deleteS06Record(SlsOrdInput slsOrdInpt) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "DELETE FROM xcvs06_rec WHERE s06_cmpy_id=:cmpy_id AND s06_src_co_id='SOG' AND s06_trc_ctl_no=:trc_ctl_no";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", slsOrdInpt.getTrcCtlNo());

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void deleteS09Record(SlsOrdInput slsOrdInpt) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "DELETE FROM xcvs09_rec WHERE s09_cmpy_id=:cmpy_id AND s09_src_co_id='SOG' AND s09_trc_ctl_no=:trc_ctl_no";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", slsOrdInpt.getTrcCtlNo());

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void deleteS11Record(SlsOrdInput slsOrdInpt) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "DELETE FROM xcvs11_rec WHERE s11_cmpy_id=:cmpy_id AND s11_src_co_id='SOG' AND s11_trc_ctl_no=:trc_ctl_no";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", slsOrdInpt.getTrcCtlNo());

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void deleteS16Record(SlsOrdInput slsOrdInpt) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "DELETE FROM xcvs16_rec WHERE s16_cmpy_id=:cmpy_id AND s16_src_co_id='SOG' AND s16_trc_ctl_no=:trc_ctl_no";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", slsOrdInpt.getTrcCtlNo());

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void deleteS52Record(SlsOrdInput slsOrdInpt) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "DELETE FROM xcvs52_rec WHERE s52_cmpy_id=:cmpy_id AND s52_src_co_id='SOG' AND s52_trc_ctl_no=:trc_ctl_no AND "
					+ "s52_ptord_pfx='" + slsOrdInpt.getOrdPfx() + "' AND s52_ord_pfx='" + slsOrdInpt.getOrdPfx() + "'";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", slsOrdInpt.getTrcCtlNo());

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void deleteS54Record(SlsOrdInput slsOrdInpt) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "DELETE FROM xcvs54_rec WHERE s54_cmpy_id=:cmpy_id AND s54_src_co_id='SOG' AND s54_trc_ctl_no=:trc_ctl_no";
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", slsOrdInpt.getTrcCtlNo());

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public String getSlsOrdStsCd(SlsOrdInput slsOrdInpt) {
		Session session = null;
		String hql = "";
		String stsCd = "";
		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT CAST(i00_sts_cd AS VARCHAR(1)) i00_sts_cd, i00_ssn_log_ctl_no FROM xcti00_rec WHERE i00_cmpy_id=:cmpy_id AND i00_intchg_pfx='XI' AND i00_intchg_no=:trc_ctl_no";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", slsOrdInpt.getTrcCtlNo());

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				// System.out.print("\n\n-----------\nSales order status Code: " +
				// row[0].toString() + "\n------------\n\n");
				stsCd = row[0].toString();
				slsOrdInpt.setLogCtlNo(Integer.parseInt(row[1].toString()));
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return stsCd;
	}

	public int getSlsOrdNo(SlsOrdInput slsOrdInpt) {
		int soNo = 0;
		Session session = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT i25_ord_asgn_no, 1 FROM xcvi25_rec WHERE i25_cmpy_id=:cmpy_id AND i25_intchg_pfx='XI' AND "
					+ "i25_intchg_no=:trc_ctl_no AND i25_src_co_id='SOG' AND i25_trc_ctl_no=:trc_ctl_no AND "
					+ "i25_ord_pfx='" + slsOrdInpt.getOrdPfx() + "'";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("trc_ctl_no", slsOrdInpt.getTrcCtlNo());

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				soNo = Integer.parseInt(row[0].toString());
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return soNo;
	}

	public int getSlsOrdNoByPO(SlsOrdInput slsOrdInpt) {
		int soNo = 0;
		Session session = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT ord_ord_no, 1 FROM ortord_rec WHERE ord_cmpy_id=:cmpy_id AND ord_ord_pfx='"
					+ slsOrdInpt.getOrdPfx() + "' AND ord_sld_cus_id=:cus_id AND "
					+ "ord_cus_po=:po_no AND ord_ord_itm=1 ORDER BY ord_ord_no LIMIT 1";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("cus_id", slsOrdInpt.getCusId());
			queryValidate.setParameter("po_no", slsOrdInpt.getPoNo());

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				soNo = Integer.parseInt(row[0].toString());
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return soNo;
	}

	public void addItemToSalesOrder(MaintanenceResponse<SlsOrdManOutput> starMaintenanceResponse,
			SlsOrdInput slsOrdInpt) {

		MaintanenceResponse<I25RecManOutput> i25MaintenanceResponse = new MaintanenceResponse<I25RecManOutput>();
		i25MaintenanceResponse.setOutput(new I25RecManOutput());
		updtI25Rec(i25MaintenanceResponse, slsOrdInpt);

		MaintanenceResponse<I21RecManOutput> i21MaintenanceResponse = new MaintanenceResponse<I21RecManOutput>();
		i21MaintenanceResponse.setOutput(new I21RecManOutput());
		updtI21Rec(i21MaintenanceResponse, slsOrdInpt);

		if (slsOrdInpt.getMtlStdList().size() > 0) {
			MaintanenceResponse<S12RecManOutput> s12MaintenanceResponse = new MaintanenceResponse<S12RecManOutput>();
			s12MaintenanceResponse.setOutput(new S12RecManOutput());
			updts12Rec(s12MaintenanceResponse, slsOrdInpt);
		}

		MaintanenceResponse<I47RecManOutput> i47MaintenanceResponse = new MaintanenceResponse<I47RecManOutput>();
		i47MaintenanceResponse.setOutput(new I47RecManOutput());
		updtI47Rec(i47MaintenanceResponse, slsOrdInpt);

		MaintanenceResponse<I48RecManOutput> i48MaintenanceResponse = new MaintanenceResponse<I48RecManOutput>();
		i48MaintenanceResponse.setOutput(new I48RecManOutput());
		updtI48Rec(i48MaintenanceResponse, slsOrdInpt);

		MaintanenceResponse<S03RecManOutput> s03MaintenanceResponse = new MaintanenceResponse<S03RecManOutput>();
		s03MaintenanceResponse.setOutput(new S03RecManOutput());
		if (slsOrdInpt.getFrtChrg() != null && slsOrdInpt.getVenId() > 0) {
			updtS03Rec(s03MaintenanceResponse, slsOrdInpt);
		}

		MaintanenceResponse<S05RecManOutput> s05MaintenanceResponse = new MaintanenceResponse<S05RecManOutput>();
		s05MaintenanceResponse.setOutput(new S05RecManOutput());
		updtS05Rec(s05MaintenanceResponse, slsOrdInpt);

		MaintanenceResponse<S06RecManOutput> s06MaintenanceResponse = new MaintanenceResponse<S06RecManOutput>();
		s06MaintenanceResponse.setOutput(new S06RecManOutput());
		updtS06Rec(s06MaintenanceResponse, slsOrdInpt);

		MaintanenceResponse<S09RecManOutput> s09MaintenanceResponse = new MaintanenceResponse<S09RecManOutput>();
		s09MaintenanceResponse.setOutput(new S09RecManOutput());
		if (slsOrdInpt.getTolFlg().equalsIgnoreCase("S")) {
			updtS09Rec(s09MaintenanceResponse, slsOrdInpt);
		}

		MaintanenceResponse<S11RecManOutput> s11MaintenanceResponse = new MaintanenceResponse<S11RecManOutput>();
		s11MaintenanceResponse.setOutput(new S11RecManOutput());
		if (slsOrdInpt.getPkgFlg().equalsIgnoreCase("S")) {
			updtS11Rec(s11MaintenanceResponse, slsOrdInpt);
		}

		MaintanenceResponse<S16RecManOutput> s16MaintenanceResponse = new MaintanenceResponse<S16RecManOutput>();
		s16MaintenanceResponse.setOutput(new S16RecManOutput());
		updtS16Rec(s16MaintenanceResponse, slsOrdInpt);

		MaintanenceResponse<S54RecManOutput> s54MaintenanceResponse = new MaintanenceResponse<S54RecManOutput>();
		s54MaintenanceResponse.setOutput(new S54RecManOutput());
		if (slsOrdInpt.getUnitPrice() != null) {
			updtS54Rec(s54MaintenanceResponse, slsOrdInpt);

			if (slsOrdInpt.getSlsCat().equalsIgnoreCase("PR")) {
				insertPrcChrgs(s54MaintenanceResponse, slsOrdInpt);
			}
		}

		if (i25MaintenanceResponse.output.rtnSts == 0 && i47MaintenanceResponse.output.rtnSts == 0
				&& i48MaintenanceResponse.output.rtnSts == 0 && s03MaintenanceResponse.output.rtnSts == 0
				&& s05MaintenanceResponse.output.rtnSts == 0 && s06MaintenanceResponse.output.rtnSts == 0
				&& s09MaintenanceResponse.output.rtnSts == 0 && s11MaintenanceResponse.output.rtnSts == 0
				&& s16MaintenanceResponse.output.rtnSts == 0 && s54MaintenanceResponse.output.rtnSts == 0) {

			MaintanenceResponse<TI00RecManOutput> i00MaintenanceResponse = new MaintanenceResponse<TI00RecManOutput>();
			i00MaintenanceResponse.setOutput(new TI00RecManOutput());
			updtTI00Rec(i00MaintenanceResponse, slsOrdInpt);

			if (i00MaintenanceResponse.output.rtnSts == 0) {
				String stsCd = getSlsOrdStsCd(slsOrdInpt);

				addOrderInfo(starMaintenanceResponse, slsOrdInpt);

				Date d1 = new Date();

				// long finish = System.currentTimeMillis() + 10000;

				// while ( System.currentTimeMillis() < finish ) {
				while (true) {
					Date d2 = new Date();

					long seconds = (d2.getTime() - d1.getTime()) / 1000;

					if (seconds >= 60) {
						break;
					}

					if (!stsCd.equalsIgnoreCase("E") && !stsCd.equalsIgnoreCase("Y")) {
						stsCd = getSlsOrdStsCd(slsOrdInpt);
					} else {
						break;
					}
				}

				starMaintenanceResponse.output.stsCd = stsCd;

				if (stsCd.equalsIgnoreCase("Y")) {
					starMaintenanceResponse.output.soNo = slsOrdInpt.getSoNo();
					starMaintenanceResponse.output.ordItmNo = getSoOrdItm(slsOrdInpt);

					updtOdrInfo(starMaintenanceResponse, slsOrdInpt, slsOrdInpt.getSoNo());
				} else if (stsCd.equalsIgnoreCase("E")) {
					getSlsOrdErr(starMaintenanceResponse, slsOrdInpt);
				} else if (stsCd.equalsIgnoreCase("N")) {
					delInsrtdSOGTblData(slsOrdInpt);

					starMaintenanceResponse.output.rtnSts = 1;
					starMaintenanceResponse.output.messages.add("E - " + CommonConstants.SERVER_ERR);

					System.out.print("\n\n Sales Order Gateway not processing data.... \n\n");
				}
			} else {
				delInsrtdSOGTblData(slsOrdInpt);

				starMaintenanceResponse.output.rtnSts = 1;
				starMaintenanceResponse.output.messages = i00MaintenanceResponse.output.messages;

				System.out.print("\n\n INSERT ERROR in updtTI00Rec \n\n");
			}
		} else {
			delInsrtdSOGTblData(slsOrdInpt);

			starMaintenanceResponse.output.rtnSts = 1;

			if (i25MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = i25MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtI25Rec \n\n");
			} else if (i47MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = i47MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtI47Rec \n\n");
			} else if (i48MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = i48MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtI48Rec \n\n");
			} else if (s03MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = s03MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtS03Rec \n\n");
			} else if (s05MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = s05MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtS05Rec \n\n");
			} else if (s06MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = s06MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtS06Rec \n\n");
			} else if (s09MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = s09MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtS09Rec \n\n");
			} else if (s11MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = s11MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtS11Rec \n\n");
			} else if (s16MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = s16MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtS16Rec \n\n");
			} else if (s54MaintenanceResponse.output.messages.size() > 0) {
				starMaintenanceResponse.output.messages = s54MaintenanceResponse.output.messages;
				System.out.print("\n\n INSERT ERROR in updtS54Rec \n\n");
			}
		}
	}

	public void getSlsOrdErr(MaintanenceResponse<SlsOrdManOutput> starMaintenanceResponse, SlsOrdInput slsOrdInpt) {
		Session session = null;
		String hql = "";
		String errMsg = "";
		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT msg_err_msg_typ, msg_msg_var FROM sctmsg_rec, sctslg_rec, xcti00_rec WHERE msg_cmpy_id=slg_cmpy_id AND "
					+ " slg_cmpy_id=i00_cmpy_id AND i00_ssn_log_ctl_no=slg_ssn_log_ctl_no "
					+ " AND slg_ssn_log_seq_no=1 AND msg_clnt_dtts=slg_clnt_dtts "
					+ " AND msg_clnt_host_nm=slg_clnt_host_nm " + " AND msg_clnt_tty_nm=slg_clnt_tty_nm "
					+ " AND msg_clnt_pid=slg_clnt_pid " + " AND msg_clnt_dtts=slg_clnt_dtts "
					+ " AND msg_clnt_dtms=slg_clnt_dtms "
					+ " AND slg_cmpy_id=:cmpy_id AND slg_ssn_log_ctl_no=:log_ctl_no ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("log_ctl_no", slsOrdInpt.getLogCtlNo());

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				if (errMsg.length() > 0) {
					errMsg += "|";
				}
				errMsg += row[0].toString() + "-" + row[1].toString().trim();

				starMaintenanceResponse.output.messages.add(row[0].toString() + "-" + row[1].toString().trim());
			}

			updtOdrInfoErr(starMaintenanceResponse, slsOrdInpt, errMsg);

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public int getSoOrdItm(SlsOrdInput slsOrdInpt) {
		Session session = null;
		int ordItm = 0;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT MAX(ord_ord_itm), 1 FROM ortord_rec WHERE ord_cmpy_id=:cmpy_id AND ord_ord_pfx='"
					+ slsOrdInpt.getOrdPfx() + "' AND ord_ord_no=:ord_no";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("ord_no", slsOrdInpt.getSoNo());

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				ordItm = Integer.parseInt(row[0].toString());
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return ordItm;
	}

	public void addOrderInfo(MaintanenceResponse<SlsOrdManOutput> starMaintenanceResponse, SlsOrdInput slsOrdInpt) {
		Session session = null;
		int rowCnt = 0;
		String hql = "";
		try {
			session = SessionUtil.getSession();

			hql = "SELECT COUNT(doc_id), 1 FROM order_info WHERE doc_id=:doc_id AND doc_row_no=:row_no AND cus_po=:cus_po";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("doc_id", slsOrdInpt.getDocId());
			queryValidate.setParameter("row_no", slsOrdInpt.getDocRowNo());
			queryValidate.setParameter("cus_po", slsOrdInpt.getPoNo());

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				rowCnt = Integer.parseInt(row[0].toString());
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		if (rowCnt == 0) {
			insertOrderInfo(starMaintenanceResponse, slsOrdInpt);
		}
	}

	public void insertOrderInfo(MaintanenceResponse<SlsOrdManOutput> starMaintenanceResponse, SlsOrdInput slsOrdInpt) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			OrderInfo ordInfo = new OrderInfo();

			ordInfo.setDocId(slsOrdInpt.getDocId());
			ordInfo.setDocRowNo(slsOrdInpt.getDocRowNo());
			ordInfo.setCusPo(slsOrdInpt.getPoNo());
			ordInfo.setRefItm(0);
			ordInfo.setCtlNo(slsOrdInpt.getTrcCtlNo());
			ordInfo.setRefPfx("SO");
			ordInfo.setRefNo(0);
			ordInfo.setMsg("");

			session.save(ordInfo);

			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			// starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtOdrInfo(MaintanenceResponse<SlsOrdManOutput> starMaintenanceResponse, SlsOrdInput slsOrdInpt,
			int soNo) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "UPDATE order_info SET ref_no=:so_no, ref_itm=:ref_itm WHERE doc_id=:doc_id AND doc_row_no=:row_no AND cus_po=:po_no";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("so_no", starMaintenanceResponse.output.soNo);
			queryValidate.setParameter("ref_itm", slsOrdInpt.getItmNo());
			queryValidate.setParameter("doc_id", slsOrdInpt.getDocId());
			queryValidate.setParameter("row_no", slsOrdInpt.getDocRowNo());
			queryValidate.setParameter("po_no", slsOrdInpt.getPoNo());

			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			// starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtOdrInfoErr(MaintanenceResponse<SlsOrdManOutput> starMaintenanceResponse, SlsOrdInput slsOrdInpt,
			String msg) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "UPDATE order_info SET msg=:msg WHERE doc_id=:doc_id AND doc_row_no=:row_no AND cus_po=:po_no";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("msg", msg);
			queryValidate.setParameter("doc_id", slsOrdInpt.getDocId());
			queryValidate.setParameter("row_no", slsOrdInpt.getDocRowNo());
			queryValidate.setParameter("po_no", slsOrdInpt.getPoNo());

			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			// starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public int getCustPoRow(SlsOrdInput slsOrdInpt) {
		Session session = null;
		int rowCnt = 0;
		String hql = "";
		try {
			session = SessionUtil.getSession();

			hql = "SELECT COUNT(doc_id), 1 FROM order_info WHERE cus_po=:cus_po AND doc_id <> :doc_id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cus_po", slsOrdInpt.getPoNo());
			queryValidate.setParameter("doc_id", slsOrdInpt.getDocId());

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				rowCnt = Integer.parseInt(row[0].toString());
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return rowCnt;
	}

	public void getSrcWhs(SlsOrdInput slsOrdInpt) {
		Session session = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT CAST(rpv_rto_lvl_val AS VARCHAR(8)), 1 FROM mxrrpv_rec WHERE rpv_rto_nm='DFLT-SRC-WHS' AND rpv_cmpy_id=:cmpy_id "
					+ "AND rpv_brh=:brh";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("brh", slsOrdInpt.getRefBrh());

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				if (row[0] != null) {
					slsOrdInpt.setSrcWhs(row[0].toString());
				}
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void getPwcInformation(SlsOrdInput slsOrdInpt, List<String> pwcDetails) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = "select cast(dpp_pwg as varchar(3)), cast(dpp_pwc as varchar(3)), cast(prs_info.prs as varchar(3)), prs_info.cc  from "
					+ "(select spr_cmpy_id, spr_rte_sls_cat, spr_prs_rte, spr_prs_1 prs,spr_cc_no_1 cc, spr_actv from orrspr_rec union select spr_cmpy_id, "
					+ "spr_rte_sls_cat, spr_prs_rte, spr_prs_2 prs,spr_cc_no_2 cc, spr_actv from orrspr_rec union select spr_cmpy_id, spr_rte_sls_cat, spr_prs_rte, "
					+ "spr_prs_3 prs,spr_cc_no_3 cc, spr_actv from orrspr_rec union select spr_cmpy_id, spr_rte_sls_cat, spr_prs_rte, spr_prs_4 prs,spr_cc_no_5 cc, "
					+ "spr_actv from orrspr_rec union select spr_cmpy_id, spr_rte_sls_cat, spr_prs_rte, spr_prs_5 prs,spr_cc_no_5 cc, spr_actv from orrspr_rec union "
					+ "select spr_cmpy_id, spr_rte_sls_cat, spr_prs_rte, spr_prs_6 prs,spr_cc_no_6 cc, spr_actv from orrspr_rec union select spr_cmpy_id, spr_rte_sls_cat, "
					+ "spr_prs_rte, spr_prs_7 prs,spr_cc_no_7 cc, spr_actv from orrspr_rec union select spr_cmpy_id, spr_rte_sls_cat, spr_prs_rte, "
					+ "spr_prs_8 prs,spr_cc_no_8 cc, spr_actv from orrspr_rec union select spr_cmpy_id, spr_rte_sls_cat, spr_prs_rte, spr_prs_9 prs,spr_cc_no_9 cc, "
					+ "spr_actv from orrspr_rec union select spr_cmpy_id, spr_rte_sls_cat, spr_prs_rte, spr_prs_10 prs,spr_cc_no_10 cc, spr_actv from orrspr_rec) "
					+ "prs_info, iprdpp_rec, iprvpp_rec where dpp_cmpy_id = vpp_cmpy_id and dpp_whs = vpp_whs and vpp_pwc = dpp_pwc and vpp_prs = prs_info.prs and "
					+ "vpp_cmpy_id = prs_info.spr_cmpy_id and dpp_cmpy_id = :cmpy_id and dpp_whs=:whs and prs_info.spr_actv = 1 and prs_info.prs <>'' and spr_rte_sls_cat=:sls_cat "
					+ "and spr_prs_rte=:prs_rte";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("whs", slsOrdInpt.getSrcWhs());
			queryValidate.setParameter("sls_cat", slsOrdInpt.getSlsCat());
			queryValidate.setParameter("prs_rte", slsOrdInpt.getStndrdRoute());

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				if (row[0] != null) {
					pwcDetails.add(row[0].toString());
				} else {
					pwcDetails.add("");
				}

				if (row[1] != null) {
					pwcDetails.add(row[1].toString());
				} else {
					pwcDetails.add("");
				}

				if (row[2] != null) {
					pwcDetails.add(row[2].toString());
				} else {
					pwcDetails.add("");
				}

				if (row[3] != null) {
					pwcDetails.add(row[3].toString());
				} else {
					pwcDetails.add("");
				}

			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void getProcessCharge(SlsOrdInput slsOrdInpt, List<String> prsCostDetails, String prs) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = "select prs_cc_no, cast(ccr_desc20 as varchar(20)) from orrprs_rec,ctrccr_rec where prs_cc_no = ccr_cc_no and prs_cmpy_id = :cmpy_id and prs_prs=:prs";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("prs", prs);

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				if (row[0] != null) {
					prsCostDetails.add(row[0].toString());
				} else {
					prsCostDetails.add("");
				}

				if (row[1] != null) {
					prsCostDetails.add(row[1].toString());
				} else {
					prsCostDetails.add("");
				}

			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void getPlngWhs(SlsOrdInput slsOrdInpt) {
		Session session = null;
		String hql = "";
		String plngWhs = "";
		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT CAST(rpv_rto_lvl_val AS VARCHAR(8)), 1 FROM mxrrpv_rec WHERE rpv_rto_nm='DFLT-PLNG-WHS' AND rpv_cmpy_id=:cmpy_id "
					+ "AND rpv_brh=:brh";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("brh", slsOrdInpt.getRefBrh());

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				if (row[0] != null) {
					plngWhs = row[0].toString();
				}
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		if (plngWhs.trim().equalsIgnoreCase("")) {
			slsOrdInpt.setPlngWhs(slsOrdInpt.getSrcWhs());
		} else {
			slsOrdInpt.setPlngWhs(plngWhs);
		}
	}

	public void getShpgWhs(SlsOrdInput slsOrdInpt) {
		Session session = null;
		String hql = "";
		String shpgWhs = "";
		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT CAST(rpv_rto_lvl_val AS VARCHAR(8)), 1 FROM mxrrpv_rec WHERE rpv_rto_nm='DFLT-SHPG-WHS' AND rpv_cmpy_id=:cmpy_id "
					+ "AND rpv_brh=:brh";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("brh", slsOrdInpt.getRefBrh());

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				if (row[0] != null) {
					shpgWhs = row[0].toString();
				}
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		if (shpgWhs.trim().equalsIgnoreCase("")) {
			slsOrdInpt.setShpgWhs(slsOrdInpt.getSrcWhs());
		} else {
			slsOrdInpt.setShpgWhs(shpgWhs);
		}
	}

	public void getSalesPersons(SlsOrdInput slsOrdInpt) {
		Session session = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT CAST(shp_is_slp AS VARCHAR(4)), CAST(shp_os_slp AS VARCHAR(4)) FROM arrshp_rec WHERE shp_cmpy_id=:cmpy_id AND shp_cus_id=:cus_id AND "
					+ "shp_shp_to=:shp_to LIMIT 1";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("cus_id", slsOrdInpt.getCusId());

			if (slsOrdInpt.getShpTo() != null && slsOrdInpt.getShpTo().trim().length() > 0) {
				queryValidate.setParameter("shp_to", Integer.parseInt(slsOrdInpt.getShpTo()));
			} else {
				queryValidate.setParameter("shp_to", 0);
			}

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				slsOrdInpt.setIsSlp(row[0].toString());
				slsOrdInpt.setOsSlp(row[1].toString());
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	/* GET PROCESS LIST */
	public void getProcessList(SlsOrdInput slsOrdInpt) {
		Session session = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT CAST(shp_is_slp AS VARCHAR(4)), CAST(shp_os_slp AS VARCHAR(4)) FROM arrshp_rec WHERE shp_cmpy_id=:cmpy_id AND shp_cus_id=:cus_id AND "
					+ "shp_shp_to=:shp_to LIMIT 1";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("cus_id", slsOrdInpt.getCusId());

			if (slsOrdInpt.getShpTo() != null && slsOrdInpt.getShpTo().trim().length() > 0) {
				queryValidate.setParameter("shp_to", Integer.parseInt(slsOrdInpt.getShpTo()));
			} else {
				queryValidate.setParameter("shp_to", 0);
			}

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				slsOrdInpt.setIsSlp(row[0].toString());
				slsOrdInpt.setOsSlp(row[1].toString());
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void getBasLng(SlsOrdInput slsOrdInpt) {
		Session session = null;
		String hql = "";
		String basLng = "";
		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT CAST(csc_bas_lng AS VARCHAR(2)), 1 FROM scrcsc_rec WHERE csc_cmpy_id=:cmpy_id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				if (row[0] != null) {
					basLng = row[0].toString();
				}
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		slsOrdInpt.setBasLng(basLng);
	}

	public void insertCusShpRmk(MaintanenceResponse<S04RecManOutput> starMaintenanceResponse, SlsOrdInput slsOrdInpt) {
		Session session = null;
		String hql = "";
		Integer lnNo = 1;
		try {
			session = SessionUtilInformix.getSession();

			/*
			 * 05 | Order-Internal 10 | Order-External 15 | Quote 20 | Shipping/Receiving --
			 * All trucks must be tarped. 22 | Loading 25 | Test Cert 30 | Invoicing 45 |
			 * QT/SO Item Res ******** Heat Number must be marked on each piece ********* 50
			 * | Order Processing 55 | Packaging 60 | Label 67 | All deliveries to Dock 3 -
			 * Ship Delivery 68 | Bill of Lading 70 | Item Test Cert
			 */

			hql = "SELECT CAST(cvr_rmk_typ AS VARCHAR(2)), cvr_txt FROM scrcvr_rec WHERE cvr_cmpy_id=:cmpy_id AND cvr_ref_pfx='CS' AND cvr_cus_ven_typ='C' "
					+ " AND cvr_cus_ven_id=:cus_id AND cvr_cus_ven_shp=:shp_to AND cvr_rmk_cl='5' AND cvr_rmk_typ IN ('10', '20', '68') AND cvr_lng=:bas_lng";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("cus_id", slsOrdInpt.getCusId());

			if (slsOrdInpt.getShpTo() != null && slsOrdInpt.getShpTo().trim().length() > 0) {
				queryValidate.setParameter("shp_to", Integer.parseInt(slsOrdInpt.getShpTo()));
			} else {
				queryValidate.setParameter("shp_to", 0);
			}

			if (slsOrdInpt.getBasLng() != null && slsOrdInpt.getBasLng().trim().length() > 0) {
				queryValidate.setParameter("bas_lng", slsOrdInpt.getBasLng());
			} else {
				queryValidate.setParameter("bas_lng", "en");
			}

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				updtS04Rec(starMaintenanceResponse, slsOrdInpt, lnNo, row[0].toString(), row[1].toString());

				lnNo++;
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void getShpToPck(SlsOrdInput slsOrdInpt) {
		Session session = null;
		String hql = "";
		String tmpStr = "";
		BigDecimal bigDcmlTmp;
		Integer intTmp;

		try {
			session = SessionUtilInformix.getSession();

			hql = " SELECT CAST(spk_pkg AS VARCHAR(3)), CAST(spk_skd_typ AS VARCHAR(3)), spk_pcs_per_tag, spk_min_pcs_ptag, spk_min_pkg_wgt, spk_max_pkg_wgt, spk_max_hgt "
					+ " FROM arrspk_rec WHERE spk_cmpy_id=:cmpy_id AND spk_cus_id=:cus_id AND spk_shp_to=:shp_to";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("cus_id", slsOrdInpt.getCusId());

			if (slsOrdInpt.getShpTo() != null && slsOrdInpt.getShpTo().trim().length() > 0) {
				queryValidate.setParameter("shp_to", Integer.parseInt(slsOrdInpt.getShpTo()));
			} else {
				queryValidate.setParameter("shp_to", 0);
			}

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				slsOrdInpt.setPkgFlg("S");

				if (row[0] != null) {
					slsOrdInpt.setPkg(row[0].toString());
				}

				if (row[1] != null) {
					slsOrdInpt.setSkdTyp(row[1].toString());
				}

				if (row[2] != null) {
					tmpStr = row[2].toString();
					intTmp = new Integer(tmpStr);
					slsOrdInpt.setPcsPerTag(intTmp);
				}

				if (row[3] != null) {
					tmpStr = row[3].toString();
					intTmp = new Integer(tmpStr);
					slsOrdInpt.setMinPcsPtag(intTmp);
				}

				if (row[4] != null) {
					tmpStr = row[4].toString();
					bigDcmlTmp = new BigDecimal(tmpStr);
					slsOrdInpt.setMinPkgWgt(bigDcmlTmp);
				}

				if (row[5] != null) {
					tmpStr = row[5].toString();
					bigDcmlTmp = new BigDecimal(tmpStr);
					slsOrdInpt.setMaxPkgWgt(bigDcmlTmp);
				}

				if (row[6] != null) {
					tmpStr = row[6].toString();
					bigDcmlTmp = new BigDecimal(tmpStr);
					slsOrdInpt.setMaxHgt(bigDcmlTmp);
				}

			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void getShpToTolSpec(SlsOrdInput slsOrdInpt) {
		Session session = null;
		String hql = "";
		String tmpStr = "";
		BigDecimal bigDcmlTmp;
		Integer intTmp;

		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT srk_wdth_tol_posv, srk_wdth_tol_neg, srk_lgth_tol_posv, srk_lgth_tol_neg, srk_idia_tol_posv, srk_idia_tol_neg, srk_odia_tol_posv, srk_odia_tol_neg, srk_wthck_tol_posv, "
					+ " srk_wthck_tol_neg, srk_ga_tol_posv, srk_ga_tol_neg, srk_diag_tol, srk_fltns, CAST(srk_cmbr AS VARCHAR(15)) FROM arrsrk_rec "
					+ " WHERE srk_cmpy_id=:cmpy_id AND srk_cus_id=:cus_id AND srk_shp_to=:shp_to AND srk_frm=:frm AND srk_size=:size";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", slsOrdInpt.getCmpyId());
			queryValidate.setParameter("cus_id", slsOrdInpt.getCusId());

			if (slsOrdInpt.getShpTo() != null && slsOrdInpt.getShpTo().trim().length() > 0) {
				queryValidate.setParameter("shp_to", Integer.parseInt(slsOrdInpt.getShpTo()));
			} else {
				queryValidate.setParameter("shp_to", 0);
			}

			queryValidate.setParameter("frm", slsOrdInpt.getFrm());
			queryValidate.setParameter("size", slsOrdInpt.getSize());

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				slsOrdInpt.setTolFlg("S");

				if (row[0] != null) {
					tmpStr = row[0].toString();
					bigDcmlTmp = new BigDecimal(tmpStr);
					slsOrdInpt.setWdthTolPosv(bigDcmlTmp);
				}

				if (row[1] != null) {
					tmpStr = row[1].toString();
					bigDcmlTmp = new BigDecimal(tmpStr);
					slsOrdInpt.setWdthTolNeg(bigDcmlTmp);
				}

				if (row[2] != null) {
					tmpStr = row[2].toString();
					bigDcmlTmp = new BigDecimal(tmpStr);
					slsOrdInpt.setLgthTolPosv(bigDcmlTmp);
				}

				if (row[3] != null) {
					tmpStr = row[3].toString();
					bigDcmlTmp = new BigDecimal(tmpStr);
					slsOrdInpt.setLgthTolNeg(bigDcmlTmp);
				}

				if (row[4] != null) {
					tmpStr = row[4].toString();
					bigDcmlTmp = new BigDecimal(tmpStr);
					slsOrdInpt.setIdiaTolPosv(bigDcmlTmp);
				}

				if (row[5] != null) {
					tmpStr = row[5].toString();
					bigDcmlTmp = new BigDecimal(tmpStr);
					slsOrdInpt.setIdiaTolNeg(bigDcmlTmp);
				}

				if (row[6] != null) {
					tmpStr = row[6].toString();
					bigDcmlTmp = new BigDecimal(tmpStr);
					slsOrdInpt.setOdiaTolPosv(bigDcmlTmp);
				}

				if (row[7] != null) {
					tmpStr = row[7].toString();
					bigDcmlTmp = new BigDecimal(tmpStr);
					slsOrdInpt.setOdiaTolNeg(bigDcmlTmp);
				}

				if (row[8] != null) {
					tmpStr = row[8].toString();
					bigDcmlTmp = new BigDecimal(tmpStr);
					slsOrdInpt.setWthckTolPosv(bigDcmlTmp);
				}

				if (row[9] != null) {
					tmpStr = row[9].toString();
					bigDcmlTmp = new BigDecimal(tmpStr);
					slsOrdInpt.setWthckTolNeg(bigDcmlTmp);
				}

				if (row[10] != null) {
					tmpStr = row[10].toString();
					bigDcmlTmp = new BigDecimal(tmpStr);
					slsOrdInpt.setGaTolPosv(bigDcmlTmp);
				}

				if (row[11] != null) {
					tmpStr = row[11].toString();
					bigDcmlTmp = new BigDecimal(tmpStr);
					slsOrdInpt.setGaTolNeg(bigDcmlTmp);
				}

				if (row[12] != null) {
					tmpStr = row[12].toString();
					bigDcmlTmp = new BigDecimal(tmpStr);
					slsOrdInpt.setDiagTol(bigDcmlTmp);
				}

				if (row[13] != null) {
					tmpStr = row[13].toString();
					bigDcmlTmp = new BigDecimal(tmpStr);
					slsOrdInpt.setFltns(bigDcmlTmp);
				}

				if (row[14] != null) {
					slsOrdInpt.setCmbr(row[14].toString());
				}

			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public String getDfltWhsFromBrh(SlsOrdInput slsOrdInpt) {
		Session session = null;
		String hql = "";
		String whs = "";
		try {
			session = SessionUtil.getSession();

			hql = "SELECT CAST(whs AS VARCHAR(3)), 1 FROM brh_whs_cnfg WHERE brh=:brh";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("brh", slsOrdInpt.getRefBrh());

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				if (row[0] != null) {
					whs = row[0].toString();
				}
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return whs.trim();
	}

}
