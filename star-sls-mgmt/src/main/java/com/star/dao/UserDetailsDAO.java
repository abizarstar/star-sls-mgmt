package com.star.dao;

import java.math.BigInteger;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.AuthenticationChecksum;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.common.SessionUtilInformix;
import com.star.linkage.auth.UserBrowseOutput;
import com.star.linkage.auth.UserDetails;
import com.star.linkage.auth.UserDetailsInput;
import com.star.linkage.auth.UserDetailsManOutput;
import com.star.linkage.auth.UserGroupManOutput;
import com.star.linkage.auth.UserManOutput;
import com.star.linkage.usrgrp.UsrGrpBrowseList;
import com.star.linkage.usrgrp.UsrGrpBrowseOutput;
import com.star.linkage.usrgrp.UsrGrpManOutput;
import com.star.modal.UsrDtlsSls;
import com.star.modal.UsrGroup;

public class UserDetailsDAO {

	private static Logger logger = LoggerFactory.getLogger(UserDetailsDAO.class);

	public void addUser(MaintanenceResponse<UserDetailsManOutput> starMaintenanceResponse, UserDetailsInput usrInp) throws Exception {
		Session session = null;

		int iUsrExist = validateUser(usrInp.getUsrId());

		if (iUsrExist == 0) {
			try {
				session = SessionUtil.getSession();
				session.beginTransaction();

				UsrDtlsSls usrDtls = new UsrDtlsSls();

				usrDtls.setUsrId(usrInp.getUsrId());
				usrDtls.setUsrNm(usrInp.getUsrNm());
				usrDtls.setUsrEml(usrInp.getEml());
				usrDtls.setUsrGrp(Integer.parseInt(usrInp.getUsrGrp()));
				usrDtls.setUsrTyp(usrInp.getUsrType());
				usrDtls.setChkSum(usrInp.getChkSm());

				usrDtls.setIsActv(true);

				session.save(usrDtls);

				session.getTransaction().commit();
			} catch (Exception e) {

				logger.debug("User Details Info : {}", e.getMessage(), e);

			} finally {
				if (session != null) {
					session.close();
				}
			}
		} else {
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(CommonConstants.USR_ERR);
		}
	}

	public void getUsersAll(BrowseResponse<UserBrowseOutput> starBrowseResponse, String usrId) {
		Session session = null;

		try {
			session = SessionUtil.getSession();

			String hql = "";

			hql = " select usr_id, usr_nm, usr_eml, is_actv, usr_typ,"
					+ " (select grp_nm from usr_group where grp_id=usr_grp) grp, usr_grp, chk_sum"
					+ " from usr_dtls_sls dtls where 1=1";

			if (usrId.length() > 0) {
				hql = hql + " and usr_id=:usrid";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (usrId.length() > 0) {
				queryValidate.setParameter("usrid", usrId);
			}

			List<Object[]> usrLst = queryValidate.list();

			for (Object[] usr : usrLst) {

				UserDetails userDetails = new UserDetails();

				userDetails.setUserId(usr[0].toString());
				userDetails.setUserNm(usr[1].toString());
				userDetails.setEmailId(usr[2].toString());
				userDetails.setActvSts(usr[3].toString());
				

				if (usr[4] != null) {
					userDetails.setUsrTyp(usr[4].toString());
				} else {
					userDetails.setUsrTyp("");
				}


				if (usr[5] != null) {
					userDetails.setUsrGrp(usr[5].toString());
					userDetails.setUsrGrpId(usr[6].toString());
				} else {
					userDetails.setUsrGrp("");
					userDetails.setUsrGrpId("");
				}
				
				if(usr[7] != null)
				{
					userDetails.setChkSum(usr[7].toString());
				}

				starBrowseResponse.output.fldTblUsers.add(userDetails);
			}

		} catch (Exception e) {

			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public int validateUser(String usrId) {
		Session session = null;
		int iRcrdCount = 0;

		try {
			session = SessionUtil.getSession();

			String hql = "";

			hql = " select * from usr_dtls_sls where usr_id=:usrId";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("usrId", usrId);

			List<Object[]> usrLst = queryValidate.list();

			for (Object[] usr : usrLst) {

				iRcrdCount = iRcrdCount + 1;
			}

		} catch (Exception e) {

			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return iRcrdCount;
	}

	public void updUsrDtls(MaintanenceResponse<UserDetailsManOutput> maintanenceResponse,
			UserDetailsInput fieldsInput) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			if(fieldsInput.getChkSm().length() == 0)
			{
				hql = "update usr_dtls_sls set usr_nm = :usrNm , usr_eml=:usrEml, usr_grp=:usrgrp where "
						+ "usr_id=:usr_id ";
			}
			else
			{
				hql = "update usr_dtls_sls set usr_nm = :usrNm , usr_eml=:usrEml, usr_grp=:usrgrp, chk_sum=:chksum where "
						+ "usr_id=:usr_id ";
			}
			
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("usr_id", fieldsInput.getUsrId());
			queryValidate.setParameter("usrNm", fieldsInput.getUsrNm());
			queryValidate.setParameter("usrEml", fieldsInput.getEml());
			queryValidate.setParameter("usrgrp", Integer.parseInt(fieldsInput.getUsrGrp()));
			
			if(fieldsInput.getChkSm().length() > 0)
			{
				queryValidate.setParameter("chksum", fieldsInput.getChkSm());
			}
			
			queryValidate.executeUpdate();
			
			tx.commit();
		} catch (Exception e) {

			logger.debug("User Details Info : {}", e.getMessage(), e);
			maintanenceResponse.output.rtnSts = 1;
			maintanenceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void updUsrProfile(MaintanenceResponse<UserDetailsManOutput> maintanenceResponse,
			UserDetailsInput fieldsInput) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			
			hql = "update usr_dtls_sls set usr_nm = :usrNm , usr_eml=:usrEml where usr_id=:usr_id ";
			

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("usr_id", fieldsInput.getUsrId());

			queryValidate.setParameter("usrNm", fieldsInput.getUsrNm());
			queryValidate.setParameter("usrEml", fieldsInput.getEml());
			

			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {

			logger.debug("User Details Info : {}", e.getMessage(), e);
			maintanenceResponse.output.rtnSts = 1;
			maintanenceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void getUsersGroup(BrowseResponse<UsrGrpBrowseList> starBrowseResponse, String grpNm, int grpFlg) {
		Session session = null;

		try {
			session = SessionUtil.getSession();

			String hql = "";

			hql = " select grp_id, grp_nm, count(usr_id) as UserCount from usr_group LEFT JOIN usr_dtls_sls ON usr_group.grp_id=usr_dtls_sls.usr_grp where 1=1 and grp_nm   ";

			if (grpFlg == 0) {
				hql = hql + "like ('" + grpNm + "%')";
			} else {
				hql = hql + "= '" + grpNm + "'";
			}
			
			hql = hql + " group by grp_id,grp_nm";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> usrGrpLst = queryValidate.list();

			for (Object[] usrGrp : usrGrpLst) {

				UsrGrpBrowseOutput usrGroup = new UsrGrpBrowseOutput();

				usrGroup.setGrpId(Integer.parseInt(usrGrp[0].toString()));
				usrGroup.setGrpNm(usrGrp[1].toString());
				usrGroup.setUsrCnt(usrGrp[2].toString());

				starBrowseResponse.output.fldTblUsrGroup.add(usrGroup);
			}

		} catch (Exception e) {

			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public String getUserCustomer(String usrid) {
		Session session = null;
		String customerId = "";

		try {
			session = SessionUtil.getSession();

			String hql = "";

			hql = " select * from usr_ext_cus where usr_id=:usrid";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("usrid", usrid);

			List<Object[]> usrCusLst = queryValidate.list();

			for (Object[] customer : usrCusLst) {

				customerId = customer[1].toString();
			}

		} catch (Exception e) {

			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return customerId;
	}

	public boolean getUsersGroupbyId(int grpId) {
		Session session = null;

		boolean adminSts = false;

		try {
			session = SessionUtil.getSession();

			String hql = "";

			hql = " select * from usr_group where grp_id = :grpid ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("grpid", grpId);

			List<Object[]> usrGrpLst = queryValidate.list();

			for (Object[] usrGrp : usrGrpLst) {

				if (usrGrp[1].toString() != null) {
					if (usrGrp[1].toString().equals("ADMIN")) {
						adminSts = true;
					}
				}
			}

		} catch (Exception e) {

			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return adminSts;
	}

	public void addUserGroup(MaintanenceResponse<UserGroupManOutput> starMaintenanceResponse, String groupNm) {
		Session session = null;
		Transaction tx = null;

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			UsrGroup grp = new UsrGroup();

			grp.setGrpNm(groupNm);

			session.save(grp);

			starMaintenanceResponse.output.fldTblUsrGroup.add(grp);

			tx.commit();
		} catch (Exception e) {

			starMaintenanceResponse.output.rtnSts = 1;

			logger.debug("User Details Info : {}", e.getMessage(), e);
			tx.rollback();

		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public List<String> getUsersbyGroup(MaintanenceResponse<UserGroupManOutput> starManResponse, Session session,
			String grpId) {

		String hql = "";

		List<String> usrList = null;

		try {

			hql = "select usr_id from usr_dtls_sls where usr_grp= :grpid";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("grpid", Integer.parseInt(grpId));

			usrList = queryValidate.list();

		} catch (Exception e) {

			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());
			logger.debug("User Details Info : {}", e.getMessage(), e);
		}

		return usrList;
	}

	public boolean validatePassword(String usrId, String password) {
		Session session = null;

		boolean loginSts = false;

		try {
			session = SessionUtil.getSession();

			String hql = "";

			hql = " select usr_id,usr_nm,chk_sum from usr_dtls_sls where usr_id = :usrId";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("usrId", usrId);

			List<Object[]> validate = queryValidate.list();

			if (validate.size() > 0) {

				loginSts = AuthenticationChecksum.validatePassword(password, validate.get(0)[2].toString());
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);

		} finally {
			if (session != null) {
				session.close();
			}
		}

		return loginSts;
	}

	public void addGrp(MaintanenceResponse<UsrGrpManOutput> starManResponse, String grpNm) {
		Session session = null;
		Transaction tx = null;
		BigInteger Cnt;
		int iRcrdCnt;
		String hql="";

		try {
			session = SessionUtil.getSession();
			
			hql = "select count(*) as count from usr_group where grp_nm=:grpnm ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("grpnm",grpNm);
			
			List<BigInteger[]> groupList = queryValidate.list();
			
			Cnt=(BigInteger) queryValidate.uniqueResult();
			
			iRcrdCnt=Cnt.intValue();
			
			if(iRcrdCnt==0)
			{
				tx = session.beginTransaction();

				UsrGroup usrGrp = new UsrGroup();

				usrGrp.setGrpNm(grpNm);

				session.save(usrGrp);

				tx.commit();
			}
			else
			{
				starManResponse.output.rtnSts = 1;
				starManResponse.output.messages.add("Group Name already exist");
			}

		} catch (Exception e) {

			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());
			logger.debug("User Details Info : {}", e.getMessage(), e);
			tx.rollback();

		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	public void editGrp(MaintanenceResponse<UsrGrpManOutput> starManResponse, String grpdId, String grpNm) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();
			
			hql = "select count(*) as count from usr_group where grp_nm=:grpnm ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("grpnm",grpNm);
			
			List<BigInteger[]> groupList = queryValidate.list();
			
			BigInteger Cnt = (BigInteger) queryValidate.uniqueResult();
			
			int iRcrdCnt = Cnt.intValue();
			
			if(iRcrdCnt==0)
			{
				tx = session.beginTransaction();

				hql = "update usr_group set grp_nm=:grp_nm where grp_id=:grp_id ";

				queryValidate = session.createSQLQuery(hql);

				queryValidate.setParameter("grp_nm", grpNm);
				queryValidate.setParameter("grp_id",Integer.parseInt(grpdId));

				queryValidate.executeUpdate();

				tx.commit();
			}
			else
			{
				starManResponse.output.rtnSts = 1;
				starManResponse.output.messages.add("Group Name already exist");
			}
			
		} catch (Exception e) {

			logger.debug("User Details Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	public void delGrp(MaintanenceResponse<UsrGrpManOutput> starManResponse, String grpdId) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		BigInteger Cnt;
		int iRcrdCnt=0;
		try {
			session = SessionUtil.getSession();

			hql = "select count(*) as count from usr_dtls_sls where usr_grp=:grpid ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("grpid", Integer.parseInt(grpdId));
			
			List<BigInteger[]> groupList = queryValidate.list();
			
			Cnt=(BigInteger) queryValidate.uniqueResult();
			
			iRcrdCnt=Cnt.intValue();
			
			if(iRcrdCnt==0)
			{
				tx = session.beginTransaction();
				
				hql = "delete from usr_group where grp_id=:grp_id ";
				
			    queryValidate = session.createSQLQuery(hql);

				queryValidate.setParameter("grp_id", Integer.parseInt(grpdId) );
				
				queryValidate.executeUpdate();
				
				tx.commit();
			}
			else
			{
				starManResponse.output.rtnSts =1;
				starManResponse.output.messages.add("Could not delete this group as it is assigned to Users");
			}

		} catch (Exception e) {

			logger.debug("User Details Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void actvUsr(MaintanenceResponse<UserManOutput> starManResponse, String usrId, String actv) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();

			tx = session.beginTransaction();

			hql = "update usr_dtls_sls set is_actv=" + actv + " where usr_id='" + usrId + "'";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.executeUpdate();

			tx.commit();

		} catch (Exception e) {

			logger.debug("User Details Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	public List<Object[]> getUser(String usrId) {
		Session session = null;
		List<Object[]> usrLst=null;
		try {
			session = SessionUtil.getSession();

			String hql = "";

			hql = " select * from usr_dtls_sls where usr_id=:usrId";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("usrId", usrId);

			 usrLst = queryValidate.list();

		} catch (Exception e) {

			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return usrLst;
	}

	public String getUserCompany(String usrid) {
		Session session = null;
		String cmpyId = "";

		try {
			session = SessionUtilInformix.getSession();

			String hql = "";

			hql = " select cast(usr_usr_cmpy_id as varchar(3)) cmpy_id, 1 from mxrusr_rec where usr_lgn_id=:usrid";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("usrid", usrid);

			List<Object[]> usrCompanyLst = queryValidate.list();

			for (Object[] cmpy : usrCompanyLst) {

				cmpyId = cmpy[0].toString();
			}

		} catch (Exception e) {

			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return cmpyId;
	}
	
	
	public void getSingUsr(BrowseResponse<UserBrowseOutput> starBrowseResponse, UserDetails fieldsInput) {
		Session session = null;

		try {
			session = SessionUtil.getSession();

			String hql = "";

			hql = " select * from usr_dtls_sls where " + "usr_id=:usr_id ";

			Query queryValidate = session.createSQLQuery(hql);
			queryValidate.setParameter("usr_id", fieldsInput.getUserId());

			List<Object[]> usrLst = queryValidate.list();

			for (Object[] usr : usrLst) {

				UserDetails userDetails = new UserDetails();

				userDetails.setUserId(usr[0].toString());
				userDetails.setUserNm(usr[1].toString());
				userDetails.setEmailId(usr[2].toString());
				userDetails.setUsrTyp(usr[3].toString());
				userDetails.setUsrGrp(usr[4].toString());

				starBrowseResponse.output.fldTblUsers.add(userDetails);
			}

		} catch (Exception e) {

			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
}
