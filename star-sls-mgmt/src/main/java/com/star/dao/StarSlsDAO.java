package com.star.dao;

import java.text.SimpleDateFormat;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonFunctions;
import com.star.common.SessionUtilInformix;
import com.star.linkage.SalesOrder.SOItemBrowseOutput;
import com.star.linkage.SalesOrder.SOItemnfo;
import com.star.linkage.starslstest.FullOuterjoin;
import com.star.linkage.starslstest.StarSOItemBrowseOutput;

public class StarSlsDAO {

	private static Logger logger = LoggerFactory.getLogger(StarSlsDAO.class);
	CommonFunctions cmn = new CommonFunctions();

	public void readSoItem(BrowseResponse<SOItemBrowseOutput> starBrowseResponse, String cmpyId, int ordNo, int refNo,
			int refItm) {
		Session session = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			hql = "SELECT CAST(o.ord_cmpy_id AS VARCHAR(3))cmpy_id ,CAST(o.ord_ord_no AS VARCHAR(3)) ord_no , o.ord_ord_itm, "
					+ "CAST(o.ord_cus_po AS VARCHAR(30)) ord_cus_po, o.ord_cus_po_dt, o.ord_ord_pcs, "
					+ "o.ord_ord_msr, o.ord_ord_wgt , CAST(t.ipd_frm AS VARCHAR(6))ipt_frm, CAST(t.ipd_grd AS VARCHAR(8))ipd_grd,"
					+ "CAST(t.ipd_size AS VARCHAR(15))ipd_size, CAST(t.ipd_fnsh AS VARCHAR(8))ipd_fnsh, CAST(t.ipd_part AS VARCHAR(30))ipd_part"
					+ " from tctipd_rec t  INNER JOIN ortord_rec o ON o.ord_cmpy_id = t.ipd_cmpy_id " + " where 1=1"
					+ " AND t.ipd_cmpy_id=:ord_cmpy_id " + " AND t.ipd_ref_pfx=:ref_pfx " + " AND t.ipd_ref_no=:ref_no"
					+ " AND t.ipd_ref_itm=:ref_itm ";
			hql += " AND o.ord_cmpy_id =:ord_cmpy_id ";
			hql += " AND o.ord_ord_no =:ord_no";
			Query queryValidate = session.createSQLQuery(hql);
			queryValidate.setParameter("ord_cmpy_id", cmpyId);
			queryValidate.setParameter("ref_pfx", "SO");
			queryValidate.setParameter("ref_no", refNo);
			queryValidate.setParameter("ref_itm", refItm);
			queryValidate.setParameter("ord_no", ordNo);
			System.out.println(hql);
			String dt = "";
			List<Object[]> usrLst = queryValidate.list();
			if (usrLst != null) {
				for (Object[] usr : usrLst) {
					SOItemnfo itm = new SOItemnfo();
					itm.setCmpnyId(usr[0].toString());
					itm.setOrdNo(usr[1].toString());
					itm.setOrdItm(usr[2].toString());
					itm.setPo(usr[3].toString());
					dt = usr[4].toString();
					SimpleDateFormat frmt = new SimpleDateFormat("dd/MM/yyyy");
					itm.setPoDt(frmt.format(new SimpleDateFormat("yyyy-MM-dd").parse(dt)).toString());
					itm.setOrdpcs(usr[5].toString());
					itm.setOrdMsr(usr[6].toString());
					itm.setOrdWgt(usr[7].toString());
					itm.setIpdFrm(usr[8].toString());
					itm.setIpdGrd(usr[9].toString());
					itm.setIpdSize(usr[10].toString());
					itm.setIpdfnsh(usr[11].toString());
					itm.setIpdPart(usr[12].toString());
					starBrowseResponse.output.fldTblslsOdrItm.add(itm);
				}
			} else
				starBrowseResponse.output.setErrorMessage("No details found with this Order no");
		} catch (Exception e) {

			logger.debug("Item Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void readSoItemFull(BrowseResponse<StarSOItemBrowseOutput> starBrowseResponse, String cmpyId, int ordNo,
			int refNo, int refItm) {
		Session session = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			// hql = "select * from intprd_rec rd full outer join intpbc_rec bc on"
			// + " rd.prd_itm_ctl_no = bc.pbc_itm_ctl_no where rd.prd_cmpy_id=:cmpy_id";
			hql = "select CAST(rd.prd_cmpy_id AS VARCHAR(3)) cmpy_id, rd.prd_itm_ctl_no, CAST(rd.prd_brh AS VARCHAR(3))prd_brh, "
					+ "CAST(rd.prd_frm AS VARCHAR(6)) prd_frm,"
					+ " CAST(rd.prd_grd AS VARCHAR(8)) prd_grd, CAST(rd.prd_size AS VARCHAR(15))prd_size, bc.pbc_itm_ctl_no, bc.pbc_bgt_for_cus_id,"
					+ " bc.pbc_bgt_for_part from intprd_rec rd full outer join intpbc_rec bc on rd.prd_itm_ctl_no = bc.pbc_itm_ctl_no"
					+ " AND rd.prd_cmpy_id = bc.pbc_cmpy_id " 
					+ "where rd.prd_cmpy_id=:cmpy_id";
			Query queryValidate = session.createSQLQuery(hql);
			queryValidate.setParameter("cmpy_id", cmpyId);
			System.out.println(hql);
			String dt = "";
			int d = 0;
			List<Object[]> usrLst = queryValidate.list();
			if (usrLst != null) {
				for (Object[] usr : usrLst) {
					int i = 0;
					FullOuterjoin itm = new FullOuterjoin();
					/*
					 * while (usr[i] != null) { if (usr[i] == null) {
					 * System.out.print(" -- null --\t"); } else System.out.print(usr[i].toString()
					 * + "\t"); i++; }
					 */
					if (usr[0] == null) {
						itm.setCmpyId(" - ");
					} else
						itm.setCmpyId(usr[0].toString());
					if (usr[1] == null) {
						itm.setCtlNo(" - ");
					} else
						itm.setCtlNo(usr[1].toString());
					if (usr[2] == null) {
						itm.setBrh(" - ");
					} else
						itm.setBrh(usr[2].toString());
					if (usr[3] == null) {
						itm.setFrm(" - ");
					} else
						itm.setFrm(usr[3].toString());
					if (usr[4] == null) {
						itm.setGrd(" - ");
					} else
						itm.setGrd(usr[4].toString());
					if (usr[5] == null) {
						itm.setSize(" - ");
					} else
						itm.setSize(usr[5].toString());
					if (usr[6] == null) {
						itm.setPbcCtlNo(" - ");
					} else
						itm.setPbcCtlNo(usr[6].toString());
					if (usr[7] == null) {
						itm.setCustId(" - ");
					} else
						itm.setCustId(usr[7].toString());
					if (usr[8] == null) {
						itm.setPart(" - ");
					} else
						itm.setPart(usr[8].toString());
					starBrowseResponse.output.fldTblslsOdrItm.add(itm);
				}
			} else
				starBrowseResponse.output.setErrorMessage("No details found with this Order no");
		} catch (Exception e) {

			logger.debug("Item Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
}
