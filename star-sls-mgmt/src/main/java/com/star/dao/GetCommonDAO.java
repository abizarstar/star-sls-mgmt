package com.star.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.common.SessionUtilInformix;
import com.star.linkage.common.BankStx;
import com.star.linkage.common.BankStxBrowseOutput;
import com.star.linkage.common.Branch;
import com.star.linkage.common.BranchBrowseOutput;
import com.star.linkage.common.CompanyBrowseOutput;
import com.star.linkage.common.CompanyInfo;
import com.star.linkage.common.Country;
import com.star.linkage.common.CountryBrowseOutput;
import com.star.linkage.common.Currency;
import com.star.linkage.common.CurrencyBrowseOutput;
import com.star.linkage.common.Customer;
import com.star.linkage.common.CustomerBrowseOutput;
import com.star.linkage.common.ExchangeRtBrowseOutput;
import com.star.linkage.common.LengthTypeBrowseOutput;
import com.star.linkage.common.LengthTypeInfo;
import com.star.linkage.common.PaymentMethod;
import com.star.linkage.common.PaymentMethodBrowseOutput;
import com.star.linkage.common.ProcessRoute;
import com.star.linkage.common.ProcessRouteBrowseOutput;
import com.star.linkage.common.SalesCategory;
import com.star.linkage.common.SalesCategoryBrowseOutput;
import com.star.linkage.common.SalesPerson;
import com.star.linkage.common.SalesPersonBrowseOutput;
import com.star.linkage.common.ShipTo;
import com.star.linkage.common.ShipToBrowseOutput;
import com.star.linkage.common.TransactionStatusAction;
import com.star.linkage.common.TransactionStatusActionBrowseOutput;
import com.star.linkage.common.TransactionStatusReason;
import com.star.linkage.common.TransactionStatusReasonBrowseOutput;
import com.star.linkage.common.Um;
import com.star.linkage.common.UmBrowseOutput;
import com.star.linkage.common.UserPreference;
import com.star.linkage.common.UserPreferenceBrowseOutput;
import com.star.linkage.common.Vendor;
import com.star.linkage.common.VendorBrowseOutput;
import com.star.linkage.common.VoucherCategory;
import com.star.linkage.common.VoucherCategoryBrowseOutput;
import com.star.linkage.common.Whs;
import com.star.linkage.common.WhsBrowseOutput;
import com.star.linkage.connection.ConnectionManOutput;

public class GetCommonDAO {

	private static Logger logger = LoggerFactory.getLogger(GetCommonDAO.class);

	public void getCustomerList(BrowseResponse<CustomerBrowseOutput> starBrowseResponse, String cmpyId) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(trim(cus_cus_id) as varchar(8)) cus_id, "
					+ "cast(trim(cus_cus_long_nm) as varchar(35)) cus_nm from arrcus_rec where 1=1 ";

			if(cmpyId.length() > 0)
			{
				hql += " and cus_cmpy_id=:cmpy_id";
			}
			
			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("cmpy_id", cmpyId);
			
			List<Object[]> listCustomer = queryValidate.list();

			for (Object[] customer : listCustomer) {

				Customer output = new Customer();

				output.setCusId(customer[0].toString());
				output.setCusNm(customer[1].toString());

				starBrowseResponse.output.fldTblCustomer.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}
	
	public void readWhsList(BrowseResponse<WhsBrowseOutput> starBrowseResponse) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(whs_whs as varchar(3)) whs, cast(whs_whs_nm as varchar(15)) whs_nm from scrwhs_rec";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listWhs = queryValidate.list();

			for (Object[] whs : listWhs) {

				Whs output = new Whs();

				output.setWhs(whs[0].toString());
				output.setWhsNm(whs[1].toString());
				starBrowseResponse.output.fldTblWhs.add(output);

			}

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			session.close();
		}

	}

	public void getVendorList(BrowseResponse<VendorBrowseOutput> starBrowseResponse, String cmpyId) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(trim(ven_ven_id) as varchar(8)) ven_id, "
					+ "cast(trim(ven_ven_long_nm) as varchar(35)) ven_nm from aprven_rec where 1=1";
			
			if(cmpyId.length() > 0)
			{
				hql += " and ven_cmpy_id=:cmpy_id";
			}

			Query queryValidate = session.createSQLQuery(hql);
			
			if(cmpyId.length() > 0)
			{
				queryValidate.setParameter("cmpy_id", cmpyId);
			}

			List<Object[]> listVendor = queryValidate.list();

			for (Object[] vendor : listVendor) {

				Vendor output = new Vendor();

				output.setVenId(vendor[0].toString());
				output.setVenNm(vendor[1].toString());

				starBrowseResponse.output.fldTblVendor.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public String getVendorName(String cmpyId, String venId) {

		Session session = null;
		String hql = "";
		String venNm = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(trim(ven_ven_id) as varchar(8)) ven_id, cast(trim(ven_ven_long_nm) as varchar(35)) ven_nm from aprven_rec ";

			hql = hql + " where ven_cmpy_id =:cmpy and ven_ven_id=:ven";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy", cmpyId);
			queryValidate.setParameter("ven", venId);

			List<Object[]> listVendor = queryValidate.list();

			for (Object[] vendor : listVendor) {

				Vendor output = new Vendor();

				output.setVenId(vendor[0].toString());
				output.setVenNm(vendor[1].toString());

				venNm = vendor[1].toString();
			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return venNm;
	}

	public void getCountryList(BrowseResponse<CountryBrowseOutput> starBrowseResponse) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(trim(cty_cty) as varchar(3)) cty, cast(trim(cty_desc30) as varchar(30)) cty_desc from scrcty_Rec";

			Query queryValidate = session.createSQLQuery(hql);
			
			List<Object[]> listCountry = queryValidate.list();

			for (Object[] country : listCountry) {

				Country output = new Country();

				output.setCty(country[0].toString());
				output.setCtyDesc(country[1].toString());

				starBrowseResponse.output.fldTblCountry.add(output);
			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}
	
	
	public void getStandardRoute(BrowseResponse<ProcessRouteBrowseOutput> starBrowseResponse, String cmpyId) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select distinct cast(trim(spr_prs_rte) as varchar(3)) prs_rte, cast(trim(spr_desc50) as varchar(50)) prs_rte_desc from orrspr_rec ";
			
			hql = hql + " where spr_cmpy_id =:cmpy";

			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("cmpy", cmpyId);

			List<Object[]> listCountry = queryValidate.list();

			for (Object[] prsRte : listCountry) {

				ProcessRoute output = new ProcessRoute();

				output.setPrsRte(prsRte[0].toString().trim());
				output.setPrsRteDesc(prsRte[1].toString().trim());

				starBrowseResponse.output.fldTblPrsRte.add(output);
			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}
	
	
	public void getUserPreference(BrowseResponse<UserPreferenceBrowseOutput> starBrowseResponse, String cmpyId, String usrId) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select upr_flwp_dy, upr_expy_dy, upr_dbld_mtl_chrg from mxrupr_rec ";
			
			hql = hql + " where upr_cmpy_id =:cmpy and upr_lgn_id=:usr and upr_pgm_nm='ordeord' and upr_ref_pfx='SO'";

			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("cmpy", cmpyId);
			queryValidate.setParameter("usr", usrId);
			

			List<Object[]> listPreference = queryValidate.list();

			for (Object[] usrPref : listPreference) {

				UserPreference output = new UserPreference();

				output.setFlwUpDays(Integer.parseInt(usrPref[0].toString()));
				output.setExpiryDays(Integer.parseInt(usrPref[1].toString()));
				output.setBuildUp(Integer.parseInt(usrPref[2].toString()));

				starBrowseResponse.output.fldTblUsrPref.add(output);
			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public void getBranchList(BrowseResponse<BranchBrowseOutput> starBrowseResponse, String cmpyId) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(trim(brh_brh) as varchar(3)) brh_brh,"
					+ "cast(trim(brh_brh_nm) as varchar(15)) brh_brh_nm from scrbrh_rec where 1=1 ";

			if(cmpyId.length() > 0)
			{
				hql += " and brh_cmpy_id=:cmpy_id";
			}
			
			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("cmpy_id", cmpyId);
			

			List<Object[]> listBranch = queryValidate.list();

			for (Object[] branch : listBranch) {

				Branch output = new Branch();

				output.setBrhBrh(branch[0].toString());
				output.setBrhBrhNm(branch[1].toString());

				starBrowseResponse.output.fldTblBranch.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public void getBankList(BrowseResponse<BankStxBrowseOutput> starBrowseResponse, String cmpyId) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(trim(bnk_bnk) as varchar(3)) bnk_bnk,cast(trim(bnk_desc30) as varchar(30)) bnk_nm, "
					+ "cast(trim(bnk_cry) as varchar(3)), bnk_actv, (select crx_xexrt from scrcrx_rec, scrcsc_rec "
					+ "where  crx_cmpy_id = csc_cmpy_id and crx_orig_cry=bnk_cry and crx_eqv_cry=csc_bas_cry "
					+ "and crx_cmpy_id =:cmpy) exc_rt from scrbnk_rec";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy", cmpyId);

			List<Object[]> listBank = queryValidate.list();

			for (Object[] bank : listBank) {

				BankStx output = new BankStx();

				output.setBnk(bank[0].toString());
				output.setBnkDesc(bank[1].toString());
				output.setBnkCry(bank[2].toString());
				output.setBnkActv(Integer.parseInt(bank[3].toString()));
				
				if(bank[4] != null)
				{
					output.setExcRt(Double.parseDouble(bank[4].toString()));
				}
				else
				{
					output.setExcRt(0);
				}

				starBrowseResponse.output.fldTblBank.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public void getExchangeRate(BrowseResponse<ExchangeRtBrowseOutput> starBrowseResponse, String cmpyId,
			String origCry, String eqvCry) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select * from scrcrx_rec where crx_cmpy_id =:cmpy and crx_orig_cry=:origCry and crx_eqv_cry =:eqvCry";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy", cmpyId);
			queryValidate.setParameter("origCry", origCry);
			queryValidate.setParameter("eqvCry", eqvCry);

			starBrowseResponse.output.excRt = String.valueOf(queryValidate.list().get(0));

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public void getCurrencyList(BrowseResponse<CurrencyBrowseOutput> starBrowseResponse) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(trim(cry_cry) as varchar(3)) cry,cast(trim(cry_desc15) as varchar(15)) cry_nm from scrcry_rec";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listCry = queryValidate.list();

			for (Object[] currency : listCry) {

				Currency output = new Currency();

				output.setCry(currency[0].toString());
				output.setCryDesc(currency[1].toString());

				starBrowseResponse.output.fldTblCry.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public void validateCon(MaintanenceResponse<ConnectionManOutput> starManResponse) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select first 1 8 from systables";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listCheck = queryValidate.list();

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.ERP_CON_FAILED);
		} finally {
			session.close();
		}

	}

	public List<Vendor> getVendorList(String VenNm) {

		Session session = null;
		String hql = "";
		List<Vendor> vendorList = new ArrayList<Vendor>();
		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(trim(ven_ven_id) as varchar(8)) ven_id, cast(trim(ven_ven_long_nm) as varchar(35)) ven_nm "
					+ "from aprven_rec ";

			/* hql = " where ven_ven_long_nm like :ven_nm OR ven_ven_nm like :ven_nm"; */

			Query queryValidate = session.createSQLQuery(hql);

			/* queryValidate.setParameter("ven_nm", VenNm + '%'); */// + '%' '%' +

			List<Object[]> listVendor = queryValidate.list();

			for (Object[] vendor : listVendor) {

				Vendor output = new Vendor();

				output.setVenId(vendor[0].toString());
				output.setVenNm(vendor[1].toString());

				vendorList.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return vendorList;
	}

	public void getPaymentMethodList(BrowseResponse<PaymentMethodBrowseOutput> starBrowseResponse) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			hql = " SELECT id, mthd_cd, mthd_nm FROM pay_mthd ORDER BY mthd_nm";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listPymtMthd = queryValidate.list();

			for (Object[] pymtMthd : listPymtMthd) {

				PaymentMethod output = new PaymentMethod();

				output.setMthdId(pymtMthd[0].toString());
				output.setMthdCd(pymtMthd[1].toString());
				output.setMthdNm(pymtMthd[2].toString());

				starBrowseResponse.output.fldTblPymtMthd.add(output);
			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public void getVouCatList(BrowseResponse<VoucherCategoryBrowseOutput> starBrowseResponse) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(vct_vchr_cat as varchar(4)) vchr_cat, cast(vct_desc as varchar(30)) as desc from aprvct_rec WHERE vct_actv=1 ORDER BY vct_vchr_cat";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listVouCat = queryValidate.list();

			for (Object[] category : listVouCat) {

				VoucherCategory output = new VoucherCategory();

				output.setVchrCat(category[0].toString());
				output.setDesc(category[1].toString());

				starBrowseResponse.output.fldTblVouCategory.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public void getTrnStsActnList(BrowseResponse<TransactionStatusActionBrowseOutput> starBrowseResponse) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = "select cast(stc_sts_actn as varchar(1)) as sts, cast(stc_desc30 as varchar(30)) as sts_desc from tcrstc_rec where stc_sts_typ='T' and stc_aplc_lvl='T'";

			// "SELECT cast(sta_sts as varchar(4)) as sts, cast(sta_desc15 as varchar(16))
			// as desc FROM scrsta_rec WHERE sta_sts_typ='T' AND sta_actv=1 ORDER BY
			// sta_sts";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listTrnStsActn = queryValidate.list();

			for (Object[] trnStsActn : listTrnStsActn) {

				TransactionStatusAction output = new TransactionStatusAction();

				output.setSts(trnStsActn[0].toString());
				output.setDesc(trnStsActn[1].toString());

				starBrowseResponse.output.fldTblTrnStsActn.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}
	}

	public void getTrnStsList(BrowseResponse<TransactionStatusActionBrowseOutput> starBrowseResponse) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT cast(sta_sts as varchar(4)) as sts, cast(sta_desc15 as varchar(16)) as desc FROM scrsta_rec WHERE sta_sts_typ='T' AND sta_actv=1 ORDER BY sta_sts";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listTrnStsActn = queryValidate.list();

			for (Object[] trnStsActn : listTrnStsActn) {

				TransactionStatusAction output = new TransactionStatusAction();

				output.setSts(trnStsActn[0].toString());
				output.setDesc(trnStsActn[1].toString());

				starBrowseResponse.output.fldTblTrnStsActn.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}
	}

	public void getTrnStsRsnList(BrowseResponse<TransactionStatusReasonBrowseOutput> starBrowseResponse,
			String stsActn) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT cast(rsn_rsn as varchar(4)) as rsn, cast(rsn_desc30 as varchar(31)) as desc "
					+ " FROM scrrsn_rec WHERE rsn_rsn_typ=:typ AND rsn_actv=1 ORDER BY rsn_rsn";

			Query queryValidate = session.createSQLQuery(hql);

			if (stsActn.equals("C")) {
				queryValidate.setParameter("typ", "TRC");
			} else if (stsActn.equals("H")) {
				queryValidate.setParameter("typ", "TRH");
			}

			List<Object[]> listTrnStsRsn = queryValidate.list();

			for (Object[] trnStsRsn : listTrnStsRsn) {

				TransactionStatusReason output = new TransactionStatusReason();

				output.setRsn(trnStsRsn[0].toString());
				output.setDesc(trnStsRsn[1].toString());

				starBrowseResponse.output.fldTblTrnStsRsn.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}
	}
	
	public void readCompanyList(BrowseResponse<CompanyBrowseOutput> starBrowseResponse) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(csc_cmpy_id as varchar(3)) cmpy_id, cast(csc_co_nm as varchar(35)) co_nm from scrcsc_rec";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listCompany = queryValidate.list();

			for (Object[] company : listCompany) {

				CompanyInfo output = new CompanyInfo();

				output.setCmpyId(company[0].toString());
				output.setCmpyNm(company[1].toString());
				starBrowseResponse.output.fldTblCompany.add(output);

			}

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			session.close();
		}

	}
	
	
	public void readLengthTypes(BrowseResponse<LengthTypeBrowseOutput> starBrowseResponse) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(olt_ord_lgth_typ as varchar(2)), cast(olt_desc15 as varchar(15))  from tcrolt_rec where olt_actv=1";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listLgthType = queryValidate.list();

			for (Object[] lgthTyp : listLgthType) {

				LengthTypeInfo output = new LengthTypeInfo();

				output.setLgthTyp(lgthTyp[0].toString());
				output.setLgthTypDesc(lgthTyp[1].toString());
				starBrowseResponse.output.fldTblLengthType.add(output);

			}

		} catch (Exception e) {
			logger.debug("Length Type Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			session.close();
		}

	}
	
	public void getShipToList(BrowseResponse<ShipToBrowseOutput> starBrowseResponse, String cmpyId, String cusId) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " SELECT shp_shp_to, cast(trim(shp_shp_to_nm) as varchar(15)) shp_to_nm, "
					+ "cast(trim(shp_shp_to_long_nm) as varchar(35)) shp_to_long_nm from arrshp_rec WHERE shp_actv=1 ";

			if(cmpyId.length() > 0)
			{
				hql += " AND shp_cmpy_id=:cmpy_id";
			}
			
			if(cusId.length() > 0)
			{
				hql += " AND shp_cus_id=:cus_id";
			}
			
			hql += " ORDER BY shp_shp_to";
			
			Query queryValidate = session.createSQLQuery(hql);
			
			if(cmpyId.length() > 0)
			{
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			
			if(cusId.length() > 0)
			{
				queryValidate.setParameter("cus_id", cusId);
			}
			
			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {

				ShipTo output = new ShipTo();

				output.setShpTo(row[0].toString());
				output.setShpToNm(row[1].toString());
				output.setShpToLongNm(row[2].toString());

				starBrowseResponse.output.fldTblShpTo.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}
	
	public void getSalesPersonList(BrowseResponse<SalesPersonBrowseOutput> starBrowseResponse, String cmpyId) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();
			
			hql = "SELECT cast(trim(slp_slp) as varchar(4)) slp_slp, cast(trim(usr_nm) as varchar(35)) usr_nm FROM scrslp_rec, mxrusr_rec "
					+ " WHERE slp_actv=1 AND usr_actv=1 AND usr_lgn_id=slp_lgn_id ";
			
			if(cmpyId.length() > 0)
			{
				hql += " AND slp_cmpy_id=:cmpy_id";
			}

			hql += " ORDER BY slp_slp";
			
			Query queryValidate = session.createSQLQuery(hql);
			
			if(cmpyId.length() > 0)
			{
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			
			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {

				SalesPerson output = new SalesPerson();

				output.setSlp(row[0].toString());
				output.setSlpNm(row[1].toString());

				starBrowseResponse.output.fldTblSlp.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}
	

	
	public void getSalesCategoryList(BrowseResponse<SalesCategoryBrowseOutput> starBrowseResponse) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();
			
			hql = "SELECT cast(trim(sca_sls_cat) as varchar(2)) sca_sls_cat, cast(trim(sca_desc30) as varchar(30)) sca_desc30 "
					+ " FROM orrsca_rec WHERE sca_actv=1 ORDER BY sca_sls_cat";
			
			Query queryValidate = session.createSQLQuery(hql);
			
			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {

				SalesCategory output = new SalesCategory();

				output.setSlsCat(row[0].toString());
				output.setSlsCatDesc(row[1].toString());

				starBrowseResponse.output.fldTblSlsCat.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}
	
	
	public void getUmList(BrowseResponse<UmBrowseOutput> starBrowseResponse) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();
			
			hql = "SELECT cast(trim(umr_um) as varchar(3)) umr_um, cast(trim(umr_desc15) as varchar(15)) umr_desc15 "
					+ " FROM scrumr_rec WHERE umr_actv=1 ORDER BY umr_um";
			
			Query queryValidate = session.createSQLQuery(hql);
			
			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {

				Um output = new Um();

				output.setUm(row[0].toString());
				output.setDesc(row[1].toString());

				starBrowseResponse.output.fldTblUm.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

}
