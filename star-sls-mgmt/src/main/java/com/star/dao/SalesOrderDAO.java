package com.star.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtilInformix;
import com.star.linkage.SalesOrder.I25RecInput;
import com.star.linkage.SalesOrder.I25RecManOutput;
import com.star.linkage.SalesOrder.I26RecInput;
import com.star.linkage.SalesOrder.I26RecManOutput;
import com.star.linkage.SalesOrder.I47RecInput;
import com.star.linkage.SalesOrder.I47RecManOutput;
import com.star.linkage.SalesOrder.I48RecInput;
import com.star.linkage.SalesOrder.I48RecManOutput;
import com.star.linkage.SalesOrder.OdrInfoBrowseOutput;
import com.star.linkage.SalesOrder.OdrInfoInput;
import com.star.linkage.SalesOrder.S05RecInput;
import com.star.linkage.SalesOrder.S05RecManOutput;
import com.star.linkage.SalesOrder.S06RecInput;
import com.star.linkage.SalesOrder.S06RecManOutput;
import com.star.linkage.SalesOrder.S16RecInput;
import com.star.linkage.SalesOrder.S16RecManOutput;
import com.star.linkage.SalesOrder.S17RecInput;
import com.star.linkage.SalesOrder.S17RecManOutput;
import com.star.linkage.SalesOrder.S20RecInput;
import com.star.linkage.SalesOrder.S20RecManOutput;
import com.star.linkage.SalesOrder.S52RecInput;
import com.star.linkage.SalesOrder.S52RecManOutput;
import com.star.linkage.SalesOrder.SOItemBrowseOutput;
import com.star.linkage.SalesOrder.SOItemnfo;
import com.star.linkage.SalesOrder.SalesOrderBrowseOutput;
import com.star.linkage.SalesOrder.SalesOrderInfo;
import com.star.linkage.SalesOrder.SalesOrderInput;
import com.star.linkage.SalesOrder.SalesOrderManOutput;
import com.star.linkage.SalesOrder.TI00RecInput;
import com.star.linkage.SalesOrder.TI00RecManOutput;

public class SalesOrderDAO {

	private static Logger logger = LoggerFactory.getLogger(SalesOrderDAO.class);
	CommonFunctions cmn = new CommonFunctions();

	public void updtS01Rec(MaintanenceResponse<SalesOrderManOutput> maintanenceResponse, int so,
			SalesOrderInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvs01_rec (s01_cmpy_id, s01_src_co_id, s01_trc_ctl_no, s01_ord_brh, s01_ord_pfx, s01_sld_cus_id,"
					+ " s01_shp_to, s01_is_slp, s01_os_slp, s01_tkn_slp, s01_cr_dt_ovrd, s01_shp_itm_tgth, s01_blnd_shpt_mthd,"
					+ " s01_shpg_whs, s01_ord_typ, s01_cry, s01_exrt, s01_ex_rt_typ, s01_pttrm, s01_disc_trm, s01_mthd_pmt,"
					+ " s01_sls_cat, s01_cntr_sls, s01_multp_rls)"
					+ " VALUES(:cmpy_id, 'SOG', :trc_ctl_no, :ord_brh, 'SO', :cus_id, :shp_to, :is_slp, :os_slp, :tkn_slp, 0, 0, 'N', 'BIR', :ord_typ, :cry,"
					+ " 1, 'V', 2, 2, 'CH', :sls_cat, 0, 0);";
			Query queryValidate = session.createSQLQuery(hql);

			if (input.getS01CmpyId() != null) {
				queryValidate.setParameter("cmpy_id", input.getS01CmpyId());
			} else {
				queryValidate.setParameter("cmpy_id", "");
			}
			queryValidate.setParameter("trc_ctl_no", so);
			if (input.getS01SldCusId() > 0) {
				queryValidate.setParameter("cus_id", input.getS01SldCusId());
			} else {
				queryValidate.setParameter("cus_id", 0);
			}
			if (input.getS01OrdBrh() != null) {
				queryValidate.setParameter("ord_brh", input.getS01OrdBrh());
			} else {
				queryValidate.setParameter("ord_brh", "");
			}
			if (input.getS01ShpTo() > 0) {
				queryValidate.setParameter("shp_to", input.getS01ShpTo());
			} else {
				queryValidate.setParameter("shp_to", 0);
			}
			if (input.getS01IsSlp() != null) {
				queryValidate.setParameter("is_slp", input.getS01IsSlp());
			} else {
				queryValidate.setParameter("is_slp", 0);
			}
			if (input.getS01OsSlp() != null) {
				queryValidate.setParameter("os_slp", input.getS01OsSlp());
			} else {
				queryValidate.setParameter("os_slp", 0);
			}
			if (input.getS01TknSlp() != null) {
				queryValidate.setParameter("tkn_slp", input.getS01TknSlp());
			} else {
				queryValidate.setParameter("tkn_slp", 0);
			}
			if (input.getS01OrdTyp() != null) {
				queryValidate.setParameter("ord_typ", input.getS01OrdTyp());
			} else {
				queryValidate.setParameter("ord_typ", 0);
			}
			if (input.getS01SlsCat() != null) {
				queryValidate.setParameter("sls_cat", input.getS01SlsCat());
			} else {
				queryValidate.setParameter("sls_cat", 0);
			}
			if (input.getS01Cry() != null) {
				queryValidate.setParameter("cry", input.getS01Cry());
			} else {
				queryValidate.setParameter("cry", 0);
			}

			// queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			maintanenceResponse.output.rtnSts = 1;
			maintanenceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtI25Rec(MaintanenceResponse<I25RecManOutput> starMaintenanceResponse, int so, I25RecInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvi25_rec (i25_cmpy_id,i25_intchg_pfx,i25_intchg_no,"
					+ "i25_intchg_itm,i25_src_co_id,i25_trc_ctl_no,i25_trc_itm,i25_ord_pfx,i25_ord_no,i25_ord_itm,"
					+ "i25_ord_sitm,i25_gat_ent_md,i25_gat_lvl,i25_ord_asgn_no) "
					+ "VALUES (:cmpy_id,'XI',:intch_no,0,'SOG',:trc_ctl_no,0,'SO',0,0,0,'A','H',0)";
			Query queryValidate = session.createSQLQuery(hql);

			if (input.getI25CmpyId() != null) {
				queryValidate.setParameter("cmpy_id", input.getI25CmpyId());
			} else {
				queryValidate.setParameter("cmpy_id", "");
			}
			queryValidate.setParameter("intch_no", so);
			queryValidate.setParameter("trc_ctl_no", so);
			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtI26Rec(MaintanenceResponse<I26RecManOutput> starMaintenanceResponse, int so, I26RecInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvi26_rec (i26_cmpy_id,i26_src_co_id,i26_trc_ctl_no,i26_hdr_gat,i26_nm_addr_gat,i26_cntc_gat,"
					+ "i26_frt_gat,i26_tx_gat,i26_instr_gat,i26_sts_gat,i26_toll_blg_gat) "
					+ "VALUES (:cmpy_id,'SOG',:trc_ctl_no,'S','N','N','N','N','N','N','N')";
			Query queryValidate = session.createSQLQuery(hql);

			if (input.getI26CmpyId() != null) {
				queryValidate.setParameter("cmpy_id", input.getI26CmpyId());
			} else {
				queryValidate.setParameter("cmpy_id", "");
			}
			queryValidate.setParameter("trc_ctl_no", so);

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtI47Rec(MaintanenceResponse<I47RecManOutput> starMaintenanceResponse, int so, I47RecInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvi47_rec (i47_cmpy_id,i47_src_co_id,i47_trc_ctl_no,i47_trc_itm,i47_ord_gat,i47_instr_gat,"
					+ "i47_sts_gat,i47_prd_gat,i47_prd_desc_gat,i47_chrg_gat,i47_tx_gat,i47_cond_gat,i47_tol_gat,i47_coil_pkg_gat,"
					+ "i47_pkg_gat,i47_std_spec_gat,i47_tst_gat,i47_chm_spec_gat,i47_tst_cert_gat,i47_orig_zn_gat,i47_rdflt_cps,"
					+ "i47_frt_gat,i47_nm_addr_gat,i47_byot_prd_gat,i47_byot_cst_gat,i47_byot_instr_gat,i47_trgt_oir_gat) "
					+ "VALUES (:cmpy_id,'SOG',:trc_ctl_no,1,'N','N','N','C','N','S','N','N','N','N','N','N','N','N','N','N','','N','N','N',"
					+ "'N','N','N')";
			Query queryValidate = session.createSQLQuery(hql);

			if (input.getI47CmpyId() != null) {
				queryValidate.setParameter("cmpy_id", input.getI47CmpyId());
			} else {
				queryValidate.setParameter("cmpy_id", "");
			}
			queryValidate.setParameter("trc_ctl_no", so);
			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtI48Rec(MaintanenceResponse<I48RecManOutput> starMaintenanceResponse, int so, I48RecInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvi48_rec (i48_cmpy_id,i48_src_co_id,i48_trc_ctl_no,i48_trc_itm,i48_trc_sitm,"
					+ "i48_rls_gat,i48_sts_gat,i48_rtng_gat) VALUES (:cmpy_id,'SOG',:trc_ctl_no,1,1,'S','N','N')";
			Query queryValidate = session.createSQLQuery(hql);

			if (input.getI48CmpyId() != null) {
				queryValidate.setParameter("cmpy_id", input.getI48CmpyId());
			} else {
				queryValidate.setParameter("cmpy_id", "");
			}
			queryValidate.setParameter("trc_ctl_no", so);

			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtS05Rec(MaintanenceResponse<S05RecManOutput> starMaintenanceResponse, int so, S05RecInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvs05_rec (s05_cmpy_id,s05_src_co_id,s05_trc_ctl_no,s05_trc_itm,s05_alw_prtl_shp,"
					+ "s05_ovrshp_pct,s05_undshp_pct,s05_cus_po,s05_cus_po_dt,s05_cus_ctrct,s05_end_usr_po,s05_ord_pcs,"
					+ "s05_ord_msr,s05_ord_wgt,s05_ord_qty,s05_ord_wgt_um,s05_bkt_pcs,s05_bkt_msr,s05_bkt_wgt,s05_bkt_qty,"
					+ "s05_bkt_wgt_um,s05_sls_cat,s05_chrg_qty_typ,s05_wfl_sls,s05_use_wgt_mult,s05_aly_schg_cls,s05_src,"
					+ "s05_bld_mtl_chrg,s05_end_use,s05_end_use_desc,s05_krf_wdth,s05_krf_lgth,s05_krf_ls_wdth,s05_krf_ls_lgth,"
					+ "s05_std_prs_rte,s05_invt_qlty,s05_src_whs,s05_jbspy_chrg_um,s05_multp_dim_tmpl,s05_apl_reb,"
					+ "s05_toll_dist_mthd,s05_blnd_shpt) "
					+ "VALUES (:cmpy_id,'SOG',:trc_ctl_no,1,1,0,0,:cus_po,:cus_po_dt,'','',:ord_pcs,:ord_msr,:ord_wgt,:ord_qty,:ord_wgt_um,0,0,0,0,'',"
					+ ":sls_cat,'A',0,0,'N',:src,0,'','',0,'001',0,0,'','-','BIR','','0',0,'','')";
			Query queryValidate = session.createSQLQuery(hql);

			if (input.getS05CmpyId() != null) {
				queryValidate.setParameter("cmpy_id", input.getS05CmpyId());
			} else {
				queryValidate.setParameter("cmpy_id", "");
			}
			if (input.getS05CusPo() != null) {
				queryValidate.setParameter("cus_po", input.getS05CusPo());
			} else {
				queryValidate.setParameter("cus_po", "");
			}
			if (input.getS05CusPoDt() != null) {
				System.out.println(input.getS05CusPoDt());
				/*
				 * String dtobj=input.getS05CusPoDt(); Date date1 = new
				 * SimpleDateFormat("dd-mm-yyyy").parse(dtobj); String frmt = new
				 * SimpleDateFormat("yyyy-MM-dd").format(date1); System.out.println(date1);
				 * System.out.println(frmt); int dt = Integer.parseInt(frmt);
				 */
				queryValidate.setParameter("cus_po_dt", input.getS05CusPoDt());
				// System.out.println(dt);
			} else {
				queryValidate.setParameter("cus_po_dt", "");
			}
			if (input.getS05OrdPcs() > 0) {
				queryValidate.setParameter("ord_pcs", input.getS05OrdPcs());
			} else {
				queryValidate.setParameter("ord_pcs", 0);
			}
			if (input.getS05OrdMsr() > 0) {
				queryValidate.setParameter("ord_msr", input.getS05OrdMsr());
			} else {
				queryValidate.setParameter("ord_msr", 0);
			}
			if (input.getS05OrdWgt() > 0) {
				queryValidate.setParameter("ord_wgt", input.getS05OrdWgt());
			} else {
				queryValidate.setParameter("ord_wgt", 0);
			}
			if (input.getS05OrdQty() > 0) {
				queryValidate.setParameter("ord_qty", input.getS05OrdQty());
			} else {
				queryValidate.setParameter("ord_qty", 0);
			}
			if (input.getS05OrdWgtUm() != null) {
				queryValidate.setParameter("ord_wgt_um", input.getS05OrdWgtUm());
			} else {
				queryValidate.setParameter("ord_wgt_um", "");
			}
			if (input.getS05SlsCat() != null) {
				queryValidate.setParameter("sls_cat", input.getS05SlsCat());
			} else {
				queryValidate.setParameter("sls_cat", "");
			}
			if (input.getS05Src() != null) {
				queryValidate.setParameter("src", input.getS05Src());
			} else {
				queryValidate.setParameter("src", "");
			}
			queryValidate.setParameter("trc_ctl_no", so);
			// queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtS06Rec(MaintanenceResponse<S06RecManOutput> starMaintenanceResponse, int so, S06RecInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvs06_rec (s06_cmpy_id,s06_src_co_id,s06_trc_ctl_no,s06_trc_itm,s06_frm,s06_grd,"
					+ "s06_num_size1,s06_num_size2,s06_num_size3,s06_num_size4,s06_num_size5,s06_size,s06_fnsh,"
					+ "s06_ef_evar,s06_wdth,s06_lgth,s06_dim_dsgn,s06_idia,s06_odia,s06_wthck,s06_ga_size,s06_ga_typ,"
					+ "s06_alt_size,s06_rdm_dim_1,s06_rdm_dim_2,s06_rdm_dim_3,s06_rdm_dim_4,s06_rdm_dim_5,s06_rdm_dim_6,"
					+ "s06_rdm_dim_7,s06_rdm_dim_8,s06_rdm_area,s06_ent_msr,s06_lgth_disp_fmt,s06_frc_fmt,s06_dia_frc_fmt,"
					+ "s06_ord_lgth_typ,s06_fmt_desc_frc,s06_fmt_size_desc,s06_part_cus_id,s06_part,s06_part_revno,s06_grs_wdth,"
					+ "s06_grs_lgth,s06_grs_pcs,s06_grs_wdth_intgr,s06_grs_wdth_nmr,s06_grs_wdth_dnm,s06_grs_lgth_intgr,"
					+ "s06_grs_lgth_nmr,s06_grs_lgth_dnm,s06_wdth_intgr,s06_wdth_nmr,s06_wdth_dnm,s06_lgth_intgr,s06_lgth_nmr,"
					+ "s06_lgth_dnm,s06_idia_intgr,s06_idia_nmr,s06_idia_dnm,s06_odia_intgr,s06_odia_nmr,s06_odia_dnm,s06_cstm_sha)"
					+ " VALUES (:cmpy_id,'SOG',:trc_ctl_no,1,'','',0,0,0,0,0,'','','',0,0,'F',0,0,0,0,'N','',0,0,0,0,0,0,0,0,0,'E',0,0,0,'',0"
					+ ",0,:prt_cus_id,:prt,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'')";
			Query queryValidate = session.createSQLQuery(hql);

			if (input.getS06CmpyId() != null) {
				queryValidate.setParameter("cmpy_id", input.getS06CmpyId());
			} else {
				queryValidate.setParameter("cmpy_id", "");
			}
			if (input.getPrtCusId() != null) {
				queryValidate.setParameter("prt_cus_id", input.getPrtCusId());
			} else {
				queryValidate.setParameter("prt_cus_id", "");
			}
			if (input.getPrt() != null) {
				queryValidate.setParameter("prt", input.getPrt());
			} else {
				queryValidate.setParameter("prt", "");
			}
			queryValidate.setParameter("trc_ctl_no", so);
			// queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtS16Rec(MaintanenceResponse<S16RecManOutput> starMaintenanceResponse, int so, S16RecInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvs16_rec (s16_cmpy_id,s16_src_co_id,s16_trc_ctl_no,s16_trc_itm,s16_trc_sitm,s16_cus_rls,s16_rls_pcs,"
					+ "s16_rls_pcs_typ,s16_rls_msr,s16_rls_msr_typ,s16_rls_wgt,s16_rls_wgt_typ,s16_rls_qty,s16_rls_qty_typ,"
					+ "s16_due_dt_qlf,s16_due_dt_fmt,s16_due_dt_ent,s16_due_fm_dt,s16_due_to_dt,s16_due_dt_wk_no,s16_due_dt_wk_cy,"
					+ "s16_due_fm_hr,s16_due_fm_mt,s16_due_to_hr,s16_due_to_mt,s16_rqst_dt_qlf,s16_rqst_dt_fmt,s16_rqst_dt_ent,"
					+ "s16_rqst_fm_dt,s16_rqst_to_dt,s16_rqst_dt_wk_no,s16_rqst_dt_wk_cy,s16_rqst_fm_hr,s16_rqst_fm_mt,"
					+ "s16_rqst_to_hr,s16_rqst_to_mt,s16_rdy_by_dt,s16_rdy_by_hr,s16_rdy_by_mt,s16_shpg_whs,s16_plng_whs,"
					+ "s16_dflt_trrte,s16_bko,s16_ord_plng_mthd,s16_plng_mthd_ovrd) "
					+ "VALUES (:cmpy_id,'SOG',:trc_ctl_no,1,1,'Q4 2019 BLANKET',15,'T',108009,'T',:rls_wgt,'A',:rls_qty,'A','','D',:due_dt_ent,"
					+ "NULL,NULL,0,0,0,0,0,0,'','D',:rqst_dt_ent,NULL,NULL,0,0,0,0,0,0,NULL,0,0,'BIR','BIR','',0,0,0)";
			Query queryValidate = session.createSQLQuery(hql);
			if (input.getS16CmpyId() != null) {
				queryValidate.setParameter("cmpy_id", input.getS16CmpyId());
			} else {
				queryValidate.setParameter("cmpy_id", "");
			}
			if (input.getS16RlsWgt() > 0) {
				queryValidate.setParameter("rls_wgt", input.getS16RlsWgt());
			} else {
				queryValidate.setParameter("rls_wgt", 0);
			}
			if (input.getS16RlsQty() > 0) {
				queryValidate.setParameter("rls_qty", input.getS16RlsQty());
			} else {
				queryValidate.setParameter("rls_qty", 0);
			}
			if (input.getDueDtEnt() != null) {
				Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(input.getDueDtEnt());
				String frmt = new SimpleDateFormat("yyyyMMdd").format(date1);
				int dt = Integer.parseInt(frmt);
				System.out.println(dt);
				queryValidate.setParameter("due_dt_ent", dt);
			} else {
				queryValidate.setParameter("due_dt_ent", 0);
			}
			String frmt = new SimpleDateFormat("yyyyMMdd").format(new Date());
			int dt = Integer.parseInt(frmt);
			System.out.println(dt);
			queryValidate.setParameter("rqst_dt_ent", dt);

			queryValidate.setParameter("trc_ctl_no", so);
			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtS17Rec(MaintanenceResponse<S17RecManOutput> starMaintenanceResponse, int so, S17RecInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "";
			Query queryValidate = session.createSQLQuery(hql);

			if (input.getS17CmpyId() != null) {
				queryValidate.setParameter("s17_cmpy_id", input.getS17CmpyId());
			} else {
				queryValidate.setParameter("s17_cmpy_id", "");
			}
			if (input.getS17TrcCtlNo() > 0) {
				queryValidate.setParameter("s17_trc_ctl_no", so);
			} else {
				queryValidate.setParameter("s17_trc_ctl_no", 0);
			}
			queryValidate.executeUpdate();
			tx.commit();
			session.close();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtS20Rec(MaintanenceResponse<S20RecManOutput> starMaintenanceResponse, int so, S20RecInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			hql = "";
			Query queryValidate = session.createSQLQuery(hql);

			if (input.getS20CmyId() != null) {
				queryValidate.setParameter("s20_cmpy_id", input.getS20CmyId());
			} else {
				queryValidate.setParameter("s20_cmpy_id", "");
			}
			if (input.getS20TrcCtlNo() > 0) {
				queryValidate.setParameter("s20_trc_ctl_no", so);
			} else {
				queryValidate.setParameter("s20_trc_ctl_no", 0);
			}
			queryValidate.executeUpdate();
			tx.commit();
			session.close();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtTI00Rec(MaintanenceResponse<TI00RecManOutput> starMaintenanceResponse, int so, TI00RecInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcti00_rec (i00_cmpy_id, i00_intchg_pfx, i00_intchg_no, i00_evnt, i00_usr_id, i00_sts_cd, "
					+ "i00_ssn_log_ctl_no, i00_intrf_cl) VALUES(:cmpy_id, 'XI', :intchg_no, 'SOG','rijot', 'N', 0, 'E')";
			Query queryValidate = session.createSQLQuery(hql);

			if (input.getI00CmpyId() != null) {
				queryValidate.setParameter("cmpy_id", input.getI00CmpyId());
			} else {
				queryValidate.setParameter("cmpy_id", "");
			}
			queryValidate.setParameter("intchg_no", so);
			/*
			 * if (input.getUserId() != null) { queryValidate.setParameter("usr_id",
			 * input.getUserId()); } else { queryValidate.setParameter("usr_id", ""); }
			 */
			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);

			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updtS52Rec(MaintanenceResponse<S52RecManOutput> starMaintenanceResponse, int so, S52RecInput input) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();
			hql = "INSERT INTO xcvs52_rec (s52_cmpy_id,s52_src_co_id,s52_trc_ctl_no,s52_trc_itm,s52_ptord_pfx,s52_ptord_no,"
					+ "s52_ptord_itm,s52_ptord_rls_no,s52_ord_pfx,s52_ord_no)"
					+ " VALUES (:cmpy_id,'SOG',:trc_ctl_no,1,'SO',0,0,1,'SO',0)";
			Query queryValidate = session.createSQLQuery(hql);

			if (input.getS52CmpyId() != null) {
				queryValidate.setParameter("cmpy_id", input.getS52CmpyId());
			} else {
				queryValidate.setParameter("cmpy_id", "");
			}
			queryValidate.setParameter("trc_ctl_no", so);
			queryValidate.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	private int validateUser(String usrId) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getmaxRec() {
		Session session = null;
		int mx = 0;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			hql = "select max(s01_trc_ctl_no),1 from xcvs01_rec";
			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listUser = queryValidate.list();

			for (Object[] user : listUser) {
				mx = Integer.parseInt(user[0].toString());
				System.out.println(mx);
			}

		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return mx;
	}

	public void readSo(BrowseResponse<SalesOrderBrowseOutput> starBrowseResponse, String custId, String cmpy) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();
			hql = "SELECT CAST(orh_cmpy_id AS VARCHAR(3)) cmpy_id, orh_ord_no, CAST(orh_sld_cus_id AS VARCHAR(8)) cus_id, "
					+ "CAST(cus_cus_long_nm AS VARCHAR(35)) cus_nm, orh_shp_to, CAST(shp_shp_to_long_nm AS VARCHAR(35)) shp_to_nm, "
					+ "CAST(orh_is_slp AS VARCHAR(4)) is_slp, CAST(orh_os_slp AS VARCHAR(4)) os_slp, CAST(orh_tkn_slp AS VARCHAR(4)) tkn_slp, "
					+ "CAST(ord_cus_po AS VARCHAR(31)) cus_po FROM ortorh_rec, arrshp_rec, arrcus_rec, ortord_rec WHERE 1=1 "
					+ "AND shp_cmpy_id=orh_cmpy_id AND orh_shp_to=shp_shp_to AND orh_sld_cus_id=shp_cus_id "
					+ "AND cus_cmpy_id=orh_cmpy_id AND orh_sld_cus_id=cus_cus_id "
					+ "AND ord_cmpy_id=orh_cmpy_id AND ord_ord_pfx='SO' AND ord_ord_no=orh_ord_no AND ord_ord_itm=1 "
					+ "AND orh_cmpy_id=:cmpy_id AND orh_sld_cus_id=:cus_id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", cmpy);
			queryValidate.setParameter("cus_id", custId);

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				SalesOrderInfo userDetails = new SalesOrderInfo();

				userDetails.setCmpnyId(row[0].toString());
				userDetails.setOrdNo(row[1].toString());
				userDetails.setCusId(row[2].toString());
				userDetails.setCusNm(row[3].toString());
				userDetails.setShpId(row[4].toString());
				userDetails.setShpNm(row[5].toString());
				userDetails.setInSlp(row[6].toString());
				userDetails.setOutSlp(row[7].toString());
				userDetails.settSlp(row[8].toString());
				userDetails.setPo(row[9].toString());

				starBrowseResponse.output.fldTblslsOdr.add(userDetails);
			}
		} catch (Exception e) {
			logger.debug("User Details Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(CommonConstants.SERVER_ERR);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public String readCust(String id, String cmpyid) {
		Session session = null;
		String name = "";
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			hql = "select CAST(cus_cmpy_id AS VARCHAR(3)) cmpy_id, CAST(cus_cus_id AS VARCHAR(8)) cus_id, CAST(cus_cus_nm AS VARCHAR(15))cus_nm from arrcus_rec where cus_cus_id=:cus_id ";
			hql += "AND cus_cmpy_id =:cmpy_id";
			Query queryValidate = session.createSQLQuery(hql);
			queryValidate.setParameter("cus_id", id);
			queryValidate.setParameter("cmpy_id", cmpyid);

			List<Object[]> usrLst = queryValidate.list();
			for (Object[] usr : usrLst) {
				name = usr[2].toString();
			}

		} catch (Exception e) {

			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return name;
	}

	public String readShp(String cusid, int shpid, String cmpyid) {
		Session session = null;
		String name = "";
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			hql = "select CAST(shp_cus_id AS VARCHAR(4))shp_cus_id,CAST(shp_Shp_to_nm AS VARCHAR(15)) shp_to_nm, shp_shp_to,CAST(shp_cmpy_id AS VARCHAR(3))cmpy_id from arrshp_rec where shp_cus_id=:cus_id and shp_shp_to=:shp_to and shp_cmpy_id=:cmpy_id";
			Query queryValidate = session.createSQLQuery(hql);
			queryValidate.setParameter("cus_id", cusid);
			queryValidate.setParameter("shp_to", shpid);
			queryValidate.setParameter("cmpy_id", cmpyid);

			List<Object[]> usrLst = queryValidate.list();
			for (Object[] usr : usrLst) {
				name = usr[1].toString();
			}

		} catch (Exception e) {

			logger.debug("User Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return name;
	}

	public void readSoItem(BrowseResponse<SOItemBrowseOutput> starBrowseResponse, String cmpyId, int ordNo) {
		Session session = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			
			hql = "SELECT CAST(o.ord_cmpy_id AS VARCHAR(3))cmpy_id ,CAST(o.ord_ord_no AS VARCHAR(3)) ord_no , o.ord_ord_itm, "
					+ "CAST(o.ord_cus_po AS VARCHAR(30)) ord_cus_po, o.ord_cus_po_dt, o.ord_ord_pcs, o.ord_ord_msr, o.ord_ord_wgt , "
					+ "CAST(t.ipd_frm AS VARCHAR(6))ipt_frm, CAST(t.ipd_grd AS VARCHAR(8))ipd_grd, CAST(t.ipd_size AS VARCHAR(15))ipd_size, "
					+ "CAST(t.ipd_fnsh AS VARCHAR(8))ipd_fnsh, CAST(t.ipd_part AS VARCHAR(30))ipd_part FROM tctipd_rec t INNER JOIN ortord_rec o "
					+ "ON o.ord_cmpy_id = t.ipd_cmpy_id WHERE 1=1 AND t.ipd_ref_pfx=o.ord_ord_pfx AND t.ipd_ref_no=o.ord_ord_no AND "
					+ "t.ipd_ref_itm=o.ord_ord_itm AND t.ipd_ref_pfx=:ref_pfx AND o.ord_cmpy_id=:cmpy_id AND o.ord_ord_no=:ord_no";
			
			Query queryValidate = session.createSQLQuery(hql);
			queryValidate.setParameter("cmpy_id", cmpyId);
			queryValidate.setParameter("ref_pfx", "SO");
			queryValidate.setParameter("ord_no", ordNo);

			System.out.println(hql);
			String dt = "";
			List<Object[]> usrLst = queryValidate.list();
			if (usrLst != null) {
				for (Object[] usr : usrLst) {
					SOItemnfo itm = new SOItemnfo();
					itm.setCmpnyId(usr[0].toString());
					itm.setOrdNo(usr[1].toString());
					itm.setOrdItm(usr[2].toString());
					itm.setPo(usr[3].toString());
					dt = usr[4].toString();
					SimpleDateFormat frmt = new SimpleDateFormat("dd/MM/yyyy");
					itm.setPoDt(frmt.format(new SimpleDateFormat("yyyy-MM-dd").parse(dt)).toString());
					itm.setOrdpcs(usr[5].toString());
					itm.setOrdMsr(usr[6].toString());
					itm.setOrdWgt(usr[7].toString());
					itm.setIpdFrm(usr[8].toString());
					itm.setIpdGrd(usr[9].toString());
					itm.setIpdSize(usr[10].toString());
					itm.setIpdfnsh(usr[11].toString());
					itm.setIpdPart(usr[12].toString());
					starBrowseResponse.output.fldTblslsOdrItm.add(itm);
				}
			} else
				starBrowseResponse.output.setErrorMessage("No details found with this Order no");
			System.out.println(hql);
		} catch (Exception e) {

			logger.debug("Item Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/*
	 * public void updtOdrInfo(MaintanenceResponse<OdrInfoManOutput>
	 * starMaintenanceResponse, OdrInfoInput input) { Session session = null;
	 * Transaction tx = null; String hql = ""; try { session =
	 * SessionUtilInformix.getSession(); tx = session.beginTransaction();
	 * 
	 * if (input != null) { // Add new Odr object OdrInfo odr = new OdrInfo();
	 * odr.setCusPo(input.getCusPo()); odr.setPoItm(input.getPoItm());
	 * odr.setCtlNo(input.getCtlNo()); odr.setRefPfx(input.getRefPfx());
	 * odr.setRefNo(input.getRefNo());// Save the employee in database
	 * session.save(odr);
	 * 
	 * } else { starMaintenanceResponse.output.rtnSts = 1; } tx.commit();
	 * session.close(); } catch (Exception e) {
	 * logger.debug("User Details Info : {}", e.getMessage(), e);
	 * starMaintenanceResponse.output.rtnSts = 1;
	 * starMaintenanceResponse.output.messages.add(CommonConstants.SERVER_ERR);
	 * tx.rollback(); } finally { if (session != null) { session.close(); } } }
	 */
	public void readodrInfo(BrowseResponse<OdrInfoBrowseOutput> starBrowseResponse, String cmpyId, int ordNo) {
		Session session = null;
		String hql = "";
		try {
			session = SessionUtilInformix.getSession();
			hql = "select CAST(cus_po AS VARCHAR(30))cus_po,cus_po_itm,ctl_no, CAST(ref_pfx AS VARCHAR(2)) ref_pfx, ref_no "
					+ "from order_info where 1=1 ";
			hql += " AND ref_pfx =:ref_pfx ";
			hql += " AND ref_no =:ref_no";
			Query queryValidate = session.createSQLQuery(hql);
			queryValidate.setParameter("ref_pfx", cmpyId);
			queryValidate.setParameter("ref_no", ordNo);
			System.out.println(hql);
			List<Object[]> usrLst = queryValidate.list();
			if (usrLst != null) {
				for (Object[] usr : usrLst) {
					OdrInfoInput itm = new OdrInfoInput();
					itm.setCusPo(usr[0].toString());
					itm.setPoItm(Integer.parseInt(usr[1].toString()));
					itm.setCtlNo(Integer.parseInt(usr[2].toString()));
					itm.setRefPfx(usr[3].toString());
					itm.setRefNo(Integer.parseInt(usr[4].toString()));
					starBrowseResponse.output.fldTblOdrinfo.add(itm);
				}
			} else
				starBrowseResponse.output.setErrorMessage("No details found with this Order no");
			System.out.println(hql);
		} catch (Exception e) {

			logger.debug("Item Details Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
