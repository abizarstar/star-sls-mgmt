package com.star.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.linkage.saveddocs.SavedDocs;
import com.star.linkage.saveddocs.SavedDocsBrowseOutput;
import com.star.linkage.saveddocs.SavedDocsManOutput;

public class SavedDocsDAO {

	private static Logger logger = LoggerFactory.getLogger(SavedDocsDAO.class);
	CommonFunctions cmn = new CommonFunctions();

	public void getSavedDocs(BrowseResponse<SavedDocsBrowseOutput> starBrowseResponse) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			//hql = " SELECT doc_id, doc_name, doc_orig_name, crt_dtts, usr_nm FROM cstm_ord_docs, usr_dtls_sls WHERE upld_by=usr_id ORDER BY doc_id DESC ";
			
			hql = "SELECT D.doc_id, D.doc_name, D.doc_orig_name, D.crt_dtts, usr_nm, "
					+ "(SELECT COUNT(O.ref_no) FROM order_info O WHERE O.ref_no > 0 and O.doc_id=D.doc_id group by D.doc_id) " 
					+ "FROM cstm_ord_docs D, usr_dtls_sls "
					+ "WHERE D.upld_by=usr_id ORDER BY D.doc_id DESC ";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {

				SavedDocs output = new SavedDocs();

				output.setDocId(row[0].toString());
				output.setDocNm(row[1].toString());
				output.setDocOrgNm(row[2].toString());
				
				if (row[3] != null) {
					output.setUpldOn((cmn.formatDate((Date) row[3])));
				}
				
				output.setUpldBy(row[4].toString());
				
				if (row[5] != null) {
					output.setPrsRowCnt(row[5].toString());
				}
				
				starBrowseResponse.output.fldTblSavedDocs.add(output);

			}

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	public void deleteSavedDocs(MaintanenceResponse<SavedDocsManOutput> starManResponse, Integer docId) {
		Session session = null;
		String hql = "";
		Transaction tx = null;
		
		try {
			session = SessionUtil.getSession();
			
			tx = session.beginTransaction();

			hql = "DELETE FROM order_info WHERE doc_id=:doc_id";
			
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("doc_id", docId);

			queryValidate.executeUpdate();
			
			
			hql = "DELETE FROM cstm_ord_docs WHERE doc_id=:doc_id";
			
			queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("doc_id", docId);

			queryValidate.executeUpdate();

			tx.commit();

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

}
