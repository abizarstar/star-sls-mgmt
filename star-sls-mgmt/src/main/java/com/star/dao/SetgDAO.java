package com.star.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.linkage.auth.SettingBrowseOutput;
import com.star.linkage.setg.ConCnfg;
import com.star.linkage.setg.ConfigManOutput;

public class SetgDAO {

	private static Logger logger = LoggerFactory.getLogger(SetgDAO.class);

	public void DelInsConCnfg(MaintanenceResponse<ConfigManOutput> maintanenceResponse, ConCnfg fieldsInput) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();

			/* Delete all data from con_cnfg */
			tx = session.beginTransaction();

			hql = "Delete from con_cnfg";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.executeUpdate();

			tx.commit();

			/* Insert new record into con_cnfg */
			tx = session.beginTransaction();

			hql = "insert into  con_cnfg (driver,url,usr_nm,pass,doc_freq_min) values (" + "'" + fieldsInput.getDriver()
			+ "'," + "'" + fieldsInput.getUrl() + "'," + "'" + fieldsInput.getUsr_nm() + "'," + "'"
			+ fieldsInput.getPass() + "'," + "'" + fieldsInput.getDoc_freq_min() + "'" + ")";

			queryValidate = session.createSQLQuery(hql);

			queryValidate.executeUpdate();

			tx.commit();

		} catch (Exception e) {

			logger.debug("Set Connection Info : {}", e.getMessage(), e);
			maintanenceResponse.output.rtnSts = 1;
			maintanenceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public ConCnfg getCon(BrowseResponse<SettingBrowseOutput> starBrowseResponse, String conTyp) {
		Session session = null;
		ConCnfg conCnfg = new ConCnfg();

		try {
			session = SessionUtil.getSession();

			String hql = "";

			hql = " select * from con_cnfg where con_typ=:typ ";

			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("typ", conTyp);
			
			List<Object[]> usrLst = queryValidate.list();

			for (Object[] usr : usrLst) {

				conCnfg.setDriver(usr[0].toString());
				conCnfg.setUrl(usr[1].toString());
				conCnfg.setUsr_nm(usr[2].toString());
				conCnfg.setPass(usr[3].toString());

				starBrowseResponse.output.fldTblUsers.add(conCnfg);
			}

		} catch (Exception e) {
			logger.debug("Set Connection Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return conCnfg;
	}

}
