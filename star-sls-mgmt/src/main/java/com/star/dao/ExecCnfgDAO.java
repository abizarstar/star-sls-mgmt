package com.star.dao;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.SessionUtil;
import com.star.linkage.execcnfg.ExecCnfgBrowseOutput;
import com.star.linkage.execcnfg.ExecCnfgInfo;

public class ExecCnfgDAO {

	private static Logger logger = LoggerFactory.getLogger(ExecCnfgDAO.class);
	
	/* GET FIELDS FOR IN-PROCESS/PAID */
	public void getExecInfo(BrowseResponse<ExecCnfgBrowseOutput> starBrowseRes) throws Exception {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			hql = " select * from con_exec_cnfg";
						
			Query queryValidate = session.createSQLQuery(hql);
		
			List<Object[]> listExecInfo = queryValidate.list();

			for (Object[] execInfoVal : listExecInfo) {
				ExecCnfgInfo execCnfgInfo = new ExecCnfgInfo();
				
				execCnfgInfo.setId((Integer) (execInfoVal[0]));
				
				execCnfgInfo.setHostExec(String.valueOf(execInfoVal[1]));
				execCnfgInfo.setPort(String.valueOf(execInfoVal[2]));
				execCnfgInfo.setEnv(String.valueOf(execInfoVal[3]));
				execCnfgInfo.setClasses(String.valueOf(execInfoVal[4]));
				execCnfgInfo.setUsrId(String.valueOf(execInfoVal[5]));
				execCnfgInfo.setChkSum(String.valueOf(execInfoVal[6]));
				execCnfgInfo.setHostPrm(String.valueOf(execInfoVal[7]));
				execCnfgInfo.setPrmPort(String.valueOf(execInfoVal[8]));
				
				starBrowseRes.output.fldTblExec.add(execCnfgInfo);
			}

		} catch (Exception e) {

			logger.debug("Exec Config Info : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);

			session.close();
		}
		finally {
			if (session != null) {
				session.close();
			}
		}


	}
	
	
	
}
