package com.star.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.SessionUtil;
import com.star.common.SessionUtilInformix;
import com.star.linkage.User.UserERPBrowseOutput;
import com.star.linkage.User.UserERPInfo;

public class UserDAO {

	private static Logger logger = LoggerFactory.getLogger(UserDAO.class);
	
	public void readUserList(BrowseResponse<UserERPBrowseOutput> starBrowseResponse)
	{
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(usr_lgn_id as varchar(8))lgn_id, cast(usr_nm as varchar(35))usr_nm, "
					+ "cast(usr_email as varchar(50))email from mxrusr_rec "
					+ "where usr_actv=1";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listUser = queryValidate.list();

			for (Object[] user : listUser) {

				UserERPInfo erpUserInfo = new UserERPInfo();
				
				erpUserInfo.setUsrId(user[0].toString());
				erpUserInfo.setUsrNm(user[1].toString());
				erpUserInfo.setUsrEmail(user[2].toString());
				
				starBrowseResponse.output.fldTblERPUser.add(erpUserInfo);

			}

		} catch (Exception e) {
			logger.debug("User Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}
	
	
	public void readApproverList(BrowseResponse<UserERPBrowseOutput> starBrowseResponse)
	{
		Session session = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			hql = " SELECT U.usr_id, U.usr_nm, U.usr_eml " + 
					"FROM usr_dtls_sls U, usr_group G " + 
					"WHERE U.usr_grp = G.grp_id AND G.grp_nm='APPROVER' AND U.is_actv=true";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listUser = queryValidate.list();

			for (Object[] user : listUser) {

				UserERPInfo erpUserInfo = new UserERPInfo();
				
				erpUserInfo.setUsrId(user[0].toString());
				erpUserInfo.setUsrNm(user[1].toString());
				erpUserInfo.setUsrEmail(user[2].toString());
				
				starBrowseResponse.output.fldTblERPUser.add(erpUserInfo);

			}

		} catch (Exception e) {
			logger.debug("User Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}
	
}
