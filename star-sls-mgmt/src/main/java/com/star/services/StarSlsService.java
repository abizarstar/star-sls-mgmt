package com.star.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.StarResponseBuilder;
import com.star.dao.StarSlsDAO;
import com.star.linkage.SalesOrder.SOItemBrowseOutput;
import com.star.linkage.SalesOrder.SalesOrderInfo;
import com.star.linkage.starslstest.StarSOItemBrowseOutput;

@Path("/StarslsOrd")
public class StarSlsService {
	@POST
	@Path("/read-starsoitm")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getSoItemwithjoin(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {
		BrowseResponse<SOItemBrowseOutput> starBrowseResponse = new BrowseResponse<SOItemBrowseOutput>();
		starBrowseResponse.setOutput(new SOItemBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		int ordNo = 0;
		int refNo = 0;
		int refItm = 0;
		SalesOrderInfo input = new SalesOrderInfo();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		ordNo = jsonObject.get("ordNo").getAsInt();
		refNo = jsonObject.get("refNo").getAsInt();
		refItm = jsonObject.get("refItm").getAsInt();
		input.setCmpnyId(cmpyId);
		StarSlsDAO SalesOrderDAO = new StarSlsDAO();
		SalesOrderDAO.readSoItem(starBrowseResponse, cmpyId, ordNo, refNo, refItm);

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputitm(starBrowseResponse));
	}

	private GenericEntity<?> getGenericBrowseOutputitm(BrowseResponse<SOItemBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<SOItemBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Path("/read-starsoitmfull")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getSoItemfull(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {
		BrowseResponse<StarSOItemBrowseOutput> starBrowseResponse = new BrowseResponse<StarSOItemBrowseOutput>();
		starBrowseResponse.setOutput(new StarSOItemBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		int ordNo = 0;
		int refNo = 0;
		int refItm = 0;
		SalesOrderInfo input = new SalesOrderInfo();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		ordNo = jsonObject.get("ordNo").getAsInt();
		refNo = jsonObject.get("refNo").getAsInt();
		refItm = jsonObject.get("refItm").getAsInt();
		input.setCmpnyId(cmpyId);
		StarSlsDAO SalesOrderDAO = new StarSlsDAO();
		SalesOrderDAO.readSoItemFull(starBrowseResponse, cmpyId, ordNo, refNo, refItm);

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputitm2(starBrowseResponse));
	}

	private GenericEntity<?> getGenericBrowseOutputitm2(BrowseResponse<StarSOItemBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<StarSOItemBrowseOutput>>(starBrowseResponse) {
		};
	};
}
