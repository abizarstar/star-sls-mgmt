package com.star.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.UserDetailsDAO;
import com.star.linkage.auth.UserManOutput;
import com.star.linkage.usrgrp.UsrGrpBrowseList;
import com.star.linkage.usrgrp.UsrGrpManOutput;

@Path("/usrgroup")
public class UserGroupService {

	@POST
	@Secured
	@Path("/read")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getDocument(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<UsrGrpBrowseList> starBrowseResponse = new BrowseResponse<UsrGrpBrowseList>();

		starBrowseResponse.setOutput(new UsrGrpBrowseList());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		String grpSrchStr = jsonObject.get("grp").getAsString();
		
		UserDetailsDAO detailsDAO = new UserDetailsDAO();

		detailsDAO.getUsersGroup(starBrowseResponse, grpSrchStr, 0);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblUsrGroup.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
	}
	
	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<UsrGrpBrowseList> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<UsrGrpBrowseList>>(starBrowseResponse) {
		};
	}

	
	@POST
	@Secured
	@Path("/add-grp")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addGrp(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<UsrGrpManOutput> starManResponse = new MaintanenceResponse<UsrGrpManOutput>();

		starManResponse.setOutput(new UsrGrpManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String grpNm = jsonObject.get("grp_nm").getAsString();

		UserDetailsDAO detailsDAO = new UserDetailsDAO();

		detailsDAO.addGrp(starManResponse, grpNm);

		return starResponseBuilder.getSuccessResponse(getGenericManOutput(starManResponse));

	}
	
	private GenericEntity<?> getGenericManOutput(MaintanenceResponse<UsrGrpManOutput> starBrowseResponse) {
		return new GenericEntity<MaintanenceResponse<UsrGrpManOutput>>(starBrowseResponse) {
		};
	}
	
	@POST
	@Secured
	@Path("/edit-grp")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response editGrp(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<UsrGrpManOutput> starManResponse = new MaintanenceResponse<UsrGrpManOutput>();

		starManResponse.setOutput(new UsrGrpManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String grpId = jsonObject.get("grp_id").getAsString();
		String grpNm = jsonObject.get("grp_nm").getAsString();
		

		UserDetailsDAO detailsDAO = new UserDetailsDAO();

		detailsDAO.editGrp(starManResponse, grpId,grpNm);

		return starResponseBuilder.getSuccessResponse(getGenericManOutput(starManResponse));

	}
	
	@POST
	@Secured
	@Path("/del-grp")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response delGrp(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<UsrGrpManOutput> starManResponse = new MaintanenceResponse<UsrGrpManOutput>();

		starManResponse.setOutput(new UsrGrpManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String grpId = jsonObject.get("grp_id").getAsString();		

		UserDetailsDAO detailsDAO = new UserDetailsDAO();

		detailsDAO.delGrp(starManResponse, grpId);

		return starResponseBuilder.getSuccessResponse(getGenericManOutput(starManResponse));

	}
	
	@POST
	@Secured
	@Path("/actv-usr")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response actvUser(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<UserManOutput> starManResponse = new MaintanenceResponse<UserManOutput>();

		starManResponse.setOutput(new UserManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		String UserId = jsonObject.get("usr_id").getAsString();		

		String actv = jsonObject.get("actv").getAsString();		

		UserDetailsDAO detailsDAO = new UserDetailsDAO();

		detailsDAO.actvUsr(starManResponse, UserId,actv);

		return starResponseBuilder.getSuccessResponse(getGenericManOutputActive(starManResponse));

	}
	
	private GenericEntity<?> getGenericManOutputActive(MaintanenceResponse<UserManOutput> starBrowseResponse) {
		return new GenericEntity<MaintanenceResponse<UserManOutput>>(starBrowseResponse) {
		};
	}
}
