package com.star.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.GetCommonDAO;
import com.star.linkage.common.BankStxBrowseOutput;
import com.star.linkage.common.BranchBrowseOutput;
import com.star.linkage.common.CompanyBrowseOutput;
import com.star.linkage.common.CountryBrowseOutput;
import com.star.linkage.common.CurrencyBrowseOutput;
import com.star.linkage.common.CustomerBrowseOutput;
import com.star.linkage.common.ExchangeRtBrowseOutput;
import com.star.linkage.common.LengthTypeBrowseOutput;
import com.star.linkage.common.PaymentMethodBrowseOutput;
import com.star.linkage.common.ProcessRouteBrowseOutput;
import com.star.linkage.common.SalesCategoryBrowseOutput;
import com.star.linkage.common.SalesPersonBrowseOutput;
import com.star.linkage.common.ShipToBrowseOutput;
import com.star.linkage.common.TransactionStatusActionBrowseOutput;
import com.star.linkage.common.TransactionStatusReasonBrowseOutput;
import com.star.linkage.common.UmBrowseOutput;
import com.star.linkage.common.UserPreferenceBrowseOutput;
import com.star.linkage.common.VendorBrowseOutput;
import com.star.linkage.common.VoucherCategoryBrowseOutput;
import com.star.linkage.common.WhsBrowseOutput;
import com.star.linkage.connection.ConnectionManOutput;

@Path("/common")
public class CommonService {

	@POST
	@Secured
	@Path("/read-cty")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getCountry(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<CountryBrowseOutput> starBrowseResponse = new BrowseResponse<CountryBrowseOutput>();
		starBrowseResponse.setOutput(new CountryBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GetCommonDAO commonDAO = new GetCommonDAO();

		commonDAO.getCountryList(starBrowseResponse);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblCountry.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputCty(starBrowseResponse));
	}

	private GenericEntity<?> getGenericBrowseOutputCty(BrowseResponse<CountryBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<CountryBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/validate-con")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response validateCon(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		MaintanenceResponse<ConnectionManOutput> starManResponse = new MaintanenceResponse<ConnectionManOutput>();
		starManResponse.setOutput(new ConnectionManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GetCommonDAO commonDAO = new GetCommonDAO();

		commonDAO.validateCon(starManResponse);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}

	@POST
	@Path("/cus-list")
	@Produces({ "application/json" })
	public Response getCustomerList(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<CustomerBrowseOutput> starBrowseResponse = new BrowseResponse<CustomerBrowseOutput>();

		starBrowseResponse.setOutput(new CustomerBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GetCommonDAO dao = new GetCommonDAO();

		dao.getCustomerList(starBrowseResponse, cmpyId);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblCustomer.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));

	}

	@POST
	@Path("/ven-list")
	@Produces({ "application/json" })
	public Response getVendorList(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<VendorBrowseOutput> starBrowseResponse = new BrowseResponse<VendorBrowseOutput>();

		starBrowseResponse.setOutput(new VendorBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GetCommonDAO dao = new GetCommonDAO();

		dao.getVendorList(starBrowseResponse, cmpyId);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblVendor.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputVendor(starBrowseResponse));

	}

	@POST
	@Path("/brh-list")
	@Produces({ "application/json" })
	public Response getBranchList(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<BranchBrowseOutput> starBrowseResponse = new BrowseResponse<BranchBrowseOutput>();

		starBrowseResponse.setOutput(new BranchBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GetCommonDAO dao = new GetCommonDAO();

		dao.getBranchList(starBrowseResponse, cmpyId);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblBranch.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputBranch(starBrowseResponse));

	}

	@POST
	@Path("/bnk-list")
	@Produces({ "application/json" })
	public Response getBankList(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<BankStxBrowseOutput> starBrowseResponse = new BrowseResponse<BankStxBrowseOutput>();

		starBrowseResponse.setOutput(new BankStxBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GetCommonDAO dao = new GetCommonDAO();

		dao.getBankList(starBrowseResponse, cmpyId);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblBank.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputBank(starBrowseResponse));

	}

	@POST
	@Path("/ex-rt")
	@Produces({ "application/json" })
	public Response getExchangeRate(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<ExchangeRtBrowseOutput> starBrowseResponse = new BrowseResponse<ExchangeRtBrowseOutput>();

		starBrowseResponse.setOutput(new ExchangeRtBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String origCry = jsonObject.get("origCry").getAsString();
		String eqvCry = jsonObject.get("eqvCry").getAsString();

		GetCommonDAO dao = new GetCommonDAO();

		dao.getExchangeRate(starBrowseResponse, cmpyId, origCry, eqvCry);

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputExcRt(starBrowseResponse));

	}

	@POST
	@Path("/cry-list")
	@Produces({ "application/json" })
	public Response getCurrencyList(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<CurrencyBrowseOutput> starBrowseResponse = new BrowseResponse<CurrencyBrowseOutput>();

		starBrowseResponse.setOutput(new CurrencyBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GetCommonDAO dao = new GetCommonDAO();

		dao.getCurrencyList(starBrowseResponse);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblCry.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputCurrency(starBrowseResponse));

	}

	@POST
	@Path("/pymt-mthd")
	@Produces({ "application/json" })
	public Response getPaymentMethodList(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<PaymentMethodBrowseOutput> starBrowseResponse = new BrowseResponse<PaymentMethodBrowseOutput>();

		starBrowseResponse.setOutput(new PaymentMethodBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GetCommonDAO dao = new GetCommonDAO();

		dao.getPaymentMethodList(starBrowseResponse);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblPymtMthd.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputPaymentMethod(starBrowseResponse));

	}

	@POST
	@Path("/vchr-cat")
	@Produces({ "application/json" })
	public Response getVoucherCategoryList(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<VoucherCategoryBrowseOutput> starBrowseResponse = new BrowseResponse<VoucherCategoryBrowseOutput>();

		starBrowseResponse.setOutput(new VoucherCategoryBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GetCommonDAO dao = new GetCommonDAO();

		dao.getVouCatList(starBrowseResponse);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblVouCategory.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputVoucherCategory(starBrowseResponse));

	}

	@POST
	@Path("/trn-sts-actn")
	@Produces({ "application/json" })
	public Response getTrnStsActnList(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<TransactionStatusActionBrowseOutput> starBrowseResponse = new BrowseResponse<TransactionStatusActionBrowseOutput>();

		starBrowseResponse.setOutput(new TransactionStatusActionBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GetCommonDAO dao = new GetCommonDAO();

		dao.getTrnStsActnList(starBrowseResponse);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblTrnStsActn.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputTrnStsActn(starBrowseResponse));

	}

	@POST
	@Path("/trn-sts-rsn")
	@Produces({ "application/json" })
	public Response getTrnStsRsnList(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<TransactionStatusReasonBrowseOutput> starBrowseResponse = new BrowseResponse<TransactionStatusReasonBrowseOutput>();

		starBrowseResponse.setOutput(new TransactionStatusReasonBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GetCommonDAO dao = new GetCommonDAO();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String trnSts = jsonObject.get("trnSts").getAsString();

		dao.getTrnStsRsnList(starBrowseResponse, trnSts);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblTrnStsRsn.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputTrnStsRsn(starBrowseResponse));

	}

	@POST
	@Path("/trn-sts")
	@Produces({ "application/json" })
	public Response getTrnSts(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		BrowseResponse<TransactionStatusActionBrowseOutput> starBrowseResponse = new BrowseResponse<TransactionStatusActionBrowseOutput>();

		starBrowseResponse.setOutput(new TransactionStatusActionBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GetCommonDAO dao = new GetCommonDAO();

		dao.getTrnStsList(starBrowseResponse);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblTrnStsActn.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputTrnStsActn(starBrowseResponse));

	}
	
	@POST
	@Secured
	@Path("/cmpy-list")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getCompany(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<CompanyBrowseOutput> starBrowseResponse = new BrowseResponse<CompanyBrowseOutput>();
		starBrowseResponse.setOutput(new CompanyBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		GetCommonDAO dao = new GetCommonDAO();
		
		dao.readCompanyList(starBrowseResponse);
		
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblCompany.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputCompany(starBrowseResponse));
	}
	
	@POST
	@Secured
	@Path("/whs-list")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getWhs(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<WhsBrowseOutput> starBrowseResponse = new BrowseResponse<WhsBrowseOutput>();
		starBrowseResponse.setOutput(new WhsBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		GetCommonDAO dao = new GetCommonDAO();
		
		dao.readWhsList(starBrowseResponse);
		
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblWhs.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputWhs(starBrowseResponse));
	}
	
	
	@POST
	@Secured
	@Path("/lgth-typ-list")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getOrderLength(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<LengthTypeBrowseOutput> starBrowseResponse = new BrowseResponse<LengthTypeBrowseOutput>();
		starBrowseResponse.setOutput(new LengthTypeBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		GetCommonDAO dao = new GetCommonDAO();
		
		dao.readLengthTypes(starBrowseResponse);
		
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblLengthType.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputLengthType(starBrowseResponse));
	}

	@POST
	@Path("/shp-to")
	@Produces({ "application/json" })
	public Response getShipTo(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		BrowseResponse<ShipToBrowseOutput> starBrowseResponse = new BrowseResponse<ShipToBrowseOutput>();

		starBrowseResponse.setOutput(new ShipToBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GetCommonDAO dao = new GetCommonDAO();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String cusId = jsonObject.get("cusId").getAsString();
		dao.getShipToList(starBrowseResponse, cmpyId, cusId);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblShpTo.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputShipTo(starBrowseResponse));

	}

	@POST
	@Path("/slp")
	@Produces({ "application/json" })
	public Response getSalesPerson(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		BrowseResponse<SalesPersonBrowseOutput> starBrowseResponse = new BrowseResponse<SalesPersonBrowseOutput>();

		starBrowseResponse.setOutput(new SalesPersonBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GetCommonDAO dao = new GetCommonDAO();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		//String cusId = jsonObject.get("cusId").getAsString();
		dao.getSalesPersonList(starBrowseResponse, cmpyId);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblSlp.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputSalesPerson(starBrowseResponse));

	}

	@POST
	@Path("/sls-cat")
	@Produces({ "application/json" })
	public Response getSalesCategory(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		BrowseResponse<SalesCategoryBrowseOutput> starBrowseResponse = new BrowseResponse<SalesCategoryBrowseOutput>();

		starBrowseResponse.setOutput(new SalesCategoryBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GetCommonDAO dao = new GetCommonDAO();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		//String cusId = jsonObject.get("cusId").getAsString();
		dao.getSalesCategoryList(starBrowseResponse);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblSlsCat.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputSalesCategory(starBrowseResponse));

	}
	
	
	@POST
	@Path("/um")
	@Produces({ "application/json" })
	public Response getUnitOfMeasure(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		BrowseResponse<UmBrowseOutput> starBrowseResponse = new BrowseResponse<UmBrowseOutput>();

		starBrowseResponse.setOutput(new UmBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GetCommonDAO dao = new GetCommonDAO();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		//String cusId = jsonObject.get("cusId").getAsString();
		dao.getUmList(starBrowseResponse);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblUm.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputUm(starBrowseResponse));

	}
	
	
	@POST
	@Path("/std-prs-list")
	@Produces({ "application/json" })
	public Response getProcessRouteList(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<ProcessRouteBrowseOutput> starBrowseResponse = new BrowseResponse<ProcessRouteBrowseOutput>();

		starBrowseResponse.setOutput(new ProcessRouteBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GetCommonDAO dao = new GetCommonDAO();

		dao.getStandardRoute(starBrowseResponse, cmpyId);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblPrsRte.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputProcessRoute(starBrowseResponse));

	}
	
	
	@POST
	@Path("/usr-pref")
	@Produces({ "application/json" })
	public Response getUserPreference(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<UserPreferenceBrowseOutput> starBrowseResponse = new BrowseResponse<UserPreferenceBrowseOutput>();

		starBrowseResponse.setOutput(new UserPreferenceBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GetCommonDAO dao = new GetCommonDAO();

		dao.getUserPreference(starBrowseResponse, cmpyId, userId);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblUsrPref.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputUserPref(starBrowseResponse));

	}

	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<CustomerBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<CustomerBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericBrowseOutputProcessRoute(BrowseResponse<ProcessRouteBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<ProcessRouteBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericBrowseOutputUserPref(BrowseResponse<UserPreferenceBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<UserPreferenceBrowseOutput>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputBranch(BrowseResponse<BranchBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<BranchBrowseOutput>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputBank(BrowseResponse<BankStxBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<BankStxBrowseOutput>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputVendor(BrowseResponse<VendorBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<VendorBrowseOutput>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputCurrency(BrowseResponse<CurrencyBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<CurrencyBrowseOutput>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericMaintenanceOutput(
			MaintanenceResponse<ConnectionManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<ConnectionManOutput>>(starMaintenanceResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputPaymentMethod(
			BrowseResponse<PaymentMethodBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<PaymentMethodBrowseOutput>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputVoucherCategory(
			BrowseResponse<VoucherCategoryBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<VoucherCategoryBrowseOutput>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputTrnStsActn(
			BrowseResponse<TransactionStatusActionBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<TransactionStatusActionBrowseOutput>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputTrnStsRsn(
			BrowseResponse<TransactionStatusReasonBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<TransactionStatusReasonBrowseOutput>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputExcRt(BrowseResponse<ExchangeRtBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<ExchangeRtBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericBrowseOutputCompany(BrowseResponse<CompanyBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<CompanyBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericBrowseOutputLengthType(BrowseResponse<LengthTypeBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<LengthTypeBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	
	private GenericEntity<?> getGenericBrowseOutputWhs(BrowseResponse<WhsBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<WhsBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericBrowseOutputShipTo(BrowseResponse<ShipToBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<ShipToBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericBrowseOutputSalesPerson(BrowseResponse<SalesPersonBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<SalesPersonBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericBrowseOutputSalesCategory(BrowseResponse<SalesCategoryBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<SalesCategoryBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericBrowseOutputUm(BrowseResponse<UmBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<UmBrowseOutput>>(starBrowseResponse) {
		};
	}

}
