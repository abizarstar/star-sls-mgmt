package com.star.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.invera.stratix.ws.exec.authentication.GtwyLgnResponseType;
import com.star.common.AuthenticationChecksum;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.JwtTokenHelper;
import com.star.common.SessionUtilGL;
import com.star.common.SessionUtilInformix;
import com.star.common.StarResponseBuilder;
import com.star.common.ValidateExecParams;
import com.star.common.storage.StorageType;
import com.star.dao.ExecCnfgDAO;
import com.star.dao.UserDetailsDAO;
import com.star.linkage.auth.AuthBrowseOutput;
import com.star.linkage.auth.AuthenticateSTXQDS;
import com.star.linkage.auth.UserBrowseOutput;
import com.star.linkage.auth.UserDetailsInput;
import com.star.linkage.execcnfg.ExecCnfgBrowseOutput;

@Path("/auth")
public class AuthService {

	@POST
	@Path("/validate")
	@Consumes({ "application/json" })
	public Response validateUser(String data) throws Exception {
		String output = "";

		BrowseResponse<AuthBrowseOutput> starBrowseResponseAuth = new BrowseResponse<AuthBrowseOutput>();

		starBrowseResponseAuth.setOutput(new AuthBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		try {
			String userId = "";
			String nginxURL = "";
			boolean skipDecifer = false;
			String password = "";
			String execParam = "";

			GtwyLgnResponseType gatewayLoginResponseType = null;
			
			UserDetailsInput detailsInput = new Gson().fromJson(data, UserDetailsInput.class);

			userId = detailsInput.getUsrId();
			password = detailsInput.getPassword();
			execParam = detailsInput.getParam();
			nginxURL=detailsInput.getNginxURL();
			
			if(execParam.contains("inpAuth+=") )
			{
				execParam = execParam.replace("inpAuth+=", "");
				skipDecifer = true;
				starBrowseResponseAuth.output.authToken = execParam;
				starBrowseResponseAuth.output.loginUser = userId;
			}
			
			if(execParam.length() > 0  && skipDecifer == false)
			{
				ValidateExecParams execParams = new ValidateExecParams();
				execParams.deciferJobString(execParam, starBrowseResponseAuth);
			}

			ExecCnfgDAO execCnfg = new ExecCnfgDAO();

			BrowseResponse<ExecCnfgBrowseOutput> starBrowseResponse = new BrowseResponse<ExecCnfgBrowseOutput>();

			starBrowseResponse.setOutput(new ExecCnfgBrowseOutput());

			execCnfg.getExecInfo(starBrowseResponse);

			int loopSize = starBrowseResponse.output.fldTblExec.size();

			String execPort = "";// "35839";
			String execHost = "";
			String execEnv = "";
			String execClass = "";
			String execUsrId = "";
			String execChkSum = "";
			String prmHost = "";
			String prmPort = "";

			for (int i = 0; i < loopSize; i++) {

				execPort = String.valueOf(starBrowseResponse.output.fldTblExec.get(i).getPort());
				execHost = String.valueOf(starBrowseResponse.output.fldTblExec.get(i).getHostExec());
				execEnv = String.valueOf(starBrowseResponse.output.fldTblExec.get(i).getEnv());
				execClass = String.valueOf(starBrowseResponse.output.fldTblExec.get(i).getClasses());
				execUsrId = String.valueOf(starBrowseResponse.output.fldTblExec.get(i).getUsrId());
				execChkSum = String.valueOf(starBrowseResponse.output.fldTblExec.get(i).getChkSum());
				prmHost = String.valueOf(starBrowseResponse.output.fldTblExec.get(i).getHostPrm());
				prmPort = String.valueOf(starBrowseResponse.output.fldTblExec.get(i).getPrmPort());
			}
			
			SessionUtilInformix.resetSessionDetails();
			StorageType.getStorgaeType();

			UserDetailsDAO dao = new UserDetailsDAO();

			BrowseResponse<UserBrowseOutput> starBrowseResponseUsr = new BrowseResponse<UserBrowseOutput>();
			starBrowseResponseUsr.setOutput(new UserBrowseOutput());
			
			
			if (execParam.length() == 0) {

				dao.getUsersAll(starBrowseResponseUsr, userId);
	
				if (starBrowseResponseUsr.output.fldTblUsers.size() > 0) {
	
					if (starBrowseResponseUsr.output.fldTblUsers.get(0).getUsrTyp().equals("STX")
							&& Boolean.parseBoolean(starBrowseResponseUsr.output.fldTblUsers.get(0).getActvSts())) {
	
						AuthenticateSTXQDS authenticate = new AuthenticateSTXQDS();
	
						gatewayLoginResponseType = authenticate.stxAuthToken(userId, password, execPort, execHost, execEnv,
								execClass, nginxURL, starBrowseResponseAuth);
	
						starBrowseResponseAuth.output.loginUser = userId;
	
						if (gatewayLoginResponseType != null) {
	
							starBrowseResponseAuth.output.authToken = gatewayLoginResponseType.getAuthenticationToken()
									.getValue();
							starBrowseResponseAuth.output.appHost = prmHost;
							starBrowseResponseAuth.output.appPort = prmPort;
	
							UserDetailsDAO userdtlDAO = new UserDetailsDAO();
	
							List<Object[]> lst = userdtlDAO.getUser(userId);
	
							String cmpyId = userdtlDAO.getUserCompany(userId);
	
							if (lst.size() > 0) {
	
								for (Object[] userVal : lst) {
	
									starBrowseResponseAuth.output.usrNm = String.valueOf(userVal[1]);
									starBrowseResponseAuth.output.usrEmail = String.valueOf(userVal[2]);
									starBrowseResponseAuth.output.usrTyp = cmpyId;
									starBrowseResponseAuth.output.usrGrp = String.valueOf(userVal[4]);
									starBrowseResponseAuth.output.usrIsActive = String.valueOf(userVal[5]);
	
								}
	
								String privateKey = JwtTokenHelper.getInstance().generatePrivateKey(userId, password);
	
								starBrowseResponseAuth.output.appToken = privateKey;
	
							} else {
	
								starBrowseResponseAuth.output.appToken = "";
								starBrowseResponseAuth.output.usrNm = "";
								starBrowseResponseAuth.output.usrEmail = "";
								starBrowseResponseAuth.output.usrTyp = "";
								starBrowseResponseAuth.output.usrGrp = "";
								starBrowseResponseAuth.output.usrIsActive = "";
							}
	
						} else {
	
							starBrowseResponseAuth.output.rtnSts = 1;
						}
					} else if (starBrowseResponseUsr.output.fldTblUsers.get(0).getUsrTyp().equals("APP")
							&& Boolean.parseBoolean(starBrowseResponseUsr.output.fldTblUsers.get(0).getActvSts())) {
						Boolean authFlg = AuthenticationChecksum.validatePassword(detailsInput.getPassword(),
								starBrowseResponseUsr.output.fldTblUsers.get(0).getChkSum());
	
						if (authFlg == true) {
	
							AuthenticateSTXQDS authenticate = new AuthenticateSTXQDS();
	
							gatewayLoginResponseType = authenticate.stxAuthToken(execUsrId, execChkSum, execPort, execHost,
									execEnv, execClass, nginxURL, starBrowseResponseAuth);
	
							starBrowseResponseAuth.output.loginUser = userId;
	
							if (gatewayLoginResponseType != null) {
	
								starBrowseResponseAuth.output.authToken = gatewayLoginResponseType.getAuthenticationToken()
										.getValue();
								starBrowseResponseAuth.output.appHost = prmHost;
								starBrowseResponseAuth.output.appPort = prmPort;
	
								UserDetailsDAO userdtlDAO = new UserDetailsDAO();
	
								String cmpyId = userdtlDAO.getUserCompany(execUsrId);
	
								starBrowseResponseAuth.output.usrNm = starBrowseResponseUsr.output.fldTblUsers.get(0)
										.getUserNm();
								starBrowseResponseAuth.output.usrEmail = starBrowseResponseUsr.output.fldTblUsers.get(0)
										.getEmailId();
								starBrowseResponseAuth.output.usrTyp = cmpyId;
								starBrowseResponseAuth.output.usrGrp = starBrowseResponseUsr.output.fldTblUsers.get(0)
										.getUsrGrpId();
								starBrowseResponseAuth.output.usrIsActive = starBrowseResponseUsr.output.fldTblUsers.get(0)
										.getActvSts();
	
								String privateKey = JwtTokenHelper.getInstance().generatePrivateKey(userId, password);
								starBrowseResponseAuth.output.appToken = privateKey;
	
								
	
							} else {
								starBrowseResponseAuth.output.appToken = "";
								starBrowseResponseAuth.output.usrNm = "";
								starBrowseResponseAuth.output.usrEmail = "";
								starBrowseResponseAuth.output.usrTyp = "";
								starBrowseResponseAuth.output.usrGrp = "";
								starBrowseResponseAuth.output.usrIsActive = "";
	
								starBrowseResponseAuth.output.rtnSts = 1;
								starBrowseResponseAuth.output.messages.add(CommonConstants.EXEC_USR_ERR);
							}
						} else {
							starBrowseResponseAuth.output.rtnSts = 1;
							starBrowseResponseAuth.output.messages.add(CommonConstants.PASS_ERR);
						}
					}
				}
			}else if(skipDecifer == true) {
				
				dao.getUsersAll(starBrowseResponseUsr, starBrowseResponseAuth.output.loginUser);

				starBrowseResponseAuth.output.appHost = prmHost;
				starBrowseResponseAuth.output.appPort = prmPort;

				UserDetailsDAO userdtlDAO = new UserDetailsDAO();

				List<Object[]> lst = userdtlDAO.getUser(starBrowseResponseAuth.output.loginUser);

				String cmpyId = userdtlDAO.getUserCompany(starBrowseResponseAuth.output.loginUser);

				if (lst.size() > 0) {

					for (Object[] userVal : lst) {

						starBrowseResponseAuth.output.usrNm = String.valueOf(userVal[1]);
						starBrowseResponseAuth.output.usrEmail = String.valueOf(userVal[2]);
						starBrowseResponseAuth.output.usrTyp = cmpyId;
						starBrowseResponseAuth.output.usrGrp = String.valueOf(userVal[4]);
						starBrowseResponseAuth.output.usrIsActive = String.valueOf(userVal[5]);

					}

					String privateKey = JwtTokenHelper.getInstance().generatePrivateKey(userId, password);

					starBrowseResponseAuth.output.appToken = privateKey;

					/* Initialize Storage variable */
					StorageType.getStorgaeType();	
				}
				
				
				if (starBrowseResponseAuth.output.rtnSts == 0 && starBrowseResponseUsr.output.fldTblUsers.size() == 0) {
					starBrowseResponseAuth.output.rtnSts = 1;
					starBrowseResponseAuth.output.messages.add(CommonConstants.USR_NA);
				}

				if (starBrowseResponseUsr.output.fldTblUsers.size() > 0) {
					if (!Boolean.parseBoolean(starBrowseResponseUsr.output.fldTblUsers.get(0).getActvSts())) {
						starBrowseResponseAuth.output.rtnSts = 1;
						starBrowseResponseAuth.output.messages.add(CommonConstants.USR_DIS);
					}
				}

			

			
			}else {
				
				dao.getUsersAll(starBrowseResponseUsr, starBrowseResponseAuth.output.loginUser);

				starBrowseResponseAuth.output.appHost = prmHost;
				starBrowseResponseAuth.output.appPort = prmPort;

				UserDetailsDAO userdtlDAO = new UserDetailsDAO();

				List<Object[]> lst = userdtlDAO.getUser(starBrowseResponseAuth.output.loginUser);

				String cmpyId = userdtlDAO.getUserCompany(starBrowseResponseAuth.output.loginUser);

				if (lst.size() > 0) {

					for (Object[] userVal : lst) {

						starBrowseResponseAuth.output.usrNm = String.valueOf(userVal[1]);
						starBrowseResponseAuth.output.usrEmail = String.valueOf(userVal[2]);
						starBrowseResponseAuth.output.usrTyp = cmpyId;
						starBrowseResponseAuth.output.usrGrp = String.valueOf(userVal[4]);
						starBrowseResponseAuth.output.usrIsActive = String.valueOf(userVal[5]);
						
					}

					String privateKey = JwtTokenHelper.getInstance().generatePrivateKey(userId, password);

					starBrowseResponseAuth.output.appToken = privateKey;
	
				}
				
				
				if (starBrowseResponseAuth.output.rtnSts == 0 && starBrowseResponseUsr.output.fldTblUsers.size() == 0) {
					starBrowseResponseAuth.output.rtnSts = 1;
					starBrowseResponseAuth.output.messages.add(CommonConstants.USR_NA);
				}

				if (starBrowseResponseUsr.output.fldTblUsers.size() > 0) {
					if (!Boolean.parseBoolean(starBrowseResponseUsr.output.fldTblUsers.get(0).getActvSts())) {
						starBrowseResponseAuth.output.rtnSts = 1;
						starBrowseResponseAuth.output.messages.add(CommonConstants.USR_DIS);
					}
				}
			
			}

			if (starBrowseResponseAuth.output.rtnSts == 0 && starBrowseResponseUsr.output.fldTblUsers.size() == 0) {
				starBrowseResponseAuth.output.rtnSts = 1;
				starBrowseResponseAuth.output.messages.add(CommonConstants.USR_NA);
			}else {
				if(!Boolean.parseBoolean(starBrowseResponseUsr.output.fldTblUsers.get(0).getActvSts()))
				{
					starBrowseResponseAuth.output.rtnSts = 1;
					starBrowseResponseAuth.output.messages.add(CommonConstants.USR_DIS);
				}
			}


		} catch (Exception e) {
			starBrowseResponseAuth.output.rtnSts = 1;
			starBrowseResponseAuth.output.messages.add(CommonConstants.AUTH_TOKN_VALD);
		}
		
		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponseAuth));

	}

	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<AuthBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<AuthBrowseOutput>>(starBrowseResponse) {
		};
	}

}
