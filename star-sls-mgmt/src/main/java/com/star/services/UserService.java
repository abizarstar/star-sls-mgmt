package com.star.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.AuthenticationChecksum;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.UserDAO;
import com.star.dao.UserDetailsDAO;
import com.star.linkage.User.UserERPBrowseOutput;
import com.star.linkage.auth.UserBrowseOutput;
import com.star.linkage.auth.UserDetails;
import com.star.linkage.auth.UserDetailsInput;
import com.star.linkage.auth.UserDetailsManOutput;

@Path("/user")
public class UserService {

	
	@POST
	@Secured
	@Path("/read-erp")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getVendor(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<UserERPBrowseOutput> starBrowseResponse = new BrowseResponse<UserERPBrowseOutput>();
		starBrowseResponse.setOutput(new UserERPBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		UserDAO userDAO= new UserDAO();
		
		userDAO.readUserList(starBrowseResponse);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblERPUser.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
	}
	
	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<UserERPBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<UserERPBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	
	@POST
	  @Path("/add")
	  @Consumes({"application/json"})
	  @Produces({"application/json"})
	  public Response addLogin(String data) throws Exception
	  {		  
		  MaintanenceResponse<UserDetailsManOutput> starMaintenanceResponse = new MaintanenceResponse<UserDetailsManOutput>();
			
			starMaintenanceResponse.setOutput(new UserDetailsManOutput());
		
			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
			
			 UserDetailsInput detailsInput = new Gson().fromJson(data, UserDetailsInput.class);
			
			 UserDetailsDAO detailsDAO = new UserDetailsDAO();
			 
			 if(detailsInput.getUsrType().equals("APP"))
			 {
				 String generatedSecuredPasswordHash = AuthenticationChecksum.generateStorngPasswordHash(detailsInput.getChkSm());
				 
				 detailsInput.setChkSm(generatedSecuredPasswordHash);
			 }
			 else
			 {
				 detailsInput.setChkSm("");
			 }
			 
			detailsDAO.addUser(starMaintenanceResponse, detailsInput);

			 System.out.println(detailsInput.getUsrNm());
			 System.out.println(detailsInput.getPassword());
			 
			 return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starMaintenanceResponse));
		 
	  }
	
	
	 @POST
	  @Path("/read-app")
	  @Consumes({"application/json"})
	  @Produces({"application/json"})
	  public Response getUsers(String data) throws Exception
	  {
		  BrowseResponse<UserBrowseOutput> starBrowseResponse = new BrowseResponse<UserBrowseOutput>();
			
		  starBrowseResponse.setOutput(new UserBrowseOutput());
		
			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
			
			JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
			
			String usrId = "";
			
			usrId = jsonObject.get("usrId").getAsString();
			
			 UserDetailsDAO detailsDAO = new UserDetailsDAO();
			 			 
			 detailsDAO.getUsersAll(starBrowseResponse, usrId);
			 
			 return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputUserList(starBrowseResponse));
		 
	  }
	 
	 
	 @POST
	  @Path("/update")
	  @Consumes({"application/json"})
	  @Produces({"application/json"})
	  public Response updateUser(String data) throws Exception
	  {
		 MaintanenceResponse<UserDetailsManOutput> starMaintenanceResponse = new MaintanenceResponse<UserDetailsManOutput>();
			
			starMaintenanceResponse.setOutput(new UserDetailsManOutput());
		
			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
			
			 UserDetailsInput detailsInput = new Gson().fromJson(data, UserDetailsInput.class);
			
			 UserDetailsDAO detailsDAO = new UserDetailsDAO();
			 
			 if(detailsInput.getChkSm().length() > 0)
			 {
				 String generatedSecuredPasswordHash = AuthenticationChecksum.generateStorngPasswordHash(detailsInput.getChkSm());
				 detailsInput.setChkSm(generatedSecuredPasswordHash);
			 }
			 
			detailsDAO.updUsrDtls(starMaintenanceResponse, detailsInput);
			 
			 return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starMaintenanceResponse));
		 
	  }
	
	
	private GenericEntity<?> getGenericMaintenanceOutput(MaintanenceResponse<UserDetailsManOutput> starBrowseResponse) {
		return new GenericEntity<MaintanenceResponse<UserDetailsManOutput>>(starBrowseResponse) {
		};
	}
	
	
	@POST
	@Secured
	@Path("/read-aprv")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getApprovers(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<UserERPBrowseOutput> starBrowseResponse = new BrowseResponse<UserERPBrowseOutput>();
		starBrowseResponse.setOutput(new UserERPBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		UserDAO userDAO= new UserDAO();
		
		userDAO.readApproverList(starBrowseResponse);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblERPUser.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
	}
	
	@POST
	@Secured
	@Path("/UpdUsrDtls")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response UpdUsrDtls(@HeaderParam ("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<UserDetailsManOutput> starManResponse = new MaintanenceResponse<UserDetailsManOutput>();

		starManResponse.setOutput(new UserDetailsManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		String usrNm = jsonObject.get("usrNm").getAsString();
		String usrEml = jsonObject.get("eml").getAsString();
		String oldPass = jsonObject.get("oldPass").getAsString();
		String newPass = jsonObject.get("newPass").getAsString();
		
		UserDetailsDAO userDetailsDAO = new UserDetailsDAO();
		
		UserDetailsInput dfltFieldsInput= new UserDetailsInput();
		
		dfltFieldsInput.setUsrNm(usrNm);
		dfltFieldsInput.setEml(usrEml);
		dfltFieldsInput.setUsrId(userId);
		
		if(oldPass.length() > 0 && newPass.length() > 0)
		{
			boolean passwordSts = userDetailsDAO.validatePassword(userId, oldPass);
		
			if(passwordSts == true)
			{
				String generatedSecuredPasswordHash = AuthenticationChecksum.generateStorngPasswordHash(newPass);
				 
				dfltFieldsInput.setPassword(generatedSecuredPasswordHash);	
			}
			else
			{
				starManResponse.output.rtnSts = 1;
				starManResponse.output.messages.add(CommonConstants.OLD_PASS_ERR);
				
				return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputFields(starManResponse));
			}
		}
		else
		{
			dfltFieldsInput.setPassword("");
		}
		
		userDetailsDAO.updUsrProfile(starManResponse, dfltFieldsInput);
		
		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputFields(starManResponse));
		
	}

	private GenericEntity<?> getGenericMaintenanceOutputFields(
			MaintanenceResponse<UserDetailsManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<UserDetailsManOutput>>(starMaintenanceResponse) {
		};
	}
	
	@POST
	  @Path("/get-sing-usr")
	  @Consumes({"application/json"})
	  @Produces({"application/json"})
	  public Response getSingUsr(@HeaderParam ("user-id") String userId,String data) throws Exception
	  {
		  BrowseResponse<UserBrowseOutput> starBrowseResponse = new BrowseResponse<UserBrowseOutput>();
			
		  starBrowseResponse.setOutput(new UserBrowseOutput());
		
			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
			
			UserDetails dfltFieldsInput= new UserDetails();
			dfltFieldsInput.setUserId(userId);
			
			UserDetailsDAO userDetailsDAO = new UserDetailsDAO();
			 			 
			userDetailsDAO.getSingUsr(starBrowseResponse,dfltFieldsInput);
			 
			 return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputUserList(starBrowseResponse));
		 
	  }

	private GenericEntity<?> getGenericBrowseOutputUserList(BrowseResponse<UserBrowseOutput> starBrowseResponse) {
    	return new GenericEntity<BrowseResponse<UserBrowseOutput>>(starBrowseResponse){};
    }
	
}
