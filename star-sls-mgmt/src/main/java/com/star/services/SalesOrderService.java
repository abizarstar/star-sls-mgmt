package com.star.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.SalesOrderDAO;
import com.star.dao.StarSlsDAO;
import com.star.linkage.SalesOrder.I25RecManOutput;
import com.star.linkage.SalesOrder.I26RecInput;
import com.star.linkage.SalesOrder.I26RecManOutput;
import com.star.linkage.SalesOrder.I47RecInput;
import com.star.linkage.SalesOrder.I47RecManOutput;
import com.star.linkage.SalesOrder.I48RecInput;
import com.star.linkage.SalesOrder.I48RecManOutput;
import com.star.linkage.SalesOrder.OdrInfoBrowseOutput;
import com.star.linkage.SalesOrder.OdrInfoInput;
import com.star.linkage.SalesOrder.OdrInfoManOutput;
import com.star.linkage.SalesOrder.S05RecInput;
import com.star.linkage.SalesOrder.S05RecManOutput;
import com.star.linkage.SalesOrder.S06RecInput;
import com.star.linkage.SalesOrder.S06RecManOutput;
import com.star.linkage.SalesOrder.S16RecInput;
import com.star.linkage.SalesOrder.S16RecManOutput;
import com.star.linkage.SalesOrder.S17RecInput;
import com.star.linkage.SalesOrder.S17RecManOutput;
import com.star.linkage.SalesOrder.S20RecInput;
import com.star.linkage.SalesOrder.S20RecManOutput;
import com.star.linkage.SalesOrder.S52RecInput;
import com.star.linkage.SalesOrder.S52RecManOutput;
import com.star.linkage.SalesOrder.SOItemBrowseOutput;
import com.star.linkage.SalesOrder.SalesOrderBrowseOutput;
import com.star.linkage.SalesOrder.SalesOrderInfo;
import com.star.linkage.SalesOrder.SalesOrderInput;
import com.star.linkage.SalesOrder.SalesOrderManOutput;
import com.star.linkage.SalesOrder.TI00RecInput;
import com.star.linkage.SalesOrder.TI00RecManOutput;

@Path("/sales")
public class SalesOrderService {
	int so = 0;

	@POST
	@Secured
	@Path("/updt-CrtSo")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response crtSo(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		MaintanenceResponse<I25RecManOutput> starMaintenanceResponse = new MaintanenceResponse<I25RecManOutput>();

		starMaintenanceResponse.setOutput(new I25RecManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		// String asgnUser = jsonObject.get("asgnTo").getAsString();
		if (maxsls() != 0) {
			int s = maxsls();
			so = s + 1;
		}
		// addI25Rec(userId, cmpyId, data);
		/*
		 * addI26Rec(userId, cmpyId, data); addI47Rec(userId, cmpyId, data);
		 * addI48Rec(userId, cmpyId, data);
		 */
		// addS01Rec(userId, cmpyId, data); crymethd
		addS05Rec(userId, cmpyId, data); // podtfrmt
		// addS06Rec(userId, cmpyId, data);
		// addS16Rec(userId, cmpyId, data);
		/*
		 * addS52Rec(userId, cmpyId, data); addTI00Rec(userId, cmpyId, data);
		 */
		System.out.println("Query Runing Successfully  ---- " + so);
		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput1(starMaintenanceResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutputso(
			MaintanenceResponse<SalesOrderManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<SalesOrderManOutput>>(starMaintenanceResponse) {
		};
	}

	@POST
	@Secured
	@Path("/updt-mxsls")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public static int maxsls() throws Exception {
		SalesOrderDAO slsOdrDAO = new SalesOrderDAO();
		return slsOdrDAO.getmaxRec();

	}

	@POST
	@Secured
	@Path("/updt-slsodr")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addS01Rec(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		MaintanenceResponse<SalesOrderManOutput> starMaintenanceResponse = new MaintanenceResponse<SalesOrderManOutput>();

		starMaintenanceResponse.setOutput(new SalesOrderManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		SalesOrderInput input = new SalesOrderInput();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		JsonArray lotArray = jsonObject.get("slsOrdData").getAsJsonArray();

		for (int i = 0; i < lotArray.size(); i++) {

			JsonObject lotObject = lotArray.get(i).getAsJsonObject();
			String typ = lotObject.get("fldTyp").getAsString();
			System.out.println(typ);
			System.out.println(lotObject.get("fldVal").getAsString());
			if (typ.equalsIgnoreCase("REF_BRH")) {
				input.setS01OrdBrh(lotObject.get("fldVal").getAsString());
			}
			if (typ.equalsIgnoreCase("REF_BRH")) {
				input.setS01OrdBrh(lotObject.get("fldVal").getAsString());
			}
			if (typ.equalsIgnoreCase("CUS_ID")) {
				input.setS01SldCusId(lotObject.get("fldVal").getAsInt());
			}
			if (typ.equalsIgnoreCase("SHP_TO_1")) {
				input.setS01ShpTo(lotObject.get("fldVal").getAsInt());
			}
			if (typ.equalsIgnoreCase("IS_SLSPSN")) {
				input.setS01IsSlp(lotObject.get("fldVal").getAsString());
			}
			if (typ.equalsIgnoreCase("OS_SLSPSN")) {
				input.setS01OsSlp(lotObject.get("fldVal").getAsString());
			}
			if (typ.equalsIgnoreCase("TKN_SLP")) {
				input.setS01TknSlp(lotObject.get("fldVal").getAsString());
			}
			if (typ.equalsIgnoreCase("ORD_TYP")) {
				input.setS01OrdTyp(lotObject.get("fldVal").getAsString());
			}
			if (typ.equalsIgnoreCase("SLS_CAT")) {
				input.setS01SlsCat(lotObject.get("fldVal").getAsString());
			}
			if (typ.equalsIgnoreCase("TKN_SLP")) {
				input.setS01TknSlp(lotObject.get("fldVal").getAsString());
			}
			String cry = "USD";
			input.setS01Cry(cry);
			input.setS01CmpyId(cmpyId);
		}
		SalesOrderDAO slsOdrDAO = new SalesOrderDAO();

		slsOdrDAO.updtS01Rec(starMaintenanceResponse, so, input);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starMaintenanceResponse));

	}

	@POST
	@Secured
	@Path("/updt-i25rec")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addI25Rec(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		MaintanenceResponse<I25RecManOutput> starMaintenanceResponse = new MaintanenceResponse<I25RecManOutput>();

		starMaintenanceResponse.setOutput(new I25RecManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		JsonArray lotArray = jsonObject.get("slsOrdData").getAsJsonArray();

		for (int i = 0; i < lotArray.size(); i++) {

			JsonObject lotObject = lotArray.get(i).getAsJsonObject();
			System.out.println(lotObject.get("fldTyp").getAsString());
			/*
			 * checkLotInfo.setLotNo(lotObject.get("lotNo").getAsInt());
			 * checkLotInfo.setLotInfo(lotObject.get("shrtInfo").getAsString());
			 * checkLotInfo.setChkFrm(lotObject.get("chkFrm").getAsString());
			 * checkLotInfo.setChkTo(lotObject.get("chkTo").getAsString());
			 * 
			 * lotInfos.add(checkLotInfo);
			 */
		}

		/*
		 * Gson gson = new GsonBuilder().setPrettyPrinting().create(); I25RecInput
		 * detailsInput = new I25RecInput(); SalesOrderDAO slsOdrDAO = new
		 * SalesOrderDAO(); List<JsonArray> list1 =
		 * Arrays.asList(jsonObject.getAsJsonArray("slsOrdData"));
		 * System.out.println(list1); Type listType = new TypeToken<List<JType>>()
		 * {}.getType(); List<JType> obj = gson.fromJson(data, listType); for (JsonArray
		 * typ : list1) {
		 * 
		 * String fld = typ.getAsString(); if (fld == "SHP_BRH") { //
		 * System.out.println("-------- value of json pfx --- " + typ.getFldVal()); }
		 */
		// }
		// detailsInput.setI25CmpyId(cmpyId);

		// slsOdrDAO.updtI25Rec(starMaintenanceResponse, so, detailsInput);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput1(starMaintenanceResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutput(
			MaintanenceResponse<SalesOrderManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<SalesOrderManOutput>>(starMaintenanceResponse) {
		};

	}

	private GenericEntity<?> getGenericMaintenanceOutput1(
			MaintanenceResponse<I25RecManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<I25RecManOutput>>(starMaintenanceResponse) {
		};
	}

	@POST
	@Secured
	@Path("/updt-i26rec")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addI26Rec(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		MaintanenceResponse<I26RecManOutput> starMaintenanceResponse = new MaintanenceResponse<I26RecManOutput>();

		starMaintenanceResponse.setOutput(new I26RecManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		I26RecInput detailsInput = new I26RecInput();

		detailsInput.setI26CmpyId(cmpyId);

		SalesOrderDAO slsOdrDAO = new SalesOrderDAO();

		slsOdrDAO.updtI26Rec(starMaintenanceResponse, so, detailsInput);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputI26(starMaintenanceResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutputI26(
			MaintanenceResponse<I26RecManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<I26RecManOutput>>(starMaintenanceResponse) {
		};
	}

	@POST
	@Secured
	@Path("/updt-i47rec")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addI47Rec(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		MaintanenceResponse<I47RecManOutput> starMaintenanceResponse = new MaintanenceResponse<I47RecManOutput>();

		starMaintenanceResponse.setOutput(new I47RecManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		I47RecInput detailsInput = new I47RecInput();
		detailsInput.setI47CmpyId(cmpyId);

		SalesOrderDAO slsOdrDAO = new SalesOrderDAO();

		slsOdrDAO.updtI47Rec(starMaintenanceResponse, so, detailsInput);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputI47(starMaintenanceResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutputI47(
			MaintanenceResponse<I47RecManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<I47RecManOutput>>(starMaintenanceResponse) {
		};
	}

	@POST
	@Secured
	@Path("/updt-i48rec")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addI48Rec(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		MaintanenceResponse<I48RecManOutput> starMaintenanceResponse = new MaintanenceResponse<I48RecManOutput>();

		starMaintenanceResponse.setOutput(new I48RecManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		I48RecInput detailsInput = new I48RecInput();
		detailsInput.setI48CmpyId(cmpyId);
		SalesOrderDAO slsOdrDAO = new SalesOrderDAO();

		slsOdrDAO.updtI48Rec(starMaintenanceResponse, so, detailsInput);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputI48(starMaintenanceResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutputI48(
			MaintanenceResponse<I48RecManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<I48RecManOutput>>(starMaintenanceResponse) {
		};
	}

	@POST
	@Secured
	@Path("/updt-S05rec")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addS05Rec(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		MaintanenceResponse<S05RecManOutput> starMaintenanceResponse = new MaintanenceResponse<S05RecManOutput>();

		starMaintenanceResponse.setOutput(new S05RecManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		S05RecInput input = new S05RecInput();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		JsonArray lotArray = jsonObject.get("slsOrdData").getAsJsonArray();

		for (int i = 0; i < lotArray.size(); i++) {

			JsonObject lotObject = lotArray.get(i).getAsJsonObject();
			String typ = lotObject.get("fldTyp").getAsString();
			System.out.println(typ);
			System.out.println(lotObject.get("fldVal").getAsString());
			if (typ.equalsIgnoreCase("PO_NO")) {
				input.setS05CusPo(lotObject.get("fldVal").getAsString());
			}
			if (typ.equalsIgnoreCase("PO_DT")) {
				input.setS05CusPoDt(lotObject.get("fldVal").getAsString());
			}
			if (typ.equalsIgnoreCase("ORD_PCS")) {
				input.setS05OrdPcs(lotObject.get("fldVal").getAsInt());
			}
			if (typ.equalsIgnoreCase("ORD_MSR")) {
				input.setS05OrdMsr(lotObject.get("fldVal").getAsDouble());
			}
			if (typ.equalsIgnoreCase("WGT")) {
				input.setS05OrdWgt(lotObject.get("fldVal").getAsDouble());
			}
			if (typ.equalsIgnoreCase("QTY")) {
				input.setS05OrdQty(lotObject.get("fldVal").getAsDouble());
			}
			if (typ.equalsIgnoreCase("UM")) {
				input.setS05BktWgtUm(lotObject.get("fldVal").getAsString());
			}
			if (typ.equalsIgnoreCase("SLS_CAT")) {
				input.setS05SlsCat(lotObject.get("fldVal").getAsString());
			}
			if (typ.equalsIgnoreCase("SOURCE")) {
				input.setS05Src(lotObject.get("fldVal").getAsString());
			}
		}
		input.setS05CmpyId(cmpyId);

		SalesOrderDAO slsOdrDAO = new SalesOrderDAO();

		slsOdrDAO.updtS05Rec(starMaintenanceResponse, so, input);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputS05(starMaintenanceResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutputS05(
			MaintanenceResponse<S05RecManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<S05RecManOutput>>(starMaintenanceResponse) {
		};
	}

	@POST
	@Secured
	@Path("/updt-S06rec")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addS06Rec(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		MaintanenceResponse<S06RecManOutput> starMaintenanceResponse = new MaintanenceResponse<S06RecManOutput>();

		starMaintenanceResponse.setOutput(new S06RecManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		S06RecInput input = new S06RecInput();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		JsonArray lotArray = jsonObject.get("slsOrdData").getAsJsonArray();

		for (int i = 0; i < lotArray.size(); i++) {

			JsonObject lotObject = lotArray.get(i).getAsJsonObject();
			String typ = lotObject.get("fldTyp").getAsString();
			System.out.println(typ);
			System.out.println(lotObject.get("fldVal").getAsString());
			if (typ.equalsIgnoreCase("CUS_ID")) {
				input.setPrtCusId(lotObject.get("fldVal").getAsString());
			}
			if (typ.equalsIgnoreCase("CUST_ITEM")) {
				input.setPrt(lotObject.get("fldVal").getAsString());
			}
		}

		input.setS06CmpyId(cmpyId);
		SalesOrderDAO slsOdrDAO = new SalesOrderDAO();

		slsOdrDAO.updtS06Rec(starMaintenanceResponse, so, input);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputS06(starMaintenanceResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutputS06(
			MaintanenceResponse<S06RecManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<S06RecManOutput>>(starMaintenanceResponse) {
		};
	}

	@POST
	@Secured
	@Path("/updt-S16rec")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addS16Rec(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		MaintanenceResponse<S16RecManOutput> starMaintenanceResponse = new MaintanenceResponse<S16RecManOutput>();

		starMaintenanceResponse.setOutput(new S16RecManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		S16RecInput input = new S16RecInput();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		JsonArray lotArray = jsonObject.get("slsOrdData").getAsJsonArray();

		for (int i = 0; i < lotArray.size(); i++) {

			JsonObject lotObject = lotArray.get(i).getAsJsonObject();
			String typ = lotObject.get("fldTyp").getAsString();
			System.out.println(typ);
			System.out.println(lotObject.get("fldVal").getAsString());
			if (typ.equalsIgnoreCase("QTY")) {
				input.setS16RlsWgt(lotObject.get("fldVal").getAsDouble());
				input.setS16RlsQty(lotObject.get("fldVal").getAsDouble());
			}
			if (typ.equalsIgnoreCase("DEL_DT")) {
				input.setDueDtEnt(lotObject.get("fldVal").getAsString());
			}
		}

		input.setS16CmpyId(cmpyId);
		SalesOrderDAO slsOdrDAO = new SalesOrderDAO();

		slsOdrDAO.updtS16Rec(starMaintenanceResponse, so, input);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputS16(starMaintenanceResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutputS16(
			MaintanenceResponse<S16RecManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<S16RecManOutput>>(starMaintenanceResponse) {
		};
	}

	@POST
	@Secured
	@Path("/updt-S17rec")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addS17Rec(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<S17RecManOutput> starMaintenanceResponse = new MaintanenceResponse<S17RecManOutput>();

		starMaintenanceResponse.setOutput(new S17RecManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		S17RecInput detailsInput = new Gson().fromJson(data, S17RecInput.class);

		SalesOrderDAO slsOdrDAO = new SalesOrderDAO();

		slsOdrDAO.updtS17Rec(starMaintenanceResponse, so, detailsInput);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputS17(starMaintenanceResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutputS17(
			MaintanenceResponse<S17RecManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<S17RecManOutput>>(starMaintenanceResponse) {
		};
	}

	@POST
	@Secured
	@Path("/updt-S20rec")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addS20Rec(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<S20RecManOutput> starMaintenanceResponse = new MaintanenceResponse<S20RecManOutput>();

		starMaintenanceResponse.setOutput(new S20RecManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		S20RecInput detailsInput = new Gson().fromJson(data, S20RecInput.class);

		SalesOrderDAO slsOdrDAO = new SalesOrderDAO();

		slsOdrDAO.updtS20Rec(starMaintenanceResponse, so, detailsInput);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputS20(starMaintenanceResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutputS20(
			MaintanenceResponse<S20RecManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<S20RecManOutput>>(starMaintenanceResponse) {
		};
	}

	@POST
	@Secured
	@Path("/updt-S52rec")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addS52Rec(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		MaintanenceResponse<S52RecManOutput> starMaintenanceResponse = new MaintanenceResponse<S52RecManOutput>();

		starMaintenanceResponse.setOutput(new S52RecManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		S52RecInput detailsInput = new S52RecInput();
		detailsInput.setS52CmpyId(cmpyId);
		SalesOrderDAO slsOdrDAO = new SalesOrderDAO();

		slsOdrDAO.updtS52Rec(starMaintenanceResponse, so, detailsInput);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputS52(starMaintenanceResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutputS52(
			MaintanenceResponse<S52RecManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<S52RecManOutput>>(starMaintenanceResponse) {
		};
	}

	@POST
	@Secured
	@Path("/updt-TI00rec")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addTI00Rec(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		MaintanenceResponse<TI00RecManOutput> starMaintenanceResponse = new MaintanenceResponse<TI00RecManOutput>();

		starMaintenanceResponse.setOutput(new TI00RecManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		TI00RecInput detailsInput = new TI00RecInput();
		detailsInput.setI00CmpyId(cmpyId);
		detailsInput.setUserId(userId);
		SalesOrderDAO slsOdrDAO = new SalesOrderDAO();

		slsOdrDAO.updtTI00Rec(starMaintenanceResponse, so, detailsInput);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputTI00(starMaintenanceResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutputTI00(
			MaintanenceResponse<TI00RecManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<TI00RecManOutput>>(starMaintenanceResponse) {
		};
	}

	@POST
	@Path("/read-so")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getSo(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {
		BrowseResponse<SalesOrderBrowseOutput> starBrowseResponse = new BrowseResponse<SalesOrderBrowseOutput>();

		starBrowseResponse.setOutput(new SalesOrderBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		String cusId = jsonObject.get("cusId").getAsString();
		
		SalesOrderDAO SalesOrderDAO = new SalesOrderDAO();
		
		SalesOrderDAO.readSo(starBrowseResponse, cusId, cmpyId);
		
		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
	}

	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<SalesOrderBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<SalesOrderBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Path("/read-soitm")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getSoItem(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {
		BrowseResponse<SOItemBrowseOutput> starBrowseResponse = new BrowseResponse<SOItemBrowseOutput>();
		starBrowseResponse.setOutput(new SOItemBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		int ordNo = 0;
		SalesOrderInfo input = new SalesOrderInfo();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		ordNo = jsonObject.get("ordNo").getAsInt();
		input.setCmpnyId(cmpyId);
		SalesOrderDAO SalesOrderDAO = new SalesOrderDAO();
		SalesOrderDAO.readSoItem(starBrowseResponse, cmpyId, ordNo);

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputitm(starBrowseResponse));
	}

	private GenericEntity<?> getGenericBrowseOutputitm(BrowseResponse<SOItemBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<SOItemBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Path("/read-odrinfo")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getodrInfo(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {
		BrowseResponse<OdrInfoBrowseOutput> starBrowseResponse = new BrowseResponse<OdrInfoBrowseOutput>();
		starBrowseResponse.setOutput(new OdrInfoBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		String refPfx = "";
		int refNo = 0;
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		refPfx = jsonObject.get("REF_PFX").getAsString();
		refNo = jsonObject.get("REF_NO").getAsInt();

		SalesOrderDAO SalesOrderDAO = new SalesOrderDAO();
		SalesOrderDAO.readodrInfo(starBrowseResponse, refPfx, refNo);

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputodrinfo(starBrowseResponse));
	}

	private GenericEntity<?> getGenericBrowseOutputodrinfo(BrowseResponse<OdrInfoBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<OdrInfoBrowseOutput>>(starBrowseResponse) {
		};
	}
}
