package com.star.services;

import java.io.File;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.SavedDocsDAO;
import com.star.linkage.saveddocs.SavedDocsBrowseOutput;
import com.star.linkage.saveddocs.SavedDocsManOutput;

@Path("/savedDocs")
public class SavedDocsService {
	@POST
	@Secured
	@Path("/read")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getSavedDocs(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		BrowseResponse<SavedDocsBrowseOutput> starBrowseResponse = new BrowseResponse<SavedDocsBrowseOutput>();
		starBrowseResponse.setOutput(new SavedDocsBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		SavedDocsDAO savedDocsDAO = new SavedDocsDAO();

		savedDocsDAO.getSavedDocs(starBrowseResponse);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblSavedDocs.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputSavedDocs(starBrowseResponse));

	}

	private GenericEntity<?> getGenericBrowseOutputSavedDocs(BrowseResponse<SavedDocsBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<SavedDocsBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	@POST
	@Secured
	@Path("/delete")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response deleteSavedDocs(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		MaintanenceResponse<SavedDocsManOutput> starManResponse = new MaintanenceResponse<SavedDocsManOutput>();
		starManResponse.setOutput(new SavedDocsManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		Integer docId = jsonObject.get("docId").getAsInt();
		String docNm = jsonObject.get("docNm").getAsString();
		String docOrgNm = jsonObject.get("docOrgNm").getAsString();
		
		String filePath = CommonConstants.ROOT_DIR + docNm;

		SavedDocsDAO savedDocsDAO = new SavedDocsDAO();

		savedDocsDAO.deleteSavedDocs(starManResponse, docId);
		
		if( starManResponse.output.rtnSts == 0 ) {
			File myFile = new File(filePath);
	        if (myFile.delete()) {
	            System.out.println("Deleted the file: " + myFile.getName());
	        } else {
	            System.out.println("Failed to delete the file.");
	        }
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintanenceOutput(starManResponse));

	}

	private GenericEntity<?> getGenericMaintanenceOutput(MaintanenceResponse<SavedDocsManOutput> starManResponse) {
		return new GenericEntity<MaintanenceResponse<SavedDocsManOutput>>(starManResponse) {
		};
	}

}
