package com.star.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.glassfish.jersey.media.multipart.BodyPart;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.ManageDirectory;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.common.ValidateValue;
import com.star.common.abbyy.PerformOCR;
import com.star.dao.CstmDocInfoDAO;
import com.star.dao.ExcelDAO;
import com.star.linkage.cstmdocinfo.CstmDocInfoBrowseOutput;
import com.star.linkage.cstmdocinfo.CstmDocInfoManOutput;
import com.star.linkage.cstmdocinfo.DocumentHeader;
import com.star.linkage.cstmdocinfo.GetReconBrowse;
import com.star.linkage.cstmdocinfo.ValidateInvoiceBrowse;
import com.star.linkage.cstmdocinfo.ValidatePOBrowse;
import com.star.linkage.cstmdocinfo.VchrInfo;
import com.star.linkage.cstmorddocs.CstmOrdDocsInput;
import com.star.linkage.cstmorddocs.CstmOrdDocsManOutput;
import com.star.linkage.excel.Excel;
import com.star.linkage.excel.ExcelBrowseOutput;
import com.star.linkage.excel.ProductInfo;
import com.star.linkage.excel.ProductSpecs;

@Path("/cstmdoc")
public class CstmDocInfoService {

	CommonFunctions cmn = new CommonFunctions();
	/*
	 * String[] slsOrdAllowCols = {"CUSTOMER", "PODATE", "PO_NUMBER", "CUST_ITEM",
	 * "LINE_ITEM", "SHIP_TO_1", "SHIP_TO_2", "SHIP_TO_3", "DESCRIPTN", "DELIVERY",
	 * "QUANTITY", "UOM", "UNIT_PRICE", "TYPE", "SALES_CAT", "TKN_SLSPSN",
	 * "REF_BRH", "FRM", "GRD", "SIZE", "FNSH", "WIDTH", "LENGTH", "SOURCE",
	 * "IS_SLSPSN", "OS_SLSPSN", "WRK_ORD_DT", "FRT_CHRG", "FRT_COST", "STATUS",
	 * "SHP_BRH", "VENDOR", "FOB_PT", "AMOUNT", "HEADER_REMARK", "TRM_TRD",
	 * "DLVY_MTHD", "FRT_COST_UM", "FRT_CHRG_UM"};
	 */
	String[] slsOrdAllowCols = { "CUSTOMER", "PODATE", "PO_NUMBER", "CUST_ITEM", "LINE_ITEM", "SHIP_TO", "DESCRIPTN",
			"DELIVERY", "PCS", "WEIGHT", "UOM", "UNIT_PRICE", "PRICE_UOM", "SALES_CAT", "PRS_RTE", "TKN_SLSPSN", "REF_BRH",
			"FRM", "GRD", "SIZE", "FNSH", "WIDTH", "LENGTH", "GA_SIZE", "GA_TYPE", "SOURCE", "IS_SLSPSN", "OS_SLSPSN",
			"FRT_CHRG", "FRT_COST", "VENDOR", "FOB_PT", "HEADER_REMARK", "TRM_TRD", "DLVY_MTHD", "FRT_UOM", "ROUTE",
			"OD", "LENGTH_TYP", "SRC_WHS", "MEMO_PART", "ID", "OD", "METAL_STD" };
	Integer glblCusId = 0;

	@POST
	@Secured
	@Path("/readdoc")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getDocument(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			@HeaderParam("user-grp") String userGrp, String data) throws Exception {

		List<DocumentHeader> tblHeaderList = new ArrayList<DocumentHeader>();

		BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse = new BrowseResponse<CstmDocInfoBrowseOutput>();

		starBrowseResponse.setOutput(new CstmDocInfoBrowseOutput());

		try {
			CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

			JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

			String trnSts = jsonObject.get("trnSts").getAsString();
			String year = jsonObject.get("year").getAsString();

			int pageNo = 0;

			if (jsonObject.has("pageNo")) {
				pageNo = Integer.parseInt(jsonObject.get("pageNo").getAsString());
			}

			if (!trnSts.equals("H")) {
				docInfoDAO.readStxFields(starBrowseResponse, trnSts, pageNo, cmpyId, year);
			}

			if (trnSts.length() == 0 && pageNo == 0) {
				docInfoDAO.readFields(starBrowseResponse, trnSts, userId, userGrp, year);
			}

			if (trnSts.equals("H")) {
				docInfoDAO.readFields(starBrowseResponse, trnSts, userId, userGrp, year);
			}

			docInfoDAO.readCounts(starBrowseResponse, userId, cmpyId, year);
			docInfoDAO.readCountDocument(starBrowseResponse, userId, userGrp, year);

			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

			starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblDoc.size();

			starBrowseResponse.output.fldTblHdrList = tblHeaderList;

			if (starBrowseResponse.output.toReviewCount > pageNo * 100) {
				starBrowseResponse.output.eof = 0;
			} else {
				starBrowseResponse.output.eof = 1;
			}

			return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));

		} catch (Exception e) {
			throw e;
		}

	}

	@POST
	@Secured
	@Path("/viewdoc")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response veiwRecord(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		List<DocumentHeader> tblHeaderList = new ArrayList<DocumentHeader>();

		BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse = new BrowseResponse<CstmDocInfoBrowseOutput>();

		starBrowseResponse.setOutput(new CstmDocInfoBrowseOutput());

		try {
			CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

			JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

			int ctlNo = jsonObject.get("ctlNo").getAsInt();

			docInfoDAO.viewRecord(starBrowseResponse, ctlNo, cmpyId);

			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

			starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblDoc.size();

			// starBrowseResponse.output.fldTblHdrList = tblHeaderList;
			starBrowseResponse.output.fldTblHdrList = tblHeaderList;
			return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));

		} catch (Exception e) {
			throw e;
		}

	}

	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<CstmDocInfoBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@RolesAllowed("ADMIN")
	@Path("/adddoc")
	@Consumes({ MediaType.MULTIPART_FORM_DATA })
	public Response uploadMultiplePdfFiles(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			@FormDataParam("file") FormDataBodyPart body, @FormDataParam("header") String header) throws Exception {
		// TODO ,@HeaderParam("user-id") String userId
		ManageDirectory directory = new ManageDirectory();

		BrowseResponse<ExcelBrowseOutput> starBrowseResponse = new BrowseResponse<ExcelBrowseOutput>();
		starBrowseResponse.setOutput(new ExcelBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		int fileCount = 0;

		ValidateValue validateValue = new ValidateValue();

		for (BodyPart part : body.getParent().getBodyParts()) {

			System.out.println(part.getContentDisposition().getParameters().size());

			InputStream is = part.getEntityAs(InputStream.class);
			ContentDisposition meta = part.getContentDisposition();

			if (meta.getFileName() != null) {

				/* Calling Doc Sequence to generate unique Doc Name */
				CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();

				int docId = cstmDocInfoDAO.getDocId();

				String strDocId = String.valueOf(docId);

				String fileName = "0000000000".substring(strDocId.length()) + strDocId;

				String docNmIntrnl = "upld-sls-ord-doc" + "-" + fileName;

				String fileExtn = validateValue.getFileExtension(meta.getFileName());

				/* CREATE FILE BASED ON THE INPUT */
				boolean flg = directory.createFile(docNmIntrnl, fileExtn, is);

				if (flg) {
					String filePath = CommonConstants.ROOT_DIR + docNmIntrnl + "." + fileExtn;

					readExcelFile(starBrowseResponse, filePath, header, cmpyId);

					if (starBrowseResponse.output.rtnSts == 0) {
						MaintanenceResponse<CstmOrdDocsManOutput> starMaintenanceDocResponse = new MaintanenceResponse<CstmOrdDocsManOutput>();

						starMaintenanceDocResponse.setOutput(new CstmOrdDocsManOutput());

						CstmOrdDocsInput cstmOrdDocsInput = new CstmOrdDocsInput();

						cstmOrdDocsInput.setDocId(docId);
						cstmOrdDocsInput.setDocName(docNmIntrnl + "." + fileExtn);
						cstmOrdDocsInput.setDocOrigName(meta.getFileName());
						cstmOrdDocsInput.setUpldBy(userId);

						cstmDocInfoDAO.addOrderDocument(starMaintenanceDocResponse, cstmOrdDocsInput);

						starBrowseResponse.output.docId = docId;
					}
				} else {
					starBrowseResponse.output.rtnSts = 1;
					starBrowseResponse.output.messages.add(CommonConstants.SOMETHING_WENT_WRONG);
				}
			}

		}

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputExcel(starBrowseResponse));

		// System.out.println(fileCount);

		// return Response.ok("Data uploaded successfully !!").build();
	}

	private GenericEntity<?> getGenericBrowseOutputExcel(BrowseResponse<ExcelBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<ExcelBrowseOutput>>(starBrowseResponse) {
		};
	}

	public String validateCellVal(String colTitle, String colVal, ProductInfo prdInfo, String cmpyId) {
		ExcelDAO excelDao = new ExcelDAO();

		if (colTitle.equalsIgnoreCase("CUSTOMER")) {
			if (cmn.isInteger(colVal)) {
				glblCusId = Integer.parseInt(colVal);
			} else {
				if (colVal.trim().length() > 0) {
					colVal = excelDao.getCusIdByName(colVal);

					if (colVal.trim().length() > 0) {
						glblCusId = Integer.parseInt(colVal);
					}
				}
			}
		} else if (colTitle.equalsIgnoreCase("SHIP_TO") || colTitle.equalsIgnoreCase("SHIP_TO_1")
				|| colTitle.equalsIgnoreCase("SHIP_TO_2") || colTitle.equalsIgnoreCase("SHIP_TO_3")) {
			if (cmn.isInteger(colVal)) {
				colVal = excelDao.getShpToByCusId(glblCusId, colVal);
			} else {
				if (colVal.trim().length() > 0) {
					colVal = excelDao.getShpToByName(glblCusId, colVal);
				}
			}
		} else if (colTitle.equalsIgnoreCase("PODATE")) {
			// System.out.print("\nPODATE: "+colVal);
		} else if (colTitle.equalsIgnoreCase("SALES_CAT")) {
			if (prdInfo.getFlg() == 1) {
				colVal = prdInfo.getSlsCat();
			} else if (colVal.trim().length() > 2) {
				colVal = excelDao.getSalesCategoryByName(colVal);
			}
		} else if (colTitle.equalsIgnoreCase("TKN_SLSPSN") || colTitle.equalsIgnoreCase("IS_SLSPSN")
				|| colTitle.equalsIgnoreCase("OS_SLSPSN")) {
			if (colVal.trim().length() > 0) {
				colVal = excelDao.getSalesPersonByName(colVal);
			}
		} else if (colTitle.equalsIgnoreCase("BRH") || colTitle.equalsIgnoreCase("REF_BRH")
				|| colTitle.equalsIgnoreCase("SHP_BRH")) {
			if (colVal.trim().length() > 0) {
				colVal = excelDao.getSalesBranchByName(colVal);
			}
		} else if (colTitle.equalsIgnoreCase("QUANTITY") || colTitle.equalsIgnoreCase("QTY")) {
			if (colVal == null || colVal.trim().length() <= 0) {
				colVal = "0";
			}
			Float price = Float.parseFloat(colVal);
			DecimalFormat df = new DecimalFormat("0.000");
			df.setMaximumFractionDigits(3);
			colVal = df.format(price);
		} else if (colTitle.equalsIgnoreCase("CPS") || colTitle.equalsIgnoreCase("CUST_ITEM")) {
			if(colVal.trim().length() > 0)
			{
				colVal = excelDao.getCPSInfo(colVal, prdInfo, cmpyId);
			}
		} else if (colTitle.equalsIgnoreCase("FRM")) {
			if (prdInfo.getFlg() == 1) {
				colVal = prdInfo.getFrm();
			}
		} else if (colTitle.equalsIgnoreCase("GRD")) {
			if (prdInfo.getFlg() == 1) {
				colVal = prdInfo.getGrd();
			}
		} else if (colTitle.equalsIgnoreCase("SIZE")) {
			if (prdInfo.getFlg() == 1) {
				colVal = prdInfo.getSize();
			}
		} else if (colTitle.equalsIgnoreCase("FNSH")) {
			if (prdInfo.getFlg() == 1) {
				colVal = prdInfo.getFnsh();
			}
		} else if (colTitle.equalsIgnoreCase("WIDTH") || colTitle.equalsIgnoreCase("WDTH")) {
			if (prdInfo.getFlg() == 1) {
				colVal = prdInfo.getWdth();
			}
		} else if (colTitle.equalsIgnoreCase("LENGTH") || colTitle.equalsIgnoreCase("LGTH")) {
			if (prdInfo.getFlg() == 1) {
				colVal = prdInfo.getLgth();
			}
		} else if (colTitle.equalsIgnoreCase("GA_SIZE") || colTitle.equalsIgnoreCase("GA_SIZE")) {
			if (prdInfo.getFlg() == 1) {
				colVal = prdInfo.getGaSize();
			}
		} else if (colTitle.equalsIgnoreCase("GA_TYPE") || colTitle.equalsIgnoreCase("GA_TYPE")) {
			if (prdInfo.getFlg() == 1) {
				colVal = prdInfo.getGaType();
			}
		} else if (colTitle.equalsIgnoreCase("SOURCE") || colTitle.equalsIgnoreCase("SRC")) {
			if (prdInfo.getFlg() == 1) {
				colVal = prdInfo.getSrc();
			}
		} else if (colTitle.equalsIgnoreCase("ADMIN_BRH")) {
			if (prdInfo.getFlg() == 1) {
				colVal = prdInfo.getAdminBrh();
			}
		}  else if (colTitle.equalsIgnoreCase("VENDOR")) {
			if (!cmn.isInteger(colVal) && colVal.trim().length() > 0) {
				colVal = excelDao.getVenIdByName(colVal);
			}
		}

		return colVal;
	}

	public void readExcelFile(BrowseResponse<ExcelBrowseOutput> starBrowseResponse, String filePath, String header,
			String cmpyId) throws Exception {
		ExcelDAO excelDao = new ExcelDAO();
		// obtaining input bytes from a file
		FileInputStream fis = new FileInputStream(new File(filePath));
		// creating workbook instance that refers to .xls file
		// HSSFWorkbook wb = new HSSFWorkbook(fis);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		// creating a Sheet object to retrieve the object
		// HSSFSheet sheet = wb.getSheetAt(0);
		XSSFSheet sheet = wb.getSheetAt(0);
		int rowCnt = 0;
		int colCnt = 0;
		String colVal = "";
		String colTitle = "";
		Boolean errFlg = false;
		ArrayList<String> colHdrArr = new ArrayList<String>();
		// evaluating cell type
		FormulaEvaluator formulaEvaluator = wb.getCreationHelper().createFormulaEvaluator();
		for (Row row : sheet) // iteration over row using for each loop
		{
			Excel output = new Excel();
			colCnt = 1;
			ProductInfo prdInfo = new ProductInfo();
			Cell cell;

			int loopLimit = 0;

			if (colHdrArr.size() > 0) {
				loopLimit = colHdrArr.size();
			} else {
				loopLimit = row.getLastCellNum();
			}
			
			String tmpFrm = "", tmpGrd = "", tmpSize = "", tmpPrsRte = "";
			
			// for(Cell cell: row){ //iteration over cell using for each loop
			for (int i = 0; i < loopLimit; i++) {

				cell = row.getCell(i, Row.CREATE_NULL_AS_BLANK);

				if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC && DateUtil.isCellDateFormatted(cell)) {
					SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
					colVal = dateFormat.format(cell.getDateCellValue());
				} else if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
					colVal = "";
				} else {

					cell.setCellType(Cell.CELL_TYPE_STRING);

					DataFormatter formatter = new DataFormatter(); // creating formatter using the default locale
					colVal = formatter.formatCellValue(cell);

					/*
					 * System.out.println("TYPE---->" + cell.getCellType());
					 * System.out.println("VALUE---->" + cell.getStringCellValue()); colVal =
					 * cell.getStringCellValue();
					 */
				}

				// if(header.equalsIgnoreCase("true") && rowCnt == 0) {
				if (rowCnt == 0) {
					if (Arrays.asList(slsOrdAllowCols).contains(colVal)) {
						colHdrArr.add(colVal);
					} else {
						errFlg = true;
						starBrowseResponse.output.rtnSts = 1;
						starBrowseResponse.output.messages.add("<strong>" + colVal
								+ "</strong> column not allowed in this application, please download sample file for reference.");
						break;
					}
				} else {
					if ((colCnt - 1) >= colHdrArr.size()) {
						colTitle = "";
					} else {
						colTitle = colHdrArr.get(colCnt - 1);
					}

					
					
					colVal = validateCellVal(colTitle, colVal, prdInfo, cmpyId);
					
					if (colCnt == 14) {
						tmpPrsRte = colVal;
					} else if (colCnt == 20) {
						tmpFrm = colVal;
					} else if (colCnt == 21) {
						tmpGrd = colVal;
					} else if (colCnt == 22) {
						tmpSize = colVal;
					}
					
					/* SET DEFAULT VALUES PROCESS ROUTE */
					if (colCnt == 15) {
						String dfltPrsRoute = excelDao.getPrsRoute(tmpPrsRte);
						colVal = dfltPrsRoute;
					}
					
					/* SET DEFAULT VALUES FOR OD BAR IF THE PRODUCT IS ROUND BAR */
					if (colCnt == 27) {
						ProductSpecs productSpecs = excelDao.getRoundBarOd(tmpFrm, tmpGrd, tmpSize);
						colVal = productSpecs.geteOdDflt();
					}

					if (colTitle.equalsIgnoreCase("PO_NUMBER")) {
						String poNo = excelDao.getStatusFromOrderInfo(output, starBrowseResponse.output.docId, rowCnt);

						if (poNo != "") {
							colVal = poNo;
						}
					}
				}

				if (colCnt == 1) {
					output.setField01(colVal);
				} else if (colCnt == 2) {
					output.setField02(colVal);
				} else if (colCnt == 3) {
					output.setField03(colVal);
				} else if (colCnt == 4) {
					output.setField04(colVal);
				} else if (colCnt == 5) {
					output.setField05(colVal);
				} else if (colCnt == 6) {
					output.setField06(colVal);
				} else if (colCnt == 7) {
					output.setField07(colVal);
				} else if (colCnt == 8) {
					output.setField08(colVal);
				} else if (colCnt == 9) {
					output.setField09(colVal);
				} else if (colCnt == 10) {
					output.setField10(colVal);
				} else if (colCnt == 11) {
					output.setField11(colVal);
				} else if (colCnt == 12) {
					output.setField12(colVal);
				} else if (colCnt == 13) {
					output.setField13(colVal);
				} else if (colCnt == 14) {
					output.setField14(colVal);
				} else if (colCnt == 15) {
					output.setField15(colVal);
				} else if (colCnt == 16) {
					output.setField16(colVal);
				} else if (colCnt == 17) {
					output.setField17(colVal);
				} else if (colCnt == 18) {
					output.setField18(colVal);
				} else if (colCnt == 19) {
					output.setField19(colVal);
				} else if (colCnt == 20) {
					output.setField20(colVal);
				} else if (colCnt == 21) {
					output.setField21(colVal);
				} else if (colCnt == 22) {
					output.setField22(colVal);
				} else if (colCnt == 23) {
					output.setField23(colVal);
				} else if (colCnt == 24) {
					output.setField24(colVal);
				} else if (colCnt == 25) {
					output.setField25(colVal);
				} else if (colCnt == 26) {
					output.setField26(colVal);
				} else if (colCnt == 27) {
					output.setField27(colVal);
				} else if (colCnt == 28) {
					output.setField28(colVal);
				} else if (colCnt == 29) {
					output.setField29(colVal);
				} else if (colCnt == 30) {
					output.setField30(colVal);
				} else if (colCnt == 31) {
					output.setField31(colVal);
				} else if (colCnt == 32) {
					output.setField32(colVal);
				} else if (colCnt == 33) {
					output.setField33(colVal);
				} else if (colCnt == 34) {
					output.setField34(colVal);
				} else if (colCnt == 35) {
					output.setField35(colVal);
				} else if (colCnt == 36) {
					output.setField36(colVal);
				} else if (colCnt == 37) {
					output.setField37(colVal);
				} else if (colCnt == 38) {
					output.setField38(colVal);
				} else if (colCnt == 39) {
					output.setField39(colVal);
				} else if (colCnt == 40) {
					output.setField40(colVal);
				} else if (colCnt == 41) {
					output.setField41(colVal);
				} else if (colCnt == 42) {
					output.setField42(colVal);
				} else if (colCnt == 43) {
					output.setField43(colVal);
				} else if (colCnt == 44) {
					output.setField44(colVal);
				} else if (colCnt == 45) {
					output.setField45(colVal);
				} else if (colCnt == 46) {
					output.setField46(colVal);
				}

				colCnt++;
			}

			if (errFlg) {
				break;
			}

			if (header.equalsIgnoreCase("true") && rowCnt == 0) {
				starBrowseResponse.output.fldTblExcelheader.add(output);
			} else {
				starBrowseResponse.output.fldTblExcelData.add(output);
			}

			rowCnt++;
		}
	}

	@POST
	@Path("/con")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getTxtFromPDF(String data) throws Exception {
		// JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		// String[] base64ImgString =
		// jsonObject.get("imgValue").getAsString().split(",");

		// String[] tmpStr = base64ImgString[0].split(";");

		// String[] imgExtn = tmpStr[0].split("/");

		// String imgFlPath = CommonConstants.ROOT_DIR + "OCR_" + (new Date()).getTime()
		// + "." + imgExtn[1];
		// String txtFlPath = CommonConstants.ROOT_DIR + "OCR_" + (new Date()).getTime()
		// + ".txt";

		String imgFlPath = CommonConstants.ROOT_DIR + "Test.pdf";
		String txtFlPath = CommonConstants.ROOT_DIR + "Test.txt";

		// byte[] base64DecodedByteArray = Base64.decodeBase64(base64ImgString[1]);

		// FileOutputStream imageOutFile = new FileOutputStream(imgFlPath);
		// imageOutFile.write(base64DecodedByteArray);
		// imageOutFile.close();
		// System.out.println("Image successfully encoded and decoded");

		PerformOCR performOCR = new PerformOCR();

		String[] inputArray = new String[3];

		inputArray[0] = "recognize";
		inputArray[1] = imgFlPath;
		inputArray[2] = txtFlPath;

		performOCR.performOcrOperation(inputArray);

		String returnStr = "";

		// returnStr = performOCR.performTessarect(inputArray);

		// returnStr = readFile(txtFlPath);

		// System.out.println(returnStr);

		return Response.status(200).entity(returnStr).build();

	}

	@POST
	@Secured
	@Path("/pdf")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getPdf(@HeaderParam("user-id") String userId, String data) throws Exception {

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		int ctlNo = jsonObject.get("ctlNo").getAsInt();

		CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();

		String PdfLocation = cstmDocInfoDAO.getPdf(ctlNo);

		return Response.status(200).entity(PdfLocation).build();
	}

	@GET
	@Path("/download")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadPdfFile(@HeaderParam("user-id") String userId, @QueryParam("docId") int docId)
			throws Exception {

		ManageDirectory directory = new ManageDirectory();

		/*
		 * BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse = new
		 * BrowseResponse<CstmDocInfoBrowseOutput>();
		 * 
		 * starBrowseResponse.setOutput(new CstmDocInfoBrowseOutput());
		 */

		/*
		 * JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		 * 
		 * int ctlNo = jsonObject.get("ctlNo").getAsInt();
		 */

		CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

		List<Object[]> lst = docInfoDAO.getDocName(docId);

		String fileName = "", origName = "";

		for (Object[] cstmVal : lst) {
			fileName = String.valueOf(cstmVal[0]);
			origName = String.valueOf(cstmVal[1]);
		}

		InputStream fileInputStream;
		javax.ws.rs.core.Response.ResponseBuilder responseBuilder = null;

		fileInputStream = directory.downloadDoc(fileName);

		responseBuilder = javax.ws.rs.core.Response.ok((Object) fileInputStream);

		responseBuilder.type("application/xlsx");
		responseBuilder.header("Content-Disposition", "attachment;filename=" + origName);

		return responseBuilder.build();
	}

	@GET
	@Path("/downloadget")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getDownloadPdfFile(@HeaderParam("user-id") String userId, @QueryParam("ctlNo") int ctlNo)
			throws Exception {

		ManageDirectory directory = new ManageDirectory();

		CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

		List<Object[]> lst = docInfoDAO.getDocName(ctlNo);

		String fileName = "", origName = "";

		for (Object[] cstmVal : lst) {

			fileName = String.valueOf(cstmVal[0]);
			origName = String.valueOf(cstmVal[1]);
		}

		InputStream fileInputStream;
		javax.ws.rs.core.Response.ResponseBuilder responseBuilder = null;

		fileInputStream = directory.downloadDoc(fileName);

		responseBuilder = javax.ws.rs.core.Response.ok((Object) fileInputStream);

		responseBuilder.type("application/pdf");
		responseBuilder.header("Content-Disposition", "inline;filename=" + origName);

		return responseBuilder.build();

	}

	@POST
	@Path("/upddoc")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response updateVoucherInfo(@HeaderParam("user-id") String userId, String data) throws Exception {
		MaintanenceResponse<CstmDocInfoManOutput> starMaintenanceResponse = new MaintanenceResponse<CstmDocInfoManOutput>();

		starMaintenanceResponse.setOutput(new CstmDocInfoManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		// DocInfo detailsInput = new Gson().fromJson(data, DocInfo.class);

		VchrInfo detailsInput = new VchrInfo();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		CommonFunctions objCom = new CommonFunctions();

		detailsInput.setVchrCtlNo(jsonObject.get("vchrCtlNo").getAsInt());
		detailsInput.setVchrInvNo(jsonObject.get("vchrInvNo").getAsString());
		detailsInput.setVchrVenId(jsonObject.get("vchrVenId").getAsString());
		detailsInput.setVchrVenNm(jsonObject.get("vchrVenNm").getAsString());
		detailsInput.setVchrBrh(jsonObject.get("vchrBrh").getAsString());
		detailsInput.setVchrAmt(Double.parseDouble(jsonObject.get("vchrAmt").getAsString()));
		detailsInput.setVchrExtRef(jsonObject.get("vchrExtRef").getAsString());
		detailsInput.setVchrInvDt(objCom.formatDateWithoutTime(jsonObject.get("vchrInvDt").getAsString()));
		detailsInput.setVchrDueDt(objCom.formatDateWithoutTime(jsonObject.get("vchrDueDt").getAsString()));
		detailsInput.setVchrPayTerm(jsonObject.get("vchrPayTerm").getAsString());
		detailsInput.setVchrCry(jsonObject.get("vchrCry").getAsString());

		CstmDocInfoDAO cstmDocDAO = new CstmDocInfoDAO();

		JsonArray reconList = jsonObject.get("costRecon").getAsJsonArray();

		JsonArray glList = jsonObject.get("glData").getAsJsonArray();

		cstmDocDAO.updDocDtls(starMaintenanceResponse, detailsInput, reconList, glList);

		System.out.println(detailsInput.getVchrCtlNo());
		System.out.println(detailsInput.getVchrVenId());

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starMaintenanceResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutput(MaintanenceResponse<CstmDocInfoManOutput> starBrowseResponse) {
		return new GenericEntity<MaintanenceResponse<CstmDocInfoManOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/prsdoc")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response prsOrPaidRecord(@HeaderParam("user-id") String userId, String data) throws Exception {

		List<DocumentHeader> tblHeaderList = new ArrayList<DocumentHeader>();

		BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse = new BrowseResponse<CstmDocInfoBrowseOutput>();

		starBrowseResponse.setOutput(new CstmDocInfoBrowseOutput());

		try {
			CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

			JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

			Boolean isPrs = jsonObject.get("isPrs").getAsBoolean();

			docInfoDAO.isPrsOrPaidFields(starBrowseResponse, isPrs);

			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

			starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblDoc.size();

			// starBrowseResponse.output.fldTblHdrList = tblHeaderList;
			starBrowseResponse.output.fldTblHdrList = tblHeaderList;
			return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));

		} catch (Exception e) {
			throw e;
		}

	}

	@POST
	@Secured
	@Path("/deldoc")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response deleletDoc(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<CstmDocInfoManOutput> starManResponse = new MaintanenceResponse<CstmDocInfoManOutput>();

		starManResponse.setOutput(new CstmDocInfoManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		int ctlNo = jsonObject.get("ctlNo").getAsInt();

		CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();

		cstmDocInfoDAO.deleteDoc(starManResponse, ctlNo);

		return starResponseBuilder.getSuccessResponse(getGenericManOutput(starManResponse));

	}

	@POST
	@Secured
	@Path("/rejdoc")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response rejectDoc(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<CstmDocInfoManOutput> starManResponse = new MaintanenceResponse<CstmDocInfoManOutput>();

		starManResponse.setOutput(new CstmDocInfoManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		int ctlNo = jsonObject.get("ctlNo").getAsInt();
		String trnRmk = jsonObject.get("trnRmk").getAsString();

		CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();

		cstmDocInfoDAO.rejectDoc(starManResponse, ctlNo, trnRmk);

		return starResponseBuilder.getSuccessResponse(getGenericManOutput(starManResponse));

	}

	private GenericEntity<?> getGenericManOutput(MaintanenceResponse<CstmDocInfoManOutput> starBrowseResponse) {
		return new GenericEntity<MaintanenceResponse<CstmDocInfoManOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/validate-inv")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response validateInv(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<ValidateInvoiceBrowse> starBrowseResponse = new BrowseResponse<ValidateInvoiceBrowse>();

		starBrowseResponse.setOutput(new ValidateInvoiceBrowse());

		try {
			CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

			JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

			String venId = jsonObject.get("venId").getAsString();
			String invNo = jsonObject.get("invNo").getAsString();

			String vchrNo = docInfoDAO.validateInvoice(venId, invNo);

			if (vchrNo.length() > 0) {
				starBrowseResponse.output.vchrRefNo = vchrNo;
				starBrowseResponse.output.rtnSts = 1;
			}

			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

			return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputValidInv(starBrowseResponse));

		} catch (Exception e) {
			throw e;
		}

	}

	@POST
	@Secured
	@Path("/validate-po")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response validatePO(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<ValidatePOBrowse> starBrowseResponse = new BrowseResponse<ValidatePOBrowse>();

		starBrowseResponse.setOutput(new ValidatePOBrowse());

		try {
			CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

			JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

			String poNo = jsonObject.get("poNo").getAsString();
			String poItm = jsonObject.get("poItm").getAsString();

			String vendorId = docInfoDAO.validatePO(poNo, poItm);

			if (vendorId.length() > 0) {
				starBrowseResponse.output.venId = vendorId;
				starBrowseResponse.output.rtnSts = 0;
			} else {
				starBrowseResponse.output.rtnSts = 1;
			}

			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

			return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputValidPO(starBrowseResponse));

		} catch (Exception e) {
			throw e;
		}

	}

	@POST
	@Secured
	@Path("/get-recon-data")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getReconData(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<GetReconBrowse> starBrowseResponse = new BrowseResponse<GetReconBrowse>();

		starBrowseResponse.setOutput(new GetReconBrowse());

		try {
			CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

			JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

			String venId = jsonObject.get("venId").getAsString();
			String poNo = jsonObject.get("poNo").getAsString();
			String cry = jsonObject.get("cry").getAsString();
			String exRef = jsonObject.get("exRef").getAsString();

			VchrInfo vchrInfo = new VchrInfo();

			docInfoDAO.getReconDetails(cmpyId, venId, poNo, cry, exRef, vchrInfo);

			starBrowseResponse.output.reconAmt = vchrInfo.getReconAmt();
			starBrowseResponse.output.reconCount = vchrInfo.getReconCount();
			starBrowseResponse.output.reconAmtStr = vchrInfo.getReconAmtStr();

			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

			return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputRecon(starBrowseResponse));

		} catch (Exception e) {
			throw e;
		}

	}

	private GenericEntity<?> getGenericBrowseOutputRecon(BrowseResponse<GetReconBrowse> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<GetReconBrowse>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputValidInv(BrowseResponse<ValidateInvoiceBrowse> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<ValidateInvoiceBrowse>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputValidPO(BrowseResponse<ValidatePOBrowse> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<ValidatePOBrowse>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/updSts")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response updateDocStatus(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<CstmDocInfoManOutput> starManResponse = new MaintanenceResponse<CstmDocInfoManOutput>();

		starManResponse.setOutput(new CstmDocInfoManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		int ctlNo = jsonObject.get("ctlNo").getAsInt();
		String rmk = jsonObject.get("rmk").getAsString();
		String trnSts = jsonObject.get("trnSts").getAsString();

		CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();

		cstmDocInfoDAO.updateDocStatus(starManResponse, ctlNo, trnSts, rmk);

		return starResponseBuilder.getSuccessResponse(getGenericManOutput(starManResponse));

	}

	@POST
	@Secured
	@Path("/read-saved-excel")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response readSavedExcel(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		ManageDirectory directory = new ManageDirectory();

		BrowseResponse<ExcelBrowseOutput> starBrowseResponse = new BrowseResponse<ExcelBrowseOutput>();
		starBrowseResponse.setOutput(new ExcelBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		starBrowseResponse.output.docId = jsonObject.get("docId").getAsInt();
		String docNm = jsonObject.get("docNm").getAsString();

		String filePath = CommonConstants.ROOT_DIR + docNm;

		readExcelFile(starBrowseResponse, filePath, "true", cmpyId);

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputExcel(starBrowseResponse));
	}

}
