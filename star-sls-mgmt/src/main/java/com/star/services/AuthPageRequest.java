package com.star.services;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/auth-page")
public class AuthPageRequest {

	@POST
	  @Path("/valid")
	  public Response valid() throws Exception
	  {
		return Response.ok().build();
	  }
	
}
