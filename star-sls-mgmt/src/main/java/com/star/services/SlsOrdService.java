package com.star.services;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.ExcelDAO;
import com.star.dao.SlsOrdDAO;
import com.star.dao.ValidateDataDAO;
import com.star.linkage.excel.ProductInfo;
import com.star.linkage.slsord.SOSpecs;
import com.star.linkage.slsord.SlsOrdInfoOutput;
import com.star.linkage.slsord.SlsOrdInput;
import com.star.linkage.slsord.SlsOrdManOutput;
import com.star.linkage.slsord.SlsOrdOutput;

@Path("/slsOrd")
public class SlsOrdService {
	@POST
	@Secured
	@Path("/updt-CrtSo")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response crtSo(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {
		
		int trcCtlNo = 0;
		
		BrowseResponse<SlsOrdInfoOutput> starBrowseResponse = new BrowseResponse<SlsOrdInfoOutput>();
		starBrowseResponse.setOutput(new SlsOrdInfoOutput());
		
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		SlsOrdInput slsOrdInpt = new SlsOrdInput();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		JsonArray slsOrdDataMultiArr = jsonObject.get("slsOrdData").getAsJsonArray();

		for (int i = 0; i < slsOrdDataMultiArr.size(); i++) {
			JsonArray slsOrdDataArr = slsOrdDataMultiArr.get(i).getAsJsonArray();
			
			for (int j = 0; j < slsOrdDataArr.size(); j++) {
				JsonObject slsOrdDataObj = slsOrdDataArr.get(j).getAsJsonObject();
				String typ = slsOrdDataObj.get("fldTyp").getAsString();
	
				if (typ.equalsIgnoreCase("REF_BRH")) {
					slsOrdInpt.setRefBrh(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("CUS_ID")) {
					slsOrdInpt.setCusId(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("SHP_TO")) {
					slsOrdInpt.setShpTo(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("IS_SLSPSN")) {
					slsOrdInpt.setIsSlp(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("OS_SLSPSN")) {
					slsOrdInpt.setOsSlp(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("TKN_SLP")) {
					slsOrdInpt.setTknSlp(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("ORD_TYP")) {
					slsOrdInpt.setOrdTyp(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("SLS_CAT")) {
					slsOrdInpt.setSlsCat(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("PO_NO")) {
					slsOrdInpt.setPoNo(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("PO_DT")) {
					slsOrdInpt.setPoDt(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("ORD_PCS")) {
					slsOrdInpt.setOrdPcs(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("ORD_MSR")) {
					slsOrdInpt.setOrdMsr(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("QTY")) {
					slsOrdInpt.setQty(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("UM")) {
					slsOrdInpt.setUm(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("SLS_CAT")) {
					slsOrdInpt.setSlsCat(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("SOURCE")) {
					slsOrdInpt.setSource(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("CUST_ITEM")) {
					slsOrdInpt.setCustItem(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("DEL_DT")) {
					slsOrdInpt.setDelDt(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("DOC_ID")) {
					if( slsOrdDataObj.get("fldVal").getAsString().trim().length() > 0 ) {
						slsOrdInpt.setDocId(slsOrdDataObj.get("fldVal").getAsInt());
					}
				}else if (typ.equalsIgnoreCase("DOC_ROW_NO")) {
					if( slsOrdDataObj.get("fldVal").getAsString().trim().length() > 0 ) {
						slsOrdInpt.setDocRowNo(slsOrdDataObj.get("fldVal").getAsInt());
					}
				}else if (typ.equalsIgnoreCase("UNIT_PRICE")) {
					if( slsOrdDataObj.get("fldVal").getAsString().trim().length() > 0 ) {
						slsOrdInpt.setUnitPrice(slsOrdDataObj.get("fldVal").getAsBigDecimal());
					}
				}else if (typ.equalsIgnoreCase("PRICE_UOM")) {
					slsOrdInpt.setPriceUm(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("FRT_CHRG")) {
					if( slsOrdDataObj.get("fldVal").getAsString().trim().length() > 0 ) {
						slsOrdInpt.setFrtChrg(slsOrdDataObj.get("fldVal").getAsBigDecimal());
					}
				}else if (typ.equalsIgnoreCase("FRT_COST")) {
					if( slsOrdDataObj.get("fldVal").getAsString().trim().length() > 0 ) {
						slsOrdInpt.setCstChrg(slsOrdDataObj.get("fldVal").getAsBigDecimal());
					}
				}else if (typ.equalsIgnoreCase("VENDOR")) {
					if( slsOrdDataObj.get("fldVal").getAsString().trim().length() > 0 ) {
						slsOrdInpt.setVenId(slsOrdDataObj.get("fldVal").getAsInt());
					}
				}else if (typ.equalsIgnoreCase("LINE_ITEM")) {
					if( slsOrdDataObj.get("fldVal").getAsString().trim().length() > 0 ) {
						slsOrdInpt.setItmNo(slsOrdDataObj.get("fldVal").getAsInt());
					}
				}else if (typ.equalsIgnoreCase("TRM_TRD")) {
					slsOrdInpt.setTrmTrd(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("DLVY_MTHD")) {
					slsOrdInpt.setDlvyMthd(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("EXRT")) {
					if( slsOrdDataObj.get("fldVal").getAsString().trim().length() > 0 ) {
						slsOrdInpt.setExrt(slsOrdDataObj.get("fldVal").getAsBigDecimal());
					}
				}else if (typ.equalsIgnoreCase("EXRT_TYP")) {
					slsOrdInpt.setExrtTyp(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("FRT_UOM")) {
					slsOrdInpt.setFrtUm(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("HEADER_REMARK")) {
					slsOrdInpt.setHdrRmk(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("ROUTE")) {
					slsOrdInpt.setStndrdRoute(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("FRM")) {
					slsOrdInpt.setFrm(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("GRD")) {
					slsOrdInpt.setGrd(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("SIZE")) {
					slsOrdInpt.setSize(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("FNSH")) {
					slsOrdInpt.setFnsh(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("WIDTH")) {
					slsOrdInpt.setWdth(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("LENGTH")) {
					slsOrdInpt.setLgth(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("GA_SIZE")) {
					slsOrdInpt.setGaSize(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("GA_TYPE")) {
					slsOrdInpt.setGaType(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("LENGTH_TYP")) {
					slsOrdInpt.setLgthType(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("PCS")) {
					slsOrdInpt.setOrdPcs(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("WEIGHT")) {
					slsOrdInpt.setOrdWgt(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("MEMO_PART")) {
					slsOrdInpt.setMemoPart(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("ID")) {
					slsOrdInpt.setIdia(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("OD")) {
					slsOrdInpt.setOdia(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("WHS")) {
					slsOrdInpt.setSrcWhs(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("PRS_RTE")) {
					slsOrdInpt.setStndrdRoute(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("FLW_UP_DAYS")) {
					slsOrdInpt.setFlwUpDays(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("EXPIRY_DAYS")) {
					slsOrdInpt.setExpiryDays(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("BUILD_UP")) {
					slsOrdInpt.setBuildUp(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("ORD_PFX")) {
					slsOrdInpt.setOrdPfx(slsOrdDataObj.get("fldVal").getAsString());
				}else if (typ.equalsIgnoreCase("METAL_STD")) {
					slsOrdInpt.setMetalStd(slsOrdDataObj.get("fldVal").getAsString());
				}
			}
			
			
			
			slsOrdInpt.setCmpyId(cmpyId);
			slsOrdInpt.setUsrId(userId);
			
			SlsOrdDAO slsOrdDAO = new SlsOrdDAO();
			int soNo = slsOrdDAO.getSlsOrdNoByPO(slsOrdInpt);
			
			if( slsOrdInpt.getIsSlp().length() == 0 || slsOrdInpt.getOsSlp().length() == 0 ) {
				slsOrdDAO.getSalesPersons(slsOrdInpt);
			}
			
			slsOrdInpt.setSoNo(soNo);
			
			slsOrdDAO.getCustRefData(slsOrdInpt);
			
			if(slsOrdInpt.getSrcWhs().length() == 0)
			{
				slsOrdDAO.getSrcWhs(slsOrdInpt);
			}
			slsOrdDAO.getPlngWhs(slsOrdInpt);
			slsOrdDAO.getShpgWhs(slsOrdInpt);
			
			
						
			trcCtlNo = slsOrdDAO.getmaxRec(slsOrdInpt.getCmpyId()) + 1;
			
			trcCtlNo = trcCtlNo + (slsOrdInpt.getDocRowNo() - 1);
			
			slsOrdInpt.setTrcCtlNo(trcCtlNo);

			MaintanenceResponse<SlsOrdManOutput> starMaintenanceResponse = new MaintanenceResponse<SlsOrdManOutput>();
			starMaintenanceResponse.setOutput(new SlsOrdManOutput());
			starMaintenanceResponse.output.docRowNo = slsOrdInpt.getDocRowNo();
			
			ValidateDataDAO validateDataDAO = new ValidateDataDAO();
			
			List<SOSpecs> soSpecsList = new ArrayList<SOSpecs>();
			int iErrFlg = 0;
			
			if(slsOrdInpt.getMetalStd().length() > 0)
			 {
				 String arr[] = slsOrdInpt.getMetalStd().split(",");
				 
				 for(int mtlCnt = 0; mtlCnt < arr.length; mtlCnt++)
				 {
					 String mtlStdVal = arr[mtlCnt].trim();
					 String arrVal[] = mtlStdVal.split(" ");
					 
					 String sdo = "";
					 String stdId = "";
					 String addId = "";
					 
					 if(arrVal.length > 1)
					 {
						 sdo = arrVal[0].trim();
						 stdId = mtlStdVal.replaceFirst(sdo, "");
					 }
					 
					 List<String> mtlStd = validateDataDAO.validateMetalStandard(sdo, stdId, addId);
					 
					 if(mtlStd.size()  < 1)
					 {
						 starMaintenanceResponse.output.stsCd = "E";
						 starMaintenanceResponse.output.messages.add("Metal Standard - " + mtlStdVal + " not available");
						 iErrFlg = 1;
					 }
					 else
					 {
						 SOSpecs soSpecs = new SOSpecs();
						 
						 if(mtlStd.size() > 0)
						 {
							 soSpecs.setSdo(mtlStd.get(0));
						 }
						 
						 if(mtlStd.size() > 1)
						 {
							 soSpecs.setStdId(mtlStd.get(1));
						 }
						 
						 if(mtlStd.size() > 2)
						 {
							 soSpecs.setAddId(mtlStd.get(2));
						 }
						 
						 if(!soSpecsList.contains(soSpecs))
						 {
							 soSpecsList.add(soSpecs);
						 }
						 
					 }
					 
				 }

			 }
			
			if(soSpecsList.size() > 0)
			{
				slsOrdInpt.setMtlStdList(soSpecsList);
			}
			
			
			int custPoRow = slsOrdDAO.getCustPoRow(slsOrdInpt);
			
			
			if(iErrFlg == 0) /* CHECK FOR ERRORS BEFORE MOVING TO NEXT STEP*/
			{
				if( custPoRow == 0 ) {
					if( slsOrdInpt.getCustItem() != null && slsOrdInpt.getCustItem().trim().length() > 0 ) {
						ExcelDAO excelDao = new ExcelDAO();
						ProductInfo prdInfo = new ProductInfo();
						
						excelDao.getCPSInfo(slsOrdInpt.getCustItem(), prdInfo, slsOrdInpt.getCmpyId());
						
						slsOrdInpt.setFrm( prdInfo.getFrm() );
						slsOrdInpt.setGrd( prdInfo.getGrd() );
						slsOrdInpt.setSize( prdInfo.getSize() );
						slsOrdInpt.setFnsh( prdInfo.getFnsh() );
						slsOrdInpt.setWdth( prdInfo.getWdth() );
						slsOrdInpt.setLgth( prdInfo.getLgth() );
						slsOrdInpt.setGaSize( prdInfo.getGaSize() );
						slsOrdInpt.setGaType( prdInfo.getGaType() );
					}
					
					slsOrdInpt.setPkgFlg("N");
					slsOrdDAO.getShpToPck(slsOrdInpt);
					
					slsOrdInpt.setTolFlg("N");
					slsOrdDAO.getShpToTolSpec(slsOrdInpt);
					
					slsOrdDAO.delInsrtdSOGTblData(slsOrdInpt);
					
					if( soNo == 0 ) {
						slsOrdDAO.getBasLng(slsOrdInpt);
						
						slsOrdDAO.createSalesOrder(starMaintenanceResponse, slsOrdInpt);
						
						System.out.println("\nCreating New Sales Order trcCtlNo is  ----: " + trcCtlNo);
					}else {				
						slsOrdDAO.addItemToSalesOrder(starMaintenanceResponse, slsOrdInpt);
						
						System.out.println("\nNeed to attach item in existing sales order No  ----: " + soNo + ", trcCtlNo is  ----: " + trcCtlNo);
					}
				}else {
					String errMsg = "E-PO number ("+ slsOrdInpt.getPoNo() +") is already processed in another file.";
					starMaintenanceResponse.output.stsCd = "E";
					starMaintenanceResponse.output.messages.add(errMsg);
					
					slsOrdDAO.addOrderInfo(starMaintenanceResponse, slsOrdInpt);
					slsOrdDAO.updtOdrInfoErr(starMaintenanceResponse, slsOrdInpt, errMsg);
				}
			}
			
			SlsOrdOutput slsOrdOutput = new SlsOrdOutput();
			
			slsOrdOutput.setSoNo(starMaintenanceResponse.output.soNo);
			slsOrdOutput.setDocRowNo(starMaintenanceResponse.output.docRowNo);
			slsOrdOutput.setOrdItmNo(starMaintenanceResponse.output.ordItmNo);
			slsOrdOutput.setStsCd(starMaintenanceResponse.output.stsCd);
			
			for(int y=0; y<starMaintenanceResponse.output.messages.size(); y++) {
				slsOrdOutput.setErrorMessage(starMaintenanceResponse.output.messages.get(y));
			}
			
			starBrowseResponse.output.fldTblSlsOrd.add( slsOrdOutput );
			
			starBrowseResponse.output.messages = starMaintenanceResponse.output.messages;
		}
		
		//return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputSlsOrd(starMaintenanceResponse));
		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputSlsOrd(starBrowseResponse));

	}

	private GenericEntity<?> getGenericBrowseOutputSlsOrd(
			BrowseResponse<SlsOrdInfoOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<SlsOrdInfoOutput>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericMaintenanceOutputSlsOrd(
			MaintanenceResponse<SlsOrdManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<SlsOrdManOutput>>(starMaintenanceResponse) {
		};
	}
	
	@POST
	@Secured
	@Path("/del-trcCtlNo")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response deleteRowByTrcCtlNo(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		MaintanenceResponse<SlsOrdManOutput> starMaintenanceResponse = new MaintanenceResponse<SlsOrdManOutput>();

		starMaintenanceResponse.setOutput(new SlsOrdManOutput());
		
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		SlsOrdInput slsOrdInpt = new SlsOrdInput();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		slsOrdInpt.setTrcCtlNo(jsonObject.get("trcCtlNo").getAsInt());
		slsOrdInpt.setCmpyId(cmpyId);
		slsOrdInpt.setCmpyId(cmpyId);
		slsOrdInpt.setUsrId(userId);
		
		SlsOrdDAO slsOrdDAO = new SlsOrdDAO();
		
		slsOrdDAO.delInsrtdSOGTblData(slsOrdInpt);
		
		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputSlsOrd(starMaintenanceResponse));

	}

}
