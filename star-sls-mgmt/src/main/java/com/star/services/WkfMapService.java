package com.star.services;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.joda.time.Days;
import org.joda.time.LocalDate;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.WkfMapDAO;
import com.star.linkage.cstmvchrinfo.CstmVchrInfoInput;
import com.star.linkage.wkf.WkfHisBrowseOutput;
import com.star.linkage.wkf.WkfMapManOutput;
import com.star.modal.CstmWkfMap;
import com.star.modal.WorkflowStep;

@Path("/wkf-map")
public class WkfMapService {

	public void validateVouWkf(String cmpyId, String usrId, CstmVchrInfoInput cstmVchrInfo) {

		int iAmtChk = 1;
		int iBrhChk = 1;
		int iVenChk = 1;
		int iDayChk = 1;

		WkfMapDAO dao = new WkfMapDAO();
		WorkflowStep stepFinal = null;

		List<WorkflowStep> listWorkflow = dao.getWorkflowDetails();

		for (int i = 0; i < listWorkflow.size(); i++) {

			iAmtChk = 1;
			iBrhChk = 1;
			iVenChk = 1;
			iDayChk = 1;

			if (cstmVchrInfo != null) {
				String brh = cstmVchrInfo.getVchrBrh();
				float amount = (float) cstmVchrInfo.getVchrAmt();
				String venId = cstmVchrInfo.getVchrVenId();
				Date invDt = null;
				Date dueDt = null;
				
				if(cstmVchrInfo.getVchrInvDt() != null)
				{
					invDt = cstmVchrInfo.getVchrInvDt();
				}
				
				if(cstmVchrInfo.getVchrDueDt() != null)
				{
					dueDt = cstmVchrInfo.getVchrDueDt();
				}

				if (listWorkflow.get(i).getAmtFlg() == 1) {

					iAmtChk = 0;
					if (amount > listWorkflow.get(i).getAmtFrm() && amount < listWorkflow.get(i).getAmtTo()) {
						iAmtChk = 1;
					}

				}

				if (listWorkflow.get(i).getVenFlg() == 1) {
					iVenChk = 0;
					if (listWorkflow.get(i).getVenId().contains(venId) && venId.length() > 0) {
						iVenChk = 1;
					}
				}

				if (listWorkflow.get(i).getBrhFlg() == 1) {
					iBrhChk = 0;
					if (listWorkflow.get(i).getBrh().contains(brh) && brh.length() > 0) {
						iBrhChk = 1;
					}
				}

				if (listWorkflow.get(i).getDayFlg() == 1) {

					iDayChk = 0;
					
					if(listWorkflow.get(i).getDayBef() != 0)
					{
						if(dueDt != null)
						{							
							Calendar calendarDueDt = toCalendar(dueDt);
							Calendar calendarCurDt = toCalendar(new Date());
							
							LocalDate d1 = new LocalDate(calendarDueDt.getTimeInMillis());
							LocalDate d2 = new LocalDate(calendarCurDt.getTimeInMillis());
							int days = Days.daysBetween(d2, d1).getDays();
							
							if(days <= listWorkflow.get(i).getDays())
							{
								iDayChk = 1;
							}
							
						}
					}
					else
					{
						if(invDt != null)
						{
							Calendar calendarInvDt = toCalendar(invDt);
							Calendar calendarCurDt = toCalendar(new Date());
							
							LocalDate d1 = new LocalDate(calendarInvDt.getTimeInMillis());
							LocalDate d2 = new LocalDate(calendarCurDt.getTimeInMillis());
							int days = Days.daysBetween(d1, d2).getDays();
							
							if(days >= listWorkflow.get(i).getDays())
							{
								iDayChk = 1;
							}
						}
					}
					
				}	

				if (iAmtChk == 1 && iBrhChk == 1 && iVenChk == 1 && iDayChk == 1) {
					stepFinal = listWorkflow.get(i);
				}

			}

		}

		if (stepFinal != null) {
			dao.addWkfMap(cstmVchrInfo.getVchrCtlNo(), (int) stepFinal.getWkfId(), stepFinal.getApvr());
		}

	}

	@POST
	@Secured
	@Path("/update")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response updateVouWkf(@HeaderParam("user-id") String usrId, @HeaderParam("cmpy-id") String cmpyId,
			String data) {

		MaintanenceResponse<WkfMapManOutput> starManResponse = new MaintanenceResponse<WkfMapManOutput>();

		starManResponse.setOutput(new WkfMapManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String asgnUser = jsonObject.get("asgnTo").getAsString();
		String wkfSts = jsonObject.get("sts").getAsString();
		String ctlNo = jsonObject.get("ctlNo").getAsString();
		String wkfId = jsonObject.get("wkfId").getAsString();
		String remark = jsonObject.get("rmk").getAsString();

		CstmWkfMap cstmWkfMap = new CstmWkfMap();

		cstmWkfMap.setCtlNo(Integer.parseInt(ctlNo));
		cstmWkfMap.setWkfId(Integer.parseInt(wkfId));
		cstmWkfMap.setWkfAsgnTo(asgnUser);
		cstmWkfMap.setWkfSts(wkfSts);
		cstmWkfMap.setWkfRmk(remark);

		WkfMapDAO wkfMapDAO = new WkfMapDAO();

		wkfMapDAO.updateWkfMap(starManResponse, cstmWkfMap);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputUpdate(starManResponse));

	}

	@POST
	@Secured
	@Path("/hist")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getWorkflowHistory(@HeaderParam("user-id") String usrId, @HeaderParam("cmpy-id") String cmpyId,
			String data) {

		BrowseResponse<WkfHisBrowseOutput> starBrowseResponse = new BrowseResponse<WkfHisBrowseOutput>();

		starBrowseResponse.setOutput(new WkfHisBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String wkfId = jsonObject.get("wkfId").getAsString();
		String ctlNo = jsonObject.get("ctlNo").getAsString();

		WkfMapDAO wkfMapDAO = new WkfMapDAO();

		wkfMapDAO.getWorkflowHistory(starBrowseResponse, wkfId, ctlNo);

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutputUpdate(
			MaintanenceResponse<WkfMapManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<WkfMapManOutput>>(starMaintenanceResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<WkfHisBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<WkfHisBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	
	public static Calendar toCalendar(Date date){ 
		  Calendar cal = Calendar.getInstance();
		  cal.setTime(date);
		  return cal;
		}

}
