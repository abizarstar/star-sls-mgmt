package com.star.services;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.MenuDAO;
import com.star.linkage.menu.MenuBrowseOutput;
import com.star.linkage.menu.MenuInput;
import com.star.linkage.menu.MenuManOutput;
import com.star.linkage.menu.SubMenuInput;

@Path("/menu")
public class MenuService {

	@POST
	@Secured
	@Path("/read")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getMenuDetails(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<MenuBrowseOutput> starBrowseResponse = new BrowseResponse<MenuBrowseOutput>();

		starBrowseResponse.setOutput(new MenuBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String grpId = jsonObject.get("grpId").getAsString();
		
		MenuDAO menuDao = new MenuDAO();
		
		menuDao.getMenubyGroup(starBrowseResponse, grpId);
		
		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));

	}
	
	@POST
	@Secured
	@Path("/read-all")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getAllMenu(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<MenuBrowseOutput> starBrowseResponse = new BrowseResponse<MenuBrowseOutput>();

		starBrowseResponse.setOutput(new MenuBrowseOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		MenuDAO menuDao = new MenuDAO();
		
		menuDao.getMenuAll(starBrowseResponse);
		
		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));

	}
	
	
	@POST
	@Secured
	@Path("/update")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response updateGroupMenu(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<MenuManOutput> starManResponse = new MaintanenceResponse<MenuManOutput>();

		starManResponse.setOutput(new MenuManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String grpId = jsonObject.get("grpId").getAsString();
		
		JsonArray jsonArrayMenu = jsonObject.get("menuLst").getAsJsonArray();
		
		JsonArray jsonArraySubMenu = jsonObject.get("subMenuLst").getAsJsonArray();
		
		MenuInput menuInput = new MenuInput();
		
		List<String> menuLst = new ArrayList<String>();
		
		for(int i = 0; i < jsonArrayMenu.size() ; i++)
		{
			menuLst.add(jsonArrayMenu.get(i).getAsString());
		}
		
		menuInput.setMenuId(menuLst);		
		
		List<SubMenuInput> menuInputs = new ArrayList<SubMenuInput>();
		
		for(int i = 0 ; i < jsonArraySubMenu.size() ; i++)
		{
			
			JsonObject object = jsonArraySubMenu.get(i).getAsJsonObject();
			
			SubMenuInput subMenuInput = new SubMenuInput();
			
			subMenuInput.setMenuId(object.get("menuId").getAsString());
			subMenuInput.setSubMenuId(object.get("subMenuId").getAsString());
			
			
			menuInputs.add(subMenuInput);
		}
		
		menuInput.setSubMenList(menuInputs);
		
		menuInput.setGrpId(grpId);
		
		MenuDAO dao = new MenuDAO();
		
		dao.addMenuInformation(starManResponse, menuInput);
		
		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));

	}
	
	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<MenuBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<MenuBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	
	private GenericEntity<?> getGenericMaintenanceOutput(
			MaintanenceResponse<MenuManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<MenuManOutput>>(starMaintenanceResponse) {
		};
	}
	
}
