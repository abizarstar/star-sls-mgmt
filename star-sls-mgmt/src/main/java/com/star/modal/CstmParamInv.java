package com.star.modal;

import java.io.Serializable;
import java.util.Date;

public class CstmParamInv implements Serializable{

	private String reqId;
	private String invNo;
	private boolean invSts;
	private int vchrPayMthd;
    private String notes;
    private String chkNo;
    private Date chkVdOn;
    private String chkVdBy;
    private String chkVdRmk;

	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	
	
	public String getInvNo() {
		return invNo;
	}
	public void setInvNo(String invNo) {
		this.invNo = invNo;
	}
	public boolean isInvSts() {
		return invSts;
	}
	public void setInvSts(boolean invSts) {
		this.invSts = invSts;
	}
	
	public int getVchrPayMthd() {
		return vchrPayMthd;
	}
	public void setVchrPayMthd(int vchrPayMthd) {
		this.vchrPayMthd = vchrPayMthd;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getChkNo() {
		return chkNo;
	}
	public void setChkNo(String chkNo) {
		this.chkNo = chkNo;
	}
	public Date getChkVdOn() {
		return chkVdOn;
	}
	public void setChkVdOn(Date chkVdOn) {
		this.chkVdOn = chkVdOn;
	}
	public String getChkVdBy() {
		return chkVdBy;
	}
	public void setChkVdBy(String chkVdBy) {
		this.chkVdBy = chkVdBy;
	}
	public String getChkVdRmk() {
		return chkVdRmk;
	}
	public void setChkVdRmk(String chkVdRmk) {
		this.chkVdRmk = chkVdRmk;
	}
	
	
}
