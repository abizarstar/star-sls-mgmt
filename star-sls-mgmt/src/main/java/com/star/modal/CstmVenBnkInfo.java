package com.star.modal;

import java.io.Serializable;
import java.util.Date;

public class CstmVenBnkInfo implements Serializable{

	private String cmpyId;
	private String venId;
	private String bnkNm;
	private String bnkAccNo;
	private String bnkKey;
	private String extRef;
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	public String getBnkNm() {
		return bnkNm;
	}
	public void setBnkNm(String bnkNm) {
		this.bnkNm = bnkNm;
	}
	public String getBnkAccNo() {
		return bnkAccNo;
	}
	public void setBnkAccNo(String bnkAccNo) {
		this.bnkAccNo = bnkAccNo;
	}
	public String getBnkKey() {
		return bnkKey;
	}
	public void setBnkKey(String bnkKey) {
		this.bnkKey = bnkKey;
	}
	public String getExtRef() {
		return extRef;
	}
	public void setExtRef(String extRef) {
		this.extRef = extRef;
	}
	
	
}
