package com.star.modal;

import java.io.Serializable;

public class UsrGrpMenu implements Serializable{
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + grpId;
		result = prime * result + menuId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsrGrpMenu other = (UsrGrpMenu) obj;
		if (grpId != other.grpId)
			return false;
		if (menuId != other.menuId)
			return false;
		return true;
	}
	private int grpId;
	private int menuId;
	public int getGrpId() {
		return grpId;
	}
	public void setGrpId(int grpId) {
		this.grpId = grpId;
	}
	public int getMenuId() {
		return menuId;
	}
	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}
	
	

}
