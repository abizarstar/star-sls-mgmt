package com.star.modal;

import java.io.Serializable;

public class UsrAprConAmt implements Serializable {

	private String usrId;
	private double frmAmt;
	private double toAmt;
	public String getUsrId() {
		return usrId;
	}
	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}
	public double getFrmAmt() {
		return frmAmt;
	}
	public void setFrmAmt(double frmAmt) {
		this.frmAmt = frmAmt;
	}
	public double getToAmt() {
		return toAmt;
	}
	public void setToAmt(double toAmt) {
		this.toAmt = toAmt;
	}
	
	
}
