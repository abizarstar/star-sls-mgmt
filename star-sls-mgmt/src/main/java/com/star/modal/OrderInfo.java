package com.star.modal;

import java.io.Serializable;

public class OrderInfo implements Serializable{
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cusPo == null) ? 0 : cusPo.hashCode());
		result = prime * result + docId;
		result = prime * result + docRowNo;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderInfo other = (OrderInfo) obj;
		if (cusPo == null) {
			if (other.cusPo != null)
				return false;
		} else if (!cusPo.equals(other.cusPo))
			return false;
		if (docId != other.docId)
			return false;
		if (docRowNo != other.docRowNo)
			return false;
		return true;
	}
	private int docId;
	private int docRowNo;
	private String cusPo;
	private int refItm;
	private int ctlNo;
	private String refPfx;
	private int refNo;
	private String msg;
	
	public int getDocId() {
		return docId;
	}
	public void setDocId(int docId) {
		this.docId = docId;
	}
	
	public int getDocRowNo() {
		return docRowNo;
	}
	public void setDocRowNo(int docRowNo) {
		this.docRowNo = docRowNo;
	}

	public String getCusPo() {
		return cusPo;
	}
	public void setCusPo(String cusPo) {
		this.cusPo = cusPo;
	}
	
	public int getRefItm() {
		return refItm;
	}
	public void setRefItm(int refItm) {
		this.refItm = refItm;
	}
	
	public int getCtlNo() {
		return ctlNo;
	}
	public void setCtlNo(int ctlNo) {
		this.ctlNo = ctlNo;
	}
	
	public String getRefPfx() {
		return refPfx;
	}
	public void setRefPfx(String refPfx) {
		this.refPfx = refPfx;
	}
	
	public int getRefNo() {
		return refNo;
	}
	public void setRefNo(int refNo) {
		this.refNo = refNo;
	}
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}

}
