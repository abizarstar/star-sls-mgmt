package com.star.modal;

import java.util.Date;

public class CstmOrdDocs {
	
	private int docId;
	private String docName;
	private String docOrigName;
	private Date crtDtts;
	private String upldBy;
	  
	public int getDocId() {
		return docId;
	}
	public void setDocId(int docId) {
		this.docId = docId;
	}

	public String getDocName() {
		return docName;
	}
	public void setDocName(String docName) {
		this.docName = docName;
	}
	
	public String getDocOrigName() {
		return docOrigName;
	}
	public void setDocOrigName(String docOrigName) {
		this.docOrigName = docOrigName;
	}

	public Date getCrtDtts() {
		return crtDtts;
	}
	public void setCrtDtts(Date crtDtts) {
		this.crtDtts = crtDtts;
	}
	
	public String getUpldBy() {
		return upldBy;
	}
	public void setUpldBy(String upldBy) {
		this.upldBy = upldBy;
	}

}
