package com.star.modal;

import java.io.Serializable;

public class UsrAprConVen implements Serializable {

	private String usrId;
	private String venId;
	public String getUsrId() {
		return usrId;
	}
	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	
	
}
