package com.star.modal;

import java.io.Serializable;

public class UsrGrpSubMenu implements Serializable{

	private int grpId;
	private int menuId;
	private int subMenuId;
	public int getGrpId() {
		return grpId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + grpId;
		result = prime * result + menuId;
		result = prime * result + subMenuId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsrGrpSubMenu other = (UsrGrpSubMenu) obj;
		if (grpId != other.grpId)
			return false;
		if (menuId != other.menuId)
			return false;
		if (subMenuId != other.subMenuId)
			return false;
		return true;
	}
	public void setGrpId(int grpId) {
		this.grpId = grpId;
	}
	public int getMenuId() {
		return menuId;
	}
	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}
	public int getSubMenuId() {
		return subMenuId;
	}
	public void setSubMenuId(int subMenuId) {
		this.subMenuId = subMenuId;
	}
	
	
}
