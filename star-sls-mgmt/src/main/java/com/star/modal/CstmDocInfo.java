package com.star.modal;

import java.util.Date;

public class CstmDocInfo {
	
	private int ctlNo;
	private int gatCtlNo;
	private String flPdf;
	private String flPdfName;
	private String flTxt; 
	private Date crtDtts;
	private Date prsDtts;
	private String wrkFlw;
	private String srcFlg;
	private Boolean isPrs;
	private String approverTo;
	private String trnSts;
	private int vchrNo;
	private String upldBy;
	private String trnRmk;
	  
	public int getCtlNo() {
		return ctlNo;
	}
	public void setCtlNo(int ctlNo) {
		this.ctlNo = ctlNo;
	}
	public int getGatCtlNo() {
		return gatCtlNo;
	}
	public void setGatCtlNo(int gatCtlNo) {
		this.gatCtlNo = gatCtlNo;
	}
	public String getFlPdf() {
		return flPdf;
	}
	public void setFlPdf(String flPdf) {
		this.flPdf = flPdf;
	}
	public String getFlPdfName() {
		return flPdfName;
	}
	public void setFlPdfName(String flPdfName) {
		this.flPdfName = flPdfName;
	}
	public String getFlTxt() {
		return flTxt;
	}
	public void setFlTxt(String flTxt) {
		this.flTxt = flTxt;
	}
	public Date getCrtDtts() {
		return crtDtts;
	}
	public void setCrtDtts(Date crtDtts) {
		this.crtDtts = crtDtts;
	}
	public Date getPrsDtts() {
		return prsDtts;
	}
	public void setPrsDtts(Date prsDtts) {
		this.prsDtts = prsDtts;
	}
	public String getWrkFlw() {
		return wrkFlw;
	}
	public void setWrkFlw(String wrkFlw) {
		this.wrkFlw = wrkFlw;
	}
	public String getSrcFlg() {
		return srcFlg;
	}
	public void setSrcFlg(String srcFlg) {
		this.srcFlg = srcFlg;
	}
	public Boolean getIsPrs() {
		return isPrs;
	}
	public void setIsPrs(Boolean isPrs) {
		this.isPrs = isPrs;
	}
	public String getApproverTo() {
		return approverTo;
	}
	public void setApproverTo(String approverTo) {
		this.approverTo = approverTo;
	}
	public String getTrnSts() {
		return trnSts;
	}
	public void setTrnSts(String trnSts) {
		this.trnSts = trnSts;
	}
	public int getVchrNo() {
		return vchrNo;
	}
	public void setVchrNo(int vchrNo) {
		this.vchrNo = vchrNo;
	}
	public String getUpldBy() {
		return upldBy;
	}
	public void setUpldBy(String upldBy) {
		this.upldBy = upldBy;
	}
	public String getTrnRmk() {
		return trnRmk;
	}
	public void setTrnRmk(String trnRmk) {
		this.trnRmk = trnRmk;
	}
	
	
	
	

}
