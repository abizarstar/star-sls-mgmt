package com.star.modal;

public class I47Rec {
private String i47CmpyId;
private String i47SrcCoId;
private int i47TrcCtlNo;
private String i47TrcItm;
private String i47OrdGat;
private String i47InstrGat;
private String i47StsGat;
private String i47PrdGat;
private String i47PrdDescGat;
private String i47ChrgGat;
private String i47TxGat;
private String i47CondGat;
private String i47TolGat;
private String i47CoilPkgGat;
private String i47PkgGat;
private String i47StdSpecGat;
private String i47TstGat;
private String i47ChmSpecGat;
private String i47TstCertGat;
private String i47OrigZnGat;
private String i47RdfltCps;
public String getI47CmpyId() {
	return i47CmpyId;
}
public void setI47CmpyId(String i47CmpyId) {
	this.i47CmpyId = i47CmpyId;
}
public String getI47SrcCoId() {
	return i47SrcCoId;
}
public void setI47SrcCoId(String i47SrcCoId) {
	this.i47SrcCoId = i47SrcCoId;
}
public int getI47TrcCtlNo() {
	return i47TrcCtlNo;
}
public void setI47TrcCtlNo(int i47TrcCtlNo) {
	this.i47TrcCtlNo = i47TrcCtlNo;
}
public String getI47TrcItm() {
	return i47TrcItm;
}
public void setI47TrcItm(String i47TrcItm) {
	this.i47TrcItm = i47TrcItm;
}
public String getI47OrdGat() {
	return i47OrdGat;
}
public void setI47OrdGat(String i47OrdGat) {
	this.i47OrdGat = i47OrdGat;
}
public String getI47InstrGat() {
	return i47InstrGat;
}
public void setI47InstrGat(String i47InstrGat) {
	this.i47InstrGat = i47InstrGat;
}
public String getI47StsGat() {
	return i47StsGat;
}
public void setI47StsGat(String i47StsGat) {
	this.i47StsGat = i47StsGat;
}
public String getI47PrdGat() {
	return i47PrdGat;
}
public void setI47PrdGat(String i47PrdGat) {
	this.i47PrdGat = i47PrdGat;
}
public String getI47PrdDescGat() {
	return i47PrdDescGat;
}
public void setI47PrdDescGat(String i47PrdDescGat) {
	this.i47PrdDescGat = i47PrdDescGat;
}
public String getI47ChrgGat() {
	return i47ChrgGat;
}
public void setI47ChrgGat(String i47ChrgGat) {
	this.i47ChrgGat = i47ChrgGat;
}
public String getI47TxGat() {
	return i47TxGat;
}
public void setI47TxGat(String i47TxGat) {
	this.i47TxGat = i47TxGat;
}
public String getI47CondGat() {
	return i47CondGat;
}
public void setI47CondGat(String i47CondGat) {
	this.i47CondGat = i47CondGat;
}
public String getI47TolGat() {
	return i47TolGat;
}
public void setI47TolGat(String i47TolGat) {
	this.i47TolGat = i47TolGat;
}
public String getI47CoilPkgGat() {
	return i47CoilPkgGat;
}
public void setI47CoilPkgGat(String i47CoilPkgGat) {
	this.i47CoilPkgGat = i47CoilPkgGat;
}
public String getI47PkgGat() {
	return i47PkgGat;
}
public void setI47PkgGat(String i47PkgGat) {
	this.i47PkgGat = i47PkgGat;
}
public String getI47StdSpecGat() {
	return i47StdSpecGat;
}
public void setI47StdSpecGat(String i47StdSpecGat) {
	this.i47StdSpecGat = i47StdSpecGat;
}
public String getI47TstGat() {
	return i47TstGat;
}
public void setI47TstGat(String i47TstGat) {
	this.i47TstGat = i47TstGat;
}
public String getI47ChmSpecGat() {
	return i47ChmSpecGat;
}
public void setI47ChmSpecGat(String i47ChmSpecGat) {
	this.i47ChmSpecGat = i47ChmSpecGat;
}
public String getI47TstCertGat() {
	return i47TstCertGat;
}
public void setI47TstCertGat(String i47TstCertGat) {
	this.i47TstCertGat = i47TstCertGat;
}
public String getI47OrigZnGat() {
	return i47OrigZnGat;
}
public void setI47OrigZnGat(String i47OrigZnGat) {
	this.i47OrigZnGat = i47OrigZnGat;
}
public String getI47RdfltCps() {
	return i47RdfltCps;
}
public void setI47RdfltCps(String i47RdfltCps) {
	this.i47RdfltCps = i47RdfltCps;
}

}
