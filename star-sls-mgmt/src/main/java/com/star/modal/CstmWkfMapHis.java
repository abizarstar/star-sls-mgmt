package com.star.modal;

import java.io.Serializable;
import java.util.Date;

public class CstmWkfMapHis implements Serializable{

	private int wkfHisId;
	private int ctlNo;
	private int wkfId;
	private Date wkfAsgnOn;
	private String wkfAsgnTo;
	private String wkfSts;
	private String wkfRmk;
	
	public int getCtlNo() {
		return ctlNo;
	}
	public void setCtlNo(int ctlNo) {
		this.ctlNo = ctlNo;
	}
	public int getWkfId() {
		return wkfId;
	}
	public void setWkfId(int wkfId) {
		this.wkfId = wkfId;
	}
	public Date getWkfAsgnOn() {
		return wkfAsgnOn;
	}
	public void setWkfAsgnOn(Date wkfAsgnOn) {
		this.wkfAsgnOn = wkfAsgnOn;
	}
	public String getWkfAsgnTo() {
		return wkfAsgnTo;
	}
	public void setWkfAsgnTo(String wkfAsgnTo) {
		this.wkfAsgnTo = wkfAsgnTo;
	}
	public String getWkfSts() {
		return wkfSts;
	}
	public void setWkfSts(String wkfSts) {
		this.wkfSts = wkfSts;
	}
	public int getWkfHisId() {
		return wkfHisId;
	}
	public void setWkfHisId(int wkfHisId) {
		this.wkfHisId = wkfHisId;
	}
	public String getWkfRmk() {
		return wkfRmk;
	}
	public void setWkfRmk(String wkfRmk) {
		this.wkfRmk = wkfRmk;
	}
	
	
}
