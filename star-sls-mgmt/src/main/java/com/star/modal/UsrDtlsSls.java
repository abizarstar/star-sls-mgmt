package com.star.modal;

public class UsrDtlsSls {

	String usrId;
	String usrNm;
	String usrEml;
	String usrTyp;
	int usrGrp;
	Boolean isActv;
	String chkSum;
	
	public String getUsrId() {
		return usrId;
	}
	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}
	public String getUsrNm() {
		return usrNm;
	}
	public void setUsrNm(String usrNm) {
		this.usrNm = usrNm;
	}
	public Boolean getIsActv() {
		return isActv;
	}
	public void setIsActv(Boolean isActv) {
		this.isActv = isActv;
	}
	public String getUsrEml() {
		return usrEml;
	}
	public void setUsrEml(String usrEml) {
		this.usrEml = usrEml;
	}
	public String getUsrTyp() {
		return usrTyp;
	}
	public void setUsrTyp(String usrTyp) {
		this.usrTyp = usrTyp;
	}
	public int getUsrGrp() {
		return usrGrp;
	}
	public void setUsrGrp(int usrGrp) {
		this.usrGrp = usrGrp;
	}
	public String getChkSum() {
		return chkSum;
	}
	public void setChkSum(String chkSum) {
		this.chkSum = chkSum;
	}

}
