package com.star.modal;

import java.io.Serializable;

public class CstmVchrReconInfo implements Serializable{

	private int  vchrCtlNo ;
	private int vchrCrcnNo ;
	private String vchrTrsNo;
	private String vchrPoNo;
	private int vchrCstNo;
	private double vchrBalAmt;
    
    public int getVchrCtlNo() {
		return vchrCtlNo;
	}

	public int getVchrCrcnNo() {
		return vchrCrcnNo;
	}

	public void setVchrCrcnNo(int vchrCrcnNo) {
		this.vchrCrcnNo = vchrCrcnNo;
	}

	public String getVchrTrsNo() {
		return vchrTrsNo;
	}

	public void setVchrTrsNo(String vchrTrsNo) {
		this.vchrTrsNo = vchrTrsNo;
	}

	public String getVchrPoNo() {
		return vchrPoNo;
	}

	public void setVchrPoNo(String vchrPoNo) {
		this.vchrPoNo = vchrPoNo;
	}

	public int getVchrCstNo() {
		return vchrCstNo;
	}

	public void setVchrCstNo(int vchrCstNo) {
		this.vchrCstNo = vchrCstNo;
	}

	public double getVchrBalAmt() {
		return vchrBalAmt;
	}

	public void setVchrBalAmt(double vchrBalAmt) {
		this.vchrBalAmt = vchrBalAmt;
	}

	public void setVchrCtlNo(int vchrCtlNo) {
		this.vchrCtlNo = vchrCtlNo;
	}

	
	
}
