package com.star.modal;

import java.io.Serializable;

public class UsrGroup implements Serializable {

	private int grpId;
	private String grpNm;
	public int getGrpId() {
		return grpId;
	}
	public void setGrpId(int grpId) {
		this.grpId = grpId;
	}
	public String getGrpNm() {
		return grpNm;
	}
	public void setGrpNm(String grpNm) {
		this.grpNm = grpNm;
	}
	
	
}
