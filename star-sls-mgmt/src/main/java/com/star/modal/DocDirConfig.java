package com.star.modal;

public class DocDirConfig {
	
	private int dirId;
	private String s3AccessKey;
	private String s3SecretKey;
	private String s3Region;
	private String s3BktNm; 
	private String rootPath;
	private String dirType;
	
	public String getS3AccessKey() {
		return s3AccessKey;
	}
	public void setS3AccessKey(String s3AccessKey) {
		this.s3AccessKey = s3AccessKey;
	}
	public String getS3SecretKey() {
		return s3SecretKey;
	}
	public void setS3SecretKey(String s3SecretKey) {
		this.s3SecretKey = s3SecretKey;
	}
	public String getS3Region() {
		return s3Region;
	}
	public void setS3Region(String s3Region) {
		this.s3Region = s3Region;
	}
	public String getRootPath() {
		return rootPath;
	}
	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}
	public String getDirType() {
		return dirType;
	}
	public void setDirType(String dirType) {
		this.dirType = dirType;
	}
	public int getDirId() {
		return dirId;
	}
	public void setDirId(int dirId) {
		this.dirId = dirId;
	}
	public String getS3BktNm() {
		return s3BktNm;
	}
	public void setS3BktNm(String s3BktNm) {
		this.s3BktNm = s3BktNm;
	}
	
	
}
