package com.star.modal;

import java.util.Date;

public class S05Rec {
	private String s05CmpyId;
	private String s05SrcCoId;
	private int s05TrcCtlNo;
	private int s05TrcItm;
	private int s05AlwPrtlShp;
	private double s05OvrshpPct;
	private double s05UndshpPct;
	private String s05CusPo;
	private Date s05CusPoDt;
	private String s05CusCtrct;
	private String s05EndUsrPo;
	private int s05OrdPcs;
	private double s05OrdMsr;
	private double s05OrdWgt;
	private double s05OrdQty;
	private String s05OrdWgtUm;
	private int s05BktPcs;
	private double s05BktMsr;
	private double s05BktWgt;
	private double s05BktQty;
	private String s05BktWgtUm;
	private String s05SlsCat;
	private String s05ChrgQtyTyp;
	private int s05WflSls;
	private int s05UseWgtMult;
	private String s05AlySchgCls;
	private String s05Src;
	private int s05BldMtlChrg;
	private String 	s05EndUse;
	private String s05EndUseDesc;
	private double s05KrfWdth;
	private double s05KrfLgth;
	private double s05KrfLsWdth;
	private double s05KrfLsLgth;
	private String s05StdPrsRte;
	private String s05InvtQlty;
	private String s05SrcWhs;
	private String s05JbspyChrgUm;
	private String s05MultpDimTmpl;
	private int s05AplReb;
	private String s05TolDistMthd;
	private int s05ShpTo;
	private String s0BlndShpt;
	private String s0StsCd;
	public String getS05CmpyId() {
		return s05CmpyId;
	}
	public void setS05CmpyId(String s05CmpyId) {
		this.s05CmpyId = s05CmpyId;
	}
	public String getS05SrcCoId() {
		return s05SrcCoId;
	}
	public void setS05SrcCoId(String s05SrcCoId) {
		this.s05SrcCoId = s05SrcCoId;
	}
	public int getS05TrcCtlNo() {
		return s05TrcCtlNo;
	}
	public void setS05TrcCtlNo(int s05TrcCtlNo) {
		this.s05TrcCtlNo = s05TrcCtlNo;
	}
	public int getS05TrcItm() {
		return s05TrcItm;
	}
	public void setS05TrcItm(int s05TrcItm) {
		this.s05TrcItm = s05TrcItm;
	}
	public int getS05AlwPrtlShp() {
		return s05AlwPrtlShp;
	}
	public void setS05AlwPrtlShp(int s05AlwPrtlShp) {
		this.s05AlwPrtlShp = s05AlwPrtlShp;
	}
	public double getS05OvrshpPct() {
		return s05OvrshpPct;
	}
	public void setS05OvrshpPct(double s05OvrshpPct) {
		this.s05OvrshpPct = s05OvrshpPct;
	}
	public double getS05UndshpPct() {
		return s05UndshpPct;
	}
	public void setS05UndshpPct(double s05UndshpPct) {
		this.s05UndshpPct = s05UndshpPct;
	}
	public String getS05CusPo() {
		return s05CusPo;
	}
	public void setS05CusPo(String s05CusPo) {
		this.s05CusPo = s05CusPo;
	}
	public Date getS05CusPoDt() {
		return s05CusPoDt;
	}
	public void setS05CusPoDt(Date s05CusPoDt) {
		this.s05CusPoDt = s05CusPoDt;
	}
	public String getS05CusCtrct() {
		return s05CusCtrct;
	}
	public void setS05CusCtrct(String s05CusCtrct) {
		this.s05CusCtrct = s05CusCtrct;
	}
	public String getS05EndUsrPo() {
		return s05EndUsrPo;
	}
	public void setS05EndUsrPo(String s05EndUsrPo) {
		this.s05EndUsrPo = s05EndUsrPo;
	}
	public int getS05OrdPcs() {
		return s05OrdPcs;
	}
	public void setS05OrdPcs(int s05OrdPcs) {
		this.s05OrdPcs = s05OrdPcs;
	}
	public double getS05OrdMsr() {
		return s05OrdMsr;
	}
	public void setS05OrdMsr(double s05OrdMsr) {
		this.s05OrdMsr = s05OrdMsr;
	}
	public double getS05OrdWgt() {
		return s05OrdWgt;
	}
	public void setS05OrdWgt(double s05OrdWgt) {
		this.s05OrdWgt = s05OrdWgt;
	}
	public double getS05OrdQty() {
		return s05OrdQty;
	}
	public void setS05OrdQty(double s05OrdQty) {
		this.s05OrdQty = s05OrdQty;
	}
	public String getS05OrdWgtUm() {
		return s05OrdWgtUm;
	}
	public void setS05OrdWgtUm(String s05OrdWgtUm) {
		this.s05OrdWgtUm = s05OrdWgtUm;
	}
	public int getS05BktPcs() {
		return s05BktPcs;
	}
	public void setS05BktPcs(int s05BktPcs) {
		this.s05BktPcs = s05BktPcs;
	}
	public double getS05BktMsr() {
		return s05BktMsr;
	}
	public void setS05BktMsr(double s05BktMsr) {
		this.s05BktMsr = s05BktMsr;
	}
	public double getS05BktWgt() {
		return s05BktWgt;
	}
	public void setS05BktWgt(double s05BktWgt) {
		this.s05BktWgt = s05BktWgt;
	}
	public double getS05BktQty() {
		return s05BktQty;
	}
	public void setS05BktQty(double s05BktQty) {
		this.s05BktQty = s05BktQty;
	}
	public String getS05BktWgtUm() {
		return s05BktWgtUm;
	}
	public void setS05BktWgtUm(String s05BktWgtUm) {
		this.s05BktWgtUm = s05BktWgtUm;
	}
	public String getS05SlsCat() {
		return s05SlsCat;
	}
	public void setS05SlsCat(String s05SlsCat) {
		this.s05SlsCat = s05SlsCat;
	}
	public String getS05ChrgQtyTyp() {
		return s05ChrgQtyTyp;
	}
	public void setS05ChrgQtyTyp(String s05ChrgQtyTyp) {
		this.s05ChrgQtyTyp = s05ChrgQtyTyp;
	}
	public int getS05WflSls() {
		return s05WflSls;
	}
	public void setS05WflSls(int s05WflSls) {
		this.s05WflSls = s05WflSls;
	}
	public int getS05UseWgtMult() {
		return s05UseWgtMult;
	}
	public void setS05UseWgtMult(int s05UseWgtMult) {
		this.s05UseWgtMult = s05UseWgtMult;
	}
	public String getS05AlySchgCls() {
		return s05AlySchgCls;
	}
	public void setS05AlySchgCls(String s05AlySchgCls) {
		this.s05AlySchgCls = s05AlySchgCls;
	}
	public String getS05Src() {
		return s05Src;
	}
	public void setS05Src(String s05Src) {
		this.s05Src = s05Src;
	}
	public int getS05BldMtlChrg() {
		return s05BldMtlChrg;
	}
	public void setS05BldMtlChrg(int s05BldMtlChrg) {
		this.s05BldMtlChrg = s05BldMtlChrg;
	}
	public String getS05EndUse() {
		return s05EndUse;
	}
	public void setS05EndUse(String s05EndUse) {
		this.s05EndUse = s05EndUse;
	}
	public String getS05EndUseDesc() {
		return s05EndUseDesc;
	}
	public void setS05EndUseDesc(String s05EndUseDesc) {
		this.s05EndUseDesc = s05EndUseDesc;
	}
	public double getS05KrfWdth() {
		return s05KrfWdth;
	}
	public void setS05KrfWdth(double s05KrfWdth) {
		this.s05KrfWdth = s05KrfWdth;
	}
	public double getS05KrfLgth() {
		return s05KrfLgth;
	}
	public void setS05KrfLgth(double s05KrfLgth) {
		this.s05KrfLgth = s05KrfLgth;
	}
	public double getS05KrfLsWdth() {
		return s05KrfLsWdth;
	}
	public void setS05KrfLsWdth(double s05KrfLsWdth) {
		this.s05KrfLsWdth = s05KrfLsWdth;
	}
	public double getS05KrfLsLgth() {
		return s05KrfLsLgth;
	}
	public void setS05KrfLsLgth(double s05KrfLsLgth) {
		this.s05KrfLsLgth = s05KrfLsLgth;
	}
	public String getS05StdPrsRte() {
		return s05StdPrsRte;
	}
	public void setS05StdPrsRte(String s05StdPrsRte) {
		this.s05StdPrsRte = s05StdPrsRte;
	}
	public String getS05InvtQlty() {
		return s05InvtQlty;
	}
	public void setS05InvtQlty(String s05InvtQlty) {
		this.s05InvtQlty = s05InvtQlty;
	}
	public String getS05SrcWhs() {
		return s05SrcWhs;
	}
	public void setS05SrcWhs(String s05SrcWhs) {
		this.s05SrcWhs = s05SrcWhs;
	}
	public String getS05JbspyChrgUm() {
		return s05JbspyChrgUm;
	}
	public void setS05JbspyChrgUm(String s05JbspyChrgUm) {
		this.s05JbspyChrgUm = s05JbspyChrgUm;
	}
	public String getS05MultpDimTmpl() {
		return s05MultpDimTmpl;
	}
	public void setS05MultpDimTmpl(String s05MultpDimTmpl) {
		this.s05MultpDimTmpl = s05MultpDimTmpl;
	}
	public int getS05AplReb() {
		return s05AplReb;
	}
	public void setS05AplReb(int s05AplReb) {
		this.s05AplReb = s05AplReb;
	}
	public String getS05TolDistMthd() {
		return s05TolDistMthd;
	}
	public void setS05TolDistMthd(String s05TolDistMthd) {
		this.s05TolDistMthd = s05TolDistMthd;
	}
	public int getS05ShpTo() {
		return s05ShpTo;
	}
	public void setS05ShpTo(int s05ShpTo) {
		this.s05ShpTo = s05ShpTo;
	}
	public String getS0BlndShpt() {
		return s0BlndShpt;
	}
	public void setS0BlndShpt(String s0BlndShpt) {
		this.s0BlndShpt = s0BlndShpt;
	}
	public String getS0StsCd() {
		return s0StsCd;
	}
	public void setS0StsCd(String s0StsCd) {
		this.s0StsCd = s0StsCd;
	}
	
}
