package com.star.modal;

import java.util.Date;

public class S06Rec {

	private String s06CmpyId;
	private String s06SrcCoId;
	private int s06TrcCtlNo;
	private int s06TrcItm;
	private String s06Frm;
	private String s06Grd;
	private String s06Size;
	private String s06Fnsh;
	private String s06DimDsgn;
	private String s06EntMsr;
	private int s06LgthDispFmt;
	private int s06FrcFmt;
	private int s06FmtDescFrc;
	private int s06FmtSizeDesc;
	public String getS06CmpyId() {
		return s06CmpyId;
	}
	public void setS06CmpyId(String s06CmpyId) {
		this.s06CmpyId = s06CmpyId;
	}
	public String getS06SrcCoId() {
		return s06SrcCoId;
	}
	public void setS06SrcCoId(String s06SrcCoId) {
		this.s06SrcCoId = s06SrcCoId;
	}
	public int getS06TrcCtlNo() {
		return s06TrcCtlNo;
	}
	public void setS06TrcCtlNo(int s06TrcCtlNo) {
		this.s06TrcCtlNo = s06TrcCtlNo;
	}
	public int getS06TrcItm() {
		return s06TrcItm;
	}
	public void setS06TrcItm(int s06TrcItm) {
		this.s06TrcItm = s06TrcItm;
	}
	public String getS06Frm() {
		return s06Frm;
	}
	public void setS06Frm(String s06Frm) {
		this.s06Frm = s06Frm;
	}
	public String getS06Grd() {
		return s06Grd;
	}
	public void setS06Grd(String s06Grd) {
		this.s06Grd = s06Grd;
	}
	public String getS06Size() {
		return s06Size;
	}
	public void setS06Size(String s06Size) {
		this.s06Size = s06Size;
	}
	public String getS06Fnsh() {
		return s06Fnsh;
	}
	public void setS06Fnsh(String s06Fnsh) {
		this.s06Fnsh = s06Fnsh;
	}
	public String getS06DimDsgn() {
		return s06DimDsgn;
	}
	public void setS06DimDsgn(String s06DimDsgn) {
		this.s06DimDsgn = s06DimDsgn;
	}
	public String getS06EntMsr() {
		return s06EntMsr;
	}
	public void setS06EntMsr(String s06EntMsr) {
		this.s06EntMsr = s06EntMsr;
	}
	public int getS06LgthDispFmt() {
		return s06LgthDispFmt;
	}
	public void setS06LgthDispFmt(int s06LgthDispFmt) {
		this.s06LgthDispFmt = s06LgthDispFmt;
	}
	public int getS06FrcFmt() {
		return s06FrcFmt;
	}
	public void setS06FrcFmt(int s06FrcFmt) {
		this.s06FrcFmt = s06FrcFmt;
	}
	public int getS06FmtDescFrc() {
		return s06FmtDescFrc;
	}
	public void setS06FmtDescFrc(int s06FmtDescFrc) {
		this.s06FmtDescFrc = s06FmtDescFrc;
	}
	public int getS06FmtSizeDesc() {
		return s06FmtSizeDesc;
	}
	public void setS06FmtSizeDesc(int s06FmtSizeDesc) {
		this.s06FmtSizeDesc = s06FmtSizeDesc;
	}

}
