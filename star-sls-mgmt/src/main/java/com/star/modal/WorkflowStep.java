package com.star.modal;

import java.io.Serializable;
import java.util.Date;

public class WorkflowStep implements Serializable{

	private long stepId;
	private long wkfId;
	private String stepName;
	private int amtFlg;
	private int brhFlg;
	private int venFlg;
	private int dayFlg;
	private float amtFrm;
	private float amtTo;
	private String brh;
	private String venId;
	private int days;
	private int dayBef;
	public void setAmtFrm(float amtFrm) {
		this.amtFrm = amtFrm;
	}
	public void setAmtTo(float amtTo) {
		this.amtTo = amtTo;
	}
	public void setDayBef(int dayBef) {
		this.dayBef = dayBef;
	}
	private String apvr;
	private int crcnFlg;
	public long getStepId() {
		return stepId;
	}
	public void setStepId(long stepId) {
		this.stepId = stepId;
	}
	public long getWkfId() {
		return wkfId;
	}
	public void setWkfId(long wkfId) {
		this.wkfId = wkfId;
	}
	public String getStepName() {
		return stepName;
	}
	public void setStepName(String stepName) {
		this.stepName = stepName;
	}
	public int getAmtFlg() {
		return amtFlg;
	}
	public void setAmtFlg(int amtFlg) {
		this.amtFlg = amtFlg;
	}
	public int getBrhFlg() {
		return brhFlg;
	}
	public void setBrhFlg(int brhFlg) {
		this.brhFlg = brhFlg;
	}
	public int getVenFlg() {
		return venFlg;
	}
	public void setVenFlg(int venFlg) {
		this.venFlg = venFlg;
	}
	public int getDayFlg() {
		return dayFlg;
	}
	public void setDayFlg(int dayFlg) {
		this.dayFlg = dayFlg;
	}
	public void setAmtFrm(long amtFrm) {
		this.amtFrm = amtFrm;
	}
		public void setAmtTo(long amtTo) {
		this.amtTo = amtTo;
	}
	public String getBrh() {
		return brh;
	}
	public void setBrh(String brh) {
		this.brh = brh;
	}
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	public float getAmtFrm() {
		return amtFrm;
	}
	public float getAmtTo() {
		return amtTo;
	}
	public int getDayBef() {
		return dayBef;
	}
	public int getDays() {
		return days;
	}
	public void setDays(int days) {
		this.days = days;
	}
	
	public String getApvr() {
		return apvr;
	}
	public void setApvr(String apvr) {
		this.apvr = apvr;
	}
	public int getCrcnFlg() {
		return crcnFlg;
	}
	public void setCrcnFlg(int crcnFlg) {
		this.crcnFlg = crcnFlg;
	}
	}
