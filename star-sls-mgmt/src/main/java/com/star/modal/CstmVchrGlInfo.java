package com.star.modal;

import java.io.Serializable;

public class CstmVchrGlInfo implements Serializable{

	private int  vchrCtlNo ;
	private String vchrAcct ;
	private String vchrSubAcct;
	private double vchrDrAmt;
	private double vchrCrAmt;
	private String vchrRmk;
    
    public int getVchrCtlNo() {
		return vchrCtlNo;
	}

	public String getVchrAcct() {
		return vchrAcct;
	}

	public void setVchrAcct(String vchrAcct) {
		this.vchrAcct = vchrAcct;
	}

	public String getVchrSubAcct() {
		return vchrSubAcct;
	}

	public void setVchrSubAcct(String vchrSubAcct) {
		this.vchrSubAcct = vchrSubAcct;
	}

	public double getVchrDrAmt() {
		return vchrDrAmt;
	}

	public void setVchrDrAmt(double vchrDrAmt) {
		this.vchrDrAmt = vchrDrAmt;
	}

	public double getVchrCrAmt() {
		return vchrCrAmt;
	}

	public void setVchrCrAmt(double vchrCrAmt) {
		this.vchrCrAmt = vchrCrAmt;
	}

	public String getVchrRmk() {
		return vchrRmk;
	}

	public void setVchrRmk(String vchrRmk) {
		this.vchrRmk = vchrRmk;
	}

	public void setVchrCtlNo(int vchrCtlNo) {
		this.vchrCtlNo = vchrCtlNo;
	}
	
}
