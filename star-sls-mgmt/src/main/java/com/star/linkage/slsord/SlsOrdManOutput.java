package com.star.linkage.slsord;

import com.star.common.MaintenanceOutput;

public class SlsOrdManOutput extends MaintenanceOutput{
	public int soNo;
	public String stsCd;
	public int docRowNo;
	public int ordItmNo;
}
