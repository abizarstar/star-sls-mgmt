package com.star.linkage.slsord;

public class SOSpecs {

	private String sdo;
	private String stdId;
	private String addId;
	public String getSdo() {
		return sdo;
	}
	public void setSdo(String sdo) {
		this.sdo = sdo;
	}
	public String getStdId() {
		return stdId;
	}
	public void setStdId(String stdId) {
		this.stdId = stdId;
	}
	public String getAddId() {
		return addId;
	}
	public void setAddId(String addId) {
		this.addId = addId;
	}
	
}
