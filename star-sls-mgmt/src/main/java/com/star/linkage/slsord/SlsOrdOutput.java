package com.star.linkage.slsord;

import java.util.ArrayList;
import java.util.List;

public class SlsOrdOutput {
	public int soNo;
	public String stsCd;
	public int docRowNo;
	public int ordItmNo;
	public List<String> messages = new ArrayList<String>();
	
	public Integer getSoNo() {
		return soNo;
	}
	public void setSoNo(Integer soNo) {
		this.soNo = soNo;
	}
	
	public String getStsCd() {
		return stsCd;
	}
	public void setStsCd(String stsCd) {
		this.stsCd = stsCd;
	}
	
	public Integer getDocRowNo() {
		return docRowNo;
	}
	public void setDocRowNo(Integer docRowNo) {
		this.docRowNo = docRowNo;
	}
	
	public Integer getOrdItmNo() {
		return ordItmNo;
	}
	public void setOrdItmNo(Integer ordItmNo) {
		this.ordItmNo = ordItmNo;
	}
	
	public void setErrorMessage(String errorMessage) {
		if (messages ==  null) {
			messages = new ArrayList<String>();
		}
		
		messages.add(errorMessage);
	}
}
