package com.star.linkage.slsord;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SlsOrdInput {
	private int trcCtlNo;
	private int logCtlNo;
	private int soNo;
	private String ordPfx;
	private int docId;
	private int docRowNo;
	private String usrId;
	private String cmpyId;
	private String refBrh;
	private String cusId;
	private String shpTo;
	private String isSlp;
	private String osSlp;
	private String tknSlp;
	private String ordTyp;
	private String slsCat;
	private String poNo;
	private String poDt;
	private String ordPcs;
	private String ordWgt;
	private String ordMsr;
	private String qty;
	private String um;
	private String source;
	private String custItem;
	private String delDt;
	private String cry;
	private BigDecimal unitPrice;
	private String priceUm;
	private BigDecimal frtChrg;
	private BigDecimal cstChrg;
	private int venId;
	private int itmNo;
	private BigDecimal exrt;
	private String exrtTyp;	
	private int discTrm;
	private int pttrm;
	private String mthdPmt;
	private String srcWhs;
	private String plngWhs;
	private String shpgWhs;
	private String hdrRmk;
	private String trmTrd;
	private String dlvyMthd;
	private String frtUm;
	private String basLng;
	private String frm;
	private String grd;
	private String size;
	private String fnsh;
	private String wdth;
	private String lgth;
	private String gaSize;
	private String gaType;
	private String tolFlg;
	private String pkgFlg;
	private String pkg;
	private String skdTyp;
	private Integer pcsPerTag;
	private Integer minPcsPtag;
	private BigDecimal minPkgWgt;
	private BigDecimal maxPkgWgt;
	private BigDecimal maxHgt;
	private BigDecimal wdthTolPosv;
	private BigDecimal wdthTolNeg;
	private BigDecimal lgthTolPosv;
	private BigDecimal lgthTolNeg;
	private BigDecimal idiaTolPosv;
	private BigDecimal idiaTolNeg;
	private BigDecimal odiaTolPosv;
	private BigDecimal odiaTolNeg;
	private BigDecimal wthckTolPosv;
	private BigDecimal wthckTolNeg;
	private BigDecimal gaTolPosv;
	private BigDecimal gaTolNeg;
	private BigDecimal diagTol;
	private BigDecimal fltns;
	private String cmbr;
	private String StndrdRoute;
	private String lgthType;
	private String memoPart;
	private String idia;
	private String odia;
	private String prsRte;
	private String flwUpDays;
	private String expiryDays;
	private String buildUp;
	private String metalStd;
	
	private List<SOSpecs> mtlStdList = new ArrayList<SOSpecs>();
	
	
	
	public String getPrsRte() {
		return prsRte;
	}
	public void setPrsRte(String prsRte) {
		this.prsRte = prsRte;
	}
	public String getFlwUpDays() {
		return flwUpDays;
	}
	public void setFlwUpDays(String flwUpDays) {
		this.flwUpDays = flwUpDays;
	}
	public String getExpiryDays() {
		return expiryDays;
	}
	public void setExpiryDays(String expiryDays) {
		this.expiryDays = expiryDays;
	}
	public String getBuildUp() {
		return buildUp;
	}
	public void setBuildUp(String buildUp) {
		this.buildUp = buildUp;
	}
	public Integer getTrcCtlNo() {
		return trcCtlNo;
	}
	public void setTrcCtlNo(Integer trcCtlNo) {
		this.trcCtlNo = trcCtlNo;
	}
	
	public Integer getLogCtlNo() {
		return logCtlNo;
	}
	public void setLogCtlNo(Integer logCtlNo) {
		this.logCtlNo = logCtlNo;
	}
	
	public Integer getSoNo() {
		return soNo;
	}
	public void setSoNo(Integer soNo) {
		this.soNo = soNo;
	}
	
	public Integer getDocId() {
		return docId;
	}
	public void setDocId(Integer docId) {
		this.docId = docId;
	}
	
	public Integer getDocRowNo() {
		return docRowNo;
	}
	public void setDocRowNo(Integer docRowNo) {
		this.docRowNo = docRowNo;
	}
	
	public String getUsrId() {
		return usrId;
	}
	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}
	
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	
	public String getRefBrh() {
		return refBrh;
	}
	public void setRefBrh(String refBrh) {
		this.refBrh = refBrh;
	}
	
	public String getCusId() {
		return cusId;
	}
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
	public String getShpTo() {
		return shpTo;
	}
	public void setShpTo(String shpTo) {
		this.shpTo = shpTo;
	}
	
	public String getIsSlp() {
		return isSlp;
	}
	public void setIsSlp(String isSlp) {
		this.isSlp = isSlp;
	}
	
	public String getOsSlp() {
		return osSlp;
	}
	public void setOsSlp(String osSlp) {
		this.osSlp = osSlp;
	}
	
	public String getTknSlp() {
		return tknSlp;
	}
	public void setTknSlp(String tknSlp) {
		this.tknSlp = tknSlp;
	}
	
	public String getOrdTyp() {
		return ordTyp;
	}
	public void setOrdTyp(String ordTyp) {
		this.ordTyp = ordTyp;
	}
	
	public String getSlsCat() {
		return slsCat;
	}
	public void setSlsCat(String slsCat) {
		this.slsCat = slsCat;
	}
	
	public String getPoNo() {
		return poNo;
	}
	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}
	
	public String getPoDt() {
		return poDt;
	}
	public void setPoDt(String poDt) {
		this.poDt = poDt;
	}
	
	public String getOrdPcs() {
		return ordPcs;
	}
	public void setOrdPcs(String ordPcs) {
		this.ordPcs = ordPcs;
	}
	
	public String getOrdMsr() {
		return ordMsr;
	}
	public void setOrdMsr(String ordMsr) {
		this.ordMsr = ordMsr;
	}
	
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	
	public String getUm() {
		return um;
	}
	public void setUm(String um) {
		this.um = um;
	}
	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
	public String getCustItem() {
		return custItem;
	}
	public void setCustItem(String custItem) {
		this.custItem = custItem;
	}
	
	public String getDelDt() {
		return delDt;
	}
	public void setDelDt(String delDt) {
		this.delDt = delDt;
	}
	
	public String getCry() {
		return cry;
	}
	public void setCry(String cry) {
		this.cry = cry;
	}
	
	public BigDecimal getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	public String getPriceUm() {
		return priceUm;
	}
	public void setPriceUm(String priceUm) {
		this.priceUm = priceUm;
	}
	
	public BigDecimal getFrtChrg() {
		return frtChrg;
	}
	public void setFrtChrg(BigDecimal frtChrg) {
		this.frtChrg = frtChrg;
	}
	
	public BigDecimal getCstChrg() {
		return cstChrg;
	}
	public void setCstChrg(BigDecimal cstChrg) {
		this.cstChrg = cstChrg;
	}
	
	public int getVenId() {
		return venId;
	}
	public void setVenId(int venId) {
		this.venId = venId;
	}
	
	public int getItmNo() {
		return itmNo;
	}
	public void setItmNo(int itmNo) {
		this.itmNo = itmNo;
	}
	
	public String getTrmTrd() {
		return trmTrd;
	}
	public void setTrmTrd(String trmTrd) {
		this.trmTrd = trmTrd;
	}
	
	public String getDlvyMthd() {
		return dlvyMthd;
	}
	public void setDlvyMthd(String dlvyMthd) {
		this.dlvyMthd = dlvyMthd;
	}
	
	public BigDecimal getExrt() {
		return exrt;
	}
	public void setExrt(BigDecimal exrt) {
		this.exrt = exrt;
	}
	
	public String getExrtTyp() {
		return exrtTyp;
	}
	public void setExrtTyp(String exrtTyp) {
		this.exrtTyp = exrtTyp;
	}
	
	public String getFrtUm() {
		return frtUm;
	}
	public void setFrtUm(String frtUm) {
		this.frtUm = frtUm;
	}
	
	public Integer getDiscTrm() {
		return discTrm;
	}
	public void setDiscTrm(Integer discTrm) {
		this.discTrm = discTrm;
	}
	
	public Integer getPttrm() {
		return pttrm;
	}
	public void setPttrm(Integer pttrm) {
		this.pttrm = pttrm;
	}
	
	public String getMthdPmt() {
		return mthdPmt;
	}
	public void setMthdPmt(String mthdPmt) {
		this.mthdPmt = mthdPmt;
	}
	
	public String getSrcWhs() {
		return srcWhs;
	}
	public void setSrcWhs(String srcWhs) {
		this.srcWhs = srcWhs;
	}
	
	public String getPlngWhs() {
		return plngWhs;
	}
	public void setPlngWhs(String plngWhs) {
		this.plngWhs = plngWhs;
	}
	
	public String getShpgWhs() {
		return shpgWhs;
	}
	public void setShpgWhs(String shpgWhs) {
		this.shpgWhs = shpgWhs;
	}
	
	public String getHdrRmk() {
		return hdrRmk;
	}
	public void setHdrRmk(String hdrRmk) {
		this.hdrRmk = hdrRmk;
	}
	
	public String getBasLng() {
		return basLng;
	}
	public void setBasLng(String basLng) {
		this.basLng = basLng;
	}
	
	public String getFrm() {
		return frm;
	}
	public void setFrm(String frm) {
		this.frm = frm;
	}
	
	public String getGrd() {
		return grd;
	}
	public void setGrd(String grd) {
		this.grd = grd;
	}
	
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	
	public String getFnsh() {
		return fnsh;
	}
	public void setFnsh(String fnsh) {
		this.fnsh = fnsh;
	}
	
	public String getWdth() {
		return wdth;
	}
	public void setWdth(String wdth) {
		this.wdth = wdth;
	}
	
	public String getLgth() {
		return lgth;
	}
	public void setLgth(String lgth) {
		this.lgth = lgth;
	}
	
	public String getGaSize() {
		return gaSize;
	}
	public void setGaSize(String gaSize) {
		this.gaSize = gaSize;
	}
	
	public String getGaType() {
		return gaType;
	}
	public void setGaType(String gaType) {
		this.gaType = gaType;
	}
	
	public String getTolFlg() {
		return tolFlg;
	}
	public void setTolFlg(String tolFlg) {
		this.tolFlg = tolFlg;
	}
	
	public String getPkgFlg() {
		return pkgFlg;
	}
	public void setPkgFlg(String pkgFlg) {
		this.pkgFlg = pkgFlg;
	}
	
	public String getPkg() {
		return pkg;
	}
	public void setPkg(String pkg) {
		this.pkg = pkg;
	}
	
	public String getSkdTyp() {
		return skdTyp;
	}
	public void setSkdTyp(String skdTyp) {
		this.skdTyp = skdTyp;
	}
	
	public Integer getPcsPerTag() {
		return pcsPerTag;
	}
	public void setPcsPerTag(Integer pcsPerTag) {
		this.pcsPerTag = pcsPerTag;
	}
	
	public Integer getMinPcsPtag() {
		return minPcsPtag;
	}
	public void setMinPcsPtag(Integer minPcsPtag) {
		this.minPcsPtag = minPcsPtag;
	}
	
	public BigDecimal getMinPkgWgt() {
		return minPkgWgt;
	}
	public void setMinPkgWgt(BigDecimal minPkgWgt) {
		this.minPkgWgt = minPkgWgt;
	}
	
	public BigDecimal getMaxPkgWgt() {
		return maxPkgWgt;
	}
	public void setMaxPkgWgt(BigDecimal maxPkgWgt) {
		this.maxPkgWgt = maxPkgWgt;
	}
	
	public BigDecimal getMaxHgt() {
		return maxHgt;
	}
	public void setMaxHgt(BigDecimal maxHgt) {
		this.maxHgt = maxHgt;
	}
	
	public BigDecimal getWdthTolPosv() {
		return wdthTolPosv;
	}
	public void setWdthTolPosv(BigDecimal wdthTolPosv) {
		this.wdthTolPosv = wdthTolPosv;
	}
	
	public BigDecimal getWdthTolNeg() {
		return wdthTolNeg;
	}
	public void setWdthTolNeg(BigDecimal wdthTolNeg) {
		this.wdthTolNeg = wdthTolNeg;
	}
	
	public BigDecimal getLgthTolPosv() {
		return lgthTolPosv;
	}
	public void setLgthTolPosv(BigDecimal lgthTolPosv) {
		this.lgthTolPosv = lgthTolPosv;
	}
	
	public BigDecimal getLgthTolNeg() {
		return lgthTolNeg;
	}
	public void setLgthTolNeg(BigDecimal lgthTolNeg) {
		this.lgthTolNeg = lgthTolNeg;
	}
	
	public BigDecimal getIdiaTolPosv() {
		return idiaTolPosv;
	}
	public void setIdiaTolPosv(BigDecimal idiaTolPosv) {
		this.idiaTolPosv = idiaTolPosv;
	}
	
	public BigDecimal getIdiaTolNeg() {
		return idiaTolNeg;
	}
	public void setIdiaTolNeg(BigDecimal idiaTolNeg) {
		this.idiaTolNeg = idiaTolNeg;
	}
	
	public BigDecimal getOdiaTolPosv() {
		return odiaTolPosv;
	}
	public void setOdiaTolPosv(BigDecimal odiaTolPosv) {
		this.odiaTolPosv = odiaTolPosv;
	}
	
	public BigDecimal getOdiaTolNeg() {
		return odiaTolNeg;
	}
	public void setOdiaTolNeg(BigDecimal odiaTolNeg) {
		this.odiaTolNeg = odiaTolNeg;
	}public BigDecimal getWthckTolPosv() {
		return wthckTolPosv;
	}
	public void setWthckTolPosv(BigDecimal wthckTolPosv) {
		this.wthckTolPosv = wthckTolPosv;
	}
	
	public BigDecimal getWthckTolNeg() {
		return wthckTolNeg;
	}
	public void setWthckTolNeg(BigDecimal wthckTolNeg) {
		this.wthckTolNeg = wthckTolNeg;
	}
	
	public BigDecimal getGaTolPosv() {
		return gaTolPosv;
	}
	public void setGaTolPosv(BigDecimal gaTolPosv) {
		this.gaTolPosv = gaTolPosv;
	}
	
	public BigDecimal getGaTolNeg() {
		return gaTolNeg;
	}
	public void setGaTolNeg(BigDecimal gaTolNeg) {
		this.gaTolNeg = gaTolNeg;
	}
	
	public BigDecimal getDiagTol() {
		return diagTol;
	}
	public void setDiagTol(BigDecimal diagTol) {
		this.diagTol = diagTol;
	}
	
	public BigDecimal getFltns() {
		return fltns;
	}
	public void setFltns(BigDecimal fltns) {
		this.fltns = fltns;
	}
	
	public String getCmbr() {
		return cmbr;
	}
	public void setCmbr(String cmbr) {
		this.cmbr = cmbr;
	}
	public String getStndrdRoute() {
		return StndrdRoute;
	}
	public void setStndrdRoute(String stndrdRoute) {
		StndrdRoute = stndrdRoute;
	}
	public String getLgthType() {
		return lgthType;
	}
	public void setLgthType(String lgthType) {
		this.lgthType = lgthType;
	}
	public String getMemoPart() {
		return memoPart;
	}
	public void setMemoPart(String memoPart) {
		this.memoPart = memoPart;
	}
	public String getIdia() {
		return idia;
	}
	public void setIdia(String idia) {
		this.idia = idia;
	}
	public String getOdia() {
		return odia;
	}
	public void setOdia(String odia) {
		this.odia = odia;
	}
	public void setTrcCtlNo(int trcCtlNo) {
		this.trcCtlNo = trcCtlNo;
	}
	public void setLogCtlNo(int logCtlNo) {
		this.logCtlNo = logCtlNo;
	}
	public void setSoNo(int soNo) {
		this.soNo = soNo;
	}
	public void setDocId(int docId) {
		this.docId = docId;
	}
	public void setDocRowNo(int docRowNo) {
		this.docRowNo = docRowNo;
	}
	public void setDiscTrm(int discTrm) {
		this.discTrm = discTrm;
	}
	public void setPttrm(int pttrm) {
		this.pttrm = pttrm;
	}
	public String getOrdWgt() {
		return ordWgt;
	}
	public void setOrdWgt(String ordWgt) {
		this.ordWgt = ordWgt;
	}
	public String getOrdPfx() {
		return ordPfx;
	}
	public void setOrdPfx(String ordPfx) {
		this.ordPfx = ordPfx;
	}
	public String getMetalStd() {
		return metalStd;
	}
	public void setMetalStd(String metalStd) {
		this.metalStd = metalStd;
	}
	public List<SOSpecs> getMtlStdList() {
		return mtlStdList;
	}
	public void setMtlStdList(List<SOSpecs> mtlStdList) {
		this.mtlStdList = mtlStdList;
	}
	
	
	
}
