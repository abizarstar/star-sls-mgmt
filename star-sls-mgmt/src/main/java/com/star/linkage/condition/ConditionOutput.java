package com.star.linkage.condition;

import java.util.ArrayList;
import java.util.List;

public class ConditionOutput {

	private String usrId;
	private List<String> venList = new ArrayList<String>();
	private List<ConditionAmount> amtList = new ArrayList<ConditionAmount>();
	private String cond;
	public String getUsrId() {
		return usrId;
	}
	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}
	public List<String> getVenList() {
		return venList;
	}
	public void setVenList(List<String> venList) {
		this.venList = venList;
	}
	public List<ConditionAmount> getAmtList() {
		return amtList;
	}
	public void setAmtList(List<ConditionAmount> amtList) {
		this.amtList = amtList;
	}
	public String getCond() {
		return cond;
	}
	public void setCond(String cond) {
		this.cond = cond;
	}
	
	
	
}
