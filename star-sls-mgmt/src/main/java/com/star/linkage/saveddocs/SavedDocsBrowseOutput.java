package com.star.linkage.saveddocs;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class SavedDocsBrowseOutput extends BrowseOutput{
	public List<SavedDocs> fldTblSavedDocs = new ArrayList<SavedDocs>();
}
