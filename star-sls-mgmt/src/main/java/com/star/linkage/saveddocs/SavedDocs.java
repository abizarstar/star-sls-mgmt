package com.star.linkage.saveddocs;

public class SavedDocs {

	private String docId;
	private String docNm;
	private String docOrgNm;
	private String upldOn;
	private String upldBy;
	private String prsRowCnt;
	
	public String getDocId() {
		return docId;
	}
	public void setDocId(String docId) {
		this.docId = docId;
	}
	
	public String getDocNm() {
		return docNm;
	}
	public void setDocNm(String docNm) {
		this.docNm = docNm;
	}
	
	public String getDocOrgNm() {
		return docOrgNm;
	}
	public void setDocOrgNm(String docOrgNm) {
		this.docOrgNm = docOrgNm;
	}
	
	public String getUpldOn() {
		return upldOn;
	}
	public void setUpldOn(String upldOn) {
		this.upldOn = upldOn;
	}
	
	public String getUpldBy() {
		return upldBy;
	}
	public void setUpldBy(String upldBy) {
		this.upldBy = upldBy;
	}
	
	public String getPrsRowCnt() {
		return prsRowCnt;
	}
	public void setPrsRowCnt(String prsRowCnt) {
		this.prsRowCnt = prsRowCnt;
	}
	
}
