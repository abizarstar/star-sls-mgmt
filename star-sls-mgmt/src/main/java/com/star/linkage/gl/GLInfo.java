package com.star.linkage.gl;

public class GLInfo {

	String glAcct;
	String desc;
	String acctTyp;

	public String getGlAcct() {
		return glAcct;
	}
	public void setGlAcct(String glAcct) {
		this.glAcct = glAcct;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getAcctTyp() {
		return acctTyp;
	}
	public void setAcctTyp(String acctTyp) {
		this.acctTyp = acctTyp;
	}
}
