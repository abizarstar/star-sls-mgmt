package com.star.linkage.gl;

public class SubAcctComp {

	private String cmptId;
	private int cmptSeq;
	private int cmptLgth;
	public String getCmptId() {
		return cmptId;
	}
	public void setCmptId(String cmptId) {
		this.cmptId = cmptId;
	}
	public int getCmptSeq() {
		return cmptSeq;
	}
	public void setCmptSeq(int cmptSeq) {
		this.cmptSeq = cmptSeq;
	}
	public int getCmptLgth() {
		return cmptLgth;
	}
	public void setCmptLgth(int cmptLgth) {
		this.cmptLgth = cmptLgth;
	}
	
	
}
