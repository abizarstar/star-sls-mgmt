package com.star.linkage.gl;

public class SubAcctInfo {

	private String glAcct;
	private String subAcct;
	public String getGlAcct() {
		return glAcct;
	}
	public void setGlAcct(String glAcct) {
		this.glAcct = glAcct;
	}
	public String getSubAcct() {
		return subAcct;
	}
	public void setSubAcct(String subAcct) {
		this.subAcct = subAcct;
	}
	
	
}
