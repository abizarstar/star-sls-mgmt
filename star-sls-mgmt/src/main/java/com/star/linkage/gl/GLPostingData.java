package com.star.linkage.gl;

public class GLPostingData {

	private String postId;
	private String crAcct1;
	private String crAcct1Desc;
	private String crSubAcct1;
	private String crAcct2;
	private String crAcct2Desc;
	private String crSubAcct2;
	private String crAcct3;
	private String crAcct3Desc;
	private String crSubAcct3;
	
	private String drAcct1;
	private String drAcct1Desc;
	private String drSubAcct1;
	private String drAcct2;
	private String drAcct2Desc;
	private String drSubAcct2;
	private String drAcct3;
	private String drAcct3Desc;
	private String drSubAcct3;
	public String getCrAcct1() {
		return crAcct1;
	}
	public void setCrAcct1(String crAcct1) {
		this.crAcct1 = crAcct1;
	}
	public String getCrSubAcct1() {
		return crSubAcct1;
	}
	public void setCrSubAcct1(String crSubAcct1) {
		this.crSubAcct1 = crSubAcct1;
	}
	public String getCrAcct2() {
		return crAcct2;
	}
	public void setCrAcct2(String crAcct2) {
		this.crAcct2 = crAcct2;
	}
	public String getCrSubAcct2() {
		return crSubAcct2;
	}
	public void setCrSubAcct2(String crSubAcct2) {
		this.crSubAcct2 = crSubAcct2;
	}
	public String getCrAcct3() {
		return crAcct3;
	}
	public void setCrAcct3(String crAcct3) {
		this.crAcct3 = crAcct3;
	}
	public String getCrSubAcct3() {
		return crSubAcct3;
	}
	public void setCrSubAcct3(String crSubAcct3) {
		this.crSubAcct3 = crSubAcct3;
	}
	public String getDrAcct1() {
		return drAcct1;
	}
	public void setDrAcct1(String drAcct1) {
		this.drAcct1 = drAcct1;
	}
	public String getDrSubAcct1() {
		return drSubAcct1;
	}
	public void setDrSubAcct1(String drSubAcct1) {
		this.drSubAcct1 = drSubAcct1;
	}
	public String getDrAcct2() {
		return drAcct2;
	}
	public void setDrAcct2(String drAcct2) {
		this.drAcct2 = drAcct2;
	}
	public String getDrSubAcct2() {
		return drSubAcct2;
	}
	public void setDrSubAcct2(String drSubAcct2) {
		this.drSubAcct2 = drSubAcct2;
	}
	public String getDrAcct3() {
		return drAcct3;
	}
	public void setDrAcct3(String drAcct3) {
		this.drAcct3 = drAcct3;
	}
	public String getDrSubAcct3() {
		return drSubAcct3;
	}
	public void setDrSubAcct3(String drSubAcct3) {
		this.drSubAcct3 = drSubAcct3;
	}
	public String getCrAcct1Desc() {
		return crAcct1Desc;
	}
	public void setCrAcct1Desc(String crAcct1Desc) {
		this.crAcct1Desc = crAcct1Desc;
	}
	public String getCrAcct2Desc() {
		return crAcct2Desc;
	}
	public void setCrAcct2Desc(String crAcct2Desc) {
		this.crAcct2Desc = crAcct2Desc;
	}
	public String getCrAcct3Desc() {
		return crAcct3Desc;
	}
	public void setCrAcct3Desc(String crAcct3Desc) {
		this.crAcct3Desc = crAcct3Desc;
	}
	public String getDrAcct1Desc() {
		return drAcct1Desc;
	}
	public void setDrAcct1Desc(String drAcct1Desc) {
		this.drAcct1Desc = drAcct1Desc;
	}
	public String getDrAcct2Desc() {
		return drAcct2Desc;
	}
	public void setDrAcct2Desc(String drAcct2Desc) {
		this.drAcct2Desc = drAcct2Desc;
	}
	public String getDrAcct3Desc() {
		return drAcct3Desc;
	}
	public void setDrAcct3Desc(String drAcct3Desc) {
		this.drAcct3Desc = drAcct3Desc;
	}
	public String getPostId() {
		return postId;
	}
	public void setPostId(String postId) {
		this.postId = postId;
	}
	
	
	
}
