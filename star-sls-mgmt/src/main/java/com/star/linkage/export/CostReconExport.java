package com.star.linkage.export;

public class CostReconExport {

    private String poNo;
    private String brh;
    private String crcnNo;
    private String trsNo;
    private double origAmt;
    private double balAmt;
    private String cry;
	private double balQty;
    private String cost;
    private String whs;
    private String refNo;
    

   /* public CostReconExport(String poNo, String brh, String crcnNo, String trsNo, double origAmt, double balAmt, String cry, double balQty, String cost, String whs, String refNo) {
        this.poNo = poNo;
        this.brh = brh;
        this.crcnNo = crcnNo;
        this.trsNo = trsNo;
        this.origAmt = origAmt;
        this.balAmt = balAmt;
        this.cry = cry;
        this.balQty = balQty;
        this.cost = cost;
        this.whs = whs;
        this.refNo = refNo;
    }*/


	public String getPoNo() {
		return poNo;
	}


	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}


	public String getBrh() {
		return brh;
	}


	public void setBrh(String brh) {
		this.brh = brh;
	}


	public String getCrcnNo() {
		return crcnNo;
	}


	public void setCrcnNo(String crcnNo) {
		this.crcnNo = crcnNo;
	}


	public String getTrsNo() {
		return trsNo;
	}


	public void setTrsNo(String trsNo) {
		this.trsNo = trsNo;
	}


	public double getOrigAmt() {
		return origAmt;
	}


	public void setOrigAmt(double origAmt) {
		this.origAmt = origAmt;
	}


	public double getBalAmt() {
		return balAmt;
	}


	public void setBalAmt(double balAmt) {
		this.balAmt = balAmt;
	}


	public String getCry() {
		return cry;
	}


	public void setCry(String cry) {
		this.cry = cry;
	}


	public double getBalQty() {
		return balQty;
	}


	public void setBalQty(double balQty) {
		this.balQty = balQty;
	}


	public String getCost() {
		return cost;
	}


	public void setCost(String cost) {
		this.cost = cost;
	}


	public String getWhs() {
		return whs;
	}


	public void setWhs(String whs) {
		this.whs = whs;
	}


	public String getRefNo() {
		return refNo;
	}


	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

   
}
