package com.star.linkage.SalesOrder;

public class OdrInfoInput {
	
	private String cusPo;
	private int  poItm;
	private int  ctlNo; 
	private String  refPfx;
	private int  refNo;
	public String getCusPo() {
		return cusPo;
	}
	public void setCusPo(String cusPo) {
		this.cusPo = cusPo;
	}
	public int getPoItm() {
		return poItm;
	}
	public void setPoItm(int poItm) {
		this.poItm = poItm;
	}
	public int getCtlNo() {
		return ctlNo;
	}
	public void setCtlNo(int ctlNo) {
		this.ctlNo = ctlNo;
	}
	public String getRefPfx() {
		return refPfx;
	}
	public void setRefPfx(String refPfx) {
		this.refPfx = refPfx;
	}
	public int getRefNo() {
		return refNo;
	}
	public void setRefNo(int refNo) {
		this.refNo = refNo;
	}
	
}
