package com.star.linkage.SalesOrder;

public class TI00RecInput {
private String i00CmpyId;
private int i00IntchgNo;
private String userId;
public String getI00CmpyId() {
	return i00CmpyId;
}
public void setI00CmpyId(String i00CmpyId) {
	this.i00CmpyId = i00CmpyId;
}
public int getI00IntchgNo() {
	return i00IntchgNo;
}
public void setI00IntchgNo(int i00IntchgNo) {
	this.i00IntchgNo = i00IntchgNo;
}
public String getUserId() {
	return userId;
}
public void setUserId(String userId) {
	this.userId = userId;
}

}