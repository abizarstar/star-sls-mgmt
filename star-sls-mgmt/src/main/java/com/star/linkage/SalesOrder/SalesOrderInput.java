package com.star.linkage.SalesOrder;

import java.util.Date;

public class SalesOrderInput {
	private String s01CmpyId ;
	private String s01SrcCoId;
	private int s01TrcCtlNo;
	private String s01OrdBrh; 
	private String  s01OrdPfx; 
	private int s01SldCusId;
	private int s01ShpTo; 
	private String s01IsSlp; 
	private String s01OsSlp; 
	private String s01TknSlp; 
	private Date s01SlpFlwpDt; 
	private Date s01ExpyDt; 
	private int s01FlwpDy; 
	private int s01ExpyDy; 
	private int s01CrDtOvrd; 
	private int s01ShpItmTgth;
	private String s01BlndShptMthd;
	private String s01ShpgWhs; 
	private String s01OrdTyp;
	private String s01Cry;
	private double s01Exrt;
	private String s01ExRtTyp;
	private int s01Pttrm;
	private int s01DiscTrm;
	private String s01MthdPmt;
	private String s01SlsCat;
	private int s01CntrSls; 
	private int s01MultpRls;
	public String getS01CmpyId() {
		return s01CmpyId;
	}
	public void setS01CmpyId(String s01CmpyId) {
		this.s01CmpyId = s01CmpyId;
	}
	public String getS01SrcCoId() {
		return s01SrcCoId;
	}
	public void setS01SrcCoId(String s01SrcCoId) {
		this.s01SrcCoId = s01SrcCoId;
	}
	public int getS01TrcCtlNo() {
		return s01TrcCtlNo;
	}
	public void setS01TrcCtlNo(int s01TrcCtlNo) {
		this.s01TrcCtlNo = s01TrcCtlNo;
	}
	public String getS01OrdBrh() {
		return s01OrdBrh;
	}
	public void setS01OrdBrh(String s01OrdBrh) {
		this.s01OrdBrh = s01OrdBrh;
	}
	public String getS01OrdPfx() {
		return s01OrdPfx;
	}
	public void setS01OrdPfx(String s01OrdPfx) {
		this.s01OrdPfx = s01OrdPfx;
	}
	public int getS01SldCusId() {
		return s01SldCusId;
	}
	public void setS01SldCusId(int s01SldCusId) {
		this.s01SldCusId = s01SldCusId;
	}
	public int getS01ShpTo() {
		return s01ShpTo;
	}
	public void setS01ShpTo(int s01ShpTo) {
		this.s01ShpTo = s01ShpTo;
	}
	public String getS01IsSlp() {
		return s01IsSlp;
	}
	public void setS01IsSlp(String s01IsSlp) {
		this.s01IsSlp = s01IsSlp;
	}
	public String getS01OsSlp() {
		return s01OsSlp;
	}
	public void setS01OsSlp(String s01OsSlp) {
		this.s01OsSlp = s01OsSlp;
	}
	public String getS01TknSlp() {
		return s01TknSlp;
	}
	public void setS01TknSlp(String s01TknSlp) {
		this.s01TknSlp = s01TknSlp;
	}
	public Date getS01SlpFlwpDt() {
		return s01SlpFlwpDt;
	}
	public void setS01SlpFlwpDt(Date s01SlpFlwpDt) {
		this.s01SlpFlwpDt = s01SlpFlwpDt;
	}
	public Date getS01ExpyDt() {
		return s01ExpyDt;
	}
	public void setS01ExpyDt(Date s01ExpyDt) {
		this.s01ExpyDt = s01ExpyDt;
	}
	public int getS01FlwpDy() {
		return s01FlwpDy;
	}
	public void setS01FlwpDy(int s01FlwpDy) {
		this.s01FlwpDy = s01FlwpDy;
	}
	public int getS01ExpyDy() {
		return s01ExpyDy;
	}
	public void setS01ExpyDy(int s01ExpyDy) {
		this.s01ExpyDy = s01ExpyDy;
	}
	public int getS01CrDtOvrd() {
		return s01CrDtOvrd;
	}
	public void setS01CrDtOvrd(int s01CrDtOvrd) {
		this.s01CrDtOvrd = s01CrDtOvrd;
	}
	public int getS01ShpItmTgth() {
		return s01ShpItmTgth;
	}
	public void setS01ShpItmTgth(int s01ShpItmTgth) {
		this.s01ShpItmTgth = s01ShpItmTgth;
	}
	public String getS01BlndShptMthd() {
		return s01BlndShptMthd;
	}
	public void setS01BlndShptMthd(String s01BlndShptMthd) {
		this.s01BlndShptMthd = s01BlndShptMthd;
	}
	public String getS01ShpgWhs() {
		return s01ShpgWhs;
	}
	public void setS01ShpgWhs(String s01ShpgWhs) {
		this.s01ShpgWhs = s01ShpgWhs;
	}
	public String getS01OrdTyp() {
		return s01OrdTyp;
	}
	public void setS01OrdTyp(String s01OrdTyp) {
		this.s01OrdTyp = s01OrdTyp;
	}
	public String getS01Cry() {
		return s01Cry;
	}
	public void setS01Cry(String s01Cry) {
		this.s01Cry = s01Cry;
	}
	public double getS01Exrt() {
		return s01Exrt;
	}
	public void setS01Exrt(double s01Exrt) {
		this.s01Exrt = s01Exrt;
	}
	public String getS01ExRtTyp() {
		return s01ExRtTyp;
	}
	public void setS01ExRtTyp(String s01ExRtTyp) {
		this.s01ExRtTyp = s01ExRtTyp;
	}
	public int getS01Pttrm() {
		return s01Pttrm;
	}
	public void setS01Pttrm(int s01Pttrm) {
		this.s01Pttrm = s01Pttrm;
	}
	public int getS01DiscTrm() {
		return s01DiscTrm;
	}
	public void setS01DiscTrm(int s01DiscTrm) {
		this.s01DiscTrm = s01DiscTrm;
	}
	public String getS01MthdPmt() {
		return s01MthdPmt;
	}
	public void setS01MthdPmt(String s01MthdPmt) {
		this.s01MthdPmt = s01MthdPmt;
	}
	public String getS01SlsCat() {
		return s01SlsCat;
	}
	public void setS01SlsCat(String s01SlsCat) {
		this.s01SlsCat = s01SlsCat;
	}
	public int getS01CntrSls() {
		return s01CntrSls;
	}
	public void setS01CntrSls(int s01CntrSls) {
		this.s01CntrSls = s01CntrSls;
	}
	public int getS01MultpRls() {
		return s01MultpRls;
	}
	public void setS01MultpRls(int s01MultpRls) {
		this.s01MultpRls = s01MultpRls;
	}

}
