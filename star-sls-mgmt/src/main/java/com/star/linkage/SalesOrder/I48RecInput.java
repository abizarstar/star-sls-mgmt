package com.star.linkage.SalesOrder;

public class I48RecInput {

private String i48CmpyId;
private String i48SrcCoId;
private int i48TrcCtlNo;
private int i48TrcItm;
private int i48TrcSitm;
private String i48RlsGat;
private String i48StsGat;
private String i48RtngGat;
public String getI48CmpyId() {
	return i48CmpyId;
}
public void setI48CmpyId(String i48CmpyId) {
	this.i48CmpyId = i48CmpyId;
}
public String getI48SrcCoId() {
	return i48SrcCoId;
}
public void setI48SrcCoId(String i48SrcCoId) {
	this.i48SrcCoId = i48SrcCoId;
}
public int getI48TrcCtlNo() {
	return i48TrcCtlNo;
}
public void setI48TrcCtlNo(int i48TrcCtlNo) {
	this.i48TrcCtlNo = i48TrcCtlNo;
}
public int getI48TrcItm() {
	return i48TrcItm;
}
public void setI48TrcItm(int i48TrcItm) {
	this.i48TrcItm = i48TrcItm;
}
public int getI48TrcSitm() {
	return i48TrcSitm;
}
public void setI48TrcSitm(int i48TrcSitm) {
	this.i48TrcSitm = i48TrcSitm;
}
public String getI48RlsGat() {
	return i48RlsGat;
}
public void setI48RlsGat(String i48RlsGat) {
	this.i48RlsGat = i48RlsGat;
}
public String getI48StsGat() {
	return i48StsGat;
}
public void setI48StsGat(String i48StsGat) {
	this.i48StsGat = i48StsGat;
}
public String getI48RtngGat() {
	return i48RtngGat;
}
public void setI48RtngGat(String i48RtngGat) {
	this.i48RtngGat = i48RtngGat;
}


}
