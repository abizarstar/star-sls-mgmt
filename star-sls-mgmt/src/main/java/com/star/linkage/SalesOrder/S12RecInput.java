package com.star.linkage.SalesOrder;

public class S12RecInput {
	private String i25CmpyId;
	private String  i25IntchgPfx;
	private int  i25IntchgNo;
	private int  i25IntchgItm; 
	private String  i25SrcCoId;
	private int  i25TrcCtlNo;
	private int  i25TrcItm;
	private String  i25OrdPfx;
	private int  i25OrdNo;
	private int  i25OrdItm;
	private int  i25OrdSitm;
	private String  i25GatEntMd;
	private String  i25GatLvl;
	private int  i25OrdAsgnNo;
	public String getI25CmpyId() {
		return i25CmpyId;
	}
	public void setI25CmpyId(String i25CmpyId) {
		this.i25CmpyId = i25CmpyId;
	}
	public String getI25IntchgPfx() {
		return i25IntchgPfx;
	}
	public void setI25IntchgPfx(String i25IntchgPfx) {
		this.i25IntchgPfx = i25IntchgPfx;
	}
	public int getI25IntchgNo() {
		return i25IntchgNo;
	}
	public void setI25IntchgNo(int i25IntchgNo) {
		this.i25IntchgNo = i25IntchgNo;
	}
	public int getI25IntchgItm() {
		return i25IntchgItm;
	}
	public void setI25IntchgItm(int i25IntchgItm) {
		this.i25IntchgItm = i25IntchgItm;
	}
	public String getI25SrcCoId() {
		return i25SrcCoId;
	}
	public void setI25SrcCoId(String i25SrcCoId) {
		this.i25SrcCoId = i25SrcCoId;
	}
	public int getI25TrcCtlNo() {
		return i25TrcCtlNo;
	}
	public void setI25TrcCtlNo(int i25TrcCtlNo) {
		this.i25TrcCtlNo = i25TrcCtlNo;
	}
	public int getI25TrcItm() {
		return i25TrcItm;
	}
	public void setI25TrcItm(int i25TrcItm) {
		this.i25TrcItm = i25TrcItm;
	}
	public String getI25OrdPfx() {
		return i25OrdPfx;
	}
	public void setI25OrdPfx(String i25OrdPfx) {
		this.i25OrdPfx = i25OrdPfx;
	}
	public int getI25OrdNo() {
		return i25OrdNo;
	}
	public void setI25OrdNo(int i25OrdNo) {
		this.i25OrdNo = i25OrdNo;
	}
	public int getI25OrdItm() {
		return i25OrdItm;
	}
	public void setI25OrdItm(int i25OrdItm) {
		this.i25OrdItm = i25OrdItm;
	}
	public int getI25OrdSitm() {
		return i25OrdSitm;
	}
	public void setI25OrdSitm(int i25OrdSitm) {
		this.i25OrdSitm = i25OrdSitm;
	}
	public String getI25GatEntMd() {
		return i25GatEntMd;
	}
	public void setI25GatEntMd(String i25GatEntMd) {
		this.i25GatEntMd = i25GatEntMd;
	}
	public String getI25GatLvl() {
		return i25GatLvl;
	}
	public void setI25GatLvl(String i25GatLvl) {
		this.i25GatLvl = i25GatLvl;
	}
	public int getI25OrdAsgnNo() {
		return i25OrdAsgnNo;
	}
	public void setI25OrdAsgnNo(int i25OrdAsgnNo) {
		this.i25OrdAsgnNo = i25OrdAsgnNo;
	}
	
}
