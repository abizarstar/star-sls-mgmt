package com.star.linkage.SalesOrder;

public class SalesOrderInfo {

	private String cmpnyId;
	private String cusId;
	private String cusNm;
	private String ordNo;
	private String shpId;
	private String shpNm;
	private String po;
	private String inSlp;
	private String outSlp;
	private String tSlp;
	private String refNo;
	private String crtDt;
	private String flwUp;
	private String due;
	private String dm;
	private String tot;
	private String c;
	private String a;
	private String s;
	private String amt;
	
	public String getShpNm() {
		return shpNm;
	}
	public void setShpNm(String shpNm) {
		this.shpNm = shpNm;
	}
	public String getPo() {
		return po;
	}
	public void setPo(String po) {
		this.po = po;
	}
	public String getCmpnyId() {
		return cmpnyId;
	}
	public void setCmpnyId(String cmpnyId) {
		this.cmpnyId = cmpnyId;
	}
	public String getCusId() {
		return cusId;
	}
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	public String getOrdNo() {
		return ordNo;
	}
	public void setOrdNo(String ordNo) {
		this.ordNo = ordNo;
	}
	public String getCusNm() {
		return cusNm;
	}
	public void setCusNm(String cusNm) {
		this.cusNm = cusNm;
	}
	public String getShpId() {
		return shpId;
	}
	public void setShpId(String shpId) {
		this.shpId = shpId;
	}
	public String getInSlp() {
		return inSlp;
	}
	public void setInSlp(String inSlp) {
		this.inSlp = inSlp;
	}
	public String getOutSlp() {
		return outSlp;
	}
	public void setOutSlp(String outSlp) {
		this.outSlp = outSlp;
	}
	public String gettSlp() {
		return tSlp;
	}
	public void settSlp(String tSlp) {
		this.tSlp = tSlp;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getCrtDt() {
		return crtDt;
	}
	public void setCrtDt(String crtDt) {
		this.crtDt = crtDt;
	}
	public String getFlwUp() {
		return flwUp;
	}
	public void setFlwUp(String flwUp) {
		this.flwUp = flwUp;
	}
	public String getDue() {
		return due;
	}
	public void setDue(String due) {
		this.due = due;
	}
	public String getDm() {
		return dm;
	}
	public void setDm(String dm) {
		this.dm = dm;
	}
	public String getTot() {
		return tot;
	}
	public void setTot(String tot) {
		this.tot = tot;
	}
	public String getC() {
		return c;
	}
	public void setC(String c) {
		this.c = c;
	}
	public String getA() {
		return a;
	}
	public void setA(String a) {
		this.a = a;
	}
	public String getS() {
		return s;
	}
	public void setS(String s) {
		this.s = s;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	
	}
