package com.star.linkage.SalesOrder;

public class I26RecInput {

	private String i26CmpyId;
	private String i26SrcCoId;
	private int i26TrcCtlNo;
	private String i26HdrGat;
	private String i26NmAddrGat;
	private String i26CntcGat;
	private String i26FrtGat;
	private String i26TxGat;
	private String i26InstrGat;
	private String i26StsGat;
	public String getI26CmpyId() {
		return i26CmpyId;
	}
	public void setI26CmpyId(String i26CmpyId) {
		this.i26CmpyId = i26CmpyId;
	}
	public String getI26SrcCoId() {
		return i26SrcCoId;
	}
	public void setI26SrcCoId(String i26SrcCoId) {
		this.i26SrcCoId = i26SrcCoId;
	}
	public int getI26TrcCtlNo() {
		return i26TrcCtlNo;
	}
	public void setI26TrcCtlNo(int i26TrcCtlNo) {
		this.i26TrcCtlNo = i26TrcCtlNo;
	}
	public String getI26HdrGat() {
		return i26HdrGat;
	}
	public void setI26HdrGat(String i26HdrGat) {
		this.i26HdrGat = i26HdrGat;
	}
	public String getI26NmAddrGat() {
		return i26NmAddrGat;
	}
	public void setI26NmAddrGat(String i26NmAddrGat) {
		this.i26NmAddrGat = i26NmAddrGat;
	}
	public String getI26CntcGat() {
		return i26CntcGat;
	}
	public void setI26CntcGat(String i26CntcGat) {
		this.i26CntcGat = i26CntcGat;
	}
	public String getI26FrtGat() {
		return i26FrtGat;
	}
	public void setI26FrtGat(String i26FrtGat) {
		this.i26FrtGat = i26FrtGat;
	}
	public String getI26TxGat() {
		return i26TxGat;
	}
	public void setI26TxGat(String i26TxGat) {
		this.i26TxGat = i26TxGat;
	}
	public String getI26InstrGat() {
		return i26InstrGat;
	}
	public void setI26InstrGat(String i26InstrGat) {
		this.i26InstrGat = i26InstrGat;
	}
	public String getI26StsGat() {
		return i26StsGat;
	}
	public void setI26StsGat(String i26StsGat) {
		this.i26StsGat = i26StsGat;
	}
}
