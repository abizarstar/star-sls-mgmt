package com.star.linkage.SalesOrder;

import java.util.Date;

public class S16RecInput {
private String S16CmpyId;
private int S16TrcCtlNo;
private double S16RlsWgt;
private double S16RlsQty;
private String dueDtEnt;
private String rqstDtEnt;
public String getS16CmpyId() {
	return S16CmpyId;
}
public void setS16CmpyId(String s16CmpyId) {
	S16CmpyId = s16CmpyId;
}
public int getS16TrcCtlNo() {
	return S16TrcCtlNo;
}
public void setS16TrcCtlNo(int s16TrcCtlNo) {
	S16TrcCtlNo = s16TrcCtlNo;
}
public double getS16RlsWgt() {
	return S16RlsWgt;
}
public void setS16RlsWgt(double s16RlsWgt) {
	S16RlsWgt = s16RlsWgt;
}
public double getS16RlsQty() {
	return S16RlsQty;
}
public void setS16RlsQty(double s16RlsQty) {
	S16RlsQty = s16RlsQty;
}
public String getDueDtEnt() {
	return dueDtEnt;
}
public void setDueDtEnt(String dueDtEnt) {
	this.dueDtEnt = dueDtEnt;
}
public String getRqstDtEnt() {
	return rqstDtEnt;
}
public void setRqstDtEnt(String rqstDtEnt) {
	this.rqstDtEnt = rqstDtEnt;
}

}
