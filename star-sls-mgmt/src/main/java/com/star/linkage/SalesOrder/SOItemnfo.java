package com.star.linkage.SalesOrder;

public class SOItemnfo {

	private String cmpnyId;
	private String ordNo;
	private String ordItm;
	private String ordpcs;
	private String ordMsr;
	private String ordWgt;
	private String po;
	private String poDt;
	private String ipdFrm;
	private String ipdGrd;
	private String ipdSize;
	private String ipdfnsh;
	private String ipdPart;
	public String getCmpnyId() {
		return cmpnyId;
	}
	public void setCmpnyId(String cmpnyId) {
		this.cmpnyId = cmpnyId;
	}
	public String getOrdNo() {
		return ordNo;
	}
	public void setOrdNo(String ordNo) {
		this.ordNo = ordNo;
	}
	public String getOrdItm() {
		return ordItm;
	}
	public void setOrdItm(String ordItm) {
		this.ordItm = ordItm;
	}
	public String getOrdpcs() {
		return ordpcs;
	}
	public void setOrdpcs(String ordpcs) {
		this.ordpcs = ordpcs;
	}
	public String getOrdMsr() {
		return ordMsr;
	}
	public void setOrdMsr(String ordMsr) {
		this.ordMsr = ordMsr;
	}
	public String getOrdWgt() {
		return ordWgt;
	}
	public void setOrdWgt(String ordWgt) {
		this.ordWgt = ordWgt;
	}
	public String getPo() {
		return po;
	}
	public void setPo(String po) {
		this.po = po;
	}
	public String getPoDt() {
		return poDt;
	}
	public void setPoDt(String poDt) {
		this.poDt = poDt;
	}
	public String getIpdFrm() {
		return ipdFrm;
	}
	public void setIpdFrm(String ipdFrm) {
		this.ipdFrm = ipdFrm;
	}
	public String getIpdGrd() {
		return ipdGrd;
	}
	public void setIpdGrd(String ipdGrd) {
		this.ipdGrd = ipdGrd;
	}
	public String getIpdSize() {
		return ipdSize;
	}
	public void setIpdSize(String ipdSize) {
		this.ipdSize = ipdSize;
	}
	public String getIpdfnsh() {
		return ipdfnsh;
	}
	public void setIpdfnsh(String ipdfnsh) {
		this.ipdfnsh = ipdfnsh;
	}
	public String getIpdPart() {
		return ipdPart;
	}
	public void setIpdPart(String ipdPart) {
		this.ipdPart = ipdPart;
	}
	
}
