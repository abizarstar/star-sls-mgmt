package com.star.linkage.excel;

public class ProductSpecs {

	private String eOdDflt;
	private String mOdDflt;
	
	public String geteOdDflt() {
		return eOdDflt;
	}
	public void seteOdDflt(String eOdDflt) {
		this.eOdDflt = eOdDflt;
	}
	public String getmOdDflt() {
		return mOdDflt;
	}
	public void setmOdDflt(String mOdDflt) {
		this.mOdDflt = mOdDflt;
	}
	
	
}
