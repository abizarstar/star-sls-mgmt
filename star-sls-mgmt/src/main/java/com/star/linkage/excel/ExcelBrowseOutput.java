package com.star.linkage.excel;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class ExcelBrowseOutput extends BrowseOutput{
	public List<Excel> fldTblExcelheader = new ArrayList<Excel>();
	public List<Excel> fldTblExcelData = new ArrayList<Excel>();
	public int docId;
}
