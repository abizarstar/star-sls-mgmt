package com.star.linkage.excel;

public class ProductInfo {

	private int flg;
	private String cps;
	private String frm;
	private String grd;
	private String size;
	private String fnsh;
	private String wdth;
	private String lgth;
	private String gaSize;
	private String gaType;
	private String slsCat;
	private String src;
	private String adminBrh;
	
	public int getFlg() {
		return flg;
	}
	public void setFlg(int flg) {
		this.flg = flg;
	}
	
	public String getCps() {
		return cps;
	}
	public void setCps(String cps) {
		this.cps = cps;
	}
	
	public String getFrm() {
		return frm;
	}
	public void setFrm(String frm) {
		this.frm = frm;
	}
	
	public String getGrd() {
		return grd;
	}
	public void setGrd(String grd) {
		this.grd = grd;
	}
	
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	
	public String getFnsh() {
		return fnsh;
	}
	public void setFnsh(String fnsh) {
		this.fnsh = fnsh;
	}
	
	public String getWdth() {
		return wdth;
	}
	public void setWdth(String wdth) {
		this.wdth = wdth;
	}
	
	public String getLgth() {
		return lgth;
	}
	public void setLgth(String lgth) {
		this.lgth = lgth;
	}
	
	public String getGaSize() {
		return gaSize;
	}
	public void setGaSize(String gaSize) {
		this.gaSize = gaSize;
	}
	
	public String getGaType() {
		return gaType;
	}
	public void setGaType(String gaType) {
		this.gaType = gaType;
	}
	
	public String getSlsCat() {
		return slsCat;
	}
	public void setSlsCat(String slsCat) {
		this.slsCat = slsCat;
	}
	
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}
	
	public String getAdminBrh() {
		return adminBrh;
	}
	public void setAdminBrh(String adminBrh) {
		this.adminBrh = adminBrh;
	}
	
}
