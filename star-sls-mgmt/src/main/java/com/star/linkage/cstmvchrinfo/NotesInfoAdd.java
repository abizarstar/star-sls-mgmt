package com.star.linkage.cstmvchrinfo;

import java.util.Date;

public class NotesInfoAdd {

	private int noteId;
	private String usrNm;
	private Date crtdOnStr;
	private String note;
	
	public int getNoteId() {
		return noteId;
	}
	public void setNoteId(int noteId) {
		this.noteId = noteId;
	}
	public String getUsrNm() {
		return usrNm;
	}
	public void setUsrNm(String usrNm) {
		this.usrNm = usrNm;
	}

	public Date getCrtdOnStr() {
		return crtdOnStr;
	}
	public void setCrtdOnStr(Date crtdOnStr) {
		this.crtdOnStr = crtdOnStr;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	
}
