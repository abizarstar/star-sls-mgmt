package com.star.linkage.cstmvchrinfo;

import java.util.Date;

public class NotesInfo {

	private String noteId;
	private String usrId;
	private String usrNm;
	private String crtdOnStr;
	private String note;
	
	public String getNoteId() {
		return noteId;
	}
	public void setNoteId(String noteId) {
		this.noteId = noteId;
	}
	
	public String getUsrId() {
		return usrId;
	}
	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}
	
	public String getUsrNm() {
		return usrNm;
	}
	public void setUsrNm(String usrNm) {
		this.usrNm = usrNm;
	}
	
	public String getCrtdOnStr() {
		return crtdOnStr;
	}
	public void setCrtdOnStr(String crtdOnStr) {
		this.crtdOnStr = crtdOnStr;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	
}
