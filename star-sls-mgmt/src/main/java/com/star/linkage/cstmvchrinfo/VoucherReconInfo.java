package com.star.linkage.cstmvchrinfo;

public class VoucherReconInfo {

	private int poNo;
	private int poItm;
	private int crcnNo;
	private String brh;
	private String trsPfx;
	private int trsNo;
	private int trsItm;
	private int trnsbItm;
	private double balQty;
	private double origAmt;
	private String origAmtStr;
	private double balAmt;
	private String balAmtStr;
	private String actvyDtStr;
	private int costNo;
	private String costDesc;
	private String refVoucher;
	private String whs;
	private String cry;
	
	public int getPoNo() {
		return poNo;
	}
	public void setPoNo(int poNo) {
		this.poNo = poNo;
	}
	public int getPoItm() {
		return poItm;
	}
	public void setPoItm(int poItm) {
		this.poItm = poItm;
	}
	public int getCrcnNo() {
		return crcnNo;
	}
	public void setCrcnNo(int crcnNo) {
		this.crcnNo = crcnNo;
	}
	public String getBrh() {
		return brh;
	}
	public void setBrh(String brh) {
		this.brh = brh;
	}
	public String getTrsPfx() {
		return trsPfx;
	}
	public void setTrsPfx(String trsPfx) {
		this.trsPfx = trsPfx;
	}
	public int getTrsNo() {
		return trsNo;
	}
	public void setTrsNo(int trsNo) {
		this.trsNo = trsNo;
	}
	public int getTrsItm() {
		return trsItm;
	}
	public void setTrsItm(int trsItm) {
		this.trsItm = trsItm;
	}
	public int getTrnsbItm() {
		return trnsbItm;
	}
	public void setTrnsbItm(int trnsbItm) {
		this.trnsbItm = trnsbItm;
	}
	public double getBalQty() {
		return balQty;
	}
	public void setBalQty(double balQty) {
		this.balQty = balQty;
	}
	public double getOrigAmt() {
		return origAmt;
	}
	public void setOrigAmt(double origAmt) {
		this.origAmt = origAmt;
	}
	public double getBalAmt() {
		return balAmt;
	}
	public void setBalAmt(double balAmt) {
		this.balAmt = balAmt;
	}
	public String getActvyDtStr() {
		return actvyDtStr;
	}
	public void setActvyDtStr(String actvyDtStr) {
		this.actvyDtStr = actvyDtStr;
	}
	public int getCostNo() {
		return costNo;
	}
	public void setCostNo(int costNo) {
		this.costNo = costNo;
	}
	public String getCostDesc() {
		return costDesc;
	}
	public void setCostDesc(String costDesc) {
		this.costDesc = costDesc;
	}
	public String getRefVoucher() {
		return refVoucher;
	}
	public void setRefVoucher(String refVoucher) {
		this.refVoucher = refVoucher;
	}
	public String getOrigAmtStr() {
		return origAmtStr;
	}
	public void setOrigAmtStr(String origAmtStr) {
		this.origAmtStr = origAmtStr;
	}
	public String getBalAmtStr() {
		return balAmtStr;
	}
	public void setBalAmtStr(String balAmtStr) {
		this.balAmtStr = balAmtStr;
	}
	public String getWhs() {
		return whs;
	}
	public void setWhs(String whs) {
		this.whs = whs;
	}
	public String getCry() {
		return cry;
	}
	public void setCry(String cry) {
		this.cry = cry;
	}
	
	
	
}
