package com.star.linkage.cstmvchrinfo;

import java.util.Date;

public class CstmVchrInfoInput {
	
	private int  vchrCtlNo ;
	private String vchrInvNo ;
	private String vchrVenId;
	private String vchrVenNm;
	private String vchrBrh;
	private double vchrAmt;
	private String vchrExtRef;
    private Date  vchrInvDt;
    private Date  vchrDueDt;
    private String vchrPayTerm ;
    private String vchrCry ;
    
	
	public int getVchrCtlNo() {
		return vchrCtlNo;
	}
	public void setVchrCtlNo(int vchrCtlNo) {
		this.vchrCtlNo = vchrCtlNo;
	}
	public String getVchrInvNo() {
		return vchrInvNo;
	}
	public void setVchrInvNo(String vchrInvNo) {
		this.vchrInvNo = vchrInvNo;
	}
	public String getVchrVenId() {
		return vchrVenId;
	}
	public void setVchrVenId(String vchrVenId) {
		this.vchrVenId = vchrVenId;
	}
	public String getVchrVenNm() {
		return vchrVenNm;
	}
	public void setVchrVenNm(String vchrVenNm) {
		this.vchrVenNm = vchrVenNm;
	}
	public String getVchrBrh() {
		return vchrBrh;
	}
	public void setVchrBrh(String vchrBrh) {
		this.vchrBrh = vchrBrh;
	}
	public double getVchrAmt() {
		return vchrAmt;
	}
	public void setVchrAmt(double vchrAmt) {
		this.vchrAmt = vchrAmt;
	}
	public String getVchrExtRef() {
		return vchrExtRef;
	}
	public void setVchrExtRef(String vchrExtRef) {
		this.vchrExtRef = vchrExtRef;
	}
	public Date getVchrInvDt() {
		return vchrInvDt;
	}
	public void setVchrInvDt(Date vchrInvDt) {
		this.vchrInvDt = vchrInvDt;
	}
	public Date getVchrDueDt() {
		return vchrDueDt;
	}
	public void setVchrDueDt(Date vchrDueDt) {
		this.vchrDueDt = vchrDueDt;
	}
	public String getVchrPayTerm() {
		return vchrPayTerm;
	}
	public void setVchrPayTerm(String vchrPayTerm) {
		this.vchrPayTerm = vchrPayTerm;
	}
    
	public String getVchrCry() {
		return vchrCry;
	}
	public void setVchrCry(String vchrCry) {
		this.vchrCry = vchrCry;
	}

}
