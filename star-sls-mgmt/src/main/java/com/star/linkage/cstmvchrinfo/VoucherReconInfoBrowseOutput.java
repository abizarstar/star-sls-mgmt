package com.star.linkage.cstmvchrinfo;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class VoucherReconInfoBrowseOutput extends BrowseOutput{

	public List<VoucherReconInfo> fldTblRecon = new ArrayList<VoucherReconInfo>();
}
