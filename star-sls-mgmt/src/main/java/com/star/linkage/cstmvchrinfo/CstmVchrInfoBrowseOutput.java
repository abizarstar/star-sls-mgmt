package com.star.linkage.cstmvchrinfo;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;
import com.star.modal.CstmVchrInfo;


public class CstmVchrInfoBrowseOutput extends BrowseOutput{

	public List<CstmVchrInfo> fldTblDoc = new ArrayList<CstmVchrInfo>();
	public List<VoucherHeader> fldTblHdrList = new ArrayList<VoucherHeader>();
}
