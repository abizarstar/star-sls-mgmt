package com.star.linkage.auth;

public class UserDetailsInput {

	private String usrId;
	private String usrNm;
	private String password;
	private String eml;
	private String usrType;
	private String usrGrp;
	private String chkSm;
	private String param;
	private String nginxURL;

	
	public String getUsrId() {
		return usrId;
	}
	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}
	public String getUsrNm() {
		return usrNm;
	}
	public void setUsrNm(String usrNm) {
		this.usrNm = usrNm;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEml() {
		return eml;
	}
	public void setEml(String eml) {
		this.eml = eml;
	}
	public String getUsrType() {
		return usrType;
	}
	public void setUsrType(String usrType) {
		this.usrType = usrType;
	}
	public String getUsrGrp() {
		return usrGrp;
	}
	public void setUsrGrp(String usrGrp) {
		this.usrGrp = usrGrp;
	}
	public String getChkSm() {
		return chkSm;
	}
	public void setChkSm(String chkSm) {
		this.chkSm = chkSm;
	}
	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}
	public String getNginxURL() {
		return nginxURL;
	}
	public void setNginxURL(String nginxURL) {
		this.nginxURL = nginxURL;
	}
	
	
}
