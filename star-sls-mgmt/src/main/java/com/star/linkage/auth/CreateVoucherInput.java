package com.star.linkage.auth;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.datatype.XMLGregorianCalendar;

public class CreateVoucherInput {
	
	     String companyId;
	     String voucherPrefix;
	     Integer voucherNumber;
	     Integer sessionId;
	     String entryDate;
	     String vendorId;
	     String vendorInvoiceNumber;
	     String extenralReference;
	     Integer materialTransferNumber;
	     String voyageNumber;
	     String vendorInvoiceDate;
	     String purchaseOrderPrefix;
	     Integer purchaseOrderNumber;
	     Integer purchaseOrderItem;
	     String voucherBranch;
	     BigDecimal pretaxVoucherAmount;
	     BigDecimal voucherAmount;
	     BigDecimal discountableAmount;
	     String voucherDescription;
	     String voucherCurrency;
	     BigDecimal exchangeRate;
	     Integer paymentTerm;
	     Integer discountTerm;
	     String dueDate;
	     String discountDate;
	     BigDecimal discountAmount;
	     String checkItemRemarks;
	     String paymentType;
	     Integer voucherCrossRefNo;
	     String authorizationReference;
	     String voucherCategory;
	     String serviceFulfillmentDate;
	     Integer prepaymentEligibility;
	     String transactionStatusAction;
	     String transactionReason;
	     String transactionStatus;
	     String transactionStatusRemarks;
	     String paymentStatusAction;
	     String paymentReason;
	     String paymentStatus;
	     String paymentStatusRemarks;
	     String userId;
	     String authToken;
	     String host;
	     String port;
		public String getCompanyId() {
			return companyId;
		}
		public void setCompanyId(String companyId) {
			this.companyId = companyId;
		}
		public String getVoucherPrefix() {
			return voucherPrefix;
		}
		public void setVoucherPrefix(String voucherPrefix) {
			this.voucherPrefix = voucherPrefix;
		}
		public Integer getVoucherNumber() {
			return voucherNumber;
		}
		public void setVoucherNumber(Integer voucherNumber) {
			this.voucherNumber = voucherNumber;
		}
		public Integer getSessionId() {
			return sessionId;
		}
		public void setSessionId(Integer sessionId) {
			this.sessionId = sessionId;
		}
		public String getEntryDate() {
			return entryDate;
		}
		public void setEntryDate(String entryDate) {
			this.entryDate = entryDate;
		}
		public String getVendorId() {
			return vendorId;
		}
		public void setVendorId(String vendorId) {
			this.vendorId = vendorId;
		}
		public String getVendorInvoiceNumber() {
			return vendorInvoiceNumber;
		}
		public void setVendorInvoiceNumber(String vendorInvoiceNumber) {
			this.vendorInvoiceNumber = vendorInvoiceNumber;
		}
		public String getExtenralReference() {
			return extenralReference;
		}
		public void setExtenralReference(String extenralReference) {
			this.extenralReference = extenralReference;
		}
		public Integer getMaterialTransferNumber() {
			return materialTransferNumber;
		}
		public void setMaterialTransferNumber(Integer materialTransferNumber) {
			this.materialTransferNumber = materialTransferNumber;
		}
		public String getVoyageNumber() {
			return voyageNumber;
		}
		public void setVoyageNumber(String voyageNumber) {
			this.voyageNumber = voyageNumber;
		}
		public String getVendorInvoiceDate() {
			return vendorInvoiceDate;
		}
		public void setVendorInvoiceDate(String vendorInvoiceDate) {
			this.vendorInvoiceDate = vendorInvoiceDate;
		}
		public String getPurchaseOrderPrefix() {
			return purchaseOrderPrefix;
		}
		public void setPurchaseOrderPrefix(String purchaseOrderPrefix) {
			this.purchaseOrderPrefix = purchaseOrderPrefix;
		}
		public Integer getPurchaseOrderNumber() {
			return purchaseOrderNumber;
		}
		public void setPurchaseOrderNumber(Integer purchaseOrderNumber) {
			this.purchaseOrderNumber = purchaseOrderNumber;
		}
		public Integer getPurchaseOrderItem() {
			return purchaseOrderItem;
		}
		public void setPurchaseOrderItem(Integer purchaseOrderItem) {
			this.purchaseOrderItem = purchaseOrderItem;
		}
		public String getVoucherBranch() {
			return voucherBranch;
		}
		public void setVoucherBranch(String voucherBranch) {
			this.voucherBranch = voucherBranch;
		}
		public BigDecimal getPretaxVoucherAmount() {
			return pretaxVoucherAmount;
		}
		public void setPretaxVoucherAmount(BigDecimal pretaxVoucherAmount) {
			this.pretaxVoucherAmount = pretaxVoucherAmount;
		}
		public BigDecimal getVoucherAmount() {
			return voucherAmount;
		}
		public void setVoucherAmount(BigDecimal voucherAmount) {
			this.voucherAmount = voucherAmount;
		}
		public BigDecimal getDiscountableAmount() {
			return discountableAmount;
		}
		public void setDiscountableAmount(BigDecimal discountableAmount) {
			this.discountableAmount = discountableAmount;
		}
		public String getVoucherDescription() {
			return voucherDescription;
		}
		public void setVoucherDescription(String voucherDescription) {
			this.voucherDescription = voucherDescription;
		}
		public String getVoucherCurrency() {
			return voucherCurrency;
		}
		public void setVoucherCurrency(String voucherCurrency) {
			this.voucherCurrency = voucherCurrency;
		}
		public BigDecimal getExchangeRate() {
			return exchangeRate;
		}
		public void setExchangeRate(BigDecimal exchangeRate) {
			this.exchangeRate = exchangeRate;
		}
		public Integer getPaymentTerm() {
			return paymentTerm;
		}
		public void setPaymentTerm(Integer paymentTerm) {
			this.paymentTerm = paymentTerm;
		}
		public Integer getDiscountTerm() {
			return discountTerm;
		}
		public void setDiscountTerm(Integer discountTerm) {
			this.discountTerm = discountTerm;
		}
		public String getDueDate() {
			return dueDate;
		}
		public void setDueDate(String dueDate) {
			this.dueDate = dueDate;
		}
		public String getDiscountDate() {
			return discountDate;
		}
		public void setDiscountDate(String discountDate) {
			this.discountDate = discountDate;
		}
		public BigDecimal getDiscountAmount() {
			return discountAmount;
		}
		public void setDiscountAmount(BigDecimal discountAmount) {
			this.discountAmount = discountAmount;
		}
		public String getCheckItemRemarks() {
			return checkItemRemarks;
		}
		public void setCheckItemRemarks(String checkItemRemarks) {
			this.checkItemRemarks = checkItemRemarks;
		}
		public String getPaymentType() {
			return paymentType;
		}
		public void setPaymentType(String paymentType) {
			this.paymentType = paymentType;
		}
		public Integer getVoucherCrossRefNo() {
			return voucherCrossRefNo;
		}
		public void setVoucherCrossRefNo(Integer voucherCrossRefNo) {
			this.voucherCrossRefNo = voucherCrossRefNo;
		}
		public String getAuthorizationReference() {
			return authorizationReference;
		}
		public void setAuthorizationReference(String authorizationReference) {
			this.authorizationReference = authorizationReference;
		}
		public String getVoucherCategory() {
			return voucherCategory;
		}
		public void setVoucherCategory(String voucherCategory) {
			this.voucherCategory = voucherCategory;
		}
		public String getServiceFulfillmentDate() {
			return serviceFulfillmentDate;
		}
		public void setServiceFulfillmentDate(String serviceFulfillmentDate) {
			this.serviceFulfillmentDate = serviceFulfillmentDate;
		}
		public Integer getPrepaymentEligibility() {
			return prepaymentEligibility;
		}
		public void setPrepaymentEligibility(Integer prepaymentEligibility) {
			this.prepaymentEligibility = prepaymentEligibility;
		}
		public String getTransactionStatusAction() {
			return transactionStatusAction;
		}
		public void setTransactionStatusAction(String transactionStatusAction) {
			this.transactionStatusAction = transactionStatusAction;
		}
		public String getTransactionReason() {
			return transactionReason;
		}
		public void setTransactionReason(String transactionReason) {
			this.transactionReason = transactionReason;
		}
		public String getTransactionStatus() {
			return transactionStatus;
		}
		public void setTransactionStatus(String transactionStatus) {
			this.transactionStatus = transactionStatus;
		}
		public String getTransactionStatusRemarks() {
			return transactionStatusRemarks;
		}
		public void setTransactionStatusRemarks(String transactionStatusRemarks) {
			this.transactionStatusRemarks = transactionStatusRemarks;
		}
		public String getPaymentStatusAction() {
			return paymentStatusAction;
		}
		public void setPaymentStatusAction(String paymentStatusAction) {
			this.paymentStatusAction = paymentStatusAction;
		}
		public String getPaymentReason() {
			return paymentReason;
		}
		public void setPaymentReason(String paymentReason) {
			this.paymentReason = paymentReason;
		}
		public String getPaymentStatus() {
			return paymentStatus;
		}
		public void setPaymentStatus(String paymentStatus) {
			this.paymentStatus = paymentStatus;
		}
		public String getPaymentStatusRemarks() {
			return paymentStatusRemarks;
		}
		public void setPaymentStatusRemarks(String paymentStatusRemarks) {
			this.paymentStatusRemarks = paymentStatusRemarks;
		}
		public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public String getAuthToken() {
			return authToken;
		}
		public void setAuthToken(String authToken) {
			this.authToken = authToken;
		}
		public String getHost() {
			return host;
		}
		public void setHost(String host) {
			this.host = host;
		}
		public String getPort() {
			return port;
		}
		public void setPort(String port) {
			this.port = port;
		}
	     
	     
	     
	  }
