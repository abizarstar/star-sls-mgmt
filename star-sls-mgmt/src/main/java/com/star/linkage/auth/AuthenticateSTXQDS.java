package com.star.linkage.auth;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.invera.stratix.ws.exec.authentication.AuthService;
import com.invera.stratix.ws.exec.authentication.GtwyLgnReqType;
import com.invera.stratix.ws.exec.authentication.GtwyLgnResponseType;
import com.invera.stratix.ws.services.security.LogoutService;
import com.star.common.BrowseResponse;
import com.ws.samples.ServiceHelper;
import com.ws.samples.handler.MsgSOAPHandler;
import com.ws.samples.handler.SecSOAPHandler;

public class AuthenticateSTXQDS {

	/**
	 * @param args
	 */
	public String stxAuth(String userId, String password, String execPort) throws MalformedURLException {
		// TODO Auto-generated method stub

		String execServer = System.getenv("PRI_EXECSRVR");

		ServiceHelper.setWpaEndpointProtocol("http", "");

		System.err.println("EXEC SERVER - " + execServer);
		System.err.println("EXEC PORT - " + execPort);

		if (execServer != null) {
			ServiceHelper.setWpaEndpointHostAndPort(execServer, Integer.parseInt(execPort));
		} else {
			ServiceHelper.setWpaEndpointHostAndPort("172.20.35.73", Integer.parseInt(execPort));
		}

		MsgSOAPHandler messageHandler = new MsgSOAPHandler();

		int loginFlg = 0;
		List<String> messages = new ArrayList<String>();

		try {

			validateLogin(messageHandler, userId, password);

			loginFlg = 1;

		} catch (com.invera.stratix.ws.exec.authentication.StratixFault fault) {

			messages = printMessages(messageHandler);

			loginFlg = 0;
		}

		Gson gson = new GsonBuilder().create();
		JsonArray myCustomArray = gson.toJsonTree(messages).getAsJsonArray();
		JsonObject jsonObject = new JsonObject();
		jsonObject.add("msg", myCustomArray);

		jsonObject.addProperty("sts", loginFlg);
		jsonObject.addProperty("role", "NA");

		return jsonObject.toString();

	}

	public GtwyLgnResponseType stxAuthToken(String userId, String password, String execPort, String execHost,
			String execEnv, String execClass, String nginxURL, BrowseResponse<AuthBrowseOutput> starBrowseResponse) throws MalformedURLException {
		// TODO Auto-generated method stub

		GtwyLgnResponseType gatewayLoginResponseType = null;

		String execServer = System.getenv("PRI_EXECSRVR");

		ServiceHelper.setWpaEndpointProtocol("http", nginxURL);

		System.err.println("EXEC SERVER - " + execServer);
		System.err.println("EXEC PORT - " + execPort);

		if (execServer != null) {
			ServiceHelper.setWpaEndpointHostAndPort(execServer, Integer.parseInt(execPort));
		} else {
			ServiceHelper.setWpaEndpointHostAndPort(execHost, Integer.parseInt(execPort));
		}

		MsgSOAPHandler messageHandler = new MsgSOAPHandler();

		List<String> messages = new ArrayList<String>();

		try {

			gatewayLoginResponseType = login(messageHandler, userId, password, execEnv, execClass);

			System.err.println("EXEC TOKEN - " + gatewayLoginResponseType.getAuthenticationToken().getValue());

		} catch (com.invera.stratix.ws.exec.authentication.StratixFault fault) {
			
			messages = printMessages(messageHandler);
			
			for(int i = 0 ; i < messages.size(); i++)
			{
				starBrowseResponse.output.messages.add(messages.get(i));
			}
			
		}

		return gatewayLoginResponseType;

	}

	private void validateLogin(MsgSOAPHandler messageHandler, String userId, String password)
			throws MalformedURLException, com.invera.stratix.ws.exec.authentication.StratixFault {

		AuthService authenticationService = ServiceHelper.createAuthenticationService(messageHandler);

		GtwyLgnReqType input = new GtwyLgnReqType();

		input.setUsername(userId);
		input.setUsername(password);

		authenticationService.validate(userId, password);

	}

	private GtwyLgnResponseType login(MsgSOAPHandler messageHandler, String userId, String password,
			String execEnv, String execClass)
			throws MalformedURLException, com.invera.stratix.ws.exec.authentication.StratixFault {

		AuthService authenticationService = ServiceHelper.createAuthenticationService(messageHandler);

		String USER = System.getenv("USER");

		String ENV_CLASS = System.getenv("ENVCLASS");

		System.err.println("EXEC USER - " + USER);
		System.err.println("EXEC ENVCLASS - " + ENV_CLASS);

		GtwyLgnReqType loginRequest = new GtwyLgnReqType();

		if (USER == null) {
			loginRequest.setEnvironmentName(execEnv);
		} else {
			loginRequest.setEnvironmentName(USER);
		}

		if (ENV_CLASS == null) {
			loginRequest.setEnvironmentClass(execClass);
		} else {
			loginRequest.setEnvironmentClass(ENV_CLASS);
		}

		// Network Access - I: Internal, E: External
		loginRequest.setConnectedAccessType("I");

		loginRequest.setUsername(userId);
		loginRequest.setPassword(password);
		// Optional: Company ID
		// loginRequest.setCompanyId("APW");

		loginRequest.setForceDisconnect(Boolean.TRUE);

		return authenticationService.gatewayLogin(loginRequest);
	}

	private static List<String> printMessages(MsgSOAPHandler messageHandler) {

		List<String> messages = messageHandler.getMessageList();
		for (String message : messages) {
			if (message != null) {

				System.out.println("IN PRINT MESSAGE");
				System.out.println(message);
			}
		}
		return messages;

	}

	public void logout(MsgSOAPHandler messageHandler, SecSOAPHandler securityHandler)
			throws MalformedURLException, com.invera.stratix.ws.services.security.StratixFault {

		LogoutService logoutService = ServiceHelper.createLogoutService(messageHandler, securityHandler);
		logoutService.logout(securityHandler.getAuthenticationToken());
	}

}
