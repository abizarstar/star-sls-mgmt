package com.star.linkage.auth;

public class UserDetails {

	private String authToken;
	private Boolean authFlag;
	private String userNm;
	private String userId;
	private String emailId;
	private String usrTyp;
	private String usrGrp;
	private String usrGrpId;
	private String actvSts;
	private String chkSum;
	
	
	public String getAuthToken() {
		return authToken;
	}
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	public Boolean getAuthFlag() {
		return authFlag;
	}
	public void setAuthFlag(Boolean authFlag) {
		this.authFlag = authFlag;
	}
	public String getUserNm() {
		return userNm;
	}
	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public String getUsrTyp() {
		return usrTyp;
	}
	public void setUsrTyp(String usrTyp) {
		this.usrTyp = usrTyp;
	}
	
	public String getUsrGrp() {
		return usrGrp;
	}
	public void setUsrGrp(String usrGrp) {
		this.usrGrp = usrGrp;
	}
	
	public String getUsrGrpId() {
		return usrGrpId;
	}
	public void setUsrGrpId(String usrGrpId) {
		this.usrGrpId = usrGrpId;
	}
	
	public String getActvSts() {
		return actvSts;
	}
	public void setActvSts(String actvSts) {
		this.actvSts = actvSts;
	}
	public String getChkSum() {
		return chkSum;
	}
	public void setChkSum(String chkSum) {
		this.chkSum = chkSum;
	}
	
	
	
}
