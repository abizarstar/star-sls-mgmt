package com.star.linkage.auth;

import com.star.common.BrowseOutput;

public class UserDetailsBrowseOutput extends BrowseOutput {

	public String authToken;
	public Boolean authFlag;
	public String userNm;
	public String userId;
	public String emailId;
	public String usrTyp;
	public String usrRoles;
	public String usrGrp;
	public String reqTyp;
}
