package com.star.linkage.auth;

public class DeleteVoucherInput {
	 
	     String companyId;
	     String voucherPrefix;
	     int voucherNumber;
		
	     String userId;
	     String authToken;
	     String host;
	     String port;
	     
	     
	    public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public String getAuthToken() {
			return authToken;
		}
		public void setAuthToken(String authToken) {
			this.authToken = authToken;
		}
		public String getHost() {
			return host;
		}
		public void setHost(String host) {
			this.host = host;
		}
		public String getPort() {
			return port;
		}
		public void setPort(String port) {
			this.port = port;
		}
		public String getCompanyId() {
			return companyId;
		}
		public void setCompanyId(String companyId) {
			this.companyId = companyId;
		}
		public String getVoucherPrefix() {
			return voucherPrefix;
		}
		public void setVoucherPrefix(String voucherPrefix) {
			this.voucherPrefix = voucherPrefix;
		}
		public int getVoucherNumber() {
			return voucherNumber;
		}
		public void setVoucherNumber(int voucherNumber) {
			this.voucherNumber = voucherNumber;
		}
	
}
