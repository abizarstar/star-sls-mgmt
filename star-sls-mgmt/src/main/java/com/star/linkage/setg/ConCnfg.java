package com.star.linkage.setg;

public class ConCnfg {
	
	String driver;	
	String url;			
	String usr_nm;		
	String pass;		
	String doc_freq_min;
	
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUsr_nm() {
		return usr_nm;
	}
	public void setUsr_nm(String usr_nm) {
		this.usr_nm = usr_nm;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getDoc_freq_min() {
		return doc_freq_min;
	}
	public void setDoc_freq_min(String doc_freq_min) {
		this.doc_freq_min = doc_freq_min;
	}

}
