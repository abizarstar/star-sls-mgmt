package com.star.linkage.other;

public class MailInfoInput {
	
	String emlFrm;
	String emlFrmUsrNm;
	String emlTo;
	String emlCc;
	String emlBcc;
	String emlSub;
	String emlBody;
	String emlAttachIds;
	public String getEmlFrm() {
		return emlFrm;
	}
	public void setEmlFrm(String emlFrm) {
		this.emlFrm = emlFrm;
	}
	public String getEmlTo() {
		return emlTo;
	}
	public void setEmlTo(String emlTo) {
		this.emlTo = emlTo;
	}
	public String getEmlCc() {
		return emlCc;
	}
	public void setEmlCc(String emlCc) {
		this.emlCc = emlCc;
	}
	public String getEmlBcc() {
		return emlBcc;
	}
	public void setEmlBcc(String emlBcc) {
		this.emlBcc = emlBcc;
	}
	public String getEmlSub() {
		return emlSub;
	}
	public void setEmlSub(String emlSub) {
		this.emlSub = emlSub;
	}
	public String getEmlBody() {
		return emlBody;
	}
	public void setEmlBody(String emlBody) {
		this.emlBody = emlBody;
	}
	public String getEmlAttachIds() {
		return emlAttachIds;
	}
	public void setEmlAttachIds(String emlAttachIds) {
		this.emlAttachIds = emlAttachIds;
	}
	public String getEmlFrmUsrNm() {
		return emlFrmUsrNm;
	}
	public void setEmlFrmUsrNm(String emlFrmUsrNm) {
		this.emlFrmUsrNm = emlFrmUsrNm;
	}
	
	

}
