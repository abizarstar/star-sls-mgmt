package com.star.linkage.usrgrp;

public class UsrGrpBrowseOutput {

	private int grpId;
	private String grpNm;
	private String UsrCnt;

	public int getGrpId() {
		return grpId;
	}

	public void setGrpId(int grpId) {
		this.grpId = grpId;
	}

	public String getGrpNm() {
		return grpNm;
	}

	public void setGrpNm(String grpNm) {
		this.grpNm = grpNm;
	}

	public String getUsrCnt() {
		return UsrCnt;
	}

	public void setUsrCnt(String usrCnt) {
		UsrCnt = usrCnt;
	}

}
