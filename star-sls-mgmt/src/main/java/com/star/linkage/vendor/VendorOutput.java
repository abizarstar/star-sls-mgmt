package com.star.linkage.vendor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.star.linkage.common.PaymentMethod;
import com.star.modal.CstmVenBnkInfo;

public class VendorOutput {

	private String cmpyId;
	private String venId;
	private String payMthd;
	private String cty;
	private String venNm;
	private String venLongNm;
	private String cry;
	private String crtdBy;
	private Date crtdOn;
	
	private List<CstmVenBnkInfo> bnkList = new ArrayList<CstmVenBnkInfo>();
	private List<PaymentMethod> payMthdList = new ArrayList<PaymentMethod>();
	
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	public String getPayMthd() {
		return payMthd;
	}
	public void setPayMthd(String payMthd) {
		this.payMthd = payMthd;
	}
	public String getCty() {
		return cty;
	}
	public void setCty(String cty) {
		this.cty = cty;
	}
	public List<CstmVenBnkInfo> getBnkList() {
		return bnkList;
	}
	public void setBnkList(List<CstmVenBnkInfo> bnkList) {
		this.bnkList = bnkList;
	}
	public String getVenNm() {
		return venNm;
	}
	public void setVenNm(String venNm) {
		this.venNm = venNm;
	}
	public String getVenLongNm() {
		return venLongNm;
	}
	public void setVenLongNm(String venLongNm) {
		this.venLongNm = venLongNm;
	}
	public String getCry() {
		return cry;
	}
	public void setCry(String cry) {
		this.cry = cry;
	}
	public String getCrtdBy() {
		return crtdBy;
	}
	public void setCrtdBy(String crtdBy) {
		this.crtdBy = crtdBy;
	}
	public Date getCrtdOn() {
		return crtdOn;
	}
	public void setCrtdOn(Date crtdOn) {
		this.crtdOn = crtdOn;
	}

	public List<PaymentMethod> getPayMthdList() {
		return payMthdList;
	}
	public void setPayMthdList(List<PaymentMethod> payMthdList) {
		this.payMthdList = payMthdList;
	}
	
	
}
