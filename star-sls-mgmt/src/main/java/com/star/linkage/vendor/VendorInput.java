package com.star.linkage.vendor;

import java.util.ArrayList;
import java.util.List;

import com.star.linkage.common.PaymentMethod;

public class VendorInput {

	private String cmpyId;
	private String venId;
	private String cty;
	private String crtdBy;
	private String cry;
	private String pymntMtd;
	private String venNm;
	private String venLongNm;
	private List<BankAccount> bankList = new ArrayList<BankAccount>();
	private List<PaymentMethod> pymntMtdList = new ArrayList<PaymentMethod>();
	
	
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	
	public String getCty() {
		return cty;
	}
	public void setCty(String cty) {
		this.cty = cty;
	}
	public String getCrtdBy() {
		return crtdBy;
	}
	public void setCrtdBy(String crtdBy) {
		this.crtdBy = crtdBy;
	}
	public String getCry() {
		return cry;
	}
	public void setCry(String cry) {
		this.cry = cry;
	}
	public List<BankAccount> getBankList() {
		return bankList;
	}
	public void setBankList(List<BankAccount> bankList) {
		this.bankList = bankList;
	}
	public String getPymntMtd() {
		return pymntMtd;
	}
	public void setPymntMtd(String pymntMtd) {
		this.pymntMtd = pymntMtd;
	}
	public String getVenNm() {
		return venNm;
	}
	public void setVenNm(String venNm) {
		this.venNm = venNm;
	}
	public String getVenLongNm() {
		return venLongNm;
	}
	public void setVenLongNm(String venLongNm) {
		this.venLongNm = venLongNm;
	}
	public List<PaymentMethod> getPymntMtdList() {
		return pymntMtdList;
	}
	public void setPymntMtdList(List<PaymentMethod> pymntMtdList) {
		this.pymntMtdList = pymntMtdList;
	}
	
	
}
