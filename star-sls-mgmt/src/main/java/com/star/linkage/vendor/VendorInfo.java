package com.star.linkage.vendor;

public class VendorInfo {

	String cmpyId;
	String venId;
	String venNm;
	String venLongNm;
	String venCry;
	String venAdminBrh;
	String pmtTyp;
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	public String getVenNm() {
		return venNm;
	}
	public void setVenNm(String venNm) {
		this.venNm = venNm;
	}
	public String getVenLongNm() {
		return venLongNm;
	}
	public void setVenLongNm(String venLongNm) {
		this.venLongNm = venLongNm;
	}
	public String getVenCry() {
		return venCry;
	}
	public void setVenCry(String venCry) {
		this.venCry = venCry;
	}
	public String getVenAdminBrh() {
		return venAdminBrh;
	}
	public void setVenAdminBrh(String venAdminBrh) {
		this.venAdminBrh = venAdminBrh;
	}
	public String getPmtTyp() {
		return pmtTyp;
	}
	public void setPmtTyp(String pmtTyp) {
		this.pmtTyp = pmtTyp;
	}
	
	
	
}
