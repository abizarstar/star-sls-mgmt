package com.star.linkage.vendor;

public class VendorDefaultGlSettingInput {

	private String cmpyId;
	private String venId;
	private String bscGlAcct;
	private String sacct;
	private String sacctTxt;
	private String distRmk;
	
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	
	public String getBscGlAcct() {
		return bscGlAcct;
	}
	public void setBscGlAcct(String bscGlAcct) {
		this.bscGlAcct = bscGlAcct;
	}
	
	public String getSacct() {
		return sacct;
	}
	public void setSacct(String sacct) {
		this.sacct = sacct;
	}
	
	public String getSacctTxt() {
		return sacctTxt;
	}
	public void setSacctTxt(String sacctTxt) {
		this.sacctTxt = sacctTxt;
	}
	
	public String getDistRmk() {
		return distRmk;
	}
	public void setDistRmk(String distRmk) {
		this.distRmk = distRmk;
	}
	
}
