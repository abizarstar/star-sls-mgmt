package com.star.linkage.vendor;

import java.util.ArrayList;
import java.util.List;

public class VendorDefaultSettingInput {

	private String cmpyId;
	private String venId;
	private String vchrBrh;
	private String vchrCat;
	private String trsStsActn;
	private String trsSts;
	private String trsRsn;
	private String ntfUsr;
	private String isActive;
	private String crtdBy;
	private String payTerm;
	private String cry;
	
	private List<VendorDefaultGlSettingInput> glAcctList = new ArrayList<VendorDefaultGlSettingInput>();
	
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	
	public String getVchrBrh() {
		return vchrBrh;
	}
	public void setVchrBrh(String vchrBrh) {
		this.vchrBrh = vchrBrh;
	}
	
	public String getVchrCat() {
		return vchrCat;
	}
	public void setVchrCat(String vchrCat) {
		this.vchrCat = vchrCat;
	}
	
	public String getTrsStsActn() {
		return trsStsActn;
	}
	public void setTrsStsActn(String trsStsActn) {
		this.trsStsActn = trsStsActn;
	}
	
	public String getTrsSts() {
		return trsSts;
	}
	public void setTrsSts(String trsSts) {
		this.trsSts = trsSts;
	}
	
	public String getTrsRsn() {
		return trsRsn;
	}
	public void setTrsRsn(String trsRsn) {
		this.trsRsn = trsRsn;
	}
	
	public String getNtfUsr() {
		return ntfUsr;
	}
	public void setNtfUsr(String ntfUsr) {
		this.ntfUsr = ntfUsr;
	}
	
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	
	public String getCrtdBy() {
		return crtdBy;
	}
	public void setCrtdBy(String crtdBy) {
		this.crtdBy = crtdBy;
	}
	
	public List<VendorDefaultGlSettingInput> getGlAcctList() {
		return glAcctList;
	}
	public void setGlAcctList(List<VendorDefaultGlSettingInput> glAcctList) {
		this.glAcctList = glAcctList;
	}
	public String getPayTerm() {
		return payTerm;
	}
	public void setPayTerm(String payTerm) {
		this.payTerm = payTerm;
	}
	public String getCry() {
		return cry;
	}
	public void setCry(String cry) {
		this.cry = cry;
	}
	
	
}
