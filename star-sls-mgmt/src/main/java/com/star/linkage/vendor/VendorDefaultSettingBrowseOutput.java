package com.star.linkage.vendor;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class VendorDefaultSettingBrowseOutput extends BrowseOutput {

	public List<VendorDefaultSettingInput> fldTblVndrDfltStng = new ArrayList<VendorDefaultSettingInput>();
	
}
