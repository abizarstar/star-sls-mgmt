package com.star.linkage.vendor;

public class BankAccount {

	private String acctNo;
	private String acctNm;
	private String bnkKey;
	private String extRef;
	public String getAcctNo() {
		return acctNo;
	}
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	public String getAcctNm() {
		return acctNm;
	}
	public void setAcctNm(String acctNm) {
		this.acctNm = acctNm;
	}
	public String getBnkKey() {
		return bnkKey;
	}
	public void setBnkKey(String bnkKey) {
		this.bnkKey = bnkKey;
	}
	public String getExtRef() {
		return extRef;
	}
	public void setExtRef(String extRef) {
		this.extRef = extRef;
	}
	
	
}
