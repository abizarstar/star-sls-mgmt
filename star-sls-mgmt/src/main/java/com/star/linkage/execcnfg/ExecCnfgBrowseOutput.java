package com.star.linkage.execcnfg;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class ExecCnfgBrowseOutput extends BrowseOutput{

	public List<ExecCnfgInfo> fldTblExec = new ArrayList<ExecCnfgInfo>();
}
