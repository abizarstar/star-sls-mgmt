package com.star.linkage.execcnfg;

public class ExecCnfgInfo {

	
	private Integer  id;
	private String  hostExec;
	private String  hostPrm;
	private String  prmPort;
	private String  port;
	private String  env;
	private String classes;
	private String usrId;
	private String chkSum;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getHostExec() {
		return hostExec;
	}
	public void setHostExec(String hostExec) {
		this.hostExec = hostExec;
	}
	public String getHostPrm() {
		return hostPrm;
	}
	public void setHostPrm(String hostPrm) {
		this.hostPrm = hostPrm;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getEnv() {
		return env;
	}
	public void setEnv(String env) {
		this.env = env;
	}
	public String getClasses() {
		return classes;
	}
	public void setClasses(String classes) {
		this.classes = classes;
	}
	public String getUsrId() {
		return usrId;
	}
	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}
	public String getChkSum() {
		return chkSum;
	}
	public void setChkSum(String chkSum) {
		this.chkSum = chkSum;
	}
	public String getPrmPort() {
		return prmPort;
	}
	public void setPrmPort(String prmPort) {
		this.prmPort = prmPort;
	}
	
	
	
}
