package com.star.linkage.wkf;

import java.util.Date;

public class WkfCondtn {
		// step variables
	private String wfName;
	private String Condition;
	private String crtdBy;
	private String crtdDate;
	public String getWfName() {
		return wfName;
	}
	public void setWfName(String wfName) {
		this.wfName = wfName;
	}
	public String getCondition() {
		return Condition;
	}
	public void setCondition(String condition) {
		Condition = condition;
	}
	public String getCrtdBy() {
		return crtdBy;
	}
	public void setCrtdBy(String crtdBy) {
		this.crtdBy = crtdBy;
	}
	public String getCrtdDate() {
		return crtdDate;
	}
	public void setCrtdDate(String formatDate) {
		this.crtdDate = formatDate;
	}
	}
