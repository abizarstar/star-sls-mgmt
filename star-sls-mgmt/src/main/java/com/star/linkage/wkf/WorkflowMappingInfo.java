package com.star.linkage.wkf;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WorkflowMappingInfo {

	private String asgnToNm;
	private Date asgnOnDt;
	private String asgnOnDtStr;
	private String sts;
	private String rmk;
	
	private List<WkfAprvr> wkfAprvrList = new ArrayList<WkfAprvr>();
	
	public String getAsgnToNm() {
		return asgnToNm;
	}
	public void setAsgnToNm(String asgnToNm) {
		this.asgnToNm = asgnToNm;
	}
	
	public Date getAsgnOnDt() {
		return asgnOnDt;
	}
	public void setAsgnOnDt(Date asgnOnDt) {
		this.asgnOnDt = asgnOnDt;
	}
	public String getAsgnOnDtStr() {
		return asgnOnDtStr;
	}
	public void setAsgnOnDtStr(String asgnOnDtStr) {
		this.asgnOnDtStr = asgnOnDtStr;
	}
	public String getSts() {
		return sts;
	}
	public void setSts(String sts) {
		this.sts = sts;
	}
	public String getRmk() {
		return rmk;
	}
	public void setRmk(String rmk) {
		this.rmk = rmk;
	}
	public List<WkfAprvr> getWkfAprvrList() {
		return wkfAprvrList;
	}
	public void setWkfAprvrList(List<WkfAprvr> wkfAprvrList) {
		this.wkfAprvrList = wkfAprvrList;
	}
	
	
}
