package com.star.linkage.wkf;

import java.util.ArrayList;
import java.util.List;

public class WorkflowStepInput {
	private String WkfId;
	private String StepName;
	private int AmtFlg;
	private int BrhFlg;
	private int VenFlg;
	private int DayFlg;
	private float AmtFrm;
	private float AmtTo;
	private String Brh;
	private String VenId;
	private int Days;
	private int DayBef;
	private String Apvr;
	private int CrcnFlg;
	
	List<WkfAprvr> wkfAprvrList = new ArrayList<WkfAprvr>();
	
	//  **************Setter & Getter*****************
		
	public String getWkfId() {
		return WkfId;
	}
	public void setWkfId(String wkfId) {
		WkfId = wkfId;
	}
	public String getStepName() {
		return StepName;
	}
	public void setStepName(String stepName) {
		StepName = stepName;
	}
	public int getAmtFlg() {
		return AmtFlg;
	}
	public void setAmtFlg(int amtFlg) {
		AmtFlg = amtFlg;
	}
	public int getBrhFlg() {
		return BrhFlg;
	}
	public void setBrhFlg(int brhFlg) {
		BrhFlg = brhFlg;
	}	
	public int getVenFlg() {
		return VenFlg;
	}
	public void setVenFlg(int venFlg) {
		VenFlg = venFlg;
	}
	public int getDayFlg() {
		return DayFlg;
	}
	public void setDayFlg(int dayFlg) {
		DayFlg = dayFlg;
	}
	public float getAmtFrm() {
		return AmtFrm;
	}
	public void setAmtFrm(float amtFrm) {
		AmtFrm = amtFrm;
	}
	public float getAmtTo() {
		return AmtTo;
	}
	public void setAmtTo(float amtTo) {
		AmtTo = amtTo;
	}
	public String getBrh() {
		return Brh;
	}
	public void setBrh(String brh) {
		Brh = brh;
	}
	public String getVenId() {
		return VenId;
	}
	public void setVenId(String venId) {
		VenId = venId;
	}
	public int getDays() {
		return Days;
	}
	public void setDays(int days) {
		Days = days;
	}
	public int getDayBef() {
		return DayBef;
	}
	public void setDayBef(int dayBef) {
		DayBef = dayBef;
	}
	public String getApvr() {
		return Apvr;
	}
	public void setApvr(String apvr) {
		Apvr = apvr;
	}
	public int getCrcnFlg() {
		return CrcnFlg;
	}
	public void setCrcnFlg(int crcnFlg) {
		CrcnFlg = crcnFlg;
	}
	public List<WkfAprvr> getWkfAprvrList() {
		return wkfAprvrList;
	}
	public void setWkfAprvrList(List<WkfAprvr> wkfAprvrList) {
		this.wkfAprvrList = wkfAprvrList;
	}
	
	

}
