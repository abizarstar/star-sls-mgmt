package com.star.linkage.wkf;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;
import com.star.modal.CstmWkfMapHis;

public class WkfHisBrowseOutput extends BrowseOutput{
	
	public List<WorkflowMappingInfo> fldTblWkHistory = new ArrayList<WorkflowMappingInfo>();

}
