package com.star.linkage.wkf;

public class AdvancePaymentInfo {

	String desc;
	String advAmt;
	String cry;
	
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getAdvAmt() {
		return advAmt;
	}
	public void setAdvAmt(String advAmt) {
		this.advAmt = advAmt;
	}
	
	public String getCry() {
		return cry;
	}
	public void setCry(String cry) {
		this.cry = cry;
	}
}
