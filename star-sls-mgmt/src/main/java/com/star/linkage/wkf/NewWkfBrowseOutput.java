package com.star.linkage.wkf;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class NewWkfBrowseOutput extends BrowseOutput {

	public List<NewWkf> fldTblNewWkf= new ArrayList<NewWkf>();
	public BigDecimal chkProof;
	
}
