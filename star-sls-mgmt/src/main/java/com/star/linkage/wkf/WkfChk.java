package com.star.linkage.wkf;

import java.util.Date;

public class WkfChk {
	// step variables
	private String wfId;
	private String wfName;
	private String crdBy;
	private String stepName;
	private String amtFlg;
	private String brhFlg;
	private String venFlg;
	private String dayFlg;
	private String amtFrom;
	private String amtTo;
	private String brh;
	private String venId;
	private String days;
	private String dayBef;
	private String apvr;
	private String brhVndrSize;
	private String costRecon;
	public String getWfId() {
		return wfId;
	}
	public void setWfId(String wfId) {
		this.wfId = wfId;
	}
	public String getWfName() {
		return wfName;
	}
	public void setWfName(String wfName) {
		this.wfName = wfName;
	}
	public String getCrdBy() {
		return crdBy;
	}
	public void setCrdBy(String crdBy) {
		this.crdBy = crdBy;
	}
	public String getStepName() {
		return stepName;
	}
	public void setStepName(String stepName) {
		this.stepName = stepName;
	}
	public String getAmtFlg() {
		return amtFlg;
	}
	public void setAmtFlg(String amtFlg) {
		this.amtFlg = amtFlg;
	}
	public String getBrhFlg() {
		return brhFlg;
	}
	public void setBrhFlg(String brhFlg) {
		this.brhFlg = brhFlg;
	}
	public String getVenFlg() {
		return venFlg;
	}
	public void setVenFlg(String venFlg) {
		this.venFlg = venFlg;
	}
	public String getDayFlg() {
		return dayFlg;
	}
	public void setDayFlg(String dayFlg) {
		this.dayFlg = dayFlg;
	}
	public String getAmtFrom() {
		return amtFrom;
	}
	public void setAmtFrom(String amtFrom) {
		this.amtFrom = amtFrom;
	}
	public String getAmtTo() {
		return amtTo;
	}
	public void setAmtTo(String amtTo) {
		this.amtTo = amtTo;
	}
	public String getBrh() {
		return brh;
	}
	public void setBrh(String brh) {
		this.brh = brh;
	}
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	public String getDays() {
		return days;
	}
	public void setDays(String days) {
		this.days = days;
	}
	public String getDayBef() {
		return dayBef;
	}
	public void setDayBef(String dayBef) {
		this.dayBef = dayBef;
	}
	public String getApvr() {
		return apvr;
	}
	public void setApvr(String apvr) {
		this.apvr = apvr;
	}
	public String getBrhVndrSize() {
		return brhVndrSize;
	}
	public void setBrhVndrSize(String brhVndrSize) {
		this.brhVndrSize = brhVndrSize;
	}
	public String getCostRecon() {
		return costRecon;
	}
	public void setCostRecon(String costRecon) {
		this.costRecon = costRecon;
	}

}
