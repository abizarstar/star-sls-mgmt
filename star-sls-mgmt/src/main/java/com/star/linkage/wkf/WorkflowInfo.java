package com.star.linkage.wkf;

import java.util.ArrayList;
import java.util.List;

public class WorkflowInfo {
	
	private String wkfId;
	private String pndgDoc;
	private String crtdBy;
	private String crtdDtts;
	private String wkfName;
	private String stpId;
	private String stpNm;
	private String amtFlg;
	private String brhFlg;
	private String venFlg;
	private String dayFlg;
	private String amtFrm;
	private String amtTo;
	private String brh;
	private String venId;
	private String days;
	private String dayBef;
	private String apvr;
	private String costRecon;
	private List<WkfAprvr> wkfAprvrList = new ArrayList<WkfAprvr>();
	
	public String getWkfId() {
		return wkfId;
	}
	public void setWkfId(String wkfId) {
		this.wkfId = wkfId;
	}
	
	public String getPndgDoc() {
		return pndgDoc;
	}
	public void setPndgDoc(String pndgDoc) {
		this.pndgDoc = pndgDoc;
	}
	
	public String getCrtdBy() {
		return crtdBy;
	}
	public void setCrtdBy(String crtdBy) {
		this.crtdBy = crtdBy;
	}
	
	public String getCrtdDtts() {
		return crtdDtts;
	}
	public void setCrtdDtts(String crtdDtts) {
		this.crtdDtts = crtdDtts;
	}
	
	public String getWkfName() {
		return wkfName;
	}
	public void setWkfName(String wkfName) {
		this.wkfName = wkfName;
	}
	
	public String getStpId() {
		return stpId;
	}
	public void setStpId(String stpId) {
		this.stpId = stpId;
	}
	
	public String getStpNm() {
		return stpNm;
	}
	public void setStpNm(String stpNm) {
		this.stpNm = stpNm;
	}
	
	public String getAmtFlg() {
		return amtFlg;
	}
	public void setAmtFlg(String amtFlg) {
		this.amtFlg = amtFlg;
	}
	
	public String getBrhFlg() {
		return brhFlg;
	}
	public void setBrhFlg(String brhFlg) {
		this.brhFlg = brhFlg;
	}
	
	public String getVenFlg() {
		return venFlg;
	}
	public void setVenFlg(String venFlg) {
		this.venFlg = venFlg;
	}
	
	public String getDayFlg() {
		return dayFlg;
	}
	public void setDayFlg(String dayFlg) {
		this.dayFlg = dayFlg;
	}
	
	public String getAmtFrm() {
		return amtFrm;
	}
	public void setAmtFrm(String amtFrm) {
		this.amtFrm = amtFrm;
	}
	
	public String getAmtTo() {
		return amtTo;
	}
	public void setAmtTo(String amtTo) {
		this.amtTo = amtTo;
	}
	
	public String getBrh() {
		return brh;
	}
	public void setBrh(String brh) {
		this.brh = brh;
	}
	
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	
	public String getDays() {
		return days;
	}
	public void setDays(String days) {
		this.days = days;
	}
	
	public String getDayBef() {
		return dayBef;
	}
	public void setDayBef(String dayBef) {
		this.dayBef = dayBef;
	}
	
	public String getApvr() {
		return apvr;
	}
	public void setApvr(String apvr) {
		this.apvr = apvr;
	}
	
	public String getCostRecon() {
		return costRecon;
	}
	public void setCostRecon(String costRecon) {
		this.costRecon = costRecon;
	}
	public List<WkfAprvr> getWkfAprvrList() {
		return wkfAprvrList;
	}
	public void setWkfAprvrList(List<WkfAprvr> wkfAprvrList) {
		this.wkfAprvrList = wkfAprvrList;
	}
	
	
}
