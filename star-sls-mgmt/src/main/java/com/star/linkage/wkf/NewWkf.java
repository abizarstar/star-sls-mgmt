package com.star.linkage.wkf;

import java.util.Date;

public class NewWkf {
		// step variables
	private String stepId;
	private String wkfId;
	private String stepName;
	private String amtFlg;
	private String brhFlg;
	private String venFlg;
	private String dayFlg;
	private String amtFrm;
	private String amtTo;
	private String brh;
	private String venId;
	private String days;
	private String dayBef;
	private String apvr;
	private String crcnFlg;
	public String getStepId() {
		return stepId;
	}
	public void setStepId(String stepId) {
		this.stepId = stepId;
	}
	public String getWkfId() {
		return wkfId;
	}
	public void setWkfId(String wkfId) {
		this.wkfId = wkfId;
	}
	public String getStepName() {
		return stepName;
	}
	public void setStepName(String stepName) {
		this.stepName = stepName;
	}
	public String getAmtFlg() {
		return amtFlg;
	}
	public void setAmtFlg(String amtFlg) {
		this.amtFlg = amtFlg;
	}
	public String getBrhFlg() {
		return brhFlg;
	}
	public void setBrhFlg(String brhFlg) {
		this.brhFlg = brhFlg;
	}
	public String getVenFlg() {
		return venFlg;
	}
	public void setVenFlg(String venFlg) {
		this.venFlg = venFlg;
	}
	public String getDayFlg() {
		return dayFlg;
	}
	public void setDayFlg(String dayFlg) {
		this.dayFlg = dayFlg;
	}
	public String getAmtFrm() {
		return amtFrm;
	}
	public void setAmtFrm(String amtFrm) {
		this.amtFrm = amtFrm;
	}
	public String getAmtTo() {
		return amtTo;
	}
	public void setAmtTo(String amtTo) {
		this.amtTo = amtTo;
	}
	public String getBrh() {
		return brh;
	}
	public void setBrh(String brh) {
		this.brh = brh;
	}
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	public String getDays() {
		return days;
	}
	public void setDays(String days) {
		this.days = days;
	}
	public String getDayBef() {
		return dayBef;
	}
	public void setDayBef(String dayBef) {
		this.dayBef = dayBef;
	}
	public String getApvr() {
		return apvr;
	}
	public void setApvr(String apvr) {
		this.apvr = apvr;
	}
	public String getCrcnFlg() {
		return crcnFlg;
	}
	public void setCrcnFlg(String crcnFlg) {
		this.crcnFlg = crcnFlg;
	}
	
}
