package com.star.linkage.cstmdocinfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.star.linkage.common.PaymentMethod;
import com.star.linkage.common.Vendor;
import com.star.linkage.gl.GLInfo;
import com.star.linkage.wkf.WkfAprvr;

public class VchrInfo extends AccountPayableInfo{

	private String cmpyId;
	private int ctlNo;
	private int gatCtlNo;
	private String flPdf;
	private String flPdfName;
	private String flTxt;
	private Date crtDtts;
	private String crtDttsStr;
	private Date prsDtts;
	private String prsDttsStr;
	/* WORK FLOW VARIALBE*/
	private int wrkFlwId;
	private String wrkFlw;
	private String wrkFlwSts;
	private String wrkFlwAprvr;
	private String wrkFlwAprvrNm;
	
	private String srcFlg;
	private Boolean isPrs;
	private String approverTo;
	private String trnSts;
	private String vchrNo;
	private String upldBy;
	private String usrNm;
	private int  vchrCtlNo ;
	private String vchrInvNo ;
	private String vchrVenId;
	private String vchrVenNm;
	private String vchrBrh;
	private double vchrAmt;
	private String vchrExtRef;
    private Date  vchrInvDt;
    private String  vchrInvDtStr;
    private Date  vchrDueDt;
    private String  vchrDueDtStr;
    private Date  vchrDiscDt;
    private String vchrPayTerm ;
    private String vchrCry ;
    private String trnRmk;
    private String poPfx;
    private String poNo;
    private String poItm;
    private String vchrPfx;
    private String vchrCat;
    private String transSts;
    private String pymntSts;
    private String transStsRsn;
    private String pymntStsRsn;
    private String holdRsn;
    private double discAmt;
    private String vchrDiscDtStr;
    private String referenceVoucherNo;
    private String vchrAmtStr;
    private double reconAmt;
    private String reconAmtStr;
    private int reconCount;
    private String chkNo;
    private String reqId;
    private String note;
    private String prsBy;
    private String voyageNo;
    
    private List<ReconInfo> costRecon = new ArrayList<ReconInfo>();
    private List<GlInfo> glData = new ArrayList<GlInfo>();
    
    private List<PaymentMethod> payMthds = new ArrayList<PaymentMethod>();
    private List<Vendor> vendorList = new ArrayList<Vendor>();
    
   
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Date getVchrDiscDt() {
		return vchrDiscDt;
	}
	public void setVchrDiscDt(Date vchrDiscDt) {
		this.vchrDiscDt = vchrDiscDt;
	}
	public String getReferenceVoucherNo() {
		return referenceVoucherNo;
	}
	public void setReferenceVoucherNo(String referenceVoucherNo) {
		this.referenceVoucherNo = referenceVoucherNo;
	}
	public String getHoldRsn() {
		return holdRsn;
	}
	public void setHoldRsn(String holdRsn) {
		this.holdRsn = holdRsn;
	}
	public int getCtlNo() {
		return ctlNo;
	}
	public void setCtlNo(int ctlNo) {
		this.ctlNo = ctlNo;
	}
	public int getGatCtlNo() {
		return gatCtlNo;
	}
	public void setGatCtlNo(int gatCtlNo) {
		this.gatCtlNo = gatCtlNo;
	}
	public String getFlPdf() {
		return flPdf;
	}
	public void setFlPdf(String flPdf) {
		this.flPdf = flPdf;
	}
	public String getFlPdfName() {
		return flPdfName;
	}
	public void setFlPdfName(String flPdfName) {
		this.flPdfName = flPdfName;
	}
	public String getFlTxt() {
		return flTxt;
	}
	public void setFlTxt(String flTxt) {
		this.flTxt = flTxt;
	}
	public Date getCrtDtts() {
		return crtDtts;
	}
	public void setCrtDtts(Date crtDtts) {
		this.crtDtts = crtDtts;
	}
	public Date getPrsDtts() {
		return prsDtts;
	}
	public void setPrsDtts(Date prsDtts) {
		this.prsDtts = prsDtts;
	}
	public String getWrkFlw() {
		return wrkFlw;
	}
	public void setWrkFlw(String wrkFlw) {
		this.wrkFlw = wrkFlw;
	}
	public String getSrcFlg() {
		return srcFlg;
	}
	public void setSrcFlg(String srcFlg) {
		this.srcFlg = srcFlg;
	}
	public Boolean getIsPrs() {
		return isPrs;
	}
	public void setIsPrs(Boolean isPrs) {
		this.isPrs = isPrs;
	}
	public String getApproverTo() {
		return approverTo;
	}
	public void setApproverTo(String approverTo) {
		this.approverTo = approverTo;
	}
	public String getTrnSts() {
		return trnSts;
	}
	public void setTrnSts(String trnSts) {
		this.trnSts = trnSts;
	}
	public String getVchrNo() {
		return vchrNo;
	}
	public void setVchrNo(String vchrNo) {
		this.vchrNo = vchrNo;
	}
	public String getUpldBy() {
		return upldBy;
	}
	public void setUpldBy(String upldBy) {
		this.upldBy = upldBy;
	}
	public String getUsrNm() {
		return usrNm;
	}
	public void setUsrNm(String usrNm) {
		this.usrNm = usrNm;
	}
	public int getVchrCtlNo() {
		return vchrCtlNo;
	}
	public void setVchrCtlNo(int vchrCtlNo) {
		this.vchrCtlNo = vchrCtlNo;
	}
	public String getVchrInvNo() {
		return vchrInvNo;
	}
	public void setVchrInvNo(String vchrInvNo) {
		this.vchrInvNo = vchrInvNo;
	}
	public String getVchrVenId() {
		return vchrVenId;
	}
	public void setVchrVenId(String vchrVenId) {
		this.vchrVenId = vchrVenId;
	}
	public String getVchrVenNm() {
		return vchrVenNm;
	}
	public void setVchrVenNm(String vchrVenNm) {
		this.vchrVenNm = vchrVenNm;
	}
	public String getVchrBrh() {
		return vchrBrh;
	}
	public void setVchrBrh(String vchrBrh) {
		this.vchrBrh = vchrBrh;
	}
	public double getVchrAmt() {
		return vchrAmt;
	}
	public void setVchrAmt(double vchrAmt) {
		this.vchrAmt = vchrAmt;
	}
	public String getVchrExtRef() {
		return vchrExtRef;
	}
	public void setVchrExtRef(String vchrExtRef) {
		this.vchrExtRef = vchrExtRef;
	}
	public Date getVchrInvDt() {
		return vchrInvDt;
	}
	public void setVchrInvDt(Date vchrInvDt) {
		this.vchrInvDt = vchrInvDt;
	}
	public Date getVchrDueDt() {
		return vchrDueDt;
	}
	public void setVchrDueDt(Date vchrDueDt) {
		this.vchrDueDt = vchrDueDt;
	}
	public String getVchrPayTerm() {
		return vchrPayTerm;
	}
	public void setVchrPayTerm(String vchrPayTerm) {
		this.vchrPayTerm = vchrPayTerm;
	}
	public String getCrtDttsStr() {
		return crtDttsStr;
	}
	public void setCrtDttsStr(String crtDttsStr) {
		this.crtDttsStr = crtDttsStr;
	}
	public String getPrsDttsStr() {
		return prsDttsStr;
	}
	public void setPrsDttsStr(String prsDttsStr) {
		this.prsDttsStr = prsDttsStr;
	}
	public String getVchrInvDtStr() {
		return vchrInvDtStr;
	}
	public void setVchrInvDtStr(String vchrInvDtStr) {
		this.vchrInvDtStr = vchrInvDtStr;
	}
	public String getVchrDueDtStr() {
		return vchrDueDtStr;
	}
	public void setVchrDueDtStr(String vchrDueDtStr) {
		this.vchrDueDtStr = vchrDueDtStr;
	}
	
	public String getVchrCry() {
		return vchrCry;
	}
	public void setVchrCry(String vchrCry) {
		this.vchrCry = vchrCry;
	}
	public String getTrnRmk() {
		return trnRmk;
	}
	public void setTrnRmk(String trnRmk) {
		this.trnRmk = trnRmk;
	}	
	
    public String getPoPfx() {
		return poPfx;
	}
	public void setPoPfx(String poPfx) {
		this.poPfx = poPfx;
	}
	
    public String getPoNo() {
		return poNo;
	}
	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}
	
    public String getPoItm() {
		return poItm;
	}
	public void setPoItm(String poItm) {
		this.poItm = poItm;
	}
	
    public String getVchrPfx() {
		return vchrPfx;
	}
	public void setVchrPfx(String vchrPfx) {
		this.vchrPfx = vchrPfx;
	}
	
    public String getVchrCat() {
		return vchrCat;
	}
	public void setVchrCat(String vchrCat) {
		this.vchrCat = vchrCat;
	}
	
    public String getTransSts() {
		return transSts;
	}
	public void setTransSts(String transSts) {
		this.transSts = transSts;
	}
	
    public String getPymntSts() {
		return pymntSts;
	}
	public void setPymntSts(String pymntSts) {
		this.pymntSts = pymntSts;
	}
    
	public List<Vendor> getVendorList() {
		return vendorList;
	}
	public void setVendorList(List<Vendor> vendorList) {
		this.vendorList = vendorList;
	}
	public String getTransStsRsn() {
		return transStsRsn;
	}
	public void setTransStsRsn(String transStsRsn) {
		this.transStsRsn = transStsRsn;
	}
	public String getPymntStsRsn() {
		return pymntStsRsn;
	}
	public void setPymntStsRsn(String pymntStsRsn) {
		this.pymntStsRsn = pymntStsRsn;
	}
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public double getDiscAmt() {
		return discAmt;
	}
	public void setDiscAmt(double discAmt) {
		this.discAmt = discAmt;
	}
	public List<PaymentMethod> getPayMthds() {
		return payMthds;
	}
	public void setPayMthds(List<PaymentMethod> payMthds) {
		this.payMthds = payMthds;
	}
	public String getVchrAmtStr() {
		return vchrAmtStr;
	}
	public void setVchrAmtStr(String vchrAmtStr) {
		this.vchrAmtStr = vchrAmtStr;
	}
	public double getReconAmt() {
		return reconAmt;
	}
	public void setReconAmt(double reconAmt) {
		this.reconAmt = reconAmt;
	}
	public int getReconCount() {
		return reconCount;
	}
	public void setReconCount(int reconCount) {
		this.reconCount = reconCount;
	}
	public String getVchrDiscDtStr() {
		return vchrDiscDtStr;
	}
	public void setVchrDiscDtStr(String vchrDiscDtStr) {
		this.vchrDiscDtStr = vchrDiscDtStr;
	}
	public String getChkNo() {
		return chkNo;
	}
	public void setChkNo(String chkNo) {
		this.chkNo = chkNo;
	}
	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	public String getReconAmtStr() {
		return reconAmtStr;
	}
	public void setReconAmtStr(String reconAmtStr) {
		this.reconAmtStr = reconAmtStr;
	}
	public List<ReconInfo> getCostRecon() {
		return costRecon;
	}
	public void setCostRecon(List<ReconInfo> costRecon) {
		this.costRecon = costRecon;
	}
	public List<GlInfo> getGlData() {
		return glData;
	}
	public void setGlData(List<GlInfo> glData) {
		this.glData = glData;
	}
	public String getPrsBy() {
		return prsBy;
	}
	public void setPrsBy(String prsBy) {
		this.prsBy = prsBy;
	}
	public String getVoyageNo() {
		return voyageNo;
	}
	public void setVoyageNo(String voyageNo) {
		this.voyageNo = voyageNo;
	}
	public int getWrkFlwId() {
		return wrkFlwId;
	}
	public void setWrkFlwId(int wrkFlwId) {
		this.wrkFlwId = wrkFlwId;
	}
	public String getWrkFlwSts() {
		return wrkFlwSts;
	}
	public void setWrkFlwSts(String wrkFlwSts) {
		this.wrkFlwSts = wrkFlwSts;
	}
	public String getWrkFlwAprvr() {
		return wrkFlwAprvr;
	}
	public void setWrkFlwAprvr(String wrkFlwAprvr) {
		this.wrkFlwAprvr = wrkFlwAprvr;
	}
	public String getWrkFlwAprvrNm() {
		return wrkFlwAprvrNm;
	}
	public void setWrkFlwAprvrNm(String wrkFlwAprvrNm) {
		this.wrkFlwAprvrNm = wrkFlwAprvrNm;
	}
	
	
	
}
