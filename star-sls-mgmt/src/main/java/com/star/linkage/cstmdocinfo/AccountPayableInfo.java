package com.star.linkage.cstmdocinfo;

public class AccountPayableInfo {

	private String balAmt;
	private String inPrsAmt;
	private String inPrsBal;
	
	
	
	public String getBalAmt() {
		return balAmt;
	}
	public void setBalAmt(String balAmt) {
		this.balAmt = balAmt;
	}
	public String getInPrsAmt() {
		return inPrsAmt;
	}
	public void setInPrsAmt(String inPrsAmt) {
		this.inPrsAmt = inPrsAmt;
	}
	public String getInPrsBal() {
		return inPrsBal;
	}
	public void setInPrsBal(String inPrsBal) {
		this.inPrsBal = inPrsBal;
	}
	
	
	
}
