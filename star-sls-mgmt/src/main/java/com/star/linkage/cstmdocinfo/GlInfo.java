package com.star.linkage.cstmdocinfo;

public class GlInfo {

	private String glAcc;
	private String glSubAcc;
	private double drAmt;
	private double crAmt;
	private String remark;
	public String getGlAcc() {
		return glAcc;
	}
	public void setGlAcc(String glAcc) {
		this.glAcc = glAcc;
	}
	public String getGlSubAcc() {
		return glSubAcc;
	}
	public void setGlSubAcc(String glSubAcc) {
		this.glSubAcc = glSubAcc;
	}
	public double getDrAmt() {
		return drAmt;
	}
	public void setDrAmt(double drAmt) {
		this.drAmt = drAmt;
	}
	public double getCrAmt() {
		return crAmt;
	}
	public void setCrAmt(double crAmt) {
		this.crAmt = crAmt;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
}
