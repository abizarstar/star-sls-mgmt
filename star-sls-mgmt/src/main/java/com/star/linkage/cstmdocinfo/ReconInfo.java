package com.star.linkage.cstmdocinfo;

public class ReconInfo {

	private String trsNo;
	private String poNo;
	private int crcnNo;
	private int costNo;
	private double balAmt;
	public String getTrsNo() {
		return trsNo;
	}
	public void setTrsNo(String trsNo) {
		this.trsNo = trsNo;
	}
	public String getPoNo() {
		return poNo;
	}
	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}
	public int getCrcnNo() {
		return crcnNo;
	}
	public void setCrcnNo(int crcnNo) {
		this.crcnNo = crcnNo;
	}
	public int getCostNo() {
		return costNo;
	}
	public void setCostNo(int costNo) {
		this.costNo = costNo;
	}
	public double getBalAmt() {
		return balAmt;
	}
	public void setBalAmt(double balAmt) {
		this.balAmt = balAmt;
	}
	
	
}
