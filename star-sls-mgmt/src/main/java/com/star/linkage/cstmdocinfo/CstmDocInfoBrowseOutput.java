package com.star.linkage.cstmdocinfo;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;


public class CstmDocInfoBrowseOutput extends BrowseOutput{

	public List<VchrInfo> fldTblDoc = new ArrayList<VchrInfo>();
	public List<VchrInfo> fldTblInvHold = new ArrayList<VchrInfo>();
	public List<DocumentHeader> fldTblHdrList = new ArrayList<DocumentHeader>();
	
	public int toReviewCount = 0;
	public int confirmedCount = 0;
	public int payApprovedCount = 0;
	public int completeCount = 0;
	public int toReviewDocCount = 0;
	public int toHoldDocCount = 0;
	
}
