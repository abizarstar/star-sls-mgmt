package com.star.linkage.common;

public class ShipTo {

	private String shpTo;
	private String shpToNm;
	private String shpToLongNm;
	
	public String getShpTo() {
		return shpTo;
	}
	public void setShpTo(String shpTo) {
		this.shpTo = shpTo;
	}
	
	public String getShpToNm() {
		return shpToNm;
	}
	public void setShpToNm(String shpToNm) {
		this.shpToNm = shpToNm;
	}
	
	public String getShpToLongNm() {
		return shpToLongNm;
	}
	public void setShpToLongNm(String shpToLongNm) {
		this.shpToLongNm = shpToLongNm;
	}
	
}
