package com.star.linkage.common;

public class LengthTypeInfo {

	private String lgthTyp;
	private String lgthTypDesc;
	
	public String getLgthTyp() {
		return lgthTyp;
	}
	public void setLgthTyp(String lgthTyp) {
		this.lgthTyp = lgthTyp;
	}
	public String getLgthTypDesc() {
		return lgthTypDesc;
	}
	public void setLgthTypDesc(String lgthTypDesc) {
		this.lgthTypDesc = lgthTypDesc;
	}
	
	
	
	
}
