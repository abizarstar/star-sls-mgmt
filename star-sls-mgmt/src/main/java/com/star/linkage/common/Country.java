package com.star.linkage.common;

public class Country {

	private String cty;
	private String ctyDesc;
	public String getCty() {
		return cty;
	}
	public void setCty(String cty) {
		this.cty = cty;
	}
	public String getCtyDesc() {
		return ctyDesc;
	}
	public void setCtyDesc(String ctyDesc) {
		this.ctyDesc = ctyDesc;
	}
	
	
}
