package com.star.linkage.common;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class TransactionStatusActionBrowseOutput extends BrowseOutput{

	public List<TransactionStatusAction> fldTblTrnStsActn = new ArrayList<TransactionStatusAction>();
}
