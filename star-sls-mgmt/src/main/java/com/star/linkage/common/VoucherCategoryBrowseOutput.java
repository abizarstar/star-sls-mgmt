package com.star.linkage.common;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class VoucherCategoryBrowseOutput extends BrowseOutput{

	public List<VoucherCategory> fldTblVouCategory = new ArrayList<VoucherCategory>();
}
