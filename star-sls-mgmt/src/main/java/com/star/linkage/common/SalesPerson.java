package com.star.linkage.common;

public class SalesPerson {

	private String slp;
	private String slpNm;
	
	public String getSlp() {
		return slp;
	}
	public void setSlp(String slp) {
		this.slp = slp;
	}
	
	public String getSlpNm() {
		return slpNm;
	}
	public void setSlpNm(String slpNm) {
		this.slpNm = slpNm;
	}
	
}
