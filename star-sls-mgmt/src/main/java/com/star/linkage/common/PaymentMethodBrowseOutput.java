package com.star.linkage.common;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class PaymentMethodBrowseOutput extends BrowseOutput {

	public List<PaymentMethod> fldTblPymtMthd = new ArrayList<PaymentMethod>();
}
