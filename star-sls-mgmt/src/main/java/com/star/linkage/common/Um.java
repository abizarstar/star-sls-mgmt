package com.star.linkage.common;

public class Um {

	private String um;
	private String desc;
	
	public String getUm() {
		return um;
	}
	public void setUm(String um) {
		this.um = um;
	}
	
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}
