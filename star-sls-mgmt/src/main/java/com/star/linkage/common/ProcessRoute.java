package com.star.linkage.common;

public class ProcessRoute {

	private String prsRte;
	private String prsRteDesc;
	
	public String getPrsRte() {
		return prsRte;
	}
	public void setPrsRte(String prsRte) {
		this.prsRte = prsRte;
	}
	public String getPrsRteDesc() {
		return prsRteDesc;
	}
	public void setPrsRteDesc(String prsRteDesc) {
		this.prsRteDesc = prsRteDesc;
	}
	
	
}
