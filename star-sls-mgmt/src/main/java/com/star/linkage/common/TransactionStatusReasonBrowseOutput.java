package com.star.linkage.common;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class TransactionStatusReasonBrowseOutput extends BrowseOutput{

	public List<TransactionStatusReason> fldTblTrnStsRsn = new ArrayList<TransactionStatusReason>();
}
