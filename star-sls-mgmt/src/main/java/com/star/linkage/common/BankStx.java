package com.star.linkage.common;

public class BankStx {

	String bnk;
	String bnkDesc;
	String bnkCry;
	int bnkActv;
	double excRt;
	
	public String getBnk() {
		return bnk;
	}
	public void setBnk(String bnk) {
		this.bnk = bnk;
	}
	public String getBnkDesc() {
		return bnkDesc;
	}
	public void setBnkDesc(String bnkDesc) {
		this.bnkDesc = bnkDesc;
	}
	public String getBnkCry() {
		return bnkCry;
	}
	public void setBnkCry(String bnkCry) {
		this.bnkCry = bnkCry;
	}
	public int getBnkActv() {
		return bnkActv;
	}
	public void setBnkActv(int bnkActv) {
		this.bnkActv = bnkActv;
	}
	public double getExcRt() {
		return excRt;
	}
	public void setExcRt(double excRt) {
		this.excRt = excRt;
	}
	
	
	
}
