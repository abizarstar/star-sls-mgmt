package com.star.linkage.common;

public class PaymentMethod {

	private String mthdId;
	private String mthdCd;
	private String mthdNm;
	private Boolean mthdDflt;
	private String cmpyId;
	private String venId;
	
	public String getMthdId() {
		return mthdId;
	}
	public void setMthdId(String mthdId) {
		this.mthdId = mthdId;
	}
	
	public String getMthdCd() {
		return mthdCd;
	}
	public void setMthdCd(String mthdCd) {
		this.mthdCd = mthdCd;
	}
	
	public String getMthdNm() {
		return mthdNm;
	}
	public void setMthdNm(String mthdNm) {
		this.mthdNm = mthdNm;
	}
	
	public Boolean getMthdDflt() {
		return mthdDflt;
	}
	public void setMthdDflt(Boolean mthdDflt) {
		this.mthdDflt = mthdDflt;
	}
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	
	
	
}
