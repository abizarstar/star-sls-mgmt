package com.star.linkage.common;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class SalesCategoryBrowseOutput extends BrowseOutput{

	public List<SalesCategory> fldTblSlsCat = new ArrayList<SalesCategory>();
}
