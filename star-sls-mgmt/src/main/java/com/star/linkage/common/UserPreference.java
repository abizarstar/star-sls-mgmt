package com.star.linkage.common;

public class UserPreference {

	private int flwUpDays;
	private int expiryDays;
	private int buildUp;
	private int isAvlbl;
	
	public int getFlwUpDays() {
		return flwUpDays;
	}
	public void setFlwUpDays(int flwUpDays) {
		this.flwUpDays = flwUpDays;
	}
	public int getExpiryDays() {
		return expiryDays;
	}
	public void setExpiryDays(int expiryDays) {
		this.expiryDays = expiryDays;
	}
	public int getBuildUp() {
		return buildUp;
	}
	public void setBuildUp(int buildUp) {
		this.buildUp = buildUp;
	}
	public int getIsAvlbl() {
		return isAvlbl;
	}
	public void setIsAvlbl(int isAvlbl) {
		this.isAvlbl = isAvlbl;
	}
	
	
}
