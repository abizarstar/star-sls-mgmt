package com.star.linkage.common;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class UserPreferenceBrowseOutput extends BrowseOutput{

	public List<UserPreference> fldTblUsrPref = new ArrayList<UserPreference>();
}
