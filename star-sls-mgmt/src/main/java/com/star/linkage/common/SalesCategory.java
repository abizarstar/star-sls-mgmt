package com.star.linkage.common;

public class SalesCategory {

	private String slsCat;
	private String slsCatDesc;
	
	public String getSlsCat() {
		return slsCat;
	}
	public void setSlsCat(String slsCat) {
		this.slsCat = slsCat;
	}
	
	public String getSlsCatDesc() {
		return slsCatDesc;
	}
	public void setSlsCatDesc(String slsCatDesc) {
		this.slsCatDesc = slsCatDesc;
	}
	
}
