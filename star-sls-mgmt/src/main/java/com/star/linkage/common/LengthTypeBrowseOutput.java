package com.star.linkage.common;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class LengthTypeBrowseOutput extends BrowseOutput{

	public List<LengthTypeInfo> fldTblLengthType = new ArrayList<LengthTypeInfo>();
}
