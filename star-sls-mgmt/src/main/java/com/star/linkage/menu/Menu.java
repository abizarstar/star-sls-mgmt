package com.star.linkage.menu;

public class Menu {

	private String menuId;
	private String menuNm;
	private String menuHtml;
	private String menuIcon;
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getMenuNm() {
		return menuNm;
	}
	public void setMenuNm(String menuNm) {
		this.menuNm = menuNm;
	}
	public String getMenuHtml() {
		return menuHtml;
	}
	public void setMenuHtml(String menuHtml) {
		this.menuHtml = menuHtml;
	}
	public String getMenuIcon() {
		return menuIcon;
	}
	public void setMenuIcon(String menuIcon) {
		this.menuIcon = menuIcon;
	}
	
	
	
}
