package com.star.linkage.menu;

import java.util.List;

public class MenuInput {

	private String grpId;
	private List<String> menuId;
	private List<SubMenuInput> subMenList;
	public String getGrpId() {
		return grpId;
	}
	public void setGrpId(String grpId) {
		this.grpId = grpId;
	}
	public List<String> getMenuId() {
		return menuId;
	}
	public void setMenuId(List<String> menuId) {
		this.menuId = menuId;
	}
	public List<SubMenuInput> getSubMenList() {
		return subMenList;
	}
	public void setSubMenList(List<SubMenuInput> subMenList) {
		this.subMenList = subMenList;
	}
	
	
	
	
}
