package com.star.linkage.starslstest;

public class FullOuterjoin {
	private String cmpyId;
	private String ctlNo;
	private String brh;
	private String frm;
	private String grd;
	private String size;
	private String pbcCtlNo;
	private String custId;
	private String part;
	
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public String getCtlNo() {
		return ctlNo;
	}
	public void setCtlNo(String ctlNo) {
		this.ctlNo = ctlNo;
	}
	public String getBrh() {
		return brh;
	}
	public void setBrh(String brh) {
		this.brh = brh;
	}
	public String getFrm() {
		return frm;
	}
	public void setFrm(String frm) {
		this.frm = frm;
	}
	public String getGrd() {
		return grd;
	}
	public void setGrd(String grd) {
		this.grd = grd;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getPbcCtlNo() {
		return pbcCtlNo;
	}
	public void setPbcCtlNo(String pbcCtlNo) {
		this.pbcCtlNo = pbcCtlNo;
	}
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	public String getPart() {
		return part;
	}
	public void setPart(String part) {
		this.part = part;
	}

}
