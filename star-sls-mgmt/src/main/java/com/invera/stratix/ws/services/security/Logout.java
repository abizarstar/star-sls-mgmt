package com.invera.stratix.ws.services.security;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.invera.stratix.schema.common.datatypes.AuthenticationToken;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following com.invera.stratix.schema fragment specifies the expected
 * content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="authenticationToken" type="{http://stratix.invera.com/com.invera.stratix.schema/common/datatypes}AuthenticationToken"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "authenticationToken" })
@XmlRootElement(name = "Logout")
public class Logout {

	@XmlElement(required = true)
	protected AuthenticationToken authenticationToken;

	/**
	 * Gets the value of the authenticationToken property.
	 * 
	 * @return possible object is {@link AuthenticationToken }
	 * 
	 */
	public AuthenticationToken getAuthenticationToken() {
		return authenticationToken;
	}

	/**
	 * Sets the value of the authenticationToken property.
	 * 
	 * @param value
	 *            allowed object is {@link AuthenticationToken }
	 * 
	 */
	public void setAuthenticationToken(AuthenticationToken value) {
		this.authenticationToken = value;
	}

}
