
package com.invera.stratix.schema.common.datatypes;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.invera.stratix.schema.common.datatypes package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.invera.stratix.schema.common.datatypes
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ServiceMessages }
     * 
     */
    public ServiceMessages createServiceMessages() {
        return new ServiceMessages();
    }

    /**
     * Create an instance of {@link AuthenticationToken }
     * 
     */
    public AuthenticationToken createAuthenticationToken() {
        return new AuthenticationToken();
    }

    /**
     * Create an instance of {@link DataElementMessageList }
     * 
     */
    public DataElementMessageList createDataElementMessageList() {
        return new DataElementMessageList();
    }

}
