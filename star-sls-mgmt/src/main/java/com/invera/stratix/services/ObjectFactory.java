
package com.invera.stratix.services;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.invera.stratix.schema.common.datatypes.AuthenticationToken;
import com.invera.stratix.schema.common.datatypes.ServiceMessages;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.invera.stratix.services package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ServiceMessagesHeader_QNAME = new QName("http://stratix.invera.com/services", "ServiceMessagesHeader");
    private final static QName _AuthenticationHeader_QNAME = new QName("http://stratix.invera.com/services", "AuthenticationHeader");
    private final static QName _StratixFault_QNAME = new QName("http://stratix.invera.com/services", "StratixFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.invera.stratix.services
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DeleteVoucher }
     * 
     */
    public DeleteVoucher createDeleteVoucher() {
        return new DeleteVoucher();
    }

    /**
     * Create an instance of {@link CreateVoucherResponse }
     * 
     */
    public CreateVoucherResponse createCreateVoucherResponse() {
        return new CreateVoucherResponse();
    }

    /**
     * Create an instance of {@link CreateVoucherOutput }
     * 
     */
    public CreateVoucherOutput createCreateVoucherOutput() {
        return new CreateVoucherOutput();
    }

    /**
     * Create an instance of {@link DeleteVoucherResponse }
     * 
     */
    public DeleteVoucherResponse createDeleteVoucherResponse() {
        return new DeleteVoucherResponse();
    }

    /**
     * Create an instance of {@link CreateVoucher }
     * 
     */
    public CreateVoucher createCreateVoucher() {
        return new CreateVoucher();
    }

    /**
     * Create an instance of {@link Voucher }
     * 
     */
    public Voucher createVoucher() {
        return new Voucher();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceMessages }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://stratix.invera.com/services", name = "ServiceMessagesHeader")
    public JAXBElement<ServiceMessages> createServiceMessagesHeader(ServiceMessages value) {
        return new JAXBElement<ServiceMessages>(_ServiceMessagesHeader_QNAME, ServiceMessages.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthenticationToken }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://stratix.invera.com/services", name = "AuthenticationHeader")
    public JAXBElement<AuthenticationToken> createAuthenticationHeader(AuthenticationToken value) {
        return new JAXBElement<AuthenticationToken>(_AuthenticationHeader_QNAME, AuthenticationToken.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://stratix.invera.com/services", name = "StratixFault")
    public JAXBElement<String> createStratixFault(String value) {
        return new JAXBElement<String>(_StratixFault_QNAME, String.class, null, value);
    }

}
