
package com.invera.stratix.services;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "GLDistributionService", targetNamespace = "http://stratix.invera.com/services", wsdlLocation = "http://172.20.100.70:60974/webservices/services/gateway/vouchers/GLDistributionService.wsdl")
public class GLDistributionService_Service
    extends Service
{

    private final static URL GLDISTRIBUTIONSERVICE_WSDL_LOCATION;
    private final static WebServiceException GLDISTRIBUTIONSERVICE_EXCEPTION;
    private final static QName GLDISTRIBUTIONSERVICE_QNAME = new QName("http://stratix.invera.com/services", "GLDistributionService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://172.20.100.70:60974/webservices/services/gateway/vouchers/GLDistributionService.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        GLDISTRIBUTIONSERVICE_WSDL_LOCATION = url;
        GLDISTRIBUTIONSERVICE_EXCEPTION = e;
    }

    public GLDistributionService_Service() {
        super(__getWsdlLocation(), GLDISTRIBUTIONSERVICE_QNAME);
    }

    public GLDistributionService_Service(WebServiceFeature... features) {
        super(__getWsdlLocation(), GLDISTRIBUTIONSERVICE_QNAME, features);
    }

    public GLDistributionService_Service(URL wsdlLocation) {
        super(wsdlLocation, GLDISTRIBUTIONSERVICE_QNAME);
    }

    public GLDistributionService_Service(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, GLDISTRIBUTIONSERVICE_QNAME, features);
    }

    public GLDistributionService_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public GLDistributionService_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns GLDistributionService
     */
    @WebEndpoint(name = "GLDistributionServiceSOAP")
    public GLDistributionService getGLDistributionServiceSOAP() {
        return super.getPort(new QName("http://stratix.invera.com/services", "GLDistributionServiceSOAP"), GLDistributionService.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns GLDistributionService
     */
    @WebEndpoint(name = "GLDistributionServiceSOAP")
    public GLDistributionService getGLDistributionServiceSOAP(WebServiceFeature... features) {
        return super.getPort(new QName("http://stratix.invera.com/services", "GLDistributionServiceSOAP"), GLDistributionService.class, features);
    }

    private static URL __getWsdlLocation() {
        if (GLDISTRIBUTIONSERVICE_EXCEPTION!= null) {
            throw GLDISTRIBUTIONSERVICE_EXCEPTION;
        }
        return GLDISTRIBUTIONSERVICE_WSDL_LOCATION;
    }

}
