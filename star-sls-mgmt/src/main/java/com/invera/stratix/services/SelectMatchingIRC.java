
package com.invera.stratix.services;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="companyId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="voucherPrefix" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="voucherNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ircSelect" type="{http://stratix.invera.com/services}IRCSelect" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "companyId",
    "voucherPrefix",
    "voucherNumber",
    "ircSelect"
})
@XmlRootElement(name = "SelectMatchingIRC")
public class SelectMatchingIRC {

    @XmlElement(required = true)
    protected String companyId;
    @XmlElement(required = true)
    protected String voucherPrefix;
    protected int voucherNumber;
    @XmlElement(required = true)
    protected List<IRCSelect> ircSelect;

    /**
     * Gets the value of the companyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * Sets the value of the companyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyId(String value) {
        this.companyId = value;
    }

    /**
     * Gets the value of the voucherPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherPrefix() {
        return voucherPrefix;
    }

    /**
     * Sets the value of the voucherPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherPrefix(String value) {
        this.voucherPrefix = value;
    }

    /**
     * Gets the value of the voucherNumber property.
     * 
     */
    public int getVoucherNumber() {
        return voucherNumber;
    }

    /**
     * Sets the value of the voucherNumber property.
     * 
     */
    public void setVoucherNumber(int value) {
        this.voucherNumber = value;
    }

    /**
     * Gets the value of the ircSelect property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ircSelect property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIrcSelect().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IRCSelect }
     * 
     * 
     */
    public List<IRCSelect> getIrcSelect() {
        if (ircSelect == null) {
            ircSelect = new ArrayList<IRCSelect>();
        }
        return this.ircSelect;
    }

}
