
package com.invera.stratix.services;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Voucher complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Voucher">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="companyId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="voucherPrefix" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="voucherNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="sessionId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="entryDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="vendorId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="vendorInvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="extenralReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="materialTransferNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="voyageNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vendorInvoiceDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="purchaseOrderPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="purchaseOrderNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="purchaseOrderItem" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="voucherBranch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pretaxVoucherAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="voucherAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="discountableAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="voucherDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="voucherCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exchangeRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="paymentTerm" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="discountTerm" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="dueDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="discountDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="discountAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="checkItemRemarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="voucherCrossRefNo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="authorizationReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="voucherCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceFulfillmentDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="prepaymentEligibility" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="transactionStatusAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionStatusRemarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentStatusAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentStatusRemarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Voucher", propOrder = {
    "companyId",
    "voucherPrefix",
    "voucherNumber",
    "sessionId",
    "entryDate",
    "vendorId",
    "vendorInvoiceNumber",
    "extenralReference",
    "materialTransferNumber",
    "voyageNumber",
    "vendorInvoiceDate",
    "purchaseOrderPrefix",
    "purchaseOrderNumber",
    "purchaseOrderItem",
    "voucherBranch",
    "pretaxVoucherAmount",
    "voucherAmount",
    "discountableAmount",
    "voucherDescription",
    "voucherCurrency",
    "exchangeRate",
    "paymentTerm",
    "discountTerm",
    "dueDate",
    "discountDate",
    "discountAmount",
    "checkItemRemarks",
    "paymentType",
    "voucherCrossRefNo",
    "authorizationReference",
    "voucherCategory",
    "serviceFulfillmentDate",
    "prepaymentEligibility",
    "transactionStatusAction",
    "transactionReason",
    "transactionStatus",
    "transactionStatusRemarks",
    "paymentStatusAction",
    "paymentReason",
    "paymentStatus",
    "paymentStatusRemarks"
})
public class Voucher {

    @XmlElement(required = true)
    protected String companyId;
    @XmlElement(required = true)
    protected String voucherPrefix;
    protected Integer voucherNumber;
    protected Integer sessionId;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar entryDate;
    @XmlElement(required = true)
    protected String vendorId;
    protected String vendorInvoiceNumber;
    protected String extenralReference;
    protected Integer materialTransferNumber;
    protected String voyageNumber;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar vendorInvoiceDate;
    protected String purchaseOrderPrefix;
    protected Integer purchaseOrderNumber;
    protected Integer purchaseOrderItem;
    protected String voucherBranch;
    protected BigDecimal pretaxVoucherAmount;
    @XmlElement(required = true)
    protected BigDecimal voucherAmount;
    protected BigDecimal discountableAmount;
    protected String voucherDescription;
    protected String voucherCurrency;
    protected BigDecimal exchangeRate;
    protected Integer paymentTerm;
    protected Integer discountTerm;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dueDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar discountDate;
    protected BigDecimal discountAmount;
    protected String checkItemRemarks;
    protected String paymentType;
    protected Integer voucherCrossRefNo;
    protected String authorizationReference;
    protected String voucherCategory;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar serviceFulfillmentDate;
    protected Integer prepaymentEligibility;
    protected String transactionStatusAction;
    protected String transactionReason;
    protected String transactionStatus;
    protected String transactionStatusRemarks;
    protected String paymentStatusAction;
    protected String paymentReason;
    protected String paymentStatus;
    protected String paymentStatusRemarks;

    /**
     * Gets the value of the companyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * Sets the value of the companyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyId(String value) {
        this.companyId = value;
    }

    /**
     * Gets the value of the voucherPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherPrefix() {
        return voucherPrefix;
    }

    /**
     * Sets the value of the voucherPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherPrefix(String value) {
        this.voucherPrefix = value;
    }

    /**
     * Gets the value of the voucherNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVoucherNumber() {
        return voucherNumber;
    }

    /**
     * Sets the value of the voucherNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVoucherNumber(Integer value) {
        this.voucherNumber = value;
    }

    /**
     * Gets the value of the sessionId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSessionId() {
        return sessionId;
    }

    /**
     * Sets the value of the sessionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSessionId(Integer value) {
        this.sessionId = value;
    }

    /**
     * Gets the value of the entryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEntryDate() {
        return entryDate;
    }

    /**
     * Sets the value of the entryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEntryDate(XMLGregorianCalendar value) {
        this.entryDate = value;
    }

    /**
     * Gets the value of the vendorId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorId() {
        return vendorId;
    }

    /**
     * Sets the value of the vendorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorId(String value) {
        this.vendorId = value;
    }

    /**
     * Gets the value of the vendorInvoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorInvoiceNumber() {
        return vendorInvoiceNumber;
    }

    /**
     * Sets the value of the vendorInvoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorInvoiceNumber(String value) {
        this.vendorInvoiceNumber = value;
    }

    /**
     * Gets the value of the extenralReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtenralReference() {
        return extenralReference;
    }

    /**
     * Sets the value of the extenralReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtenralReference(String value) {
        this.extenralReference = value;
    }

    /**
     * Gets the value of the materialTransferNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaterialTransferNumber() {
        return materialTransferNumber;
    }

    /**
     * Sets the value of the materialTransferNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaterialTransferNumber(Integer value) {
        this.materialTransferNumber = value;
    }

    /**
     * Gets the value of the voyageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoyageNumber() {
        return voyageNumber;
    }

    /**
     * Sets the value of the voyageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoyageNumber(String value) {
        this.voyageNumber = value;
    }

    /**
     * Gets the value of the vendorInvoiceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVendorInvoiceDate() {
        return vendorInvoiceDate;
    }

    /**
     * Sets the value of the vendorInvoiceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVendorInvoiceDate(XMLGregorianCalendar value) {
        this.vendorInvoiceDate = value;
    }

    /**
     * Gets the value of the purchaseOrderPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseOrderPrefix() {
        return purchaseOrderPrefix;
    }

    /**
     * Sets the value of the purchaseOrderPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseOrderPrefix(String value) {
        this.purchaseOrderPrefix = value;
    }

    /**
     * Gets the value of the purchaseOrderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }

    /**
     * Sets the value of the purchaseOrderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPurchaseOrderNumber(Integer value) {
        this.purchaseOrderNumber = value;
    }

    /**
     * Gets the value of the purchaseOrderItem property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPurchaseOrderItem() {
        return purchaseOrderItem;
    }

    /**
     * Sets the value of the purchaseOrderItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPurchaseOrderItem(Integer value) {
        this.purchaseOrderItem = value;
    }

    /**
     * Gets the value of the voucherBranch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherBranch() {
        return voucherBranch;
    }

    /**
     * Sets the value of the voucherBranch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherBranch(String value) {
        this.voucherBranch = value;
    }

    /**
     * Gets the value of the pretaxVoucherAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPretaxVoucherAmount() {
        return pretaxVoucherAmount;
    }

    /**
     * Sets the value of the pretaxVoucherAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPretaxVoucherAmount(BigDecimal value) {
        this.pretaxVoucherAmount = value;
    }

    /**
     * Gets the value of the voucherAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVoucherAmount() {
        return voucherAmount;
    }

    /**
     * Sets the value of the voucherAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVoucherAmount(BigDecimal value) {
        this.voucherAmount = value;
    }

    /**
     * Gets the value of the discountableAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountableAmount() {
        return discountableAmount;
    }

    /**
     * Sets the value of the discountableAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountableAmount(BigDecimal value) {
        this.discountableAmount = value;
    }

    /**
     * Gets the value of the voucherDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherDescription() {
        return voucherDescription;
    }

    /**
     * Sets the value of the voucherDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherDescription(String value) {
        this.voucherDescription = value;
    }

    /**
     * Gets the value of the voucherCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherCurrency() {
        return voucherCurrency;
    }

    /**
     * Sets the value of the voucherCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherCurrency(String value) {
        this.voucherCurrency = value;
    }

    /**
     * Gets the value of the exchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    /**
     * Sets the value of the exchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExchangeRate(BigDecimal value) {
        this.exchangeRate = value;
    }

    /**
     * Gets the value of the paymentTerm property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPaymentTerm() {
        return paymentTerm;
    }

    /**
     * Sets the value of the paymentTerm property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPaymentTerm(Integer value) {
        this.paymentTerm = value;
    }

    /**
     * Gets the value of the discountTerm property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDiscountTerm() {
        return discountTerm;
    }

    /**
     * Sets the value of the discountTerm property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDiscountTerm(Integer value) {
        this.discountTerm = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDueDate(XMLGregorianCalendar value) {
        this.dueDate = value;
    }

    /**
     * Gets the value of the discountDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDiscountDate() {
        return discountDate;
    }

    /**
     * Sets the value of the discountDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDiscountDate(XMLGregorianCalendar value) {
        this.discountDate = value;
    }

    /**
     * Gets the value of the discountAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    /**
     * Sets the value of the discountAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountAmount(BigDecimal value) {
        this.discountAmount = value;
    }

    /**
     * Gets the value of the checkItemRemarks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckItemRemarks() {
        return checkItemRemarks;
    }

    /**
     * Sets the value of the checkItemRemarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckItemRemarks(String value) {
        this.checkItemRemarks = value;
    }

    /**
     * Gets the value of the paymentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * Sets the value of the paymentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentType(String value) {
        this.paymentType = value;
    }

    /**
     * Gets the value of the voucherCrossRefNo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVoucherCrossRefNo() {
        return voucherCrossRefNo;
    }

    /**
     * Sets the value of the voucherCrossRefNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVoucherCrossRefNo(Integer value) {
        this.voucherCrossRefNo = value;
    }

    /**
     * Gets the value of the authorizationReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizationReference() {
        return authorizationReference;
    }

    /**
     * Sets the value of the authorizationReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizationReference(String value) {
        this.authorizationReference = value;
    }

    /**
     * Gets the value of the voucherCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherCategory() {
        return voucherCategory;
    }

    /**
     * Sets the value of the voucherCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherCategory(String value) {
        this.voucherCategory = value;
    }

    /**
     * Gets the value of the serviceFulfillmentDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getServiceFulfillmentDate() {
        return serviceFulfillmentDate;
    }

    /**
     * Sets the value of the serviceFulfillmentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setServiceFulfillmentDate(XMLGregorianCalendar value) {
        this.serviceFulfillmentDate = value;
    }

    /**
     * Gets the value of the prepaymentEligibility property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPrepaymentEligibility() {
        return prepaymentEligibility;
    }

    /**
     * Sets the value of the prepaymentEligibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPrepaymentEligibility(Integer value) {
        this.prepaymentEligibility = value;
    }

    /**
     * Gets the value of the transactionStatusAction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusAction() {
        return transactionStatusAction;
    }

    /**
     * Sets the value of the transactionStatusAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusAction(String value) {
        this.transactionStatusAction = value;
    }

    /**
     * Gets the value of the transactionReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionReason() {
        return transactionReason;
    }

    /**
     * Sets the value of the transactionReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionReason(String value) {
        this.transactionReason = value;
    }

    /**
     * Gets the value of the transactionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatus() {
        return transactionStatus;
    }

    /**
     * Sets the value of the transactionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatus(String value) {
        this.transactionStatus = value;
    }

    /**
     * Gets the value of the transactionStatusRemarks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusRemarks() {
        return transactionStatusRemarks;
    }

    /**
     * Sets the value of the transactionStatusRemarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusRemarks(String value) {
        this.transactionStatusRemarks = value;
    }

    /**
     * Gets the value of the paymentStatusAction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentStatusAction() {
        return paymentStatusAction;
    }

    /**
     * Sets the value of the paymentStatusAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentStatusAction(String value) {
        this.paymentStatusAction = value;
    }

    /**
     * Gets the value of the paymentReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentReason() {
        return paymentReason;
    }

    /**
     * Sets the value of the paymentReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentReason(String value) {
        this.paymentReason = value;
    }

    /**
     * Gets the value of the paymentStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentStatus() {
        return paymentStatus;
    }

    /**
     * Sets the value of the paymentStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentStatus(String value) {
        this.paymentStatus = value;
    }

    /**
     * Gets the value of the paymentStatusRemarks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentStatusRemarks() {
        return paymentStatusRemarks;
    }

    /**
     * Sets the value of the paymentStatusRemarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentStatusRemarks(String value) {
        this.paymentStatusRemarks = value;
    }

}
