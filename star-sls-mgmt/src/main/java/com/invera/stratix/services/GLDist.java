
package com.invera.stratix.services;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GLDist complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GLDist">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="basicGLAccount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subaccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="debitAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="creditAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="distributionRemarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLDist", propOrder = {
    "basicGLAccount",
    "subaccount",
    "debitAmount",
    "creditAmount",
    "distributionRemarks"
})
public class GLDist {

    @XmlElement(required = true)
    protected String basicGLAccount;
    protected String subaccount;
    @XmlElement(required = true)
    protected BigDecimal debitAmount;
    @XmlElement(required = true)
    protected BigDecimal creditAmount;
    protected String distributionRemarks;

    /**
     * Gets the value of the basicGLAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasicGLAccount() {
        return basicGLAccount;
    }

    /**
     * Sets the value of the basicGLAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasicGLAccount(String value) {
        this.basicGLAccount = value;
    }

    /**
     * Gets the value of the subaccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubaccount() {
        return subaccount;
    }

    /**
     * Sets the value of the subaccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubaccount(String value) {
        this.subaccount = value;
    }

    /**
     * Gets the value of the debitAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDebitAmount() {
        return debitAmount;
    }

    /**
     * Sets the value of the debitAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDebitAmount(BigDecimal value) {
        this.debitAmount = value;
    }

    /**
     * Gets the value of the creditAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditAmount() {
        return creditAmount;
    }

    /**
     * Sets the value of the creditAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditAmount(BigDecimal value) {
        this.creditAmount = value;
    }

    /**
     * Gets the value of the distributionRemarks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistributionRemarks() {
        return distributionRemarks;
    }

    /**
     * Sets the value of the distributionRemarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistributionRemarks(String value) {
        this.distributionRemarks = value;
    }

}
