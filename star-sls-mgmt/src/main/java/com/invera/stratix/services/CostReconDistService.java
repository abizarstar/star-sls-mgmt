
package com.invera.stratix.services;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebService(name = "CostReconciliationDistributionService", targetNamespace = "http://stratix.invera.com/services")
@XmlSeeAlso({
    com.invera.stratix.schema.common.datatypes.ObjectFactory.class,
    com.invera.stratix.services.ObjectFactory.class
})
public interface CostReconDistService {


    /**
     * 
     * @param ircSelect
     * @param voucherPrefix
     * @param voucherNumber
     * @param companyId
     * @throws StratixFault
     */
    @WebMethod(operationName = "SelectMatchingIRC", action = "http://stratix.invera.com/services/SelectMatchingIRC")
    @RequestWrapper(localName = "SelectMatchingIRC", targetNamespace = "http://stratix.invera.com/services", className = "com.invera.stratix.services.SelectMatchingIRC")
    @ResponseWrapper(localName = "SelectMatchingIRCResponse", targetNamespace = "http://stratix.invera.com/services", className = "com.invera.stratix.services.SelectMatchingIRCResponse")
    public void selectMatchingIRC(
        @WebParam(name = "companyId", targetNamespace = "http://stratix.invera.com/services")
        String companyId,
        @WebParam(name = "voucherPrefix", targetNamespace = "http://stratix.invera.com/services")
        String voucherPrefix,
        @WebParam(name = "voucherNumber", targetNamespace = "http://stratix.invera.com/services")
        int voucherNumber,
        @WebParam(name = "ircSelect", targetNamespace = "http://stratix.invera.com/services")
        List<IRCSelect> ircSelect)
        throws StratixFault
    ;

    /**
     * 
     * @param voucherPrefix
     * @param voucherNumber
     * @param companyId
     * @throws StratixFault
     */
    @WebMethod(operationName = "ApplyIRCDist", action = "http://stratix.invera.com/services/ApplyIRCDist")
    @RequestWrapper(localName = "ApplyIRCDist", targetNamespace = "http://stratix.invera.com/services", className = "com.invera.stratix.services.ApplyIRCDist")
    @ResponseWrapper(localName = "ApplyIRCDistResponse", targetNamespace = "http://stratix.invera.com/services", className = "com.invera.stratix.services.ApplyIRCDistResponse")
    public void applyIRCDist(
        @WebParam(name = "companyId", targetNamespace = "http://stratix.invera.com/services")
        String companyId,
        @WebParam(name = "voucherPrefix", targetNamespace = "http://stratix.invera.com/services")
        String voucherPrefix,
        @WebParam(name = "voucherNumber", targetNamespace = "http://stratix.invera.com/services")
        int voucherNumber)
        throws StratixFault
    ;

}
