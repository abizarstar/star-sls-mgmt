<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>OCR AP Service Calls</title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<script src="assets/js/jquery.cookie.js"></script>
<script src="assets/js/jquery.fileDownload.js"></script>
<script src="assets/js/script.js"></script>

<style>
html, body {
	height: 100%;
}

.card-header {
	padding: 5px 10px;
}

.card-body {
	padding: 10px;
}

.btn {
	padding: 0.175rem .35rem;
	font-size: 0.9rem;
	border-radius: .15rem;
}

#overlay {
	background: #ffffff;
	color: #666666;
	position: absolute;
	height: 100%;
	width: 100%;
	z-index: 5000;
	top: 0;
	left: 0;
	float: left;
	text-align: center;
	padding-top: 5px;
	opacity: .80;
	padding-right: 20px;
}

.spinner {
	margin: 0 auto;
	height: 40px;
	width: 40px;
	animation: rotate 0.8s infinite linear;
	border: 3px solid firebrick;
	border-right-color: transparent;
	border-radius: 50%;
	float: right;
}

@
keyframes rotate { 0% {
	transform: rotate(0deg);
}
100%
{
transform

















rotate

















(360
deg












);
}
}
</style>

<script>
	var inputData = {
		//bnkCode : bnkCd,
		//acctId : acctId,
		/* 	wkfName : "Vendor 1010",
			wkfId :4,
			crtdDate : "11-11-2020",
			crtdBy : "Ankur",
			stepName : "step12312",
			amtFlg : 1,
			brhFlg : 0,
			venFlg : 0,
			dayFlg : 0,
			amtFrm : 1200,
			amtTo : 8000,
			brh : "",
			venId : "",
			days : 0,
			dayBef : 0,
			apvr : "Abizhar",
			brhVndrSize:0,
			crcnFlg : 0,
			costRecon:0,
		 */
		ctlNo : 238,
		notesId : 2,
		user : "rijot",
		comment : "this is test msg",
		CUS_ID : "1000",
		ORD_NO :2,
		REF_PFX : "SO",
		REF_NO : 112,
		poNo : "102",
		poItm : 2,
		ctlNo : 1011,
		refPfx : "SQ",
		refNo : 111,
		ordNo : 1,
		refNo : 3285,
		refItm : 30,
	};
	/* 	{
	 "slsOrdData": [
	 {
	 "fldTyp": "CUS_ID",
	 "fldVal": "1000"
	 },
	 {
	 "fldTyp": "PO_DT",
	 "fldVal": "09-12-2020"
	 },
	 {
	 "fldTyp": "PO_NO",
	 "fldVal": "B02100430"
	 },
	 {
	 "fldTyp": "CUST_ITEM",
	 "fldVal": "TEST1"
	 },
	 {
	 "fldTyp": "LN_NO",
	 "fldVal": ""
	 },
	 {
	 "fldTyp": "SHP_TO_1",
	 "fldVal": "0"
	 },
	 {
	 "fldTyp": "SHP_TO_2",
	 "fldVal": "1"
	 },
	 {
	 "fldTyp": "SHP_TO_3",
	 "fldVal": "2"
	 },
	 {
	 "fldTyp": "DESC",
	 "fldVal": "3/4\" #9 FL X METAL"
	 },
	 {
	 "fldTyp": "DEL_DT",
	 "fldVal": "12/16/20"
	 },
	 {
	 "fldTyp": "QTY",
	 "fldVal": "5000.000"
	 },
	 {
	 "fldTyp": "UM",
	 "fldVal": "SFT"
	 },
	 {
	 "fldTyp": "UNIT_PRICE",
	 "fldVal": "32"
	 },
	 {
	 "fldTyp": "ORD_TYP",
	 "fldVal": "N"
	 },
	 {
	 "fldTyp": "SLS_CAT",
	 "fldVal": "SP"
	 },
	 {
	 "fldTyp": "TKN_SLP",
	 "fldVal": "ABC"
	 },
	 {
	 "fldTyp": "REF_BRH",
	 "fldVal": "BIR"
	 },
	 {
	 "fldTyp": "FRM",
	 "fldVal": "SR"
	 },
	 {
	 "fldTyp": "GRD",
	 "fldVal": "4301"
	 },
	 {
	 "fldTyp": "SIZE",
	 "fldVal": "50"
	 },
	 {
	 "fldTyp": "FNSH",
	 "fldVal": "TEST"
	 },
	 {
	 "fldTyp": "WIDTH",
	 "fldVal": "0.0000"
	 },
	 {
	 "fldTyp": "LENGTH",
	 "fldVal": "6000.0000"
	 },
	 {
	 "fldTyp": "SOURCE",
	 "fldVal": "D"
	 },
	 {
	 "fldTyp": "IS_SLSPSN",
	 "fldVal": "ADT"
	 },
	 {
	 "fldTyp": "OS_SLSPSN",
	 "fldVal": "ADT"
	 },
	 {
	 "fldTyp": "WRK_ORD_DT",
	 "fldVal": "11-12-2020"
	 },
	 {
	 "fldTyp": "FRT_CHARGE",
	 "fldVal": "1.7"
	 },
	 {
	 "fldTyp": "FRT_COST",
	 "fldVal": "1.7"
	 },
	 {
	 "fldTyp": "STATUS",
	 "fldVal": ""
	 },
	 {
	 "fldTyp": "SHP_BRH",
	 "fldVal": "BIR"
	 },
	 {
	 "fldTyp": "VENDOR",
	 "fldVal": ""
	 },
	 {
	 "fldTyp": "FOB_PT",
	 "fldVal": ""
	 },
	 {
	 "fldTyp": "AMOUNT",
	 "fldVal": "1600"
	 },
	 {
	 "fldTyp": "ORD_NO",
	 "fldVal": "3"
	 },
	 {
	 "fldTyp": "Ref_PFX",
	 "fldVal": "SO"
	 },
	 {
	 "fldTyp": "REF_NO",
	 "fldVal": "111"
	 }
	 ]
	 }
	 */

	$.ajax({
		headers : ajaxHeader,
		method : "Post",
		data : JSON.stringify(inputData),
		//	url : rootAppName + '/rest/wkf/vldt-stp',
		//	url : rootAppName + '/rest/wkf/read-wkf-chk',
		//	url : rootAppName + '/rest/wkf/create-wkf',
		// url : rootAppName + '/rest/vchr-note/read',
		url : rootAppName + '/rest/StarslsOrd/read-starsoitmfull',
		success : function(response) {
		},
		error : function(jqXHR, textStatus, errorThrown) {
			if (jqXHR.status == 401) {
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
</script>

</head>
<body>

</body>
</html>