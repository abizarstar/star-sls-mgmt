var ajaxHeader = { 
	'Accept' : 'application/json',
	'Content-Type' : 'application/json',
	'user-id': localStorage.getItem("starSlsMgmtUserId"),
	'auth-token': localStorage.getItem("starSlsMgmtAuthToken"),
	'app-host' : localStorage.getItem("starSlsMgmtAppHost"),
	'app-port' : localStorage.getItem("starSlsMgmtAppPort"),
	'cmpy-id' : localStorage.getItem("starSlsMgmtCmpyId"),
	'app-token' : localStorage.getItem("starSlsMgmtAppToken"),
	'user-grp' : localStorage.getItem("starSlsMgmtUsrGrp"),
	'nginx-URL' : localStorage.getItem("starSlsMgmtNginxURL"),
};
var glblCmpyId = "";

var urlWithData = document.location.href;

if(location.host.indexOf("auxinvex") > -1 || location.host.indexOf("invex.cloud") > -1)
{
	rootAppName = "/" + window.location.pathname.split("/")[1] + "/" + window.location.pathname.split("/")[2];
}
else
{
	rootAppName = "/" + window.location.pathname.split("/")[1] ;
}


if( location.pathname.indexOf("login.html") <= -1 && location.pathname.indexOf(".html") > -1 ){
	$("#main-menu-navigation").html(localStorage.getItem("starSlsMgmtSidebarMenu"));
	activeSidebarMenu();
}
	
var ajaxCallCnt = 0, ajaxResponseCnt = 0;

var glblBrhList = $("#brh").html();
var glblVndrList = $("#vndr").html();
var glblCusList = $("#cusId").html();
var glblSlsCatList = $("#slsCat").html();
var glblTknSlpList = $("#tknSlp").html();
var glblOrdTypList = $("#ordTyp").html();
var glblUmList = $("#um").html();
var glblLgthTypList = $("#lgthTyp").html();
var glblPrsRteList = $("#prsRte").html();
var glblWhsList = $("#whs").html();

$(function(){

	setAjaxHeader();
	
	if( location.pathname.indexOf("login.html") <= -1 ){
		if( localStorage.getItem("starSlsMgmtAuthToken") != null && localStorage.getItem("starSlsMgmtAuthToken") != "" ){
			validateAuthCode();
	
			//getSidebarMenus();
		}else{
			window.location.replace("login.html");
		}
	}
	
	//if( location.pathname.indexOf("login.html") <= -1 && location.pathname.indexOf("index.html") <= -1 && location.pathname.indexOf(".html") > -1 ){
	if( location.pathname.indexOf("login.html") <= -1 && location.pathname.indexOf("upld-invc.html") <= -1 && location.pathname.indexOf(".html") > -1 ){
		$("#main-menu-navigation").html(localStorage.getItem("starSlsMgmtSidebarMenu"));
		activeSidebarMenu();
	}
	

	glblCmpyId = $("#headerCmpyId").val();
	
	if( glblCmpyId == "" || glblCmpyId == null || glblCmpyId == undefined ){
		glblCmpyId = localStorage.getItem("starSlsMgmtCmpyId");
	}

	$("#mainAppWrapper").hide();
	
	$("#headerNavbar").load("includes/header.html", function(){
		headerLoaded();
	});
	
	$("#sidebarWrapper").load("includes/sidebar.html", function(){
		activeSidebarMenu();
	});
	
	$("#footerWrapper").load("includes/footer.html", function(){
		//console.log( "Footer Loaded" );
	});
		
	if( $.isFunction( $.fn.select2 ) ){
		$(".select2").select2();
	}
	
	$('[data-toggle="tooltip"]').tooltip();	
	
	if( $.isFunction( $.fn.daterangepicker ) ){
		$('.date').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true,
			opens: 'left',
			locale: {
				format: 'MM/DD/YYYY'
			}
		});
		
			
		$('body').on('focus',"input[name='daterange']", function(){
			$('input[name="daterange"]').daterangepicker({
				autoUpdateInput: false,
				opens: 'left',
				locale: {
					format: 'MM/DD/YYYY'
				}
			}, function(start, end, label) {
				console.log("A new date selection was made: " + start.format('MM/DD/YYYY') + ' to ' + end.format('MM/DD/YYYY'));
			});
			
			$('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
				$(this).trigger("change");
			});
	
			$('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
				$(this).val('');
				$(this).trigger("change");
			});
	
		});
	}
	
	$(".custom-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
	  	$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	});
	
	$("#headerNavbar").on("click", ".logout-btn", function(){
		$("#logoutModal").modal("show");
	});
	
	$(document).on("click", "#logout", function(e){
		e.preventDefault();
		
		localStorage.removeItem("starSlsMgmtAuthToken");
	    localStorage.removeItem("starSlsMgmtUserId");
	    localStorage.removeItem("starSlsMgmtUserNm");
	    localStorage.removeItem("starSlsMgmtEmailId");
	    localStorage.removeItem("starSlsMgmtUsrGrp");
	    localStorage.removeItem("starSlsMgmtAppHost");
	    localStorage.removeItem("starSlsMgmtAppPort");
	    localStorage.removeItem("starSlsMgmtAppToken");
	    localStorage.removeItem("starSlsMgmtCmpyId");
	    
	    localStorage.removeItem("starSlsMgmtSidebarMenu");
	    
	    $.removeCookie('auth-token');
	    $.removeCookie('app-token');
	    $.removeCookie('user-id');
		
		window.location.replace("login.html");
	});
	
	
	
	$("#excelDataTblWrapper").on("click", ".icon-info", function(e){
		var msgStr = "";
		
		//console.log(e);
		
		if( $(this).hasClass("text-success") ){
			msgStr = "<div class='alert alert-success m-0'>" + $(this).attr("data-msg") + "</div>";
		}else if( $(this).hasClass("text-danger")){
			var tmpStr = $(this).attr("data-msg");
			var tmpArr = tmpStr.split("|");
			var textClass = "";
			
			for( var i=0; i<tmpArr.length; i++){
				textClass = "";
				
				if(tmpArr[i].indexOf("Table:") != -1 || tmpArr[i].indexOf("Transaction cannot be processed") != -1){
				    continue;
				}
				
				if( tmpArr[i].charAt(0) == "W" ){
					textClass = "text-warning";
				}else if( tmpArr[i].charAt(0) == "E" ){
					textClass = "text-danger";
				}
				
				msgStr += "<p class='"+ textClass +" mb-1'>"+ tmpArr[i] +"</p>";
			}
		}
		
		if( msgStr != "" ){
			$("#viewSlsOrdSts .msg-area").html( msgStr );
			$("#viewSlsOrdSts .msg-area").css({"max-height": e.pageY - 105 + "px"});
			
			if( (e.pageY - 70 - $("#viewSlsOrdSts").innerHeight()) <= 20){
				$("#viewSlsOrdSts").css({"top": "0px", "left": e.pageX - 101 +"px"});
			}else{
				$("#viewSlsOrdSts").css({"top": e.pageY - 50 - $("#viewSlsOrdSts").innerHeight() + "px", "left": e.pageX - 101 +"px"});
			}
			
			$("#viewSlsOrdSts").show();
		}
			
	});
	

	$(document).mouseup(function(e){
		var container = $("#viewSlsOrdSts");

	    // if the target of the click isn't the container nor a descendant of the container
	    if (!container.is(e.target) && container.has(e.target).length === 0) {
	        container.hide();
	    }
	    
	    /*var container = $("#sendForReviewPopup");

	    // if the target of the click isn't the container nor a descendant of the container
	    if( e.target.attributes.length && e.target.attributes[0].nodeValue.indexOf("select2-results__option") < 0 && e.target.id != "assignAprv" ){
		    if (!container.is(e.target) && container.has(e.target).length === 0) {
		        container.hide();
		    }
	    }*/
	    
	});
	
	$('#mainNotify').bind('DOMSubtreeModified', function(){
		if( $('#mainNotify').html() == "" ){
			$( "#viewSlsOrdSts" ).hide();
		}
	});
	
	$("#processData").click(function(){
		if($("#excelDataTbl input[type='checkbox'][name='select-sls-ord-item']:checked").length <= 0){
			customAlert("#mainNotify", "alert-danger", "Please select any record to create Sales Order.");
			return;
		}
		
		//$("#mainLoader").show();
		$("#processData").hide();
		
		$("#excelDataTblWrapper").scrollLeft("0");
		
		ajaxCallCnt = 0, ajaxResponseCnt = 0;
		var rowNo = 0;
		var multiSlsOrdData = [];
		var lastCusId = "", lastPoNo = "";
		$("#excelDataTbl tbody tr").each(function(index, tr){
			rowNo++;
			var $tr = $(tr);
			
			if( $(tr).find("input[type='checkbox'][name='select-sls-ord-item']").prop("checked") ){
				var singleSlsOrdData = [];
				var curCusId = $tr.find('.form-control[data-type="CUS_ID"]').val();
				var curPoNo = $tr.find('.form-control[data-type="PO_NO"]').val();
				
				$tr.find(".sls-ord-sts").html('<div class="circle-loader"></div>');
				
				var slsOrdFldObj = {
					"fldTyp": "DOC_ID",
					"fldVal": $(tr).attr("data-doc-id"),
				};
				singleSlsOrdData.push(slsOrdFldObj);
				
				var slsOrdFldObj = {
					"fldTyp": "DOC_ROW_NO",
					"fldVal": rowNo,
				};
				singleSlsOrdData.push(slsOrdFldObj);
					
				$tr.find("td").each(function(index, td){
					var $td = $(td);
					var fldTyp = $td.find(".form-control").attr("data-type");
					var fldVal = $td.find(".form-control").val();
					
					if( fldVal != "" && fldVal != null && fldVal != undefined ){
						fldVal = fldVal.trim();
					}else{
						fldVal = "";
					}
					
					if( fldTyp != undefined ){
						if( fldTyp.indexOf("SHP_TO") >= 0 && fldVal != "" ){
							var shpToArr = fldVal.split("-");
							fldVal = shpToArr[0];
						}
						
						if( (fldTyp == "PO_DT" || fldTyp == "DEL_DT" || fldTyp == "WRK_ORD_DT") && fldVal != "" ){
							fldVal = fldVal.split("-").join("/");
						}
						
						var slsOrdFldObj = {
							"fldTyp": fldTyp,
							"fldVal": fldVal,
						};
						singleSlsOrdData.push(slsOrdFldObj);
					}
				});
				
				var slsOrdFldObj = {"fldTyp": "EXRT", "fldVal": "1"};
				singleSlsOrdData.push(slsOrdFldObj);
				
				var slsOrdFldObj = {"fldTyp": "EXRT_TYP", "fldVal": "V"};
				singleSlsOrdData.push(slsOrdFldObj);
				
				var slsOrdFldObj = {"fldTyp": "FLW_UP_DAYS", "fldVal": $("#flwDays").val()};
				singleSlsOrdData.push(slsOrdFldObj);
				
				var slsOrdFldObj = {"fldTyp": "EXPIRY_DAYS", "fldVal": $("#expiryDays").val()};
				singleSlsOrdData.push(slsOrdFldObj);
				
				var buildUpFlg  = "0";
				if($("#buildUp").prop("checked"))
				{
					buildUpFlg = "1";
				}
				
				var slsOrdFldObj = {"fldTyp": "BUILD_UP", "fldVal": buildUpFlg};
				singleSlsOrdData.push(slsOrdFldObj);
				
				var slsOrdFldObj = {"fldTyp": "ORD_TYP", "fldVal": $("#ordTypOption").val()};
				singleSlsOrdData.push(slsOrdFldObj);
				
				var slsOrdFldObj = {"fldTyp": "ORD_PFX", "fldVal": $("#ordPfxOption").val()};
				singleSlsOrdData.push(slsOrdFldObj);
				
				if( lastCusId != "" && lastPoNo != "" ){
					if( lastCusId != curCusId || lastPoNo != curPoNo ){
						ajaxCallCnt++;
						
						createSalesOrder(multiSlsOrdData);
						
						multiSlsOrdData = [];
					}
				}
				
				multiSlsOrdData.push(singleSlsOrdData);
				
				lastCusId = curCusId;
				lastPoNo = curPoNo;
			}
		});
		
		if( multiSlsOrdData.length ){
			ajaxCallCnt++;
			
			createSalesOrder(multiSlsOrdData);
			
			multiSlsOrdData = [];
		}
	});
	
});

function setAjaxHeader(){
	ajaxHeader = { 
		'Accept' : 'application/json',
		'Content-Type' : 'application/json',
		'user-id': localStorage.getItem("starSlsMgmtUserId"),
		'auth-token': localStorage.getItem("starSlsMgmtAuthToken"),
		'app-host' : localStorage.getItem("starSlsMgmtAppHost"),
		'app-port' : localStorage.getItem("starSlsMgmtAppPort"),
		'cmpy-id' : localStorage.getItem("starSlsMgmtCmpyId"),
		'app-token' : localStorage.getItem("starSlsMgmtAppToken"),
		'user-grp' : localStorage.getItem("starSlsMgmtUsrGrp"),
		'nginx-URL' : localStorage.getItem("starSlsMgmtNginxURL"),
	};
}

function headerLoaded(){
	$("#headerCmpyId").attr("data-selected", localStorage.getItem("starSlsMgmtCmpyId"));
	
	getCmpyIdList( $("#headerCmpyId"), false );
	
	if( $.isFunction( $.fn.select2 ) ){
		$(".select2").select2();
	}
	
	var pageExtn = ".html";
	var pageNm = window.location.pathname.split("/")[2];
	var extIndx = pageNm.indexOf(pageExtn);
	pageNm = pageNm.substr(0, extIndx + pageExtn.length);
	
	$("#headerNavbar .header-page-title").html( $("#sidebar").find('a[href="'+ pageNm +'"]').find("span.menu-page-title").html() );
}

function activeSidebarMenu(){
	$("#sidebar li").removeClass("active");
	
	var pageExtn = ".html";
	var pageNm = window.location.pathname.split("/")[2];
	var extIndx = pageNm.indexOf(pageExtn);
	pageNm = pageNm.substr(0, extIndx + pageExtn.length);
	
	$("#sidebar").find('a[href="'+ pageNm +'"]').parent().addClass("active");
	
	if( $("#sidebar").find('a[href="'+ pageNm +'"]').parent().parent().hasClass("sub-menu") ){
		$("#sidebar").find('a[href="'+ pageNm +'"]').parent().parent().parent().addClass("show");
	}
	
	$("#headerNavbar .header-page-title").html( $("#sidebar").find('a[href="'+ pageNm +'"]').find("span.menu-page-title").html() );
	
	if( pageNm == "vldt-trans.html" ){
		$("body").addClass("sidebar-icon-only");
	}
}

function getSidebarMenus(){
	var inputData = {
		grpId: localStorage.getItem("starSlsMgmtUsrGrp"),
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/menu/read',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){		            	
				var loopLimit = response.output.fldTblMenu.length;
				var innerLoopLimit = 0;
				var sidebarMenu = '';
				var redirectUrl = "";
				
				for( var i=0; i<loopLimit; i++){
					innerLoopLimit = response.output.fldTblMenu[i].subMenuList.length;
					if( innerLoopLimit ){
						if( i == 0 ){
							redirectUrl = response.output.fldTblMenu[i].subMenuList[0].menuHtml;
						}
					
						if( response.output.fldTblMenu[i].menuNm == "Settings" ){
							sidebarMenu += '<li class="nav-item d-none d-lg-block">'+
												'<div class="spacer">'+
													'<div class="progress progress-sm">'+
														'<div class="progress-bar bg-primary" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>'+
													'</div>'+
												'</div>'+
											'</li>';
						}
						var d = new Date();
						var uniqueId = "subMenu" + d.valueOf() + "" + i;
						sidebarMenu += '<li class="nav-item has-child">'+
											'<a class="nav-link has-child" href="#'+ uniqueId +'" data-toggle="collapse" aria-expanded="false">'+
												'<i class="menu-icon '+ response.output.fldTblMenu[i].menuIcon +'"></i>'+
												'<span class="menu-title">'+ response.output.fldTblMenu[i].menuNm +'</span>'+
											'</a>'+
											'<div class="collapse" id="'+ uniqueId +'">'+
												'<ul class="nav flex-column sub-menu">';
											
						for( var j=0; j<innerLoopLimit; j++){
							sidebarMenu += 			'<li class="nav-item">'+
														'<a href="'+ response.output.fldTblMenu[i].subMenuList[j].menuHtml +'" class="nav-link">'+ 
															'<i class="menu-icon '+ response.output.fldTblMenu[i].subMenuList[j].menuIcon +'"></i>'+
															'<span class="menu-page-title">'+ response.output.fldTblMenu[i].subMenuList[j].menuNm +'</span>'+
														'</a>'+
													'</li>';
						}
						sidebarMenu += 			'</ul>'+
											'</div>'+
										'</li>';
					}else{
						if( i == 0 ){
							redirectUrl = response.output.fldTblMenu[i].menuHtml;
						}
						sidebarMenu += '<li class="nav-item">'+
											'<a href="'+ response.output.fldTblMenu[i].menuHtml +'" class="nav-link">'+
												'<i class="menu-icon '+ response.output.fldTblMenu[i].menuIcon +'"></i>'+
												'<span class="menu-title menu-page-title">'+ response.output.fldTblMenu[i].menuNm +'</span>'+
											'</a>'+
										'</li>';
					}
				}
				
				//$("#main-menu-navigation").html(sidebarMenu);
				//activeSidebarMenu();
				
				localStorage.setItem("starSlsMgmtSidebarMenu", sidebarMenu);
				
				redirectUrl = (redirectUrl=="") ? "index.html" : redirectUrl;
				
				window.location.replace(redirectUrl);
	
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}

function setLoggedInUserName(){
	if( $("#loggedInUser").length ){	    
	    if( localStorage.getItem("starSlsMgmtUserNm") != null && localStorage.getItem("starSlsMgmtUserNm") != "" ){
	    	$("#loggedInUser").html( localStorage.getItem("starSlsMgmtUserNm") );
	    }else if( localStorage.getItem("starSlsMgmtUserId") != null && localStorage.getItem("starSlsMgmtUserId") != "" ){
	    	$("#loggedInUser").html( localStorage.getItem("starSlsMgmtUserId") );
	    }
	}else{
		setTimeout(function(){
			setLoggedInUserName();
		}, 500);
	}
}


function validateAuthCode(){
	$("#mainAppWrapper").show();
	
	$.ajax({
		headers: ajaxHeader,
		type: 'POST',
		url: rootAppName + '/rest/auth-page/valid',
		success: function(response, textStatus, jqXHR) {
			setLoggedInUserName();
		    
		    $("#mainAppWrapper").hide();
		},
		complete: function(jqXHR, textStatus) {

		},
		error: function( jqXHR, textStatus, errorThrown ){  
		    localStorage.removeItem("starSlsMgmtAuthToken");
		    localStorage.removeItem("starSlsMgmtUserId");
		    localStorage.removeItem("starSlsMgmtUserNm");
		    localStorage.removeItem("starSlsMgmtEmailId");
		    localStorage.removeItem("starSlsMgmtUsrGrp");
		    localStorage.removeItem("starSlsMgmtAppHost");
		    localStorage.removeItem("starSlsMgmtAppPort");
		    localStorage.removeItem("starSlsMgmtAppToken");
		    localStorage.removeItem("starSlsMgmtCmpyId");
		    
		    localStorage.removeItem("starSlsMgmtSidebarMenu");
		    
		    $.removeCookie('auth-token');
		    $.removeCookie('app-token');
		    $.removeCookie('user-id');
		    
		    window.location.replace("login.html");
		}
	});
}

function customAlert(alertArea, alertType, alertMessage){
	var uniqueId = new Date().valueOf();
	var message = '<div style="display:none;" class="alert ' + alertType + ' alert-dismissible mb-3" role="alert" id="cstmAlert-'+ uniqueId +'">' +
					'<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
						'<span aria-hidden="true">×</span>' +
					'</button>' + alertMessage +
				'</div>';
	$(alertArea).html( message );
	$("#cstmAlert-" + uniqueId).slideDown();
	
	$('html, body, .content-wrapper').animate({
        scrollTop: $(alertArea).offset().top - 100
    }, 'slow');
	
	setTimeout(function(){
		if( $("#cstmAlert-" + uniqueId).length ){
			$("#cstmAlert-" + uniqueId).slideUp(1000, function(){
				$(this).remove();
			})
		}
	}, 10000);
}


function getCountryList( $select, allowBlank=true ){
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/read-cty',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblCountry.length;
				var cty = '', ctyDesc = '', options = '';	
				
				if( allowBlank ){
					options = '<option value="">&nbsp;</option>';
				}
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					cty = response.output.fldTblCountry[i].cty.trim();
					ctyDesc = response.output.fldTblCountry[i].ctyDesc.trim();
					
					if( dfltOption == cty ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ cty +'" '+ selectOption +'>'+ cty + ' - ' + ctyDesc + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}

function getCmpyIdList( $select, allowBlank=true  ){
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/cmpy-list',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblCompany.length;
				var cmpyId = '', cmpyNm = '', options = '';
				
				if( allowBlank ){
					options = '<option value="">&nbsp;</option>';
				}
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					cmpyId = response.output.fldTblCompany[i].cmpyId.trim();
					cmpyNm = response.output.fldTblCompany[i].cmpyNm.trim();
					
					if( dfltOption == cmpyId ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ cmpyId +'" '+ selectOption +'>'+ cmpyId + ' - ' + cmpyNm + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}


function getVendorList( $select, cmpyId, allowBlank=true ){
	if( cmpyId != "" ){
		var inputData = {
			cmpyId : cmpyId,
		};
		
		$.ajax({
			headers: ajaxHeader,
			url: rootAppName + '/rest/common/ven-list',
			type: 'POST',
			data : JSON.stringify( inputData ),
			dataType: 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					var loopSize = response.output.fldTblVendor.length;
					var venId = '', venNm = '', venLongNm = '', venCry = '', options = '';	
					
					if( allowBlank ){
						options = '<option value="">&nbsp;</option>';
					}
					
					var dfltOption = "", selectOption = "";
					if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
						dfltOption = $select.attr("data-selected");
						
						$select.attr("data-selected", "");
					}
				
					for( var i=0; i<loopSize; i++ ){
						selectOption = "";
						
						venId = response.output.fldTblVendor[i].venId.trim();
						venNm = response.output.fldTblVendor[i].venNm.trim();
						//venLongNm = response.output.fldTblVendor[i].venLongNm.trim();
						//venCry = response.output.fldTblVendor[i].venCry.trim();
						
						if( dfltOption == venId ){
							selectOption = "selected";
						}
						
						options += '<option value="'+ venId +'" data-vndr-nm="'+ venNm +'" '+ selectOption +'>'+ venId + ' - ' + venNm + '</option>';
					}
					$select.html( options );
					
					if( $select.attr("data-default-value") != undefined && $select.attr("data-default-value") != "" ){
						$select.val( $select.attr("data-default-value") ).trigger("change");
					}else{
						//$("#mainLoader").hide();
					}
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
					//$("#mainLoader").hide();
				}			
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				//$("#mainLoader").hide();
			}
		});
	}else{
		cmpyId = glblCmpyId;
		
		setTimeout(function(){
			getVendorList( $select, cmpyId );
		}, 500)
	}
}


function getStxUserList( $select, allowBlank=true  ){
	//$("#mainLoader").show();
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/user/read-erp',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblERPUser.length;
				var usrId = '', usrNm = '', usrEmail = '', options = '';
				
				if( allowBlank ){
					options = '<option value="">&nbsp;</option>';
				}
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
					
				for( var i=0; i<loopSize; i++ ){
					selectOption = '';
					
					usrId = response.output.fldTblERPUser[i].usrId.trim();
					usrNm = response.output.fldTblERPUser[i].usrNm.trim();
					usrEmail = response.output.fldTblERPUser[i].usrEmail.trim();
					
					if( dfltOption == usrId ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ usrId +'" data-usr-nm="'+ usrNm +'" data-usr-email="'+ usrEmail +'" '+ selectOption +'>('+ usrId + ') ' + usrNm + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				//$("#mainLoader").hide();
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}

function getUserGroupList( $select, allowBlank=true  ){
	$.ajax({
		headers: ajaxHeader,
		url : rootAppName + '/rest/usrgroup/read',
		data : JSON.stringify({ grp: "" }),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			var loopSize = response.output.fldTblUsrGroup.length;
			var grpId = '', grpNm = '', options = '';	
			
			if( allowBlank ){
				options = '<option value="">&nbsp;</option>';
			}
			
			var dfltOption = "", selectOption = "";
			if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
				dfltOption = $select.attr("data-selected");
				
				$select.attr("data-selected", "");
			}
				
			for( var i=0; i<loopSize; i++ ){
				selectOption = '';
				
				grpId = response.output.fldTblUsrGroup[i].grpId;
				grpNm = response.output.fldTblUsrGroup[i].grpNm;
				
				if( dfltOption == grpId ){
					selectOption = "selected";
				}
				
				//if( grpNm.trim().toLowerCase() != "admin" ){
					options += '<option value="'+ grpId +'" '+ selectOption +'>'+ grpNm + '</option>';
				//}
			}
			$select.html( options );			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}


function getCustomerList( $select, allowBlank=true  ){
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/cus-list',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblCustomer.length;
				var cusId = '', cusNm = '', options = '';
				
				if( allowBlank ){
					options = '<option value="">&nbsp;</option>';
				}	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					cusId = response.output.fldTblCustomer[i].cusId.trim();
					cusNm = response.output.fldTblCustomer[i].cusNm.trim();
					
					if( dfltOption == cusId ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ cusId +'" '+ selectOption +'>'+ cusId + ' - ' + cusNm + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}


function getLengthTypeList( $select, allowBlank=true  ){
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/lgth-typ-list',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblLengthType.length;
				var cusId = '', cusNm = '', options = '';
				
				if( allowBlank ){
					options = '<option value="">&nbsp;</option>';
				}	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					lgthType = response.output.fldTblLengthType[i].lgthTyp.trim();
					lgthTypeDesc = response.output.fldTblLengthType[i].lgthTypDesc.trim();
					
					
					options += '<option value="'+ lgthType +'">'+ lgthType + ' - ' + lgthTypeDesc + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}

function getProcessRouteList( $select, cmpyId, allowBlank=true ){
	if( cmpyId != "" ){
		var inputData = {
			cmpyId : cmpyId,
		};
		
		$.ajax({
			headers: ajaxHeader,
			url: rootAppName + '/rest/common/std-prs-list',
			type: 'POST',
			data : JSON.stringify( inputData ),
			dataType: 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					var loopSize = response.output.fldTblPrsRte.length;
					var prsRte = '', prsRteDesc = '', options = '';	
					
					if( allowBlank ){
						options = '<option value="">&nbsp;</option>';
					}
					
					var dfltOption = "", selectOption = "";
					if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
						dfltOption = $select.attr("data-selected");
						
						$select.attr("data-selected", "");
					}
				
					for( var i=0; i<loopSize; i++ ){
						selectOption = "";
						
						prsRte = response.output.fldTblPrsRte[i].prsRte.trim();
						prsRteDesc = response.output.fldTblPrsRte[i].prsRteDesc.trim();
						
						
						options += '<option value="'+ prsRte +'" data-prs-rte="'+ prsRteDesc +'" '+ selectOption +'>'+ prsRte + ' - ' + prsRteDesc + '</option>';
					}
					$select.html( options );
					
					if( $select.attr("data-default-value") != undefined && $select.attr("data-default-value") != "" ){
						$select.val( $select.attr("data-default-value") ).trigger("change");
					}else{
						//$("#mainLoader").hide();
					}
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
					//$("#mainLoader").hide();
				}			
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				//$("#mainLoader").hide();
			}
		});
	}else{
		cmpyId = glblCmpyId;
		
		setTimeout(function(){
			getProcessRouteList( $select, cmpyId );
		}, 500)
	}
}


function getWhsList( $select){
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/whs-list',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblWhs.length;
				var whs = '', whsNm = '', options = '';	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					whs = response.output.fldTblWhs[i].whs.trim();
					whsNm = response.output.fldTblWhs[i].whsNm.trim();
					
					if( dfltOption == whs ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ whs +'" '+ selectOption +'>'+ whs + ' - ' + whsNm + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}



function getUserPref(allowBlank=true ){
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/usr-pref',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblUsrPref.length;
				var flwUpDays = '', expiryDays = '', buildUp = '';	
						
				flwUpDays = response.output.fldTblUsrPref[0].flwUpDays;
				expiryDays = response.output.fldTblUsrPref[0].expiryDays;
				buildUp = response.output.fldTblUsrPref[0].buildUp;
				isAvlbl = response.output.fldTblUsrPref[0].isAvlbl;
				
				$("#flwDays").val(flwUpDays);
				$("#expiryDays").val(expiryDays);
				
				if(buildUp == 0)
				{
					$("#buildUp").prop("checked", false);
				}
				else
				{
					$("#buildUp").prop("checked", true);
				}
				
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}


function getBranchList( $select, allowBlank=true ){
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/brh-list',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblBranch.length;
				var brhBrh = '', brhBrhNm = '', options = '';	
				
				if( allowBlank ){
					options = '<option value="">&nbsp;</option>';
				}
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					brhBrh = response.output.fldTblBranch[i].brhBrh.trim();
					brhBrhNm = response.output.fldTblBranch[i].brhBrhNm.trim();
					
					if( dfltOption == brhBrh ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ brhBrh +'" '+ selectOption +'>'+ brhBrh + ' - ' + brhBrhNm + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}


function getStxBankList( $select, allowBlank=true  ){
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/bnk-list',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblBank.length;
				var bnk = '', bnkDesc = '', bnkCry = '', options = '', excRt = 0.0;	
				
				if( allowBlank ){
					options = '<option value="">&nbsp;</option>';
				}
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					bnk = response.output.fldTblBank[i].bnk.trim();
					bnkDesc = response.output.fldTblBank[i].bnkDesc.trim();
					bnkCry = response.output.fldTblBank[i].bnkCry.trim();
					excRt = response.output.fldTblBank[i].excRt;
					
					if( dfltOption == bnk ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ bnk +'" data-cry="'+ bnkCry +'" data-exrt="'+ excRt +'" '+ selectOption +'>'+ bnk + ' - ' + bnkDesc + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}


function getCurrencyList( $select, allowBlank=true  ){
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/cry-list',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblCry.length;
				var cry = '', cryDesc = '', options = '<option value="">&nbsp;</option>';	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					cry = response.output.fldTblCry[i].cry.trim();
					cryDesc = response.output.fldTblCry[i].cryDesc.trim();
					
					if( dfltOption == cry ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ cry +'" '+ selectOption +'>'+ cry + ' - ' + cryDesc + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}


function getShipToList( $select, cusId, allowBlank=true  ){
	$("#mainLoader").show();
	
	var inputData = {
		cusId: cusId,
	};
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/shp-to',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblShpTo.length;
				var shpTo = '', shpToNm = '', options = '<option value="">&nbsp;</option>';	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					shpTo = response.output.fldTblShpTo[i].shpTo.trim();
					shpToNm = response.output.fldTblShpTo[i].shpToNm.trim();
					
					if( dfltOption == shpTo ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ shpTo +'" '+ selectOption +'>'+ shpTo + ' - ' + shpToNm + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();	
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getSalesPersonList( $select, allowBlank=true  ){
	var inputData = {};
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/slp',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblSlp.length;
				var slp = '', slpNm = '', options = '<option value="">&nbsp;</option>';	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					slp = response.output.fldTblSlp[i].slp.trim();
					slpNm = response.output.fldTblSlp[i].slpNm.trim();
					
					if( dfltOption == slp ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ slp +'" '+ selectOption +'>'+ slp + ' - ' + slpNm + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}


function getSalesCategoryList( $select, allowBlank=true  ){
	var inputData = {};
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/sls-cat',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblSlsCat.length;
				var slsCat = '', slsCatDesc = '', options = '<option value="">&nbsp;</option>';	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					slsCat = response.output.fldTblSlsCat[i].slsCat.trim();
					slsCatDesc = response.output.fldTblSlsCat[i].slsCatDesc.trim();
					
					if( dfltOption == slsCat ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ slsCat +'" '+ selectOption +'>'+ slsCat + ' - ' + slsCatDesc + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}


function getUmList( $select ){
	var inputData = {};
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/um',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblUm.length;
				var um = '', desc = '', options = '';	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					um = response.output.fldTblUm[i].um.trim();
					desc = response.output.fldTblUm[i].desc.trim();
					
					if( dfltOption == um ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ um +'" '+ selectOption +'>('+ um + ') ' + desc + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}



function setHeader(col, title){
	$( $("#excelDataTbl thead tr th")[col] ).html(title);
	
	var title = title.toLowerCase();
	
	if( title == "branch" || title == "brh" || title == "ref_brh" ){
		$("#excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			var curVal = $col.find(".form-control").val();
			var selectOptions = '<select class="form-control select2 brh-list" width="100px" data-type="REF_BRH">'+ glblBrhList +'</select>';
			
			$col.html( selectOptions );
			
			$col.find( ".select2" ).val(curVal);
		});
	}else if( title == "vendor" ){
		$("#excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			var curVal = $col.find(".form-control").val();
			var selectOptions = '<select class="form-control select2 vndr-list" width="100px" data-type="VENDOR">'+ glblVndrList +'</select>';
			
			$col.html( selectOptions );
			
			$col.find( ".select2" ).val(curVal);
		});
	}else if( title == "length_typ" ){
		$("#excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			var curVal = $col.find(".form-control").val();
			var selectOptions = '<select class="form-control select2 lgth-typ-list" width="100px" data-type="LENGTH_TYP">'+ glblLgthTypList +'</select>';
			
			$col.html( selectOptions );
			
			$col.find( ".select2" ).val(curVal);
		});
	}else if( title == "prs_rte" ){
		$("#excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			var curVal = $col.find(".form-control").val();
			var selectOptions = '<select class="form-control select2 prs-rte-list" width="100px" data-type="PRS_RTE">'+ glblPrsRteList +'</select>';
			
			$col.html( selectOptions );
			
			$col.find( ".select2" ).val(curVal);
		});
	}else if( title == "length_typ" ){
		$("#excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			var curVal = $col.find(".form-control").val();
			var selectOptions = '<select class="form-control select2 lgth-typ-list" width="100px" data-type="LENGTH_TYP">'+ glblLgthTypList +'</select>';
			
			$col.html( selectOptions );
			
			$col.find( ".select2" ).val(curVal);
		});
	}else if( title == "src_whs" ){
		$("#excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			var curVal = $col.find(".form-control").val();
			var selectOptions = '<select class="form-control select2 whs-list" width="60px" data-type="WHS">'+ glblWhsList +'</select>';
			
			$col.html( selectOptions );
			
			$col.find( ".select2" ).val(curVal);
		});
	}else if( title == "shp_brh" ){
		$("#excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			var curVal = $col.find(".form-control").val();
			var selectOptions = '<select class="form-control select2 brh-list" width="100px" data-type="SHP_BRH">'+ glblBrhList +'</select>';
			
			$col.html( selectOptions );
			
			$col.find( ".select2" ).val(curVal);
		});
	}else if( title == "customer id" || title == "customerid" || title.indexOf("customer") >= 0 ){
		$("#excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			var curVal = $col.find(".form-control").val();
			var selectOptions = '<select class="form-control select2 cust-list" width="100%" data-type="CUS_ID">'+ glblCusList +'</select>';
			
			$col.html( selectOptions );
			
			$col.find( ".select2" ).val(curVal);
		});
	}else if( title == "sales category" || title == "salescategory" || title.indexOf("sls cat") >= 0 || title.indexOf("sales_cat") >= 0 ){
		$("#excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			var curVal = $col.find(".form-control").val();
			var selectOptions = '<select class="form-control select2 sls-cat-list" width="100%" data-type="SLS_CAT">'+ glblSlsCatList +'</select>';
			
			$col.html( selectOptions );
			
			$col.find( ".select2" ).val(curVal);
		});
	}else if( title.indexOf("sales person") >= 0 || title.indexOf("salesperson") >= 0 || title.indexOf("taken-by slp") >= 0 || title.indexOf("tkn_slspsn") >= 0 ){
		$("#excelDataTblWrapper #excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			var curVal = $col.find(".form-control").val();
			var selectOptions = '<select class="form-control select2 tkn-sls-list" width="100%" data-type="TKN_SLP">'+ glblTknSlpList +'</select>';
			
			$col.html( selectOptions );
			
			$col.find( ".select2" ).val(curVal);
		});
	}else if( title == "is_slspsn" ){
		$("#excelDataTblWrapper #excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			var curVal = $col.find(".form-control").val();
			var selectOptions = '<select class="form-control select2 is-slspsn-list" width="100%" data-type="IS_SLSPSN">'+ glblTknSlpList +'</select>';
			
			$col.html( selectOptions );
			
			$col.find( ".select2" ).val(curVal);
		});
	}else if( title == "os_slspsn" ){
		$("#excelDataTblWrapper #excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			var curVal = $col.find(".form-control").val();
			var selectOptions = '<select class="form-control select2 os-slspsn-list" width="100%" data-type="OS_SLSPSN">'+ glblTknSlpList +'</select>';
			
			$col.html( selectOptions );
			
			$col.find( ".select2" ).val(curVal);
		});
	}else if( title == "type" ){
		$("#excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			var curVal = $col.find(".form-control").val();
			var selectOptions = '<select class="form-control select2" width="100%" data-type="ORD_TYP">'+ glblOrdTypList +'</select>';
			
			$col.html( selectOptions );
			
			$col.find( ".select2" ).val(curVal);
		});
	}else if( title == "uom" || title == "um" ){
		$("#excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			var curVal = $col.find(".form-control").val();
			var selectOptions = '<select class="form-control select2" width="100%" data-type="UM">'+ glblUmList +'</select>';
			
			$col.html( selectOptions );
			
			$col.find( ".select2" ).val(curVal);
		});
	}else if( title == "frt_uom" ){
		$("#excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			var curVal = $col.find(".form-control").val();
			var selectOptions = '<select class="form-control select2" width="100%" data-type="FRT_UOM">'+ glblUmList +'</select>';
			
			$col.html( selectOptions );
			
			$col.find( ".select2" ).val(curVal);
		});
	}else if( title == "price_uom" ){
		$("#excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			var curVal = $col.find(".form-control").val();
			var selectOptions = '<select class="form-control select2" width="100%" data-type="PRICE_UOM">'+ glblUmList +'</select>';
			
			$col.html( selectOptions );
			
			$col.find( ".select2" ).val(curVal);
		});
	}else if( title == "info" ){
		$("#excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			$col.html("<i class='icon-info fs-16 font-weight-bold'></i>");
		});
	}else if( title == "#" ){
		var chkbx = '<div class="form-check form-check-flat my-1 ml-2">'+
						'<label class="form-check-label">'+
							'<input type="checkbox" name="select-sls-ord-item" class="custom-control-input" checked>'+
							'<i class="input-helper"></i>&nbsp;'+
						'</label>'+
					'</div>';
		$("#excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			$col.html(chkbx);
		});
	}else{
		$("#excelDataTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var $col = $( $row.find("td")[col] );
			var curVal = $col.find(".form-control").val();
			var curValStr = $col.find(".form-control").attr("data-value-str");
			
			//console.log("curVal: "+ curVal+", curValStr: " + curValStr);
			
			if( curVal == null || curVal == undefined ){
				curVal = "";
			}
			
			if( curValStr != null && curValStr != undefined && curValStr != "" ){
				curVal = decodeURI(curValStr);
			}
			
			var selectOptions = '<input type="text" class="form-control" value="" />';
			
			$col.html( selectOptions );
			
			$col.find(".form-control").val(curVal);
			
			var fieldTyp = "";
			if( title == "ship-to-1" || title == "ship to 1" || title == "ship_to_1" ){
				fieldTyp = "SHP_TO_1";
			}else if( title == "ship-to-2" || title == "ship to 2" || title == "ship_to_2" ){
				fieldTyp = "SHP_TO_2";
			}else if( title == "ship-to-3" || title == "ship to 3" || title == "ship_to_3" ){
				fieldTyp = "SHP_TO_3";
			}else if( title == "ship_to"){
				fieldTyp = "SHP_TO";
			}else if( title.indexOf("requested") >= 0 ){
				fieldTyp = "REQ";
			}else if( title.indexOf("due") >= 0 ){
				fieldTyp = "DUE";
			}else if( title.indexOf("cus-po") >= 0 || title.indexOf("cus po") >= 0 ){
				fieldTyp = "CUS_PO";
			}else if( title.indexOf("cus-release") >= 0 || title.indexOf("cus release") >= 0 || title.indexOf("release") >= 0 ){
				fieldTyp = "CUS_REL";
			}else if( title.indexOf("end-user-po") >= 0 || title.indexOf("end user po") >= 0 ){
				fieldTyp = "END_USR_PO";
			}else if( title.indexOf("job-id") >= 0 || title.indexOf("job id") >= 0 ){
				fieldTyp = "JOB_ID";
			}else if( title.indexOf("order-no") >= 0 || title.indexOf("order no") >= 0 ){
				fieldTyp = "ORD_NO";
			}else if( title.indexOf("podate") >= 0 || title.indexOf("po_date") >= 0 || title.indexOf("po date") >= 0 ){
				fieldTyp = "PO_DT";
			}else if( title.indexOf("po_number") >= 0 || title.indexOf("po-number") >= 0 || title.indexOf("po number") >= 0 ){
				fieldTyp = "PO_NO";
			}else if( title.indexOf("line_item") >= 0 ){
				fieldTyp = "LINE_ITEM";
			}else if( title.indexOf("cust_item") >= 0 || title.indexOf("cust item") >= 0 || title == "cps" ){
				fieldTyp = "CUST_ITEM";
			}else if( title.indexOf("descriptn") >= 0 || title.indexOf("desc") >= 0 ){
				fieldTyp = "DESC";
			}else if( title.indexOf("delivery") >= 0 ){
				fieldTyp = "DEL_DT";
			}else if( title.indexOf("quantity") >= 0 || title.indexOf("qty") >= 0 ){
				fieldTyp = "QTY";
			}else if( title.indexOf("unit_price") >= 0 ){
				fieldTyp = "UNIT_PRICE";
			}else if( title.indexOf("price_uom") >= 0 ){
				fieldTyp = "PRICE_UOM";
			}else if( title == "form" || title == "frm" ){
				fieldTyp = "FRM";
			}else if( title == "grade" || title == "grd" ){
				fieldTyp = "GRD";
			}else if( title == "size" ){
				fieldTyp = "SIZE";
			}else if( title == "finish" || title == "fnsh" ){
				fieldTyp = "FNSH";
			}else if( title == "source" ){
				fieldTyp = "SOURCE";
			}else if( title == "is_slspsn" ){
				fieldTyp = "IS_SLSPSN";
			}else if( title == "os_slspsn" ){
				fieldTyp = "OS_SLSPSN";
			}else if( title == "wrk_ord_dt" ){
				fieldTyp = "WRK_ORD_DT";
			}else if( title == "frt_chrg" ){
				fieldTyp = "FRT_CHRG";
			}else if( title == "frt_cost" ){
				fieldTyp = "FRT_COST";
			}else if( title == "status" ){
				fieldTyp = "STATUS";
			}else if( title == "shp_brh" ){
				fieldTyp = "SHP_BRH";
			}else if( title == "fob_pt" ){
				fieldTyp = "FOB_PT";
			}else if( title == "width" ){
				fieldTyp = "WIDTH";
			}else if( title == "length" ){
				fieldTyp = "LENGTH";
			}else if( title == "ga_size" ){
				fieldTyp = "GA_SIZE";
			}else if( title == "ga_type" ){
				fieldTyp = "GA_TYPE";
			}else if( title == "amount" ){
				fieldTyp = "AMOUNT";
			}else if( title == "header_remark" ){
				fieldTyp = "HEADER_REMARK";
			}else if( title == "trm_trd" ){
				fieldTyp = "TRM_TRD";
			}else if( title == "dlvy_mthd" ){
				fieldTyp = "DLVY_MTHD";
			}else if( title == "route" ){
				fieldTyp = "ROUTE";
			}else if( title == "whs" ){
				fieldTyp = "WHS";
			}else if( title == "memo_part" ){
				fieldTyp = "MEMO_PART";
			}else if( title == "metal_std" ){
				fieldTyp = "METAL_STD";
			}else if( title == "pcs" ){
				fieldTyp = "PCS";
			}else if( title == "weight" ){
				fieldTyp = "WEIGHT";
			}else if( title == "id" ){
				fieldTyp = "ID";
			}else if( title == "od" ){
				fieldTyp = "OD";
			}
			
			
			$col.find(".form-control").attr("data-type", fieldTyp);
		});
	}
	
	//$("#excelDataTblWrapper #excelDataTbl .select2").select2();
}



function createSalesOrder(multiSlsOrdData){
	console.log(ajaxCallCnt);
	console.log( multiSlsOrdData );
	console.log( JSON.stringify(multiSlsOrdData) );
	//return;
	
	var inputData = {
		"slsOrdData": multiSlsOrdData
	};

	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/slsOrd/updt-CrtSo',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			ajaxResponseCnt++;
					
			if( response.output.rtnSts == 0 ){
				for( var i=0; i<response.output.fldTblSlsOrd.length; i++ ){
					var docRowNo = response.output.fldTblSlsOrd[i].docRowNo;
					$($("#excelDataTbl tbody tr")[docRowNo-1]).find(".sls-ord-sts").html("<i class='icon-info fs-16 font-weight-bold' data-toggle='tooltip' data-html='true'></i>");
					
					var stsMsg = "";
					if( response.output.fldTblSlsOrd[i].stsCd != undefined && response.output.fldTblSlsOrd[i].soNo != undefined && response.output.fldTblSlsOrd[i].stsCd == "Y" ){
						stsMsg = "Quote (QT-"+ response.output.fldTblSlsOrd[i].soNo + "-" + response.output.fldTblSlsOrd[i].ordItmNo +") created successfully.";
						customAlert("#mainNotify", "alert-success", stsMsg);
							
						$($("#excelDataTbl tbody tr")[docRowNo-1]).find(".icon-info").addClass("text-success c-pointer");
						$($("#excelDataTbl tbody tr")[docRowNo-1]).find(".icon-info").attr("data-msg", stsMsg);	
						$($("#excelDataTbl tbody tr")[docRowNo-1]).find(".select-sls-ord-item").html("");
						$($("#excelDataTbl tbody tr")[docRowNo-1]).find(".form-control").attr("disabled", "disabled");	
												
					}else if( response.output.fldTblSlsOrd[i].stsCd != undefined && response.output.fldTblSlsOrd[i].soNo != undefined && response.output.fldTblSlsOrd[i].stsCd == "E" ){
						var loopLimit = response.output.fldTblSlsOrd[i].messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var j=0; j<loopLimit; j++ ){
							if(response.output.fldTblSlsOrd[i].messages[j].indexOf("Table:") != -1 || response.output.fldTblSlsOrd[i].messages[j].indexOf("Transaction cannot be processed") != -1){
							    continue;
							}
							errList += '<li>'+ response.output.fldTblSlsOrd[i].messages[j] +'</li>';
							
							stsMsg += (stsMsg.length) ? "|" : "";
							stsMsg += response.output.fldTblSlsOrd[i].messages[j]; 
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
						
						$($("#excelDataTbl tbody tr")[docRowNo-1]).find(".icon-info").addClass("text-danger c-pointer");
						$($("#excelDataTbl tbody tr")[docRowNo-1]).find(".icon-info").attr("data-msg", stsMsg);
					}else if( response.output.fldTblSlsOrd[i].stsCd != undefined && response.output.fldTblSlsOrd[i].soNo != undefined && response.output.fldTblSlsOrd[i].stsCd == "N" ){
						var loopLimit = response.output.fldTblSlsOrd[i].messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var j=0; j<loopLimit; j++ ){
							if(response.output.fldTblSlsOrd[i].messages[j].indexOf("Table:") != -1 || response.output.fldTblSlsOrd[i].messages[j].indexOf("Transaction cannot be processed") != -1){
							    continue;
							}
							
							errList += '<li>'+ response.output.fldTblSlsOrd[i].messages[j] +'</li>';
							
							stsMsg += (stsMsg.length) ? "|" : "";
							stsMsg += response.output.fldTblSlsOrd[i].messages[j]; 
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
						
						$($("#excelDataTbl tbody tr")[docRowNo-1]).find(".icon-info").addClass("text-danger c-pointer");
						$($("#excelDataTbl tbody tr")[docRowNo-1]).find(".icon-info").attr("data-msg", stsMsg);
					}else{
						
						stsMsg = "Something went wrong, please try again later.";
						
						customAlert("#mainNotify", "alert-danger", stsMsg);
						
						$($("#excelDataTbl tbody tr")[docRowNo-1]).find(".icon-info").addClass("text-danger c-pointer");
						$($("#excelDataTbl tbody tr")[docRowNo-1]).find(".icon-info").attr("data-msg", stsMsg);
					}
				}
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				
				for( var i=0; i<multiSlsOrdData.length; i++ ){
					var singleOrdData = multiSlsOrdData[i];
					
					for( var j=0; j<singleOrdData.length; j++ ){
						if( singleOrdData[j].fldTyp == "DOC_ROW_NO" ){
							var docRowNo = singleOrdData[j].fldVal;
							$($("#excelDataTbl tbody tr")[docRowNo-1]).find(".sls-ord-sts").html("<i class='icon-info fs-16 font-weight-bold' data-toggle='tooltip' data-html='true'></i>");
						}
					}
				}
			}
			
			$('[data-toggle="tooltip"]').tooltip();	
			
			if(ajaxCallCnt == ajaxResponseCnt){
				//$("#mainLoader").hide();
				$("#processData").show();
			}	
		},
		error: function( jqXHR, textStatus, errorThrown ){
			ajaxResponseCnt++;
			
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			
			if(ajaxCallCnt == ajaxResponseCnt){
				//$("#mainLoader").hide();
				$("#processData").show();
			}
		}
	});
}