var varCnt = 0;

$(function(){

	//tmpReadExcelData();
	
	getBranchList( $("#brh"), false );
	
	getCustomerList( $("#cusId") );
	
	getSalesCategoryList( $("#slsCat") );
	
	getSalesPersonList( $("#tknSlp") );
	
	getUmList( $("#um") );
	
	getLengthTypeList( $("#lgthTyp"), true);
	
	getVendorList( $("#vndr"), glblCmpyId );
	
	getWhsList( $("#whs"), true);
	
	getProcessRouteList( $("#prsRte"), glblCmpyId);
	
	getUserPref();
	
	$("#cusId").change(function(){
		var cusId = $(this).val();
		
		if( cusId != "" && cusId != null && cusId != undefined ){
			getShipToList($("#shpTo"), cusId, false);
		}
	});
	
	$("#btnUploadExcelFile").click(function(){
		uploadAndReadExcel();
	});
	
	$("#updColNm").click(function(){
		$("#modifyExcelHeaders .select2-selection").removeClass("border-danger");
		var errFlg = false;
		var colNo = $("#colNoDropdown").val();
		var colNm = $("#colNmDropdown").val();
		
		if( colNo == "" || colNo == null || colNo == undefined ){
			errFlg = true;
			$("#colNoDropdown").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( colNm == "" || colNm == null || colNm == undefined ){
			errFlg = true;
			$("#colNmDropdown").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( errFlg == false ){
			setHeader((colNo-1), colNm);
		}
	});
	
});

function uploadAndReadExcel(){
	$('#excelDataTblWrapper #excelDataTbl').floatThead('destroy');
	var formElement = document.forms.namedItem("formUploadExcelFile");
	var formData = new FormData(formElement);
	var errFlg = false;
	var fileName = "";
	var file = [];
	
	$.each(formData.getAll("file"), function(index, value) {
	
		if( value.name == "" || value.size <= 0 ){
			errFlg = true;
			customAlert("#mainNotify", "alert-danger", "Please upload an excel file.");
		}
		
		if( value.size > 0 ){
			var fileName = value.name;
			var fileExt =  fileName.substring( fileName.lastIndexOf(".") );
			//console.log(fileExt);
			if( fileExt != ".xlsx" && fileExt != ".xls" ){
				errFlg = true;
				customAlert("#mainNotify", "alert-danger", "Please upload only excel file.");
			}
		}
		
		if( value.size > 5000000 ){ //Note more than 5MB file
			errFlg = true;
			customAlert("#mainNotify", "alert-danger", "File size can't be more than 5mb.");
		}
	});
	
	if( errFlg === false ){
		$("#mainLoader").show();
		
		formData.append('header', $("#headerPresent").prop("checked"));
		
		$.ajax({
			headers : {
				'user-id' : localStorage.getItem("starSlsMgmtUserId"),
				'auth-token' : localStorage.getItem("starSlsMgmtAuthToken"),
				'app-token' : localStorage.getItem("starSlsMgmtAppToken"),
				'cmpy-id' : localStorage.getItem("starSlsMgmtCmpyId"),
			},
			type : "POST",
			url : rootAppName + '/rest/cstmdoc/adddoc',
			data : formData, // serializes the form's elements.
			enctype : 'multipart/form-data',
			cache : false,
			processData : false,
			contentType : false,
			success: function(response){
				formElement.reset(); 
				$("#excelFile").siblings(".custom-file-label").removeClass("selected").html("");
					
				if( response.output.rtnSts == 0 ){
					var theadHtml = '<tr>', tbodyHtml = '', headerObj = {}, dataObj = {}, colNumOptns = '<option value="0">&nbsp;</option>';
					var tblCol = 0, colCnt = 1;
					var tblRow = response.output.fldTblExcelData.length;
					
					theadHtml += '<th>#</th>';
					theadHtml += '<th>INFO</th>';
					if( response.output.fldTblExcelheader.length ){
						tblCol = Object.keys( response.output.fldTblExcelheader[0] ).length;
						headerObj = response.output.fldTblExcelheader[0];
						
						$.each( headerObj, function( key, value ) {
							//alert( key + ": " + value );
							theadHtml += '<th>'+ value +'</th>';
							colNumOptns += '<option value="'+ colCnt +'">Column '+ colCnt +'</option>';
							colCnt++;
							//addVariable();
						});
					}else{
						tblCol = Object.keys( response.output.fldTblExcelData[0] ).length;
						headerObj = response.output.fldTblExcelData[0];
						
						$.each( headerObj, function( key, value ) {
							//alert( key + ": " + value );
							theadHtml += '<th></th>';
							colNumOptns += '<option value="'+ colCnt +'">Column '+ colCnt +'</option>';
							colCnt++;
							//addVariable();
						});
					}
					
					theadHtml += '</tr>';
					
					for( var y=0; y<tblRow; y++){
						if( response.output.fldTblExcelData[y].field01 == "" && response.output.fldTblExcelData[y].field02 == "" && response.output.fldTblExcelData[y].field03 == "" && response.output.fldTblExcelData[y].field04 == "" && response.output.fldTblExcelData[y].field05 == "" ){
							continue;
						}
						tbodyHtml += '<tr data-doc-id="'+ response.output.docId +'">';
						dataObj = response.output.fldTblExcelData[y];
						
						tbodyHtml += '<td class="select-sls-ord-item text-center"></td>';
						tbodyHtml += '<td class="sls-ord-sts text-center text-muted"></td>';
						
						$.each( dataObj, function( key, value ) {							
							tbodyHtml += '<td><input type="text" class="form-control" value="'+ value +'" data-value-str="'+ encodeURI(value) +'" /></td>';
						});
						
						tbodyHtml += '</tr>';
					}
					
					var tableData = '<table class="table" id="excelDataTbl">'+
										'<thead>'+ theadHtml +'</thead>'+
										'<tbody>'+ tbodyHtml +'</tbody>'+
									'</table>';
					
					$("#colNoDropdown").html(colNumOptns);
					//$("#excelDataTbl thead").html(theadHtml);
					//$("#excelDataTbl tbody").html(tbodyHtml);
					
					$("#excelDataTblWrapper").html(tableData);
					
					$("#excelHeaders .select2").select2();
					
					glblBrhList = $("#brh").html();
					glblVndrList = $("#vndr").html();
					glblCusList = $("#cusId").html();
					glblSlsCatList = $("#slsCat").html();
					glblTknSlpList = $("#tknSlp").html();
					glblOrdTypList = $("#ordTyp").html();
					glblLgthTypList = $("#lgthTyp").html();
					glblWhsList = $("#whs").html();
					glblUmList = $("#um").html();
					glblPrsRteList = $("#prsRte").html();
					
					var theadCnt = 0;
					$("#excelDataTblWrapper #excelDataTbl thead tr th").each(function(index, th){
						var $th = $(th);
						var colHeading = $th.text();
						
						if( colHeading.length > 0 && colHeading.trim() != "" ){
							setHeader(theadCnt, colHeading.trim());
						}
						theadCnt++;
					});
					
					$("#excelDataTblWrapper #excelDataTbl .select2").select2();
					
					$("#processData").show();
										
					var $table = $('#excelDataTblWrapper #excelDataTbl');
								
					$table.floatThead({
						useAbsolutePositioning: true,
						scrollContainer: function($table) {
							return $table.closest('.table-responsive');
						}
					});
					$table.floatThead('reflow');
	
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}	
				$("#mainLoader").hide();		
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	}
}

/**
 *	This function will add fields for Excel Header naming
 */
function addVariable(){
	var hdrVar = '<div class="col-1">'+
			'<div class="form-group">'+
				'<label class="font-weight-bold">Column '+ (varCnt+1) +'</label>'+
				'<select width="100%" class="form-control select2 excel-column-header-'+ varCnt +'" onchange="setHeader('+ varCnt +', $(this).val())">'+
					'<option></option>'+
					'<option>Branch</option>'+
					'<option>Charges</option>'+
					'<option>Customer ID</option>'+
					'<option>Customer Name</option>'+
					'<option>Customer PO</option>'+
					'<option>Due Date</option>'+
					'<option>Finish</option>'+
					'<option>Form</option>'+
					'<option>General Notes</option>'+
					'<option>Grade</option>'+
					'<option>Length</option>'+
					'<option>Order</option>'+
					'<option>PCS</option>'+
					'<option>Price</option>'+
					'<option>Quantity</option>'+
					'<option>Size</option>'+
					'<option>Thickness</option>'+
					'<option>UM</option>'+
					'<option>Unit Measure</option>'+
					'<option>Weight</option>'+
					'<option>Width</option>'+
				'</select>'+
			'</div>'+
		'</div>';
	$("#excelHeaders").append(hdrVar);
	varCnt++;
}


/*
function tmpReadExcelData(){
	var inputData = {};
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/cstmdoc/read-excel',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var theadHtml = '<tr>', tbodyHtml = '', headerObj = {}, dataObj = {}, colNumOptns = '<option value="0">&nbsp;</option>';
				var tblCol = 0, colCnt = 1;
				var tblRow = response.output.fldTblExcelData.length;
				
				if( response.output.fldTblExcelheader.length ){
					tblCol = Object.keys( response.output.fldTblExcelheader[0] ).length;
					headerObj = response.output.fldTblExcelheader[0];
					
					$.each( headerObj, function( key, value ) {
						//alert( key + ": " + value );
						theadHtml += '<th>'+ value +'</th>';
						colNumOptns += '<option value="'+ colCnt +'">Column '+ colCnt +'</option>';
						colCnt++;
						//addVariable();
					});
				}else{
					tblCol = Object.keys( response.output.fldTblExcelData[0] ).length;
					headerObj = response.output.fldTblExcelData[0];
					
					$.each( headerObj, function( key, value ) {
						//alert( key + ": " + value );
						theadHtml += '<th></th>';
						colNumOptns += '<option value="'+ colCnt +'">Column '+ colCnt +'</option>';
						colCnt++;
						//addVariable();
					});
				}
				
				theadHtml += '</tr>';
				
				for( var y=0; y<tblRow; y++){
					tbodyHtml += '<tr>';
					dataObj = response.output.fldTblExcelData[y];
					
					$.each( dataObj, function( key, value ) {
						//alert( key + ": " + value );
						tbodyHtml += '<td><input type="text" class="form-control" value="'+ value +'" /></td>';
					});
					
					tbodyHtml += '</tr>';
				}
				
				var tableData = '<table class="table" id="excelDataTbl">'+
									'<thead>'+ theadHtml +'</thead>'+
									'<tbody>'+ tbodyHtml +'</tbody>'+
								'</table>';
				
				$("#colNoDropdown").html(colNumOptns);
				//$("#excelDataTbl thead").html(theadHtml);
				//$("#excelDataTbl tbody").html(tbodyHtml);
				$("#excelDataTblWrapper").html(tableData);
				
				$("#excelHeaders .select2").select2();
				
				var theadCnt = 0;
				$("#excelDataTbl thead tr th").each(function(index, th){
					var $th = $(th);
					var colHeading = $th.text();
					
					if( colHeading.length > 0 && colHeading.trim() != "" ){
						setHeader(theadCnt, colHeading.trim());
					}
					theadCnt++;
				});

			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}
*/

function verifyInputData(){
	var loopCnt = 0;
	var tblRow = $("#excelDataTbl tbody tr").length;
	var colCnt = $("#excelDataTbl tbody tr:first-child td").length;
	
	$("#excelDataTbl tbody tr").each(function(index, tr){
		var $tr = $(tr);
		var slsOrdData = [];
		$tr.find("td").each(function(index, td){
			var $td = $(td);
			var fldTyp = $td.find(".form-control").attr("data-type");
			var fldVal = $td.find(".form-control").val().trim();
			
			if( fldTyp.indexOf("SHP_TO") >= 0 && fldVal != "" ){
				var shpToArr = fldVal.split("-");
				fldVal = shpToArr[0];
			}
			
			var slsOrdFldObj = {
				"fldTyp": fldTyp,
				"fldVal": fldVal,
			};
			slsOrdData.push(slsOrdFldObj);
		});
		console.log( slsOrdData );
		console.log( JSON.stringify(slsOrdData) );
	});
}


function deleteRecordsByTrcCtlNo(){
	var inputData = {
		"trcCtlNo": $("#trcCtlNo").val(),
	};

	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/slsOrd/del-trcCtlNo',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
								
			if( response.output.rtnSts == 0 ){
								
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}	
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
		}
	});
}