$(function(){
	
	getBranchList( $("#brh"), false );
	
	getCustomerList( $("#cusId") );
	
	getSalesCategoryList( $("#slsCat") );
	
	getSalesPersonList( $("#tknSlp") );
	
	getUmList( $("#um") );
	
	getUploadedDocuments();
	
	$("#backToUpldDocs").click(function(){
		$("#excelDataSection").hide();
		$("#upldLogSection").slideDown();
	});
	
	$("#upldLogTbl").on("click", ".del-doc-data", function(){	
		var $this = $(this);	
		var docId = $this.closest("tr").attr("data-doc-id");
		var docNm = $this.closest("tr").attr("data-doc-nm");
		var docOrgNm = $this.closest("tr").attr("data-doc-org-nm");
		
		var $delDocBtn = $("#deleteDoc");
		$delDocBtn.attr("data-doc-id", docId);
		$delDocBtn.attr("data-doc-nm", docNm);
		$delDocBtn.attr("data-doc-org-nm", docOrgNm);
		
		$("#deleteDocModal").modal("show");
	});
	
	$("#upldLogTbl").on("click", ".dwnld-doc", function(){	
		var $this = $(this);	
		var docId = $this.closest("tr").attr("data-doc-id");
		var docNm = $this.closest("tr").attr("data-doc-nm");
		var docOrgNm = $this.closest("tr").attr("data-doc-org-nm");
		
		var docUrl = rootAppName + "/rest/cstmdoc/download?docId=" + docId;

		window.open(docUrl, '_blank');
	});
	
	$("#deleteDoc").click(function(){
		var $this = $(this);	
		var docId = $this.attr("data-doc-id");
		var docNm = $this.attr("data-doc-nm");
		var docOrgNm = $this.attr("data-doc-org-nm");
		
		$("#mainLoader").show();
	
		var inputData = {
			docId: docId,
			docNm: docNm,
			docOrgNm: docOrgNm
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/savedDocs/delete',
			type: 'POST',
			dataType: 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					$("#deleteDocModal").modal("hide");
					
					customAlert("#mainNotify", "alert-success", "Document deleted successfully.");
					
					getUploadedDocuments();
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}	
				$("#mainLoader").hide();		
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});
	
	$("#upldLogTbl").on("click", ".view-doc-data", function(){		
		var docId = $(this).closest("tr").attr("data-doc-id");
		var docNm = $(this).closest("tr").attr("data-doc-nm");
		var docOrgNm = $(this).closest("tr").attr("data-doc-org-nm");
		var upldOn = $(this).closest("tr").attr("data-upld-on");
		var upldBy = $(this).closest("tr").attr("data-upld-by");
		
		$("#docOrgNmSpan").html(docOrgNm);
		$("#upldOnSpan").html(upldOn);
		$("#upldBySpan").html(upldBy);
		
		$("#mainLoader").show();
	
		var inputData = {
			docId: docId,
			docNm: docNm,
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/cstmdoc/read-saved-excel',
			type: 'POST',
			dataType: 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					var theadHtml = '<tr>', tbodyHtml = '', headerObj = {}, dataObj = {}, colNumOptns = '<option value="0">&nbsp;</option>';
					var tblCol = 0, colCnt = 1;
					var tblRow = response.output.fldTblExcelData.length;
					
					theadHtml += '<th>#</th>';
					theadHtml += '<th>INFO</th>';
					if( response.output.fldTblExcelheader.length ){
						tblCol = Object.keys( response.output.fldTblExcelheader[0] ).length;
						headerObj = response.output.fldTblExcelheader[0];
						
						$.each( headerObj, function( key, value ) {
							//alert( key + ": " + value );
							theadHtml += '<th>'+ value +'</th>';
							colNumOptns += '<option value="'+ colCnt +'">Column '+ colCnt +'</option>';
							colCnt++;
							//addVariable();
						});
					}else{
						tblCol = Object.keys( response.output.fldTblExcelData[0] ).length;
						headerObj = response.output.fldTblExcelData[0];
						
						$.each( headerObj, function( key, value ) {
							//alert( key + ": " + value );
							theadHtml += '<th></th>';
							colNumOptns += '<option value="'+ colCnt +'">Column '+ colCnt +'</option>';
							colCnt++;
							//addVariable();
						});
					}
					
					theadHtml += '</tr>';
					var fldCols = "", errMsg = "", ordNo = "";
					var chkbx = '<div class="form-check form-check-flat my-1 ml-2">'+
									'<label class="form-check-label">'+
										'<input type="checkbox" name="select-sls-ord-item" class="custom-control-input" checked>'+
										'<i class="input-helper"></i>&nbsp;'+
									'</label>'+
								'</div>';
					for( var y=0; y<tblRow; y++){
						if( response.output.fldTblExcelData[y].field01 == "" && response.output.fldTblExcelData[y].field02 == "" && response.output.fldTblExcelData[y].field03 == "" && response.output.fldTblExcelData[y].field04 == "" && response.output.fldTblExcelData[y].field05 == "" ){
							continue;
						}
						
						fldCols = "", errMsg = "", ordNo = "";
						
						dataObj = response.output.fldTblExcelData[y];
						$.each( dataObj, function( key, value ) {	
							if( key == "errMsg" || key == "ordNoInfo" ){
								if( key == "errMsg" ){
									errMsg = value;
								}else{
									ordNo = value
								}
							}else{
								fldCols += '<td><input type="text" class="form-control" value="'+ value +'" data-value-str="'+ encodeURI(value) +'" /></td>';
							}
						});
						
						tbodyHtml += '<tr data-doc-id="'+ response.output.docId +'">';
						
						
						if( ordNo != "" ){
							tbodyHtml += '<td class="select-sls-ord-item text-center"></td>';
							tbodyHtml += '<td class="sls-ord-sts text-center"><i class="icon-info fs-16 font-weight-bold c-pointer text-success" data-msg="'+ ordNo +'"></i></td>' + fldCols;
						}else if( errMsg != "" ){
							tbodyHtml += '<td class="select-sls-ord-item text-center">'+ chkbx +'</td>';
							tbodyHtml += '<td class="sls-ord-sts text-center"><i class="icon-info fs-16 font-weight-bold c-pointer text-danger" data-msg="'+ errMsg +'"></i></td>' + fldCols;
						}else{
							tbodyHtml += '<td class="select-sls-ord-item text-center">'+ chkbx +'</td>';
							tbodyHtml += '<td class="sls-ord-sts text-center"><i class="icon-info fs-16 font-weight-bold c-pointer text-muted"></i></td>' + fldCols;
						}
						
						tbodyHtml += '</tr>';
					}
					
					var tableData = '<table class="table" id="excelDataTbl">'+
										'<thead>'+ theadHtml +'</thead>'+
										'<tbody>'+ tbodyHtml +'</tbody>'+
									'</table>';
					
					$("#colNoDropdown").html(colNumOptns);
					//$("#excelDataTbl thead").html(theadHtml);
					//$("#excelDataTbl tbody").html(tbodyHtml);
					
					$("#excelDataTblWrapper").html(tableData);
					
					$("#excelHeaders .select2").select2();
					
					glblBrhList = $("#brh").html();
					glblVndrList = $("#vndr").html();
					glblCusList = $("#cusId").html();
					glblSlsCatList = $("#slsCat").html();
					glblTknSlpList = $("#tknSlp").html();
					glblOrdTypList = $("#ordTyp").html();
					glblUmList = $("#um").html();

					var theadCnt = 0;
					$("#excelDataTblWrapper #excelDataTbl thead tr th").each(function(index, th){
						var $th = $(th);
						var colHeading = $th.text();
						
						if( colHeading.length > 0 && colHeading.trim() != "" && theadCnt > 1 ){
							setHeader(theadCnt, colHeading.trim());
						}
						theadCnt++;
					});
					
					$("#excelDataTblWrapper #excelDataTbl .select2").select2();
					
					$("#excelDataTblWrapper #excelDataTbl tbody tr").each(function(index, tr){
						if( $(tr).find(".icon-info").hasClass("text-success") ){
							$(tr).find(".form-control").attr("disabled", "disabled");
						}
					});
					
					$("#upldLogSection").hide();
					$("#excelDataSection").slideDown();
					
					$('[data-toggle="tooltip"]').tooltip();
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}	
				$("#mainLoader").hide();		
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});
	
});

function getUploadedDocuments(){
	$("#mainLoader").show();
	
	var inputData = {};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/savedDocs/read',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopLimit = response.output.fldTblSavedDocs.length;
				var soHdrHtml = "", docId = "", docNm = "", docOrgNm = "", upldOn = "", upldBy = "", prsRowCnt = "", delBtn = "", dwnldBtn = "";
				
				for( var y=0; y<loopLimit; y++ ){
					docId = response.output.fldTblSavedDocs[y].docId;
					docNm = response.output.fldTblSavedDocs[y].docNm; 
					docOrgNm = response.output.fldTblSavedDocs[y].docOrgNm;
					upldOn = response.output.fldTblSavedDocs[y].upldOn;
					upldBy = response.output.fldTblSavedDocs[y].upldBy;
					prsRowCnt = (response.output.fldTblSavedDocs[y].prsRowCnt != undefined) ? response.output.fldTblSavedDocs[y].prsRowCnt : "";
					delBtn = (prsRowCnt == "") ? '<button class="btn btn-sm btn-danger ml-2 del-doc-data">Delete</button>' : "";
					dwnldBtn = '<button class="btn btn-sm btn-primary ml-2 dwnld-doc">Download</button>';
					
					soHdrHtml += '<tr data-doc-id="'+ docId +'" data-doc-nm="'+ docNm +'" data-doc-org-nm="'+ docOrgNm +'" data-upld-on="'+ upldOn +'" data-upld-by="'+ upldBy +'">'+
									'<td>'+ (y+1) +'</td>'+
									'<td>'+ docOrgNm +'</td>'+
									'<td>'+ upldOn +'</td>'+
									'<td>'+ upldBy +'</td>'+
									'<td class="text-center"><button class="btn btn-sm btn-dark view-doc-data">View</button>'+ delBtn + dwnldBtn +'</td>'+
								'</tr>';
				}
				
				$("#upldLogTbl tbody").html(soHdrHtml);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}	
			$("#mainLoader").hide();		
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});

}