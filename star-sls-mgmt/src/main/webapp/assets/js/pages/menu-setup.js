$(function(){
	
	getAllMenus();
	
	getUserGroupMenuAccessData();
		
	getUserGroupList( $("#menuUserGroup") );
	
	$("#menuUserGroup").change(function(){
		$("input[type='checkbox'][name='menu-access']").prop("checked", false);
		
		var groupId = $("#menuUserGroup").val();
		if( groupId != "" && groupId > 0 ){
			getSpecificMenuAccess( groupId );
		}
	});
	
	$("#menuList").on("change", 'input[type="checkbox"][name="menu-access"]', function(){
		if( $(this).hasClass("has-sub-menu") ){
		 	var menuId = $(this).val();
			if( $(this).prop("checked") ){
				$('input[type="checkbox"][name="menu-access"][data-parent-id="'+ menuId +'"].cstm-sub-menu').prop("checked", true);
			}else{
				$('input[type="checkbox"][name="menu-access"][data-parent-id="'+ menuId +'"].cstm-sub-menu').prop("checked", false);
			}
		} else if( $(this).hasClass("cstm-sub-menu") ){
			var parentId = $(this).attr("data-parent-id");
			if( $('input[type="checkbox"][name="menu-access"][data-parent-id="'+ parentId +'"].cstm-sub-menu:checked').length ){
				$('input[type="checkbox"][name="menu-access"][value="'+ parentId +'"].has-sub-menu').prop("checked", true);
			}else{
				$('input[type="checkbox"][name="menu-access"][value="'+ parentId +'"].has-sub-menu').prop("checked", false);
			}
		} 
	});
	
	$("#menuList").on("click", '.checkbox-disabled', function(event){
		event.preventDefault();
		event.stopPropagation();
	});
	
	$("#saveMenuAccess").click(function(){
		$(".select2-selection, #menuList").removeClass("border-danger");
		
		var groupId = $("#menuUserGroup").val();
		var errFlg = false;
		var menuArr = [], subMenuArr = [];
		var tmpObj = {};
		
		if( groupId == "" || groupId == 0 ){
			errFlg = true;
			$("#menuUserGroup").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( $("input[type='checkbox'][name='menu-access']:checked").length <= 0 ){
			errFlg = true;
			$("#menuList").addClass("border-danger");
		}
		
		$("input[type='checkbox'][name='menu-access']:checked").each(function(index, checkbox){
			if( $(checkbox).hasClass("cstm-sub-menu") ){
				tmpObj = {
					"menuId": $(checkbox).attr("data-parent-id"),
					"subMenuId": $(checkbox).val(),
				}
				subMenuArr.push(tmpObj);
			}else if( $(checkbox).hasClass("cstm-main-menu") ){
				menuArr.push( $(checkbox).val() );
			}
		});
		
		if( errFlg == false ){
			$("#mainLoader").show();

			var inputData = { 
				grpId: groupId,
				menuLst: menuArr,
				subMenuLst: subMenuArr
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/menu/update',
				type: 'POST',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						getUserGroupMenuAccessData();
						$("#cancelMenuAccess").trigger("click");
						customAlert("#mainNotify", "alert-success", 'Menu access saved successfully.');
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					$("#mainLoader").hide();
				}
			});
		}
		
	});
	
	$("#cancelMenuAccess").click(function(){
		$(".select2-selection, #menuList").removeClass("border-danger");
		$("#menuUserGroup").val("").trigger("change");
		$("input[type='checkbox'][name='menu-access']").prop("checked", false);
	});
	
});


function getAllMenus(){
	$.ajax({
		headers: ajaxHeader,
		url : rootAppName + '/rest/menu/read-all',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){		            	
				var loopLimit = response.output.fldTblMenu.length;
				var innerLoopLimit = 0;
				var allMenus = '';
				
				for( var i=0; i<loopLimit; i++){
					innerLoopLimit = response.output.fldTblMenu[i].subMenuList.length;
					if( innerLoopLimit ){
						allMenus += '<li>'+
										'<div class="form-check form-check-flat my-2">'+
				                            '<label class="form-check-label">'+
				                              	'<input type="checkbox" value="'+ response.output.fldTblMenu[i].menuId +'" name="menu-access" class="cstm-main-menu has-sub-menu custom-control-input">'+
				                              	response.output.fldTblMenu[i].menuNm +
				                            	'<i class="input-helper"></i></label>'+
				                        '</div>'+
										'<ul class="menu-content list-unstyled pl-4">';
											
						for( var j=0; j<innerLoopLimit; j++){
							allMenus += '<li>'+
											'<div class="form-check form-check-flat my-1">'+
					                            '<label class="form-check-label">'+
					                              	'<input type="checkbox" value="'+ response.output.fldTblMenu[i].subMenuList[j].menuId +'" data-parent-id="'+ response.output.fldTblMenu[i].menuId +'" name="menu-access" class="cstm-sub-menu custom-control-input">'+
					                              	response.output.fldTblMenu[i].subMenuList[j].menuNm +
					                            	'<i class="input-helper"></i></label>'+
					                        '</div>'+
										'</li>';
						}
						allMenus += 	'</ul>'+
									'</li>';
					}else{
						allMenus += '<li>'+
										'<div class="form-check form-check-flat my-2">'+
				                            '<label class="form-check-label">'+
				                              	'<input type="checkbox" value="'+ response.output.fldTblMenu[i].menuId +'" name="menu-access" class="cstm-main-menu custom-control-input">'+
				                              	response.output.fldTblMenu[i].menuNm +
				                            	'<i class="input-helper"></i></label>'+
				                        '</div>'+
									'</li>';
					}
				}
				
				$("#menuList").html(allMenus);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}		
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}

function getSpecificMenuAccess( groupId ){
	$("#mainLoader").show();
		
	var inputData = { 
		grpId : groupId,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/menu/read',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopLimit = response.output.fldTblMenu.length;
				var innerLoopLimit = 0;
				var allMenus = '';
				
				for( var i=0; i<loopLimit; i++){
					innerLoopLimit = response.output.fldTblMenu[i].subMenuList.length;
					if( innerLoopLimit ){
						$('input[type="checkbox"][name="menu-access"][value="'+ response.output.fldTblMenu[i].menuId +'"].cstm-main-menu').prop("checked", true);		
						for( var j=0; j<innerLoopLimit; j++){
							$('input[type="checkbox"][name="menu-access"][value="'+ response.output.fldTblMenu[i].subMenuList[j].menuId +'"][data-parent-id="'+ response.output.fldTblMenu[i].menuId +'"].cstm-sub-menu').prop("checked", true);
						}
					}else{
						$('input[type="checkbox"][name="menu-access"][value="'+ response.output.fldTblMenu[i].menuId +'"].cstm-main-menu').prop("checked", true);
					}
				}
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getUserGroupMenuAccessData(){
	$("#mainLoader").show();
		
	var inputData = { 
		grpId : "",
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/menu/read',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblMenu.length;
				var tblRow = '', menuAccessStr = '', grpId = '', grpNm = '', menuNm = '', rowNo = 1;
				var innerLoopLimit = 0;
        
				for( var i=0; i<loopSize; i++ ){
					if( grpNm != "" && grpNm != response.output.fldTblMenu[i].grpNm ){
						tblRow += '<tr data-grp-id="'+ grpId +'">'+
									'<td>'+ rowNo +'</td>'+
									'<td>'+ grpNm +'</td>'+
									'<td><ul class="m-0">'+ menuAccessStr +'</ul></td>'+
								'</tr>';
						rowNo++;
						menuAccessStr = "";
					}
					grpId = response.output.fldTblMenu[i].grpId;
					grpNm = response.output.fldTblMenu[i].grpNm;
					menuNm = response.output.fldTblMenu[i].menuNm;
					
					innerLoopLimit = response.output.fldTblMenu[i].subMenuList.length;
					if( innerLoopLimit ){
						menuAccessStr += '<li>'+
											'<span class="menu-title">'+ response.output.fldTblMenu[i].menuNm +'</span>'+
											'<ul class="m-0 pl-4">';
											
						for( var j=0; j<innerLoopLimit; j++){
							menuAccessStr += '<li><span class="menu-title">'+ response.output.fldTblMenu[i].subMenuList[j].menuNm +'</span></li>';
						}
						menuAccessStr += 	'</ul>'+
										'</li>';
					}else{
						menuAccessStr += '<li><span class="menu-title">'+ response.output.fldTblMenu[i].menuNm +'</span></li>';
					}
					
					if( i == (loopSize-1) ){
						tblRow += '<tr data-grp-id="'+ grpId +'">'+
									'<td>'+ rowNo +'</td>'+
									'<td>'+ grpNm +'</td>'+
									'<td><ul class="m-0">'+ menuAccessStr +'</ul></td>'+
								'</tr>';
						rowNo++;
						menuAccessStr = "";
					}
				}
				$("#menuAccessTbl tbody").html(tblRow);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}
