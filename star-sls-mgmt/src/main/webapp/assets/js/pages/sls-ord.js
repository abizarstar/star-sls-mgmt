$(function(){
	
	getCustomerList( $("#cusId") );
	
	$("#fltrSlsOrd").click(function(){
		$("#cusId").next().find(".select2-selection").removeClass("border-danger");
	
		var cusId = $("#cusId").val();
		var poNo = $("#poNo").val();
		var dueDt = $("#dueDt").val();
		var ordDt = $("#ordDt").val();
		
		if( cusId == null || cusId == undefined || cusId == "" ){
			$("#cusId").next().find(".select2-selection").addClass("border-danger");
			return;
		}
		
		$("#mainLoader").show();
		
		var inputData = {
			"cusId": cusId,
			"poNo": poNo,
			"dueDt": dueDt,
			"ordDt": ordDt,
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/sales/read-so',
			type: 'POST',
			dataType: 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					var loopLimit = response.output.fldTblslsOdr.length;
					var soHdrHtml = "";
					
					for( var y=0; y<loopLimit; y++ ){
						soHdrHtml += '<tr data-ord-no="'+ response.output.fldTblslsOdr[y].ordNo +'">'+
										'<td>('+ response.output.fldTblslsOdr[y].cusId + ') ' + response.output.fldTblslsOdr[y].cusNm +'</td>'+
										'<td>('+ response.output.fldTblslsOdr[y].shpId + ') ' + response.output.fldTblslsOdr[y].shpNm +'</td>'+
										'<td>'+ response.output.fldTblslsOdr[y].po +'</td>'+
										'<td>'+ response.output.fldTblslsOdr[y].inSlp +'</td>'+
										'<td>'+ response.output.fldTblslsOdr[y].outSlp +'</td>'+
										'<td>'+ response.output.fldTblslsOdr[y].tSlp +'</td>'+
										'<td>'+ response.output.fldTblslsOdr[y].ordNo +'</td>'+
										'<td class="text-center"><button class="btn btn-sm btn-dark view-so-items" data-call-srv="1">View</button></td>'+
									'</tr>'+
									'<tr class="p-0 m-0">'+
										'<td class="p-0 m-0" colspan="8">'+
											'<div class="sls-ord-item-wrapper display-none">'+
												'<table class="table m-0">'+
													'<thead>'+
														'<tr>'+
															'<th>Item</th>'+
															'<th>CPS</th>'+
															'<th>Form</th>'+
															'<th>Grade</th>'+
															'<th>Size</th>'+
															'<th>Finish</th>'+
															'<th>Msr</th>'+
															'<th>Wgt</th>'+
															'<th>Pcs</th>'+
															'<th>PO</th>'+
															'<th>PO Dt</th>'+
														'</tr>'+
													'</thead>'+
													'<tbody>'+
													'</tbody>'+
												'</table>'+
											'</div>'+
										'</td>'+
									'</tr>';
					}
					
					$("#slsOrdTbl tbody").html(soHdrHtml);
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}	
				$("#mainLoader").hide();		
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});
	
	
	$("#slsOrdTbl").on("click", ".view-so-items", function(){
		var $this = $(this);
		var ajaxCallFlg = $this.attr("data-call-srv");
		var ordNo = $this.closest("tr").attr("data-ord-no");
		
		if( ajaxCallFlg == 1 ){
			$("#mainLoader").show();
		
			var inputData = {
				"ordNo": ordNo,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/sales/read-soitm',
				type: 'POST',
				dataType: 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						var loopLimit = response.output.fldTblslsOdrItm.length;
						var soItmHtml = "";
						
						for( var y=0; y<loopLimit; y++ ){
							soItmHtml += '<tr>'+
											'<td>'+ response.output.fldTblslsOdrItm[y].ordItm +'</td>'+
											'<td>'+ response.output.fldTblslsOdrItm[y].ipdPart +'</td>'+
											'<td>'+ response.output.fldTblslsOdrItm[y].ipdFrm +'</td>'+
											'<td>'+ response.output.fldTblslsOdrItm[y].ipdGrd +'</td>'+
											'<td>'+ response.output.fldTblslsOdrItm[y].ipdSize +'</td>'+
											'<td>'+ response.output.fldTblslsOdrItm[y].ipdfnsh +'</td>'+
											'<td>'+ response.output.fldTblslsOdrItm[y].ordMsr +'</td>'+
											'<td>'+ response.output.fldTblslsOdrItm[y].ordWgt +'</td>'+
											'<td>'+ response.output.fldTblslsOdrItm[y].ordpcs +'</td>'+
											'<td>'+ response.output.fldTblslsOdrItm[y].po +'</td>'+
											'<td>'+ response.output.fldTblslsOdrItm[y].poDt +'</td>'+
											//'<td class="text-center"><button class="btn btn-sm btn-dark view-so-items" data-call-srv="1">View</button></td>'+
										'</tr>';
						}
						
						if( soItmHtml == "" ){
							soItmHtml = '<tr><td colspan="6">No items found.</td></tr>';
						}
						
						$this.closest("tr").next().find(".sls-ord-item-wrapper tbody").html(soItmHtml);
						$this.closest("tr").next().find(".sls-ord-item-wrapper").slideToggle();
						$this.attr("data-call-srv", "0");
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}	
					$("#mainLoader").hide();		
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}else{
			$this.closest("tr").next().find(".sls-ord-item-wrapper").slideToggle();
		}
	});
	
});

