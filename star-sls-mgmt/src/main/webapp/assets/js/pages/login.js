$(function(){
	
	var urlParams = new URLSearchParams(window.location.search);
	
	var nginxURL = "";
	if(urlWithData.includes("invex.cloud")){
		nginxURL = urlWithData.split("invex.cloud")[0]+"invex.cloud/" + window.location.pathname.split("/")[1];
	}else if(urlWithData.includes("auxinvex")){
		var fullURL = urlWithData;
		var baseUrl = new URL(fullURL).origin;
		nginxURL = baseUrl+"/" + window.location.pathname.split("/")[1];
	}	
	
	
	if(urlParams.has('param')){
		var inpParam = urlParams.get('param');
		callLogin(inpParam);
	}
	
	if(urlParams.has('auth') && urlParams.has('lgnId'))
	{
		var inpAuth = urlParams.get('auth')+'inpAuth+=';
		var inpLgnId = urlParams.get('lgnId');
		callLoginFromMenu(inpAuth, inpLgnId, nginxURL);
	}
	

	if( localStorage.getItem("starSlsMgmtAuthToken") != null && localStorage.getItem("starSlsMgmtAuthToken") != "" ){
		//window.location.replace("upld-invc.html");
		
		getSidebarMenus();
	}else{
		$("#mainAppWrapper").hide();
	}
    
    $("#password").keyup(function(event){
    	if (event.keyCode === 13) {
    		$("#btnLogin").trigger("click");
    	}
    });
    
    
	$("#btnLogin").click(function(){
		$("#btnLogin").hide();
		$("#loadingLoginWrapper").show();
		
		$("#formLogin .form-control").removeClass("border-danger");
		$("#loginError").html("");
		
		var error = false;
		
		var userId = $("#userId").val();
		var password = $("#password").val();
		
		if( userId == undefined || userId == "" || userId.trim().length == 0 ){
			error = true;
			$("#userId").addClass("border-danger");
		}
		
		if( password == undefined || password == "" || password.trim().length == 0 ){
			error = true;
			$("#password").addClass("border-danger");
		}
		
		if( error == false ){
			var inputData = {
				usrId: userId, 
				usrNm: "", 
				password: password,
				param : "",
			};
			
			$.ajax({
				headers: { 
					'accept': 'application/json',
				    'content-type': 'application/json' 
				},
				type: 'POST',
				dataType: 'json',
		        url: rootAppName + '/rest/auth/validate',
		        data: JSON.stringify( inputData ), // serializes the form's elements.
		        success: function(response, textStatus, jqXHR) {
		            console.log(response);
		            console.log(textStatus);
		            console.log(jqXHR.status);
		            
		            if( response.output.rtnSts == 1)
		            {
		            	if( response.output != undefined && response.output.messages != undefined ){
		            		var loopLimit = response.output.messages.length;
			            	if( loopLimit > 0 ){
			            		errList = '<ul class="m-0 pl-1">';
								for( var i=0; i<loopLimit; i++ ){
									errList += '<li>'+ response.output.messages[i] +'</li>';
								}
								errList += '</ul>';
			            	}
		            	}
		            	
		            	var error = '<div class="alert alert-danger alert-dismissible mb-0" role="alert">'+
										'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
											'<span aria-hidden="true">×</span>'+
										'</button>'+ errList
									'</div>';
		            	$("#loginError").html( error );
		            	
						$("#loadingLoginWrapper").hide();
		            	$("#btnLogin").show();
		            }
		            else
		            {		            
		            	localStorage.setItem("starSlsMgmtAuthToken", response.output.authToken);
		            	localStorage.setItem("starSlsMgmtUserId", response.output.loginUser);
		            	localStorage.setItem("starSlsMgmtUserNm", response.output.usrNm);
		            	localStorage.setItem("starSlsMgmtEmailId", response.output.usrEmail);
		            	localStorage.setItem("starSlsMgmtUsrGrp", response.output.usrGrp);
		            	localStorage.setItem("starSlsMgmtAppHost",response.output.appHost);
		            	localStorage.setItem("starSlsMgmtAppPort", response.output.appPort);
		            	localStorage.setItem("starSlsMgmtAppToken", response.output.appToken);
		            	localStorage.setItem("starSlsMgmtCmpyId", response.output.usrTyp);
		            	localStorage.setItem("starSlsMgmtNginxURL", "");
		            	
		            	$.cookie('auth-token', localStorage.getItem("starSlsMgmtAuthToken"));
		            	$.cookie('app-token', localStorage.getItem("starSlsMgmtAppToken"));
						$.cookie('user-id', localStorage.getItem("starSlsMgmtUserId"));
		            	
		            	setAjaxHeader();
		            	//getSidebarMenus();
		            	window.location.replace("index.html");
		            }
		        }
		    });
		}else{
			$("#loadingLoginWrapper").hide();
		    $("#btnLogin").show();
		}
	});
	
});


function callLogin(inpParam){
	$("#mainLoader").show();

	var inputData = {
		usrId: "", 
		usrNm: "", 
		password: "",
		param : inpParam,
	};

	$.ajax({
		headers: { 
			'accept': 'application/json',
		    'content-type': 'application/json' 
		},
		type: 'POST',
		dataType: 'json',
        url: rootAppName + '/rest/auth/validate',
        data: JSON.stringify( inputData ), // serializes the form's elements.
        success: function(response, textStatus, jqXHR) {
            $("#mainLoader").hide();
            if( response.output.rtnSts == 1){
            	if( response.output != undefined && response.output.messages != undefined ){
            		var loopLimit = response.output.messages.length;
	            	if( loopLimit > 0 ){
	            		errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
	            	}
            	}
            	
            	var error = '<div class="alert alert-danger alert-dismissible mb-0" role="alert">'+
								'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
									'<span aria-hidden="true">×</span>'+
								'</button>'+ errList
							'</div>';
            	$("#loginError").html( error );
            	
				$("#loadingLoginWrapper").hide();
            	$("#btnLogin").show();
            }else{		            
            	localStorage.setItem("starSlsMgmtAuthToken", response.output.authToken);
            	localStorage.setItem("starSlsMgmtUserId", response.output.loginUser);
            	localStorage.setItem("starSlsMgmtUserNm", response.output.usrNm);
            	localStorage.setItem("starSlsMgmtEmailId", response.output.usrEmail);
            	localStorage.setItem("starSlsMgmtUsrGrp", response.output.usrGrp);
            	localStorage.setItem("starSlsMgmtAppHost",response.output.appHost);
            	localStorage.setItem("starSlsMgmtAppPort", response.output.appPort);
            	localStorage.setItem("starSlsMgmtAppToken", response.output.appToken);
            	localStorage.setItem("starSlsMgmtCmpyId", response.output.usrTyp);
            	
            	$.cookie('auth-token', localStorage.getItem("starSlsMgmtAuthToken"));
            	$.cookie('app-token', localStorage.getItem("starSlsMgmtAppToken"));
				$.cookie('user-id', localStorage.getItem("starSlsMgmtUserId"));
            	
            	setAjaxHeader();
            	//getSidebarMenus();
            	window.location.replace("index.html");
            }  
        }
    });
}

function callLoginFromMenu(inpAuth, inpLgnId, nginxURL)
{
	$("#mainLoader").show();

	var inputData = {
				usrId: inpLgnId, 
				usrNm: "", 
				password: "",
				param : inpAuth,
			};

	$.ajax({
				headers: { 
					'accept': 'application/json',
				    'content-type': 'application/json' 
				},
				type: 'POST',
				dataType: 'json',
		        url: rootAppName + '/rest/auth/validate',
		        data: JSON.stringify( inputData ), // serializes the form's elements.
		        success: function(response, textStatus, jqXHR) {
		            console.log(response);
		            console.log(textStatus);
		            console.log(jqXHR.status);
		            $("#mainLoader").hide();
		            if( response.output.rtnSts == 1)
		            {
		            	if( response.output != undefined && response.output.messages != undefined ){
		            		var loopLimit = response.output.messages.length;
			            	if( loopLimit > 0 ){
			            		errList = '<ul class="m-0 pl-1">';
								for( var i=0; i<loopLimit; i++ ){
									errList += '<li>'+ response.output.messages[i] +'</li>';
								}
								errList += '</ul>';
			            	}
		            	}
		            	
		            	var error = '<div class="alert alert-danger alert-dismissible mb-0" role="alert">'+
										'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
											'<span aria-hidden="true">×</span>'+
										'</button>'+ errList
									'</div>';
		            	$("#loginError").html( error );
		            	
						$("#loadingLoginWrapper").hide();
		            	$("#btnLogin").show();
		            }
		            else
		            {	
		            		window.localStorage.removeItem("starSlsMgmtAuthToken");
		            		window.localStorage.removeItem("starSlsMgmtUserId");
						    window.localStorage.removeItem("starSlsMgmtUserNm");
						    window.localStorage.removeItem("starSlsMgmtEmailId");
						    window.localStorage.removeItem("starSlsMgmtUsrGrp");
						    window.localStorage.removeItem("starSlsMgmtAppHost");
						    window.localStorage.removeItem("starSlsMgmtAppPort");
						    window.localStorage.removeItem("starSlsMgmtAppToken");
						    window.localStorage.removeItem("starSlsMgmtCmpyId");
						    window.localStorage.removeItem("starSlsMgmtNginxURL");
		            	
			            	localStorage.setItem("starSlsMgmtAuthToken", response.output.authToken);
			            	localStorage.setItem("starSlsMgmtUserId", response.output.loginUser);
			            	localStorage.setItem("starSlsMgmtUserNm", response.output.usrNm);
			            	localStorage.setItem("starSlsMgmtEmailId", response.output.usrEmail);
			            	localStorage.setItem("starSlsMgmtUsrGrp", response.output.usrGrp);
			            	localStorage.setItem("starSlsMgmtAppHost",response.output.appHost);
			            	localStorage.setItem("starSlsMgmtAppPort", response.output.appPort);
			            	localStorage.setItem("starSlsMgmtAppToken", response.output.appToken);
			            	localStorage.setItem("starSlsMgmtCmpyId", response.output.usrTyp);
			            	
			            	if(urlWithData.includes("invex.cloud")){
							localStorage.setItem("starSlsMgmtNginxURL", urlWithData.split("invex.cloud")[0]+"invex.cloud/" + window.location.pathname.split("/")[1]);
						}else if(urlWithData.includes("auxinvex")){
							//localStorage.setItem("starOcrNginxURL", urlWithData.split("auxinvex")[0]+"auxinvex.rsac.com/" + window.location.pathname.split("/")[1]);
							localStorage.setItem("starSlsMgmtNginxURL", nginxURL);
						}else{
							localStorage.setItem("starSlsMgmtNginxURL", "");
						}
			            	
			            	$.cookie('auth-token', localStorage.getItem("starSlsMgmtAuthToken"));
			            	$.cookie('app-token', localStorage.getItem("starSlsMgmtAppToken"));
							$.cookie('user-id', localStorage.getItem("starSlsMgmtUserId"));
		            	          		            	
		            	setAjaxHeader();
		            	window.location.replace("index.html");
		            }
		            
		        }
		    });
}