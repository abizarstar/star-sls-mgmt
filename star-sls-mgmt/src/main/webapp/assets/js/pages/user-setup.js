$(function(){
	
	getStxUserList( $("#usrId") );
	
	getUserGroupList( $("#usrGrpId") );
	
	getUserGroupTableData();
	
	getUserTableData();
	
	$("#userGroupType").change(function(){
		$("#userSetupForm .form-control").removeAttr("disabled");
		$("#userSetupForm .form-control, #userSetupForm .select2-selection").removeClass("border-danger");
		$("#userSetupForm .select2").val("").trigger("change");
		$("#userSetupForm .form-control").val("");
		$("#saveUserSetup, #usrTypWrapper, .pwd-mand, .add-user").show();
		$("#updateUserSetup, #updUsrTyp, .update-user").hide();
		$("#usrPassword, #usrCnfrmPassword").removeClass("border-danger border-success border-warning");
		
		if( $("#userGroupType").prop("checked") ){	
			$(".app-field").slideUp();
			$(".stx-field").slideDown();
		}else{		
			$(".stx-field").slideUp();
			$(".app-field").slideDown();
		}

	});
	
	$("#usrId").change(function(){
		var usrId = $("#usrId").val();
		
		if( usrId != "" && usrId != null && usrId != undefined ){			
			getSpecificUserData(usrId, "STX");
		}
	});
	
	$("#appUsrId").focusout(function(){
		var appUsrId = $("#appUsrId").val();
		
		if( appUsrId != "" && appUsrId != null && appUsrId != undefined ){			
			getSpecificUserData(appUsrId, "APP");
		}
	});
	
	
	
	$("#saveUserSetup").click(function(){
		$("#userSetupForm .form-control, #userSetupForm .select2-selection").removeClass("border-danger");
	
		var errFlg = false;
		var usrType = "";
		var grpId = $("#usrGrpId").val();
		var usrId = $("#usrId").val();
		var appUsrId = $("#appUsrId").val();
		var usrNm = $("#usrNm").val();
		var usrEmail = $("#usrEmail").val();
		var usrPassword = $("#usrPassword").val();
		var usrCnfrmPassword = $("#usrCnfrmPassword").val();
		var email_reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		
		usrType = ($("#userGroupType").prop("checked")) ? "STX" : "APP";
		
		if( grpId == "" ){
			errFlg = true;
			$("#usrGrpId").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( usrNm == "" ){
			errFlg = true;
			$("#usrNm").addClass("border-danger");
		}
		
		if( usrEmail == "" ){
			errFlg = true;
			$("#usrEmail").addClass("border-danger");
		}else if (!email_reg.test(usrEmail)) {
	        errFlg = true;
	        $('#usrEmail').addClass("border-danger");
	    }
		
		if( $("#userGroupType").prop("checked") ){
			if( usrId == "" ){
				errFlg = true;
				$("#usrId").next().find(".select2-selection").addClass("border-danger");
			}
			
			usrPassword = "";
		}else{
			if( appUsrId == "" ){
				errFlg = true;
				$("#appUsrId").addClass("border-danger");
			}
			
			if( usrPassword == "" ){
				errFlg = true;
				$("#usrPassword").addClass("border-danger");
			}
			
			if( usrCnfrmPassword == "" ){
				errFlg = true;
				$("#usrCnfrmPassword").addClass("border-danger");
			}
			
			if( !$("#usrPassword").hasClass("border-success") || !$("#usrCnfrmPassword").hasClass("border-success") ){
				errFlg = true;
				$("#usrPassword, #usrCnfrmPassword").addClass("border-danger");
			}
			
			usrId = appUsrId;
		}
		
		if( errFlg == false ){
			$("#mainLoader").show();
		
			var inputData = {
				usrId : usrId,
				usrNm : usrNm,
				eml : usrEmail,
				usrType : usrType,
				usrGrp : grpId,
				chkSm: usrPassword,
			}
			
			$.ajax({
				headers: ajaxHeader,
				url : rootAppName + "/rest/user/add",
				data : JSON.stringify( inputData ),
				type: 'POST',
				dataType: 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						getUserTableData();
						$("#cancelUserSetup").trigger("click");
						customAlert("#mainNotify", "alert-success", 'User saved successfully.');
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
						$("#mainLoader").hide();
					}			
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	$("#updateUserSetup").click(function(){
		$("#userSetupForm .form-control, #userSetupForm .select2-selection").removeClass("border-danger");
	
		var errFlg = false;
		var usrType = "";
		var grpId = $("#usrGrpId").val();
		var usrId = $("#usrId").val();
		var appUsrId = $("#appUsrId").val();
		var usrNm = $("#usrNm").val();
		var usrEmail = $("#usrEmail").val();
		var usrPassword = $("#usrPassword").val();
		var usrCnfrmPassword = $("#usrCnfrmPassword").val();
		var email_reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		
		usrType = ($("#userGroupType").prop("checked")) ? "STX" : "APP";
		
		if( grpId == "" ){
			errFlg = true;
			$("#usrGrpId").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( usrNm == "" ){
			errFlg = true;
			$("#usrNm").addClass("border-danger");
		}
		
		if( usrEmail == "" ){
			errFlg = true;
			$("#usrEmail").addClass("border-danger");
		}else if (!email_reg.test(usrEmail)) {
	        errFlg = true;
	        $('#usrEmail').addClass("border-danger");
	    }
		
		if( $("#updUsrTyp .upd-usr-typ").text() == "STX" ){
			if( usrId == "" ){
				errFlg = true;
				$("#usrId").next().find(".select2-selection").addClass("border-danger");
			}
			
			usrPassword = "";
		}else if( $("#updUsrTyp .upd-usr-typ").text() == "APP" ){
			if( appUsrId == "" ){
				errFlg = true;
				$("#appUsrId").addClass("border-danger");
			}
			
			if( usrPassword != "" || usrCnfrmPassword != "" ){
				if( !$("#usrPassword").hasClass("border-success") || !$("#usrCnfrmPassword").hasClass("border-success") ){
					errFlg = true;
					$("#usrPassword, #usrCnfrmPassword").addClass("border-danger");
				}
			}
			
			usrId = appUsrId;
		}
		
		if( errFlg == false ){
			$("#mainLoader").show();
		
			var inputData = {
				usrId : usrId,
				usrNm : usrNm,
				eml : usrEmail,
				usrType : usrType,
				usrGrp : grpId,
				chkSm: usrPassword,
			}
			
			$.ajax({
				headers: ajaxHeader,
				url : rootAppName + "/rest/user/update",
				data : JSON.stringify( inputData ),
				type: 'POST',
				dataType: 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						getUserTableData();
						$("#cancelUserSetup").trigger("click");
						customAlert("#mainNotify", "alert-success", 'User updated successfully.');
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
						$("#mainLoader").hide();
					}			
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	$("#cancelUserSetup").click(function(){
		$("#userSetupForm .form-control").removeAttr("disabled");
		$("#userSetupForm .form-control, #userSetupForm .select2-selection").removeClass("border-danger");
		$("#userSetupForm .select2").val("").trigger("change");
		$("#userSetupForm .form-control").val("");
		$("#usrId").removeAttr("disabled");
		$("#saveUserSetup, #usrTypWrapper, .pwd-mand, .add-user").show();
		$("#updateUserSetup, #updUsrTyp, .update-user").hide();
		$("#usrPassword, #usrCnfrmPassword").removeClass("border-danger border-success border-warning");
		
		$("#userGroupType").prop("checked", true);
		$("#userGroupType").trigger("change");
	});
	
	$(".view-edit-user-groups").click(function(){
		$("#userGroupModal").modal("show");
	});
	
	$('#usrGrpNm').keypress(function(evt) {		
		var charCode = (evt.which) ? evt.which : event.keyCode;
		var keyChar = String.fromCharCode(charCode);
		var regex = /^[ A-Za-z0-9_-]*$/;
		return regex.test(keyChar);
	});
	
	$("#cancelUsrGrp").click(function(){
		$("#saveUsrGrp").val("Save");
		$("#usrGrpNm, #editUsrGrpId").val("");
		$("#cancelUsrGrp").addClass("d-none");
	});
	
	$("#userGroupTbl").on("click", ".edit-user-group", function(){
		var grpId = $(this).closest("tr").attr("data-grp-id");
		var grpNm = $(this).closest("tr").attr("data-grp-nm");
		
		$("#saveUsrGrp").val("Update");
		$("#editUsrGrpId").val(grpId);
		$("#usrGrpNm").val(grpNm);
		$("#cancelUsrGrp").removeClass("d-none");
	});
	
	$("#userGroupTbl").on("click", ".delete-user-group", function(){
		var grpId = $(this).closest("tr").attr("data-grp-id");
		var grpNm = $(this).closest("tr").attr("data-grp-nm");
		
		$("#delUsrGrpId").val(grpId);
		$("#deleteGroupModal").modal("show");
	});
	
	
	$("#saveUsrGrp").click(function(){
		$("#usrGrpNm").removeClass("border-danger");
		
		var inputData = {}, ajaxUrl = "";
		
		var grpNm = $("#usrGrpNm").val().trim().toUpperCase();
		var grpId = $("#editUsrGrpId").val();
		
		if( grpNm == "" ){
			$("#usrGrpNm").addClass("border-danger");
			return;
		}
		
		$("#mainLoader").show();
		
		if( grpId == "" ){
			ajaxUrl = rootAppName + "/rest/usrgroup/add-grp";
			inputData = {
				grp_nm: grpNm
			}
		}else{
			ajaxUrl = rootAppName + "/rest/usrgroup/edit-grp";
			inputData = {
				grp_id: grpId,
				grp_nm: grpNm
			}
		}
		
		$.ajax({
			headers: ajaxHeader,
			url : ajaxUrl,
			data : JSON.stringify( inputData ),
			type: 'POST',
			dataType: 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					getUserGroupTableData();
					getUserGroupList( $("#usrGrpId, #menuUserGroup") );
					$("#cancelUsrGrp").trigger("click");
					customAlert("#userGroupNotify", "alert-success", 'User group saved successfully.');
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#userGroupNotify", "alert-danger", errList);
					$("#mainLoader").hide();
				}			
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
		
	});


	$("#finalDeleteGroup").click(function(){
		$("#mainLoader").show();
		
		var grpId = $("#delUsrGrpId").val();
		var inputData = {
			grp_id: grpId,
		};
		
		$.ajax({
			headers: ajaxHeader,
			url : rootAppName + "/rest/usrgroup/del-grp",
			data : JSON.stringify( inputData ),
			type: 'POST',
			dataType: 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					getUserGroupTableData();
					$("#deleteGroupModal").modal("hide");
					customAlert("#userGroupNotify", "alert-success", 'User group deleted successfully.');
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#userGroupNotify", "alert-danger", errList);
					$("#mainLoader").hide();
				}			
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});
	
	$("#userDataTbl").on("click", ".edit-user", function(){
		var grpId = $(this).closest("tr").attr("data-grp-id");
		var usrId = $(this).closest("tr").attr("data-usr-id");
		var usrNm = $(this).closest("tr").attr("data-usr-nm");
		var usrEmail = $(this).closest("tr").attr("data-usr-email");
		var usrTyp = $(this).closest("tr").attr("data-usr-typ");
		
		$("#usrTypWrapper, #saveUserSetup, .add-user").hide();
		$("#updUsrTyp, #updateUserSetup, .update-user").show();
		
		if( usrTyp == "STX" ){
			$("#usrId").val( usrId ).trigger("change");
			$("#usrId").attr("disabled", "disabled");
			
			$("#updUsrTyp .upd-usr-typ").removeClass("app").addClass("stx");
			$("#updUsrTyp .upd-usr-typ").html("STX");
			
			$(".app-field").slideUp();
			$(".stx-field").slideDown();
		}else if( usrTyp == "APP" ){
			$("#appUsrId").val(usrId);
			$("#appUsrId").attr("disabled", "disabled");
			$("#usrNm").val(usrNm);
			$("#usrEmail").val(usrEmail);
			$("#usrGrpId").val(grpId).trigger("change");
			
			$("#updUsrTyp .upd-usr-typ").removeClass("stx").addClass("app");
			$("#updUsrTyp .upd-usr-typ").html("APP");
			
			$(".pwd-mand").hide();
			
			$(".stx-field").slideUp();
			$(".app-field").slideDown();
		}	
	});
	
	$("#userDataTbl").on("click", ".onoffswitch-label", function(e){
		e.preventDefault();
		
		var confirmMsg = "Do you really want to activate this user ?";
		var userId = $(this).closest("tr").attr("data-usr-id");
		var chkBoxId = $(this).prev().attr("id");
		var curSts = $(this).prev().prop("checked");
		
		if( curSts ){
			confirmMsg = "Do you really want to deactivate this user ?";
		}
		
		$("#activeInactiveUserBtn").attr("data-usr-id", userId);
		$("#activeInactiveUserBtn").attr("data-chk-box-id", chkBoxId);
		$("#activeInactiveUserBtn").attr("data-cur-sts", curSts);
		
		$("#activeInactiveUserModal .info-msg").html(confirmMsg);
		$("#activeInactiveUserModal").modal("show");
	});
	
	
	
	$("#activeInactiveUserBtn").click(function(){
		$("#mainLoader").show();
		
		var userId = $(this).attr("data-usr-id");
		var status = ( $(this).attr("data-cur-sts") == "true" ) ? "false" : "true";
		
		var inputData = {
			usr_id: userId,
			actv: status
		};
	
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/usrgroup/actv-usr',
			type: 'POST',
			dataType: 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					if( $("#activeInactiveUserBtn").attr("data-cur-sts") == "true" ){
						$("#"+ $("#activeInactiveUserBtn").attr("data-chk-box-id") ).prop("checked", false);
						customAlert("#mainNotify", "alert-success", 'User deactivated successfully.');
					}else{
						$("#"+ $("#activeInactiveUserBtn").attr("data-chk-box-id") ).prop("checked", true);
						customAlert("#mainNotify", "alert-success", 'User activated successfully.');
					}
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});
	
});

function getUserTableData(){
	$("#mainLoader").show();
	
	$.ajax({
		headers: ajaxHeader,
		url : rootAppName + '/rest/user/read-app',
		data : JSON.stringify({ usrId: "" }),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblUsers.length;
				var usrGrpId = '', usrGrp = '', userId = '', userNm = '', usrTyp = '', emailId = '', tblHtml = '', userStatusChecked = '', 
				userStatusStr = '', userStatusId = '', editBtn = '';	
				for( var i=0; i<loopSize; i++ ){
					userStatusChecked = '', userStatusStr = '', editBtn = '';
					
					usrGrpId = response.output.fldTblUsers[i].usrGrpId;
					usrGrp = response.output.fldTblUsers[i].usrGrp;
					userId = response.output.fldTblUsers[i].userId;
					userNm = response.output.fldTblUsers[i].userNm;
					emailId = response.output.fldTblUsers[i].emailId;
					usrTyp = response.output.fldTblUsers[i].usrTyp;
					
					userStatusId = 'usrStsChkBox'+ userId + i;
					
					if(response.output.fldTblUsers[i].actvSts.trim() == "true"){
						userStatusChecked = 'checked';
					}
					
					//if( usrGrp.trim().toLowerCase() != "admin" ){
						editBtn = '<i class="icon-note text-primary font-weight-bold menu-icon c-pointer edit-user"></i>';
						
						userStatusStr = '<div class="onoffswitch display-inline-block">'+
											'<input type="checkbox" name="user-status" class="onoffswitch-checkbox" id="'+ userStatusId +'" tabindex="0" '+ userStatusChecked +'>'+
											'<label class="onoffswitch-label m-0" for="'+ userStatusId +'">'+
												'<span class="onoffswitch-inner user-status-type"></span>'+
												'<span class="onoffswitch-switch"></span>'+
											'</label>'+
										'</div>';
					//}
				
					tblHtml += '<tr data-grp-id="'+ usrGrpId +'" data-usr-id="'+ userId +'" data-usr-nm="'+ userNm +'" data-usr-email="'+ emailId +'" data-usr-typ="'+ usrTyp +'">'+
									'<td>'+ (i+1) +'</td>'+
									'<td>'+ userId +'</td>'+
									'<td>'+ userNm +'</td>'+
									'<td>'+ emailId +'</td>'+
									'<td>'+ usrGrp +'</td>'+
									'<td>'+ usrTyp +'</td>'+
									'<td>'+ editBtn +'</td>'+
									'<td class="user-active-inactive-wrapper">'+ userStatusStr +'</td>'+
								'</tr>';
				}
				$("#userDataTbl tbody").html( tblHtml );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#userGroupNotify", "alert-danger", errList);
				$("#mainLoader").hide();
			}
			
			$("#mainLoader").hide();		
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}

function getUserGroupTableData(){
	$("#mainLoader").show();
	$.ajax({
		headers: ajaxHeader,
		url : rootAppName + '/rest/usrgroup/read',
		data : JSON.stringify({ grp: "" }),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblUsrGroup.length;
				var grpId = '', grpNm = '', tblHtml = '', srNo = 1;	
				for( var i=0; i<loopSize; i++ ){
					grpId = response.output.fldTblUsrGroup[i].grpId;
					grpNm = response.output.fldTblUsrGroup[i].grpNm;
					
					if( grpNm.trim().toLowerCase() != "admin" ){
						tblHtml += '<tr data-grp-id="'+ grpId +'" data-grp-nm="'+ grpNm +'">'+
										'<td>'+ srNo +'</td>'+
										'<td>'+ grpNm +'</td>'+
										'<td>'+ response.output.fldTblUsrGroup[i].usrCnt +'</td>'+
										'<td><i class="icon-note text-primary font-weight-bold menu-icon c-pointer edit-user-group"></i> &nbsp;<i class="icon-trash font-weight-bold text-danger  menu-icon delete-user-group c-pointer"></i></td>'+
									'</tr>';
						srNo++;
					}
				}
				$("#userGroupTbl tbody").html( tblHtml );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#userGroupNotify", "alert-danger", errList);
			}
				
			$("#mainLoader").hide();		
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}

function getSpecificUserData(usrId, usrTyp){
	$("#mainLoader").show();
	
	var inputData = {
		usrId: usrId
	};
	
	$.ajax({
		headers: ajaxHeader,
		url : rootAppName + '/rest/user/read-app',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				if( response.output.fldTblUsers.length && response.output.fldTblUsers[0].usrTyp == usrTyp ){
					$("#usrNm").val( response.output.fldTblUsers[0].userNm );
					$("#usrEmail").val( response.output.fldTblUsers[0].emailId );
					$("#usrGrpId").val( response.output.fldTblUsers[0].usrGrpId ).trigger("change");
					
					$(".add-user").hide();
					$(".update-user").show();
					
					$("#updUsrTyp .upd-usr-typ").html(usrTyp);
					
					/*if( response.output.fldTblUsers[0].usrGrp.trim().toLowerCase() == "admin" ){
						$("#usrGrpId").attr("disabled", "disabled");
					}else{
						$("#usrGrpId").removeAttr("disabled");
					}*/
				}else{
					$(".update-user").hide();
					$(".add-user").show();
					
					if( usrTyp == "STX" ){
						var $selectedUsr = $("#usrId option:selected");
						$("#usrNm").val( $selectedUsr.attr("data-usr-nm") ); 
						$("#usrEmail").val( $selectedUsr.attr("data-usr-email") );
					}else{
						$("#usrNm, #usrEmail").val("");
					}
					
					$("#usrGrpId").val( "" ).trigger("change");
					$("#usrGrpId").removeAttr("disabled");
				}
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#userGroupNotify", "alert-danger", errList);
				$("#mainLoader").hide();
			}
			
			$("#mainLoader").hide();		
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}